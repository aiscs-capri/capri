<?php
class credit_model extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return voInvoice_Id
    */
    public function __construct()
    {
        $this->load->database();
    }

    /**
    * Get product by his is
    * @param int $product_Invoice_Id 
    * @return array
    */
    public function get_manufacture_by_Credit_Id($Credit_Id)
    {
		$this->db->select('*');
		$this->db->from('capri_credit_entry');
		$this->db->where('Credit_Id', $Credit_Id);
		$query = $this->db->get();
		return $query->result_array(); 
    }    


public function save($Credit_Id)
    {
        $insert = $this->db->insert('capri_credit_entry', $data);
        return $insert;
    }   

    public function maxvalue()
    {
        $this->db->select_MAX('Credit_Id');
        $this->db->from('capri_credit_entry');
        $query = $this->db->get();
        return $query;

    }

    /**
    * Fetch salesinvoice_master data from the database
    * possibility to mix search, filter and order
    * @param string $search_string 
    * @param strong $order
    * @param string $order_type 
    * @param int $limit_start
    * @param int $limit_end
    * @return array
    */
    public function get_credit($Credit_Id=null, $search_string=null, $order=null, $order_type='Asc', $limit_start, $limit_end)
    {
        
        $this->db->select('capri_credit_entry.Credit_Id');
        $this->db->select('capri_credit_entry.CustomerName');
        $this->db->select('capri_credit_entry.BillingAddress');
        $this->db->select('capri_credit_entry.Reson');
        $this->db->select('capri_credit_entry.TIN');
        $this->db->select('capri_credit_entry.CST');
        $this->db->select('capri_credit_entry.InvoiceNumber');
        $this->db->select('capri_credit_entry.InvoiceDate');
        $this->db->select('capri_credit_entry.ReturnDate');
        $this->db->from('capri_credit_entry');
        
        

        $this->db->group_by('capri_credit_entry.Credit_Id');

        


        $this->db->limit($limit_start, $limit_end);
        //$this->db->limit('4', '4');


        $query = $this->db->get();
        
        return $query->result_array();  
    }
    /**
    * Count the number of rows
    * @param int $search_string
    * @param int $order
    * @return int
    */
public function getCreditentryDetails($lead_order)
    {
       $this->db->select('capri_credit_entry.Credit_Id, capri_master_customer.CustomerName,capri_master_customer.BillingAddress,capri_master_customer.TIN,capri_master_customer.CST');
        $this->db->from('capri_credit_entry');
        $this->db->join('capri_master_customer','capri_credit_entry.CustomerName = capri_master_customer.CustomerName','inner');
        $this->db->where('capri_credit_entry.Credit_Id',$lead_order);
        $query = $this->db->get();
        return $query->result();
    }
    public function getCreditproductDetails($Credit_product)
    {
        $this->db->select('Id,ProductName,ProductUnit,Quantity');
        $this->db->from('capri_credit_details ');       
        $this->db->where('capri_credit_details.Id',$Credit_product);
        $query = $this->db->get();
        return $query->result();
    }
    public function getcustomername()
{

    $this->db->select('Customer_Id,CustomerName');
    $this -> db -> from('capri_master_customer');  
    $query = $this -> db -> get();
    return $query->result();
}
    public function getproductname()
{

    $this->db->select('Product_Id,ProductName');
    $this -> db -> from('capri_master_product');

    $query = $this -> db -> get();
    return $query->result();
}
public function getproductdetail($Product_Id)
{

    $this->db->select('Product_Id,ProductPrice,ProductUnit');
    $this->db->where('Product_Id', $Product_Id);
    $this->db->from('capri_master_product');
    $query=$this->db->get();
    return $query->result();
}

/*$this->db->select('concat(BillStreet,',',BillVillage,',',BillCity,',',billstate,',',billpincode) as address,CST,TIN');*/
public function getcustomerdetail($Customer_Id)
{

    $this->db->select('BillingAddress');
    $this->db->select('CST,TIN');
    $this->db->from('capri_master_customer');
    $this->db->where('Customer_Id', $Customer_Id);
    $query=$this->db->get();
    return $query->result();
}

     public function get_order($ProductName=null, $search_string=null, $order=null, $order_type='Asc', $limit_start, $limit_end)
    {
        
        $this->db->select('capri_credit_details.Id');
        $this->db->select('capri_credit_details.ProductName');
        $this->db->select('capri_credit_details.ProductUnit');
        $this->db->select('capri_credit_details.Quantity');
        $this->db->select('capri_credit_details.Credit_Id');
        $this->db->select('capri_master_product.ProductName as ProductName');
        $this->db->from('capri_credit_details');
        if($ProductName != null && $ProductName != 0){
            $this->db->where('ProductName', $ProductName);
        }
        if($search_string){
            $this->db->like('ProductName', $search_string);
        }

        $this->db->join('capri_master_product', 'capri_credit_details.ProductName = capri_master_product.ProductName', 'left');
       
        
        $this->db->group_by('capri_credit_details.Id');

        if($order){
            $this->db->order_by($order, $order_type);
        }else{
            $this->db->order_by('Id', $order_type);
        }


        $this->db->limit($limit_start, $limit_end);
        //$this->db->limit('4', '4');


        $query = $this->db->get();
        
        return $query->result_array();  
    }
    /**
    * Count the number of rows
    * @param int $search_string
    * @param int $order
    * @return int
    */
   function count_Credit($Credit_Id=null, $search_string=null, $order=null)
    {
        $this->db->select('*');
        $this->db->from('capri_credit_entry');
        if($Credit_Id != null && $Credit_Id != 0){
            $this->db->where('Credit_Id', $Credit_Id);
        }
        
        if($order){
            $this->db->order_by($order, 'Asc');
        }else{
            $this->db->order_by('Credit_Id', 'Asc');
        }
        $query = $this->db->get();
        return $query->num_rows();        
    }


    /**
    * Store the new item into the database
    * @param array $data - associative array with data to store
    * @return boolean 
    */
    function store_manufacture($data)
    {
		$insert = $this->db->insert('capri_credit_entry', $data);
	    return $insert;
	}

    /**
    * Update manufacture
    * @param array $data - associative array with data to store
    * @return boolean
    */
    function update_manufacture($Credit_Id, $data)
    {
		$this->db->where('Credit_Id', $Credit_Id);
		$this->db->update('capri_credit_entry', $data);
		$report = array();
		$report['error'] = $this->db->_error_number();
		$report['message'] = $this->db->_error_message();
		if($report !== 0){
			return true;
		}else{
			return false;
		}
	}

    /**
    * Delete manufacturer
    * @param int $Invoice_Id - manufacture Invoice_Id
    * @return boolean
    */

function delete_order_details($Id){

    $this->db->where('Id',$Id);
    $this->db->delete('capri_credit_details');
}

public function order_details_maxvalue()
    {
        $this->db->select_MAX('Id');
        $this->db->from('capri_credit_details');
        $query = $this->db->get();
        return $query;

    }
 function credit_product_delete_details($Id){

    $this->db->where('Credit_Id',$Id);
    $this->db->delete('capri_credit_details');
 }

	function delete_manufacture($Credit_Id){
		$this->db->where('Credit_Id', $Credit_Id);
		$this->db->delete('capri_credit_entry'); 
	}
 
}
?>	
