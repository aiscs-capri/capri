<?php
class outpay_model extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return voId
    */
    public function __construct()
    {
        $this->load->database();
    }

    /**
    * Get product by his is
    * @param int $Id 
    * @return array
    */
    public function get_manufacture_by_Id($Id)
    {
        $this->db->select('*');
        $this->db->from('capri_outgoing_payment');
        $this->db->where('Id', $Id);
        $query = $this->db->get();
        return $query->result_array(); 
    }    

    /**
    * Fetch product data from the database
    * possibility to mix search, filter and order
    * @param string $search_string 
    * @param strong $order
    * @param string $order_type 
    * @param int $limit_start
    * @param int $limit_end
    * @return array
    */
    public function get_outpay($search_string=null, $order=null, $order_type='Asc', $limit_start=null, $limit_end=null)
    {
        
        $this->db->select('*');
        $this->db->from('capri_outgoing_payment');

        if($search_string){
            $this->db->like('Id', $search_string);
        }
        $this->db->group_by('Id');

        if($order){
            $this->db->order_by($order, $order_type);
        }else{
            $this->db->order_by('Id', $order_type);
        }

        if($limit_start && $limit_end){
          $this->db->limit($limit_start, $limit_end);   
        }

        if($limit_start != null){
          $this->db->limit($limit_start, $limit_end);    
        }
        
        $query = $this->db->get();
        
        return $query->result_array();  
    }

    /**
    * Count the number of rows
    * @param int $search_string
    * @param int $order
    * @return int
    */
    function count_outpay($search_string=null, $order=null)
    {
        $this->db->select('*');
        $this->db->from('capri_outgoing_payment');
        if($search_string){
            $this->db->like('Id', $search_string);
        }
        if($order){
            $this->db->order_by($order, 'Asc');
        }else{
            $this->db->order_by('Id', 'Asc');
        }
        $query = $this->db->get();
        return $query->num_rows();        
    }

    /**
    * Store the new item into the database
    * @param array $data - associative array with data to store
    * @return boolean 
    */
    function store_outcome($data)
    {
        $insert = $this->db->insert('capri_outgoing_payment', $data);
        return $insert;
    }

    /**
    * Update manufacture
    * @param array $data - associative array with data to store
    * @return boolean
    */
    function update_manufacture($Id, $data)
    {
        $this->db->where('Id', $Id);
        $this->db->update('capri_outgoing_payment', $data);
        $report = array();
        $report['error'] = $this->db->_error_number();
        $report['message'] = $this->db->_error_message();
        if($report !== 0){
            return true;
        }else{
            return false;
        }
    }

    public function getsuppliername()
    {

    $this->db->select('Supplier_Id,SupplierName');
    $this -> db -> from('capri_master_supplier');  
    $query = $this -> db -> get();
    return $query->result();
    }

/*
SELECT InvoiceNumber,CustomerName, TotalAmount, AdvancePayment, Balance
FROM capri_sale_invoice
WHERE CustomerName = 'dharani'*/

public function getincomedetail($Supplier_Id)
{

    $this->db->select('capri_purchase_inward.inward_Id,capri_purchase_inward.inward_number,capri_master_supplier.BillingAddress, capri_master_supplier.CST, capri_master_supplier.TIN, capri_purchase_inward.GrossTotal, capri_purchase_inward.AdvancePayment, capri_purchase_inward.Balance, capri_purchase_inward.Paid');
    $this->db->from('capri_purchase_inward');
    $this->db->join('capri_master_supplier','capri_purchase_inward.Supplier_Id = capri_master_supplier.Supplier_Id','inner');
    $this->db->where('capri_master_supplier.Supplier_Id',$Supplier_Id);
    $query = $this->db->get();
    return $query->result();

}


public function getUpdateDetail($id,$data)
{

    $this->db->where('inward_Id', $id);
    $this->db->update('capri_purchase_inward',$data);
}
public function getUpdateInvoiceDetail($invoiceNumber,$data)
{

    $this->db->where('inward_number', $invoiceNumber);
    $this->db->update('capri_purchase_inward',$data);
}

    /**
    * Delete manufacturer
    * @param int $Id - manufacture Id
    * @return boolean
    */
    function getDeleteIncomeDetails($Id){
        $this->db->where('Id', $Id);
        $this->db->delete('capri_outgoing_payment'); 
    }
    
    function getIncomeDetails($Id){
        $this->db->select('InwardNumber, Total, Pay, Balance');
        $this->db->where('Id', $Id);
        $this->db->from('capri_outgoing_payment');
        $query=$this->db->get();
        return $query->result();
    }

    public function getPaidInvoiceDetail($invoiceNumber)
    {
        $this->db->select('Paid,Balance');
        $this->db->where('inward_number', $invoiceNumber);
        $this->db->from('capri_purchase_inward');
        $query=$this->db->get();
        return $query->result();
    }
    
    
}
?>  
