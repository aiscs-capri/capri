<?php
class purchasereport_model extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return voinward_Id
    */
    public function __construct()
    {
        $this->load->database();
    }

    /**
    * Get product by his is
    * @param int $product_inward_Id 
    * @return array
    */
    public function get_manufacture_by_inward_Id($inward_Id)
    {
		$this->db->select('*');
		$this->db->from('capri_purchase_inward');
		$this->db->where('inward_Id', $inward_Id);
		$query = $this->db->get();
		return $query->result_array(); 
    }    


public function save($inward_Id)
    {
        $insert = $this->db->insert('capri_purchase_inward', $data);
        return $insert;
    }   

    public function maxvalue()
    {
        $this->db->select_MAX('inward_Id');
        $this->db->from('capri_purchase_inward');
        $query = $this->db->get();
        return $query;

    }


  /*  public function getPurchaseOrderDetails($purchase_order)
    {
        $this->db->select('capri_purchase_inward.*, capri_master_supplier.*');
        $this->db->from('capri_purchase_inward');
        $this->db->join('capri_master_supplier','capri_purchase_inward.Supplier_Id = capri_master_supplier.Supplier_Id','inner');
        $this->db->where('capri_purchase_inward.inward_Id',$purchase_order);
        $query = $this->db->query;
        return $query;
    }
    public function getPurchaseOrderproductDetails($purchase_order_product)
    {
        $this->db->select('p.productName,p.ProductUnit,pop.Quantity, pop.total');
        $this->db->from('capri_purchase_inward_product pop');
        $this->db->join('capri_master_product p','p.Product_Id = pop.Product_Id','inner');
        $this->db->where('pop.inward_Id',$purchase_order_product);
        $query = $this->db->query();
        return $query;
    }*/
    /**
    * Fetch purchase_master data from the database
    * possibility to mix search, filter and order
    * @param string $search_string 
    * @param strong $order
    * @param string $order_type 
    * @param int $limit_start
    * @param int $limit_end
    * @return array
    */
    public function get_inward($inward_Id=null, $search_string=null, $order=null, $order_type='Asc', $limit_start, $limit_end)
    {
        
        $this->db->select('capri_purchase_inward.inward_Id');
        $this->db->select('capri_purchase_inward.inward_number');
        $this->db->select('capri_purchase_inward.inwardDate');
        $this->db->select('capri_purchase_inward.BillNumber');
        $this->db->select('capri_purchase_inward.BillDate');
        $this->db->select('capri_purchase_inward.Order_number');
        $this->db->select('capri_purchase_inward.OrderDate');
        $this->db->select('capri_purchase_inward.PlaceOfDelivery');
        $this->db->select('capri_purchase_inward.Supplier_Id');
        $this->db->select('capri_purchase_inward.Total');
        $this->db->select('capri_purchase_inward.Tax_type');
        $this->db->select('capri_purchase_inward.Tax');
        $this->db->select('capri_purchase_inward.TaxAmount');
        $this->db->select('capri_purchase_inward.PackingAndForward');
        $this->db->select('capri_purchase_inward.servicetax');
        $this->db->select('capri_purchase_inward.Otherchargs');
        $this->db->select('capri_purchase_inward.GrossTotal');
        $this->db->select('capri_purchase_inward.AdvancePayment');
        $this->db->select('capri_purchase_inward.Balance');      
        $this->db->from('capri_purchase_inward');      
        $this->db->group_by('capri_purchase_inward.inward_Id');

        if($order){
            $this->db->order_by($order, $order_type);
        }else{
            $this->db->order_by('inward_Id', $order_type);
        }


        $this->db->limit($limit_start, $limit_end);
        //$this->db->limit('4', '4');


        $query = $this->db->get();
        
        return $query->result_array();  
    }
    /**
    * Count the number of rows
    * @param int $search_string
    * @param int $order
    * @return int
    */

public function getordernumber() {

    $this->db->select('Order_Id,Order_number');
    $this -> db -> from('capri_purchase_order');  
    $query = $this -> db -> get();
    return $query->result();
}
public function getOrderDetails($purchase_order) {

    $this->db->select('capri_purchase_order.OrderDate, capri_purchase_order.PlaceOfDelivery,capri_master_supplier.Supplier_Id, capri_master_supplier.SupplierName, capri_master_supplier.BillingAddress, capri_master_supplier.TIN,capri_master_supplier.CST');
    $this->db->from('capri_purchase_order');
    $this->db->join('capri_master_supplier','capri_purchase_order.Supplier_Id = capri_master_supplier.Supplier_Id','inner');
    $this->db->where('capri_purchase_order.Order_Id',$purchase_order);
    $query = $this->db->get();
    return $query->result();
}

    public function getproductname()
{

    $this->db->select('Product_Id,ProductName');
    $this -> db -> from('capri_master_product');

    $query = $this -> db -> get();
    return $query->result();
}
public function getorderdetail($Order_Id)
{

    $this->db->select('Order_Id,OrderDate,PlaceOfDelivery,Supplier_Id');
    $this->db->from('capri_purchase_order');
    $this->db->where('Order_Id', $Order_Id);
    $query=$this->db->get();
    return $query->result();
}
public function getproductdetail($Product_Id)
{

    $this->db->select('Product_Id,ProductPrice,ProductUnit');
    $this->db->where('Product_Id', $Product_Id);
    $this->db->from('capri_master_product');
    $query=$this->db->get();
    return $query->result();
}

/*$this->db->select('concat(BillStreet,',',BillVillage,',',BillCity,',',billstate,',',billpincode) as address,CST,TIN');*/
public function getsupplierdetail($Supplier_Id)
{

    $this->db->select('BillStreet,BillVillage,BillCity,BillState,BillPincode');
    $this->db->select('CST,TIN');
    $this->db->from('capri_master_supplier');
    $this->db->where('Supplier_Id', $Supplier_Id);
    $query=$this->db->get();
    return $query->result();
}

     public function get_order($Product_Id=null, $search_string=null, $order=null, $order_type='Asc', $limit_start, $limit_end)
    {
        
        $this->db->select('capri_purchase_inward_product.Id');
        $this->db->select('capri_purchase_inward_product.Product_Id');
        $this->db->select('capri_purchase_inward_product.Quantity');
        $this->db->select('capri_purchase_inward_product.Total');
         $this->db->select('capri_purchase_inward_product.inward_Id');
        $this->db->select('capri_purchase_inward_product.Product_Id');
        $this->db->select('capri_master_product.Product_Id as Product_Id');
        $this->db->from('capri_purchase_inward_product');
        if($Product_Id != null && $Product_Id != 0){
            $this->db->where('Product_Id', $Product_Id);
        }
        if($search_string){
            $this->db->like('Product_Id', $search_string);
        }

        $this->db->join('capri_master_product', 'capri_purchase_inward_product.Product_Id = capri_master_product.Product_Id', 'left');
       
        
        $this->db->group_by('capri_purchase_inward_product.Id');

        if($order){
            $this->db->order_by($order, $order_type);
        }else{
            $this->db->order_by('Id', $order_type);
        }


        $this->db->limit($limit_start, $limit_end);
        //$this->db->limit('4', '4');


        $query = $this->db->get();
        
        return $query->result_array();  
    }
    /**
    * Count the number of rows
    * @param int $search_string
    * @param int $order
    * @return int
    */
   function count_inward($Supplier_Id=null, $search_string=null, $order=null)
    {
        $this->db->select('*');
        $this->db->from('capri_purchase_inward');
        if($Supplier_Id != null && $Supplier_Id != 0){
            $this->db->where('Supplier_Id', $Supplier_Id);
        }
        if($search_string){
            $this->db->like('inward_number', $search_string);
        }
        if($order){
            $this->db->order_by($order, 'Asc');
        }else{
            $this->db->order_by('inward_Id', 'Asc');
        }
        $query = $this->db->get();
        return $query->num_rows();        
    }


    /**
    * Store the new item into the database
    * @param array $data - associative array with data to store
    * @return boolean 
    */
    function store_manufacture($data)
    {
		$insert = $this->db->insert('capri_purchase_inward', $data);
	    return $insert;
	}

    /**
    * Update manufacture
    * @param array $data - associative array with data to store
    * @return boolean
    */
    function update_manufacture($inward_Id, $data)
    {
		$this->db->where('inward_Id', $inward_Id);
		$this->db->update('capri_purchase_inward', $data);
		$report = array();
		$report['error'] = $this->db->_error_number();
		$report['message'] = $this->db->_error_message();
		if($report !== 0){
			return true;
		}else{
			return false;
		}
	}

    /**
    * Delete manufacturer
    * @param int $inward_Id - manufacture inward_Id
    * @return boolean
    */
	function delete_manufacture($inward_Id){
		$this->db->where('inward_Id', $inward_Id);
		$this->db->delete('capri_purchase_inward'); 
	}
 
    function delete_inward_details($Id){
        $this->db->where('Id',$Id);
        $this->db->delete('capri_purchase_inward_details');
    }

    public function inward_details_maxvalue()
    {
        $this->db->select_MAX('Id');
        $this->db->from('capri_purchase_inward_details');
        $query = $this->db->get();
        return $query;

    }

  /* view order and supplier*/
/*    public function getPurchaseInward($purchase_order)
    {
        inward_number VARCHAR(30) NOT NULL,
 TIMESTAMP NOT NULL,
capri_purchase_inward.BillNumber VARCHAR(20) NOT NULL,
capri_purchase_inward.BillDate TIMESTAMP NOT NULL,
capri_purchase_inward.Order_number BIGINT NOT NULL,
capri_purchase_inward.OrderDate TIMESTAMP NOT NULL,
capri_purchase_inward.PlaceOfDelivery VARCHAR(200) NULL,

capri_purchase_inward.Total NUMERIC(10,2) NOT NULL,
capri_purchase_inward.Tax_type VARCHAR(5) NOT NULL,
capri_purchase_inward.Tax NUMERIC(10,2) NULL,

        $this->db->select('capri_purchase_inward.inward_number,capri_purchase_inward.inwardDate, capri_master_supplier.SupplierName, capri_master_supplier.BillingAddress, capri_master_supplier.TIN,capri_master_supplier.CST');
        $this->db->from('capri_purchase_order');
        $this->db->join('capri_master_supplier','capri_purchase_order.Supplier_Id = capri_master_supplier.Supplier_Id','inner');
        $this->db->where('capri_purchase_order.Order_Id',$purchase_order);
        $query = $this->db->get();
        return $query->result();
    }
*/
/* show the product item details*/
public function getPurchaseInwardDetails($purchase_order_product)
    {
        $this->db->select('Id,ProductName,ProductUnit,ProductPrice,Quantity,Total');
        $this->db->from('capri_purchase_inward_details');       
        $this->db->where('capri_purchase_inward_details.inward_Id',$purchase_order_product);
        $query = $this->db->get();
        return $query->result();
    }

     public function getsupplierDetails($purchase_order)
    {
        $this->db->select('capri_purchase_inward.Order_number,capri_purchase_inward.Supplier_Id, capri_master_supplier.SupplierName, capri_master_supplier.BillingAddress, capri_master_supplier.TIN,capri_master_supplier.CST');
        $this->db->from('capri_purchase_inward');
        $this->db->join('capri_master_supplier','capri_purchase_inward.Supplier_Id = capri_master_supplier.Supplier_Id','inner');
        $this->db->where('capri_purchase_inward.inward_Id',$purchase_order);
        $query = $this->db->get();
        return $query->result();
    }


}
	
