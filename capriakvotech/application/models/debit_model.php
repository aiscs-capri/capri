<?php
class debit_model extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return voDebit_Id
    */
    public function __construct()
    {
        $this->load->database();
    }

    /**
    * Get product by his is
    * @param int $product_Debit_Id 
    * @return array
    */
    public function get_manufacture_by_Debit_Id($Debit_Id)
    {
		$this->db->select('*');
		$this->db->from('capri_debit_entry');
		$this->db->where('Debit_Id', $Debit_Id);
		$query = $this->db->get();
		return $query->result_array(); 
    }    


public function save($Debit_Id)
    {
        $insert = $this->db->insert('capri_debit_entry', $data);
        return $insert;
    }   

    public function maxvalue()
    {
        $this->db->select_MAX('Debit_Id');
        $this->db->from('capri_debit_entry');
        $query = $this->db->get();
        return $query;

    }


   public function getdebitDetails($lead_order)
    {
       $this->db->select('capri_debit_entry.InvoiceNumber, capri_master_supplier.SupplierName,capri_master_supplier.BillingAddress,capri_master_supplier.TIN,capri_master_supplier.CST');
        $this->db->from('capri_debit_entry');
        $this->db->join('capri_master_supplier','capri_debit_entry.SupplierName = capri_master_supplier.SupplierName','inner');
        $this->db->where('capri_debit_entry.Debit_Id',$lead_order);
        $query = $this->db->get();
        return $query->result();
    }
    public function getPurchaseOrderproductDetails($purchase_order_product)
    {
        $this->db->select('Id,ProductName,ProductUnit,Quantity');
        $this->db->from('capri_debit_details ');       
        $this->db->where('capri_debit_details.Debit_Id',$purchase_order_product);
        $query = $this->db->get();
        return $query->result();
    }

    /**
    * Fetch debit_master data from the database
    * possibility to mix search, filter and order
    * @param string $search_string 
    * @param strong $order
    * @param string $order_type 
    * @param int $limit_start
    * @param int $limit_end
    * @return array
    */
    public function get_debit($Customer_Id=null, $search_string=null, $order=null, $order_type='Asc', $limit_start, $limit_end)
    {
        
        $this->db->select('capri_debit_entry.Debit_Id');
        $this->db->select('capri_debit_entry.SupplierName');
        $this->db->select('capri_debit_entry.BillingAddress');
        $this->db->select('capri_debit_entry.Reson');
        $this->db->select('capri_debit_entry.TIN');
        $this->db->select('capri_debit_entry.CST');
        $this->db->select('capri_debit_entry.InvoiceDate');
        $this->db->select('capri_debit_entry.InvoiceNumber');

        $this->db->select('capri_debit_entry.ReturnDate');
                   
        $this->db->from('capri_debit_entry');
        
        

        $this->db->group_by('capri_debit_entry.Debit_Id');

        


        $this->db->limit($limit_start, $limit_end);
        //$this->db->limit('4', '4');


        $query = $this->db->get();
        
        return $query->result_array();  
    }
    /**
    * Count the number of rows
    * @param int $search_string
    * @param int $order
    * @return int
    */

    public function getsupplierName()
{

    $this->db->select('Supplier_Id,SupplierName');
    $this -> db -> from('capri_master_supplier');  
    $query = $this -> db -> get();
    return $query->result();
}
    public function getproductname()
{

    $this->db->select('Product_Id,ProductName');
    $this -> db -> from('capri_master_product');

    $query = $this -> db -> get();
    return $query->result();
}
public function getproductdetail($Product_Id)
{

    $this->db->select('Product_Id,ProductPrice,ProductUnit,ProductName');
    $this->db->where('Product_Id', $Product_Id);
    $this->db->from('capri_master_product');
    $query=$this->db->get();
    return $query->result();
}

/*$this->db->select('concat(BillStreet,',',BillVillage,',',BillCity,',',billstate,',',billpincode) as address,CST,TIN');*/
public function getsupplierdetail($Supplier_Id)
{

    $this->db->select('BillingAddress');
    $this->db->select('CST,TIN');
    $this->db->from('capri_master_supplier');
    $this->db->where('Supplier_Id', $Supplier_Id);
    $query=$this->db->get();
    return $query->result();
}

     public function get_order($Product_Id=null, $search_string=null, $order=null, $order_type='Asc', $limit_start, $limit_end)
    {
        
        $this->db->select('capri_debit_details.Id');
        $this->db->select('capri_debit_details.Product_Id');
        $this->db->select('capri_debit_details.Quantity');
      
         $this->db->select('capri_debit_details.Debit_Id');
        $this->db->select('capri_debit_details.Product_Id');
        $this->db->select('capri_master_product.Product_Id as Product_Id');
        $this->db->from('capri_debit_details');
        if($Product_Id != null && $Product_Id != 0){
            $this->db->where('Product_Id', $Product_Id);
        }
        if($search_string){
            $this->db->like('Product_Id', $search_string);
        }

        $this->db->join('capri_master_product', 'capri_debit_details.Product_Id = capri_master_product.Product_Id', 'left');
       
        
        $this->db->group_by('capri_debit_details.Id');

        if($order){
            $this->db->order_by($order, $order_type);
        }else{
            $this->db->order_by('Id', $order_type);
        }


        $this->db->limit($limit_start, $limit_end);
        //$this->db->limit('4', '4');


        $query = $this->db->get();
        
        return $query->result_array();  
    }
    /**
    * Count the number of rows
    * @param int $search_string
    * @param int $order
    * @return int
    */
   function count_debit($Customer_Id=null, $search_string=null, $order=null)
    {
        $this->db->select('*');
        $this->db->from('capri_debit_entry');
        if($Customer_Id != null && $Customer_Id != 0){
            $this->db->where('Customer_Id', $Customer_Id);
        }
        
        if($order){
            $this->db->order_by($order, 'Asc');
        }else{
            $this->db->order_by('Debit_Id', 'Asc');
        }
        $query = $this->db->get();
        return $query->num_rows();        
    }


    /**
    * Store the new item into the database
    * @param array $data - associative array with data to store
    * @return boolean 
    */
    function store_manufacture($data)
    {
		$insert = $this->db->insert('capri_debit_entry', $data);
	    return $insert;
	}

    /**
    * Update manufacture
    * @param array $data - associative array with data to store
    * @return boolean
    */
    function update_manufacture($Debit_Id, $data)
    {
		$this->db->where('Debit_Id', $Debit_Id);
		$this->db->update('capri_debit_entry', $data);
		$report = array();
		$report['error'] = $this->db->_error_number();
		$report['message'] = $this->db->_error_message();
		if($report !== 0){
			return true;
		}else{
			return false;
		}
	}

    /**
    * Delete manufacturer
    * @param int $Debit_Id - manufacture Debit_Id
    * @return boolean
    */


function delete_order_details($Id){

    $this->db->where('Id',$Id);
    $this->db->delete('capri_debit_details');
}


function insert_order_details($data)
    {
        $insert = $this->db->insert('capri_debit_details', $data);
        return $insert;
    }

    function purchase_order_delete_details($Id){

    $this->db->where('Debit_Id',$Id);
    $this->db->delete('capri_debit_details');
 }


public function order_details_maxvalue()
    {
        $this->db->select_MAX('Id');
        $this->db->from('capri_debit_details');
        $query = $this->db->get();
        return $query;

    }








	function delete_manufacture($Debit_Id){
		$this->db->where('Debit_Id', $Debit_Id);
		$this->db->delete('capri_debit_entry'); 
	}
 
}
	
