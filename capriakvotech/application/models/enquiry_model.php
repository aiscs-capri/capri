<?php
class enquiry_model extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return voEnquiry_Id
    */
    public function __construct()
    {
        $this->load->database();
    }

    /**
    * Get product by his is
    * @param int $product_Enquiry_Id 
    * @return array
    */
    public function get_manufacture_by_Enquiry_Id($Enquiry_Id)
    {
        $this->db->select('*');
        $this->db->from('capri_customer_enquiry');
        $this->db->where('Enquiry_Id', $Enquiry_Id);
        $query = $this->db->get();
        return $query->result_array(); 
    }    


public function save($Enquiry_Id)
    {
        $insert = $this->db->insert('capri_customer_enquiry', $data);
        return $insert;
    }   

    public function maxvalue()
    {
        $this->db->select_MAX('Enquiry_Id');
        $this->db->from('capri_customer_enquiry');
        $query = $this->db->get();
        return $query;

    }

  /* view order and supplier*/
    public function getenquiryOrderDetails($purchase_order)
    {
        $this->db->select('capri_customer_enquiry.enquiry_number, capri_customer_enquiry.CustomerName, capri_master_customer.CustomerName, capri_master_customer.BillingAddress, capri_master_customer.TIN,capri_master_customer.CST');
        $this->db->from('capri_customer_enquiry');
        $this->db->join('capri_master_customer','capri_customer_enquiry.CustomerName = capri_master_customer.CustomerName','inner');
        $this->db->where('capri_customer_enquiry.Enquiry_Id',$purchase_order);
        $query = $this->db->get();
        return $query->result();
    }

/* show the product item details*/
public function getenquiryOrderproductDetails($purchase_order_product)
    {
        $this->db->select('Id,ProductName,ProductUnit,ProductPrice,Quantity,Total');
        $this->db->from('capri_customer_enquiry_details');       
        $this->db->where('capri_customer_enquiry_details.Enquiry_Id',$purchase_order_product);
        $query = $this->db->get();
        return $query->result();
    }


    
    
    /**
    * Fetch purchase_master data from the database
    * possibility to mix search, filter and order
    * @param string $search_string 
    * @param strong $order
    * @param string $order_type 
    * @param int $limit_start
    * @param int $limit_end
    * @return array
    */
    public function get_enquiry($Supplier_Id=null, $search_string=null, $order=null, $order_type='Asc', $limit_start, $limit_end)
    {
        
        $this->db->select(' capri_customer_enquiry.Enquiry_Id');
        $this->db->select(' capri_customer_enquiry.Enquiry_number');
        $this->db->select(' capri_customer_enquiry.EnquiryDate');
        $this->db->select(' capri_customer_enquiry.CustomerName');
        $this->db->select(' capri_customer_enquiry.Address');
        $this->db->select(' capri_customer_enquiry.TIN');
        $this->db->select(' capri_customer_enquiry.CST');
        $this->db->select(' capri_customer_enquiry.Quality');
        $this->db->select(' capri_customer_enquiry.Note');
        $this->db->select(' capri_customer_enquiry.Remark');
         $this->db->select(' capri_customer_enquiry.PaymentTerm');
        $this->db->select(' capri_customer_enquiry.Total');
        $this->db->select(' capri_customer_enquiry.Tax_type');
        $this->db->select(' capri_customer_enquiry.Tax');
        $this->db->select(' capri_customer_enquiry.TaxAmount');
        
        $this->db->from('capri_customer_enquiry');
        

        //$this->db->join('capri_master_supplier', ' capri_customer_enquiry.Supplier_Id = capri_master_supplier.Supplier_Id', 'left');

        $this->db->group_by(' capri_customer_enquiry.Enquiry_Id');

        if($order){
            $this->db->order_by($order, $order_type);
        }else{
            $this->db->order_by('Enquiry_Id', $order_type);
        }


        $this->db->limit($limit_start, $limit_end);
        //$this->db->limit('4', '4');


        $query = $this->db->get();
        
        return $query->result_array();  
    }
    /**
    * Count the number of rows
    * @param int $search_string
    * @param int $order
    * @return int
    */

    public function getcustomername()
{

    $this->db->select('Customer_Id,CustomerName');
    $this -> db -> from('capri_master_customer');  
    $query = $this -> db -> get();
    return $query->result();
}


    public function getproductname()
{

    $this->db->select('Product_Id,ProductName');
    $this -> db -> from('capri_master_product');

    $query = $this -> db -> get();
    return $query->result();
}
public function getproductdetail($Product_Id)
{

    $this->db->select('Product_Id,ProductPrice,ProductUnit');
    $this->db->where('Product_Id', $Product_Id);
    $this->db->from('capri_master_product');
    $query=$this->db->get();
    return $query->result();
}

/*$this->db->select('concat(BillStreet,',',BillVillage,',',BillCity,',',billstate,',',billpincode) as address,CST,TIN');*/
public function getcustomerdetail($Customer_Id)
{

    $this->db->select('BillingAddress,CST,TIN');
    $this->db->from('capri_master_customer');
    $this->db->where('Customer_Id', $Customer_Id);
    $query=$this->db->get();
    return $query->result();
}

     public function get_order($Product_Id=null, $search_string=null, $order=null, $order_type='Asc', $limit_start, $limit_end)
    {
        
        $this->db->select(' capri_customer_enquiry_details.Id');
        $this->db->select(' capri_customer_enquiry_details.Product_Id');
        $this->db->select(' capri_customer_enquiry_details.Quantity');
        $this->db->select(' capri_customer_enquiry_details.Total');
         $this->db->select(' capri_customer_enquiry_details.Enquiry_Id');
        $this->db->select(' capri_customer_enquiry_details.Product_Id');
        $this->db->select('capri_master_product.Product_Id as Product_Id');
        $this->db->from(' capri_customer_enquiry_details');
        if($Product_Id != null && $Product_Id != 0){
            $this->db->where('Product_Id', $Product_Id);
        }
        if($search_string){
            $this->db->like('Product_Id', $search_string);
        }

        $this->db->join('capri_master_product', ' capri_customer_enquiry_details.Product_Id = capri_master_product.Product_Id', 'left');
       
        
        $this->db->group_by('capri_customer_enquiry_details.Id');

        if($order){
            $this->db->order_by($order, $order_type);
        }else{
            $this->db->order_by('Id', $order_type);
        }


        $this->db->limit($limit_start, $limit_end);
        //$this->db->limit('4', '4');


        $query = $this->db->get();
        
        return $query->result_array();  
    }
    /**
    * Count the number of rows
    * @param int $search_string
    * @param int $order
    * @return int
    */
   function count_enquiry($Supplier_Id=null, $search_string=null, $order=null)
    {
        $this->db->select('*');
        $this->db->from('capri_customer_enquiry');
        if($Supplier_Id != null && $Supplier_Id != 0){
            $this->db->where('Supplier_Id', $Supplier_Id);
        }
        if($search_string){
            $this->db->like('Order_number', $search_string);
        }
        if($order){
            $this->db->order_by($order, 'Asc');
        }else{
            $this->db->order_by('Enquiry_Id', 'Asc');
        }
        $query = $this->db->get();
        return $query->num_rows();        
    }


    /**
    * Store the new item into the database
    * @param array $data - associative array with data to store
    * @return boolean 
    */
    function store_manufacture($data)
    {
        $insert = $this->db->insert('capri_customer_enquiry', $data);
        return $insert;
    }

    /**
    * Update manufacture
    * @param array $data - associative array with data to store
    * @return boolean
    */
    function update_manufacture($Enquiry_Id, $data)
    {
        $this->db->where('Enquiry_Id', $Enquiry_Id);
        $this->db->update('capri_customer_enquiry', $data);
        $report = array();
        $report['error'] = $this->db->_error_number();
        $report['message'] = $this->db->_error_message();
        if($report !== 0){
            return true;
        }else{
            return false;
        }
    }

    /**
    * Delete manufacturer
    * @param int $Enquiry_Id - manufacture Enquiry_Id
    * @return boolean
    */
    function delete_manufacture($Enquiry_Id){
        $this->db->where('Enquiry_Id', $Enquiry_Id);
        $this->db->delete('capri_customer_enquiry'); 
    }
 
function delete_order_details($Id){

    $this->db->where('Id',$Id);
    $this->db->delete('capri_customer_enquiry_details');
}


function insert_order_details($data)
    {
        $insert = $this->db->insert('capri_customer_enquiry_details', $data);
        return $insert;
    }

    function purchase_order_delete_details($Id){

    $this->db->where('Enquiry_Id',$Id);
    $this->db->delete('capri_customer_enquiry_details');
 }


public function order_details_maxvalue()
    {
        $this->db->select_MAX('Id');
        $this->db->from('capri_customer_enquiry_details');
        $query = $this->db->get();
        return $query;

    }


}
