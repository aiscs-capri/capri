    <div class="container top">
      <div class="row">
     <div class="span10 columns">
        <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url("admin"); ?>">
            <?php echo ucfirst($this->uri->segment(1));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
          <?php echo ucfirst($this->uri->segment(2));?>
        </li>
      </ul>

      <div class="page-header users-header">
        <h2>
          <?php echo ucfirst($this->uri->segment(2));?> 
          <a  href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>/add" class="btn btn-success">Add Lead</a>
        </h2>
      </div>
      
    </div>
   </div>













     





       <div class="span10 columns">
          <div class="well">
           
             <?php
           
            $attributes = array('class' => 'form-inline reset-margin', 'id' => 'myform');
           
            
            //save the columns names in a array that we will use as filter         
            /*$options_purchase = array();    
            foreach ($capri_lead_order as $array) {
              foreach ($array as $key => $value) {
                $options_purchase[$key] = $key;
              }
              break;
            }*/

            echo form_open('admin/lead', $attributes);
     
              /*echo form_label('Search:', 'search_string');
              echo form_input('search_string', $search_string_selected, 'style="width: 170px;
height: 26px;"');*/

             /* echo form_label('Filter by manufacturer:', 'Supplier_Id');
              echo form_dropdown('Supplier_Id', $options_manufacture, $manufacture_selected, 'class="span2"');
*/
             /* echo form_label('Order by:', 'order');
              echo form_dropdown('order', $options_purchase, $order, 'class="span2"');

              $data_submit = array('name' => 'mysubmit', 'class' => 'btn btn-primary', 'value' => 'Go');

              $options_order_type = array('Asc' => 'Asc', 'Desc' => 'Desc');
              echo form_dropdown('order_type', $options_order_type, $order_type_selected, 'class="span2"');

              echo form_submit($data_submit);*/

            echo form_close();
            ?>

          </div>

          <table class="table table-striped table-bordered table-condensed">
            <thead>
              <tr>
                <!-- <th class="header">Enq Id</th> -->
                <th class="yellow header headerSortDown">Customer Name</th>
                <th class="yellow header headerSortDown">Address</th>
                <th class="yellow header headerSortDown">Phone Number</th>
                <th class="yellow header headerSortDown">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php
              foreach($capri_lead_order as $row)
              {
                echo '<tr>';
               /* echo '<td>'.$row['Lead_Id'].'</td>';*/
                echo '<td>'.$row['CustomerName'].'</td>';
                echo '<td>'.$row['BillingAddress'].'</td>';
                echo '<td>'.$row['PhoneNumber'].'</td>';
               
                echo '<td class="crud-actions">
                  <a href="'.site_url("admin").'/lead/update/'.$row['Lead_Id'].'"><button class="btn btn-xs btn-warning"><i class="fa fa-pencil"></i> </button></a>   
                  <a href="'.site_url("admin").'/lead/delete/'.$row['Lead_Id'].'"><button class="btn btn-xs btn-danger"><i class="fa fa-times"></i> </button></a>
                  <a href="'.site_url("admin").'/lead/view/'.$row['Lead_Id'].'"><button class="btn btn-xs btn-primary"><i class="fa fa-th-list"></i> </button></a>

                </td>';
                echo '</tr>';
              }
              ?>      
            </tbody>
          </table>

          <?php echo '<div class="pagination">'.$this->pagination->create_links().'</div>'; ?>
       </div>
   
  
    </div>