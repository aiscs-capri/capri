<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    <title>Capriakvotech Dashboard</title>

    <!-- Bootstrap core CSS -->
     <link href="<?php echo base_url(); ?>assets/css/admin/global.css" rel="stylesheet" type="text/css">
      
    <link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery-ui.css">
  <script src="<?php echo base_url(); ?>js/jquery-1.11.1.js"></script>
  <script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url(); ?>css/dashboard.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>
      $(function() {
       $( "#accordion" ).accordion(
        { active: true},
        { collapsible: true }
        );
      })


      $("#accordion > li > div").click(function(){
 
    if(false == $(this).next().is(':visible')) {
        $('#accordion ul').slideUp(300);
    }
    $(this).next().slideToggle(300);
});
  </script>
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Capri Akvotech</a>
        </div>
        <!-- <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">Settings</a></li>
            <li><a href="#">Profile</a></li>
            <li><a href="#">Help</a></li>
          </ul> -->
          
          <div class="navbar-collapse collapse">
         <ul class="nav navbar-nav navbar-right">
           <li><a href="#"><?php echo $this->session->userdata('user_name');  ?></a></li>
           <li> <a href="<?php echo base_url(); ?>admin/logout">Logout</a></li>
         </ul>         
       </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-4 col-md-2 sidebar">

<ul id="accordion">
    <li><div>MASTER</div>
        <ul>
            <li <?php if($this->uri->segment(2) == 'unit'){echo 'class="active"';}?>>
            <a href="<?php echo base_url(); ?>admin/unit">Unit</a>
          </li><BR>
           <li <?php if($this->uri->segment(2) == 'product'){echo 'class="active"';}?>>
            <a href="<?php echo base_url(); ?>admin/product">Product</a>
          </li><BR>
          <li <?php if($this->uri->segment(2) == 'customer'){echo 'class="active"';}?>>
            <a href="<?php echo base_url(); ?>admin/customer">Customer</a>
          </li><BR>
           <li <?php if($this->uri->segment(2) == 'supplier'){echo 'class="active"';}?>>
            <a href="<?php echo base_url(); ?>admin/supplier">Supplier</a>
          </li><BR>
           <li <?php if($this->uri->segment(2) == 'employee'){echo 'class="active"';}?>>
            <a href="<?php echo base_url(); ?>admin/employee">Employee</a>
          </li>
        </ul>
    </li>
    <li><div>PURCHASE</div>
        <ul>
            <li <?php if($this->uri->segment(2) == 'purchase'){echo 'class="active"';}?>>
            <a href="<?php echo base_url(); ?>admin/purchase">Purchase Order</a>
          </li><BR>
          <li <?php if($this->uri->segment(2) == 'inward'){echo 'class="active"';}?>>
            <a href="<?php echo base_url(); ?>admin/inward">Purchase Inward</a>
          </li><BR>
          <li <?php if($this->uri->segment(2) == 'outpay'){echo 'class="active"';}?>>
            <a href="<?php echo base_url(); ?>admin/outpay">Outgoing Payment</a>
          </li><BR>
          <li <?php if($this->uri->segment(2) == 'debit'){echo 'class="active"';}?>>
            <a href="<?php echo base_url(); ?>admin/debit">Debit Note</a>
          </li><BR>
        </ul>
    </li>
    <li><div>SALES</div>
        <ul>
            <li <?php if($this->uri->segment(2) == 'lead'){echo 'class="active"';}?>>
            <a href="<?php echo base_url(); ?>admin/lead">Leads</a>
          </li><BR>
          <li <?php if($this->uri->segment(2) == 'Enquiry'){echo 'class="active"';}?>>
            <a href="<?php echo base_url(); ?>admin/enquiry">Enquiry</a>
          </li><BR>
            <li <?php if($this->uri->segment(2) == 'Quotation'){echo 'class="active"';}?>>
            <a href="<?php echo base_url(); ?>admin/salesquotation">Quotation</a>
          </li><BR>
             <li <?php if($this->uri->segment(2) == 'proforma'){echo 'class="active"';}?>>
            <a href="<?php echo base_url(); ?>admin/proforma"> Proforma Invoice</a>
          </li><BR>
          <li <?php if($this->uri->segment(2) == 'salesinvoice'){echo 'class="active"';}?>>
            <a href="<?php echo base_url(); ?>admin/salesinvoice">Sales Invoice</a>
          </li><BR>
           <li <?php if($this->uri->segment(2) == 'delivery'){echo 'class="active"';}?>>
            <a href="<?php echo base_url(); ?>admin/delivery">Delivery Chellan</a>
          </li><BR>
           <li <?php if($this->uri->segment(2) == 'income'){echo 'class="active"';}?>>
            <a href="<?php echo base_url(); ?>admin/income">Incoming Payment</a>
          </li><BR>
          <li <?php if($this->uri->segment(2) == 'credit'){echo 'class="active"';}?>>
            <a href="<?php echo base_url(); ?>admin/credit">Credit Note</a>
          </li><BR>
        </ul>
    </li>
    <li><div>MARKETING</div>
        <ul>
            <li <?php if($this->uri->segment(2) == 'marketing'){echo 'class="active"';}?>>
            <a href="<?php echo base_url(); ?>admin/marketing">Marketing</a>
          </li><BR>
          
           
        </ul>
    </li>
    <li><div>REPORT</div>
        <ul>
            <li <?php if($this->uri->segment(2) == 'salesreport'){echo 'class="active"';}?>>
            <a href="<?php echo base_url(); ?>admin/salesreport">Sales Report</a>
          </li><BR>
          <li <?php if($this->uri->segment(2) == 'purchasereport'){echo 'class="active"';}?>>
            <a href="<?php echo base_url(); ?>admin/purchasereport">Purchase Report</a>
          </li><BR>
        </ul>
    </li>
    <li><div>STOCK TRANSFER</div>
        <ul>
            <li <?php if($this->uri->segment(2) == 'stocktransfer'){echo 'class="active"';}?>>
            <a href="<?php echo base_url(); ?>admin/stocktransfer">Stock Transfer</a>
          </li><BR>
         
        </ul>
    </li>
</ul>

</div>
           
         
          
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Capri Akvotech</h1>

       
        