    
 <script src="<?php echo base_url(); ?>js/jquery-1.11.1.js"></script>
 <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
<script src="<?php echo base_url(); ?>js/config.js"></script>
<script>
$(document).ready(function() {
    $( "#product" ).change(function () {

    var str = "";
    $( "#product option:selected" ).each(function() {
      str += $( this ).val() + " ";
    });

  $.ajax({
            type : "get",
            url: serverurl+"/admin_debit/get", 
            dataType: "json",
            data: {
                Product_Id : str
               },
            success: function(data){                          
               console.log(data.rows[0].ProductPrice);
               $("#pid").val(data.rows[0].Product_Id),
                $("#price").val(data.rows[0].ProductPrice),
                $("#unit").val(data.rows[0].ProductUnit);
            } ,
            error: function(data, errorThrown)
          {
              alert('request failed :'+errorThrown);
          }
        });    

  }).change(); 

      $("#addRecord").click(function(){
      console.log('test');
        var id = $("#product option:selected").val();
        var product = $("#product option:selected").text();
        var unit = $("#unit").val();
        var qun = $("#qun").val();
       /* var price = $("#price").val();
        var tot = $("#tot").val();*/
        
        $("#myTable").append("<tr><td style=display:none;>"+id+"<input type=hidden name=id[] value="+id+"></td><td>"+product+"<input type=hidden name=product[] value="+product+"></td><td>"+unit+"<input type=hidden name=unit[] value="+unit+"></td><td>"+qun+"<input type=hidden name=qun[] value="+qun+"></td><td><input type=checkbox></input></td></tr>");
        totalDisplay();
        $("#product").prop('selectedIndex',0);       
        $("#unit").val("");
        $("#qun").val("");
        /*$("#price").val("");
         $("#tot").val("");
*/
    });

    
 $("#delete").click(function(){
        $("input[type=checkbox]:checked").each(function() {
        console.log(  $(this).parent().parent().remove());
        totalDisplay();
      });
  });

//Balance calculation

 $("#ap").on("keyup", function() {
    var val = +this.value || 0;
    $("#bal").val($("#gtot").val() - val);
});



 //get customer data

 $( "#supplier" ).change(function () {

    var str = "";
    $( "#supplier option:selected" ).each(function() {
      str += $( this ).val() + " ";
    });

  $.ajax({
            type : "get",
            url: serverurl+"/admin_debit/getsupplierdata", 
            dataType: "json",
            data: {
                Supplier_Id : str
               },
            success: function(data){                          
               console.log(data.rows[0].BillState);
               $("#bil").val(data.rows[0].BillingAddress),
                $("#cst").val(data.rows[0].CST),
                $("#tin").val(data.rows[0].TIN);
            } ,
            error: function(data, errorThrown)
          {
              alert('request failed :'+errorThrown);
          }
        });    
  }).change(); 


//get supplier name store in db
 $( "#supplier" ).change(function () {
     $("#SupplierName").val($( "#supplier option:selected").text());
     console.log($( "#supplier option:selected").text());
   });

});

    

//get table total values
$("#total").click(function(){
var $table = $('#myTable'),
    $tr = $table.find('tbody tr'),
    total = 0;

$tr.each(function(i, el){
    total += parseInt($(this).find('td:last').html(), 10);
});

$table.find('tfoot td:last').html(total);
 });



function totalDisplay(){
  var totalHours=0;
  $(".amt").each(function(){
    totalHours +=parseInt($(this).text());


  });
  $("#total").val(totalHours);
}

function sum() {
            var txtFirstNumberValue = document.getElementById('price').value;
            var txtSecondNumberValue = document.getElementById('qun').value;
            var result = parseInt(txtFirstNumberValue) * parseInt(txtSecondNumberValue);
            if (!isNaN(result)) {
                document.getElementById('tot').value = result;
            }
        }

function gtotal() {
            var txtFirstNumberValue = document.getElementById('total').value;
            var txtSecondNumberValue = document.getElementById('tax').value;
            var txtThirdNumberValue = document.getElementById('paf').value;
            var txtFourthNumberValue = document.getElementById('sc').value;
            var txtFifthNumberValue = document.getElementById('oc').value;
            
            var result =(parseInt(txtFirstNumberValue) * parseInt(txtSecondNumberValue)/100);
            var result1 = parseInt(txtFirstNumberValue)+(parseInt(txtFirstNumberValue) * parseInt(txtSecondNumberValue)/100);
            var result2 = (parseInt(txtFifthNumberValue)+parseInt(txtFourthNumberValue)+parseInt(txtThirdNumberValue)+result1);
            if (!isNaN(result)) {
                document.getElementById('atax').value = result;
                document.getElementById('gtot').value = result2;
            }
        }

</script>
    <div class="container top">
         <div class="row">
        <div class="span10 columns">
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url("admin"); ?>">
            <?php echo ucfirst($this->uri->segment(1));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li>
          <a href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>">
            <?php echo ucfirst($this->uri->segment(2));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
          <a href="#">New</a>
        </li>
      </ul>
    </div></div></div>
      
      <div class="page-header">
        <h2>
          Adding <?php echo ucfirst($this->uri->segment(2));?>
        </h2>
      
    </div>
      <?php
      //flash messages
      if(isset($flash_message)){
        if($flash_message == TRUE)
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Well done!</strong> New performance created with success.';
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
          echo '</div>';          
        }
      }
      ?>
      
      <?php
      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '');
          //form validation
      echo validation_errors();
      
      echo form_open('admin/debit/add', $attributes);
      ?>
       <div class="container">
           <div class="row">
               <div class="col-md-5">
                <div class="control-group">
            <label for="inputError" class="control-label"></label>
            <div class="controls">
              <input type="hidden" id="box1" name="Debit_Id" value="<?php echo set_value('Debit_Id'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Supplier Name</label>
            <select id="supplier">
            <option value="">select</option>
                      <?php
                        foreach ($SupplierName as $row) {
                          echo "<option value=".$SupplierName = $row->Supplier_Id.">".$SupplierName = $row->SupplierName."</option>"; 
                        }
                      ?>
                      </select>
                      <input type="hidden" id="SupplierName" name="SupplierName">
                    </div>

            <div class="control-group">
            <label for="inputError" class="control-label"> Address</label>
            <div class="controls">
           <textarea rows="5" cols="20" id="bil" name="BillingAddress" readonly></textarea>
           </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label" >CST</label>
            <div class="controls">
              <input type="text" id="cst" name="CST" readonly>
              </div>
          </div>
          
           <div class="control-group">
            <label for="inputError" class="control-label" >TIN</label>
            <div class="controls">
              <input type="text" id="tin" name="TIN" readonly>
              </div>
          </div>
                  
        
          </div>
          
      <div class="col-md-5">
        

          </div>
        </div></div>
        <hr>
       
     

       <div class="container">
           <div class="row">
               <div class="col-md-5">
              <div class="control-group">
            <label for="inputError" class="control-label">Product Name</label>
            <select id="product" name="Product_Id">
              <option value="">select</option>
                      <?php
                        foreach ($ProductName as $row) {
                          echo "<option value=".$Product_Id = $row->Product_Id.">".$ProductName = $row->ProductName."</option>"; 
                        }
                      ?>
                      </select>
                    </div>
          <div class="control-group">
            <!-- <label for="inputError" class="control-label">product Id</label> -->
            <div class="controls">
              <input type="hidden" id="pid" name="Product_Id" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
<!-- 
          <div class="control-group">
            <label for="inputError" class="control-label" >Product Price</label>
            <div class="controls">
              <input type="text" id="price" name="ProductPrice"onkeyup="sum();" >
              
            </div>
          </div> -->


        </div>
          <div class="col-md-5">
           <div class="control-group">
            <label for="inputError" class="control-label" >Product Unit</label>
            <div class="controls">
              <input type="text" id="unit" name="ProductUnit" readonly>
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
                <div class="control-group">
            <label for="inputError" class="control-label">Quantity</label>
            <div class="controls">
              <input type="text" id="qun" name="Quantity" value="<?php echo set_value('Quantity'); ?>" onkeyup="sum();">
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>

          <!-- <div class="control-group">
            <label for="inputError" class="control-label">Amount</label>
            <div class="controls">
              <input type="text" id="tot" name="Total" value="<?php echo set_value('Total'); ?>" readonly>
              
            </div>
          </div> -->


        </div></div>
          <div class="row">
         <div class="col-md-10">
          <center>
          <div class="btn btn-success" id="addRecord">Add</div>
           <div class="btn btn-danger" id="delete">Delete</div></center>
           </div></div>
         <br><br>
         <div class="row">
         <div class="col-md-10">
          <table id="myTable" class="table table-striped table-bordered table-condensed">
            <tr>
            <!-- <td>id</td> -->
            <td>Product Name</td>
            <td>Product Unit</td>
            <td>Product Quantity</td>
            <!-- <td>Product Price</td>
            <td>Total</td> -->
            <td>select</td>
        </tr>
          </table>                  
  
     

    </div>
            </div><hr>
<div class="row">
  <div class="col-md-5">
    <div class="control-group">
            <label for="inputError" class="control-label">Reson For Debit</label>
            <div class="controls">
              <input type="text" id="box2" name="Reson" value="<?php echo set_value('Reson'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
    <div class="control-group">
            <label for="inputError" class="control-label">Invoice Numer</label>
            <div class="controls">
              <input type="text" id="box2" name="InvoiceNumber" value="<?php echo set_value('InvoiceNumber'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Invoice Date</label>
            <div class="controls">
              <input type="date" id="result" name="InvoiceDate" value="<?php echo set_value('InvoiceDate'); ?>"  >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
  <div class="control-group">
            <label for="inputError" class="control-label">Return Date</label>
            <div class="controls">
              <input type="date" id="result" name="ReturnDate" value="<?php echo set_value('ReturnDate'); ?>"  >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
  
           
          </div>


    
      </div>
      <div class="row">
         <div class="col-md-10">
             <div class="form-actions">
           <center><button class="btn btn-primary" type="submit">Save changes</button>
            <button class="btn" type="reset">Cancel</button></center>
          </div>
        </div>
      </div>
      <?php echo form_close(); ?>

    </div>
            </div>
          </div>
            