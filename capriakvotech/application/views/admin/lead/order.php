    

    <div class="container top">
         <div class="row">
        <div class="span10 columns">
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url("admin"); ?>">
            <?php echo ucfirst($this->uri->segment(1));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li>
          <a href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>">
            <?php echo ucfirst($this->uri->segment(2));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
          <a href="#">New</a>
        </li>
      </ul>
      
      <div class="page-header">
        <h2>
          Adding <?php echo ucfirst($this->uri->segment(2));?>
        </h2>
      
    </div>
      <?php
      //flash messages
      if(isset($flash_message)){
        if($flash_message == TRUE)
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Well done!</strong> New purchase created with success.';
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
          echo '</div>';          
        }
      }
      ?>
      
      <?php
      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '');
    $options_manufacture = array('' => "Select");
      foreach ($manufactures as $row)
      {
        $options_manufacture[$row['Product_Id']] = $row['Product_Id'];
      }
      //form validation
      echo validation_errors();
      
      echo form_open('admin/purchase/add', $attributes);
      ?>
       <div class="container">
           <div class="row">
               <div class="col-md-5">
                <div class="control-group">
            <label for="inputError" class="control-label">Order Id</label>
            <div class="controls">
              <input type="text" id="" name="Order_Id" value="<?php echo set_value('Order_Id'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Order Number</label>
            <div class="controls">
              <input type="text" id="" name="Order_number" value="<?php echo set_value('Order_number'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Order Date</label>
            <div class="controls">
              <input type="text" id="" name="OrderDate" value="<?php echo set_value('OrderDate'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
       <?php
          echo '<div class="control-group">';
            echo '<label for="Supplier_Id" class="control-label">Supplier id</label>';
            echo '<div class="controls">';
              //echo form_dropdown('Supplier_Id', $options_manufacture, '', 'class="span2"');
              
              echo form_dropdown('Supplier_Id', $options_manufacture, set_value('Supplier_Id'), 'class="span2"');

            echo '</div>';
          echo '</div">';
          ?>
            <!-- <div class="control-group">
            <label for="inputError" class="control-label">Supplier Id</label>
            <div class="controls">
              <input type="text" id="" name="Supplier_Id" value="<?php echo set_value('Supplier_Id'); ?>" >
                </div>
          </div> -->
           <div class="control-group">
            <label for="inputError" class="control-label">Date Of Delivery</label>
            <div class="controls">
              <input type="text" id="" name="DateOfDelivery" value="<?php echo set_value('DateOfDelivery'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label">Place Of Delivery</label>
            <div class="controls">
              <input type="text" id="" name="PlaceOfDelivery" value="<?php echo set_value('PlaceOfDelivery'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Quality</label>
            <div class="controls">
              <input type="text" id="" name="Quality" value="<?php echo set_value('Quality'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Note</label>
            <div class="controls">
              <input type="text" id="" name="Note" value="<?php echo set_value('Note'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">PaymentTerm</label>
            <div class="controls">
              <input type="text" id="" name="PaymentTerm" value="<?php echo set_value('PaymentTerm'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
     <div class="control-group">
            <label for="inputError" class="control-label">Total</label>
            <div class="controls">
              <input type="text" id="" name="Total" value="<?php echo set_value('Total'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Tax Type</label>
            <div class="controls">
              <input type="text" id="" name="Tax_type" value="<?php echo set_value('Tax_type'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Tax</label>
            <div class="controls">
              <input type="text" id="" name="Tax" value="<?php echo set_value('Tax'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">GrossTotal</label>
            <div class="controls">
              <input type="text" id="" name="GrossTotal" value="<?php echo set_value('GrossTotal'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
   
    
      </div>
      <!-- <div class="row">
         <div class="col-md-10"> -->
             <div class="form-actions">
           <center><button class="btn btn-primary" type="submit">Save changes</button>
            <button class="btn" type="reset">Cancel</button></center>
          </div>
        </div>
      </div>
      <?php echo form_close(); ?>

    </div>
            </div>
            