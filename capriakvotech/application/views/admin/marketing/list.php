    <div class="container top">
         <div class="row">
        <div class="span10 columns">
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url("admin"); ?>">
            <?php echo ucfirst($this->uri->segment(1));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
          <?php echo ucfirst($this->uri->segment(2));?>
        </li>
      </ul>

      <div class="page-header users-header">
        <h2>
          <?php echo ucfirst($this->uri->segment(2));?> 
          <a  href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>/add" class="btn btn-success">Add Marketing</a>
        </h2>
      </div>
      
      <div class="row">
      <div class="col-md-10 ">
          <div class="well">
           
            <?php
           
            $attributes = array('class' => 'form-inline reset-margin', 'id' => 'myform');
           
            $options_manufacture = array(0 => "all");
            foreach ($manufactures as $row)
            {
              $options_manufacture[$row['Employee_Id']] = $row['Employee_Id'];
            }
            //save the columns names in a array that we will use as filter         
            $options_marketing = array();    
            foreach ($capri_marketing_order as $array) {
              foreach ($array as $key => $value) {
                $options_marketing[$key] = $key;
              }
              break;
            }

            echo form_open('admin/marketing', $attributes);
     
              echo form_label('Search:', 'search_string');
              echo form_input('search_string', $search_string_selected, 'style="width: 170px;
height: 26px;"');

              /*echo form_label('Filter by manufacturer:', 'manufacture_id');
              echo form_dropdown('manufacture_id', $options_marketing, $manufacture_selected, 'class="span2"');*/

              echo form_label('Order by:', 'order');
              echo form_dropdown('order', $options_marketing, $order, 'class="span2"');

              $data_submit = array('name' => 'mysubmit', 'class' => 'btn btn-primary', 'value' => 'Go');

              $options_order_type = array('Asc' => 'Asc', 'Desc' => 'Desc');
              echo form_dropdown('order_type', $options_order_type, $order_type_selected, 'class="span1"');

              echo form_submit($data_submit);

            echo form_close();
            ?>

          </div>

          <table class="table table-striped table-bordered table-condensed">
            <thead>
              <tr>
                <!-- <th class="header">#</th> -->
                <th class="yellow header headerSortDown">ScheduleDate</th>
                <!-- <th class="green header">ScheduleTime</th> -->
                <th class="red header">Name</th>
                <th class="red header">Phone</th>
                <th class="red header">Address</th>
                <th class="red header">ContactPerson</th>
                <th class="red header">Mobile</th>
                <th class="red header">Status</th>
                <th class="red header">message</th>
                <th class="red header">Employee</th>
                <th class="red header">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php
              foreach($capri_marketing_order as $row)
              {
                echo '<tr>';
                /*echo '<td>'.$row['Id'].'</td>';*/
                 echo '<td>'.date('Y-M-d',strtotime($row['ScheduleDate'])).'</td>';
                
                /*echo '<td>'.$row['ScheduleTime'].'</td>';*/
                echo '<td>'.$row['Name'].'</td>';
                echo '<td>'.$row['Address'].'</td>';
                echo '<td>'.$row['Phone'].'</td>';
                echo '<td>'.$row['ContactPerson'].'</td>';
                echo '<td>'.$row['Mobile'].'</td>';
                echo '<td>'.$row['Status'].'</td>';
                echo '<td>'.$row['message'].'</td>';
                echo '<td>'.$row['EmployeeName'].'</td>';
                echo '<td class="crud-actions">
                 
                <a href="'.site_url("admin").'/marketing/delete/'.$row['Id'].'"><button class="btn btn-xs btn-danger"><i class="fa fa-times"></i> </button></a>
                  <a href="'.site_url("admin").'/marketing/view/'.$row['Id'].'"><button class="btn btn-xs btn-primary"><i class="fa fa-th-list"></i> </button></a>

                </td>';
                echo '</tr>';
              }
              ?>      
            </tbody>
          </table>

          <?php echo '<div class="pagination">'.$this->pagination->create_links().'</div>'; ?>

      </div>
      <div>
      </div>
      
    </div>
    </div>