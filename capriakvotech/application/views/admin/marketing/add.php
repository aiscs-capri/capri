      
 <script src="<?php echo base_url(); ?>js/jquery-1.11.1.js"></script>
 <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
<script src="<?php echo base_url(); ?>js/config.js"></script>
<script>
$(document).ready(function() {

       $( "#employee" ).change(function () {
     $("#employeename").val($( "#employee option:selected").text());
     console.log($( "#employee option:selected").text());
   });
       });
</script>

       <div class="container top">
        <div class="row">
        <div class="span10 columns">
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url("admin"); ?>">
            <?php echo ucfirst($this->uri->segment(1));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li>
          <a href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>">
            <?php echo ucfirst($this->uri->segment(2));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
          <a href="#">New</a>
        </li>
      </ul>
      
      <div class="page-header">
        <h2>
          Adding <?php echo ucfirst($this->uri->segment(2));?>
        </h2>
      </div>
  </div>
</div>
      <?php
      //flash messages
      if(isset($flash_message)){
        if($flash_message == TRUE)
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Well done!</strong> new product created with success.';
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
          echo '</div>';          
        }
      }
      ?>
      
      <?php
      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '');
      
           //form validation
      echo validation_errors();
      
      echo form_open('admin/marketing/add', $attributes);
      ?>
        <div class="row">
        <div class="span10 columns">
        <fieldset>
          <div class="control-group">
            <label for="inputError" class="control-label">ScheduleDate</label>
            <div class="controls">
              <input type="date" id="" name="ScheduleDate" value="<?php echo set_value('ScheduleDate'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">ScheduleTime</label>
            <div class="controls">
              <input type="time" id="" name="ScheduleTime" value="<?php echo set_value('ScheduleTime'); ?>">
              <!--<span class="help-inline">Cost Price</span>-->
            </div>
          </div>          
          <div class="control-group">
            <label for="inputError" class="control-label">Name</label>
            <div class="controls">
              <input type="text" id="" name="Name" value="<?php echo set_value('Name'); ?>">
              <!--<span class="help-inline">Cost Price</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Address</label>
            <div class="controls">
              <input type="text" name="Address" value="<?php echo set_value('Address'); ?>">
              <!--<span class="help-inline">OOps</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Phone</label>
            <div class="controls">
              <input type="text" name="Phone" value="<?php echo set_value('Phone'); ?>">
              <!--<span class="help-inline">OOps</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">ContactPerson</label>
            <div class="controls">
              <input type="text" name="ContactPerson" value="<?php echo set_value('ContactPerson'); ?>">
              <!--<span class="help-inline">OOps</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Mobile</label>
            <div class="controls">
              <input type="text" name="Mobile" value="<?php echo set_value('Mobile'); ?>">
              <!--<span class="help-inline">OOps</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Status</label>
            <div class="controls">
              <select type="text" name="Status" value="<?php echo set_value('Status'); ?>">
              <option>select</option>
              <option>Not Required</option>
              <option>Call Back</option>
              <option>Order Close</option>
            </select>
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">message</label>
            <div class="controls">
              <input type="text" name="message" value="<?php echo set_value('message'); ?>">
              <!--<span class="help-inline">OOps</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Employee Name</label>
            <select id="employee">
            <option value="">select</option>
                      <?php
                        foreach ($EmployeeName as $row) {
                          echo "<option value=".$EmployeeName = $row->Employee_Id.">".$EmployeeName = $row->EmployeeName."</option>"; 
                        }
                      ?>
                      </select>
                      <input type="hidden" id="employeename" name="EmployeeName">
                    </div>

         
          </div>
        </div>
        <div class="row">
             <div class=col-md-10>
          <div class="form-actions">
          <button class="btn btn-primary" type="submit">Save changes</button>
            <button class="btn" type="reset">Cancel</button>
          </div>
          </div>
          </div>
        </fieldset>

      <?php echo form_close(); ?>

    </div>
     