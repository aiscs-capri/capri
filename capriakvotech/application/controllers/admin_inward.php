<?php
class Admin_inward extends CI_Controller {

    /**
    * name of the folder responsible for the views 
    * which are manipulated by this controller
    * @constant string
    */
    const VIEW_FOLDER = 'admin/inward';
 
    /**
    * Responsable for auto load the model
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
         $this->load->model('inward_model');
         $this->load->model('purchase_model');
         $this->load->model('supplier_model');
         $this->load->model('product_model');
         $this->load->model('order_model');
         $this->load->model('value_model');

        if(!$this->session->userdata('is_logged_in')){
            redirect('admin/login');
        }
    }
 
    /**
    * Load the main view with all the current model model's data.
    * @return void
    */
     public function index()
    {
        /*$id = $this->purchase_model->maxvalue();
        echo "";
        echo "jdhfjkfjhasjhf jahsfjhasf ahsjfhasf ahsfjaf =========>".$id->row('inward_Id');
        */
        //all the posts sent by the view
        $Total = $this->input->post('Total');        
        $search_string = $this->input->post('search_string');        
        $order = $this->input->post('order'); 
        $order_type = $this->input->post('order_type'); 

        //pagination settings
        $config['per_page'] = 20;
        $config['base_url'] = base_url().'admin/inward';
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = 20;
        $config['full_tag_open'] = '<ul>';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';

        //limit end
        $page = $this->uri->segment(3);

        //math to get the initial record to be select in the database
        $limit_end = ($page * $config['per_page']) - $config['per_page'];
        if ($limit_end < 0){
            $limit_end = 0;
        } 

        //if order type was changed
        if($order_type){
            $filter_session_data['order_type'] = $order_type;
        }
        else{
            //we have something stored in the session? 
            if($this->session->userdata('order_type')){
                $order_type = $this->session->userdata('order_type');    
            }else{
                //if we have nothing inside session, so it's the default "Asc"
                $order_type = 'Asc';    
            }
        }
        //make the data type var avaible to our view
        $data['order_type_selected'] = $order_type;        


        //we must avoid a page reload with the previous session data
        //if any filter post was sent, then it's the first time we load the content
        //in this case we clean the session filter data
        //if any filter post was sent but we are in some page, we must load the session data

        //filtered && || paginated
        if($Total !== false && $search_string !== false && $order !== false || $this->uri->segment(3) == true){ 
           
            /*
            The comments here are the same for line 79 until 99

            if post is not null, we store it in session data array
            if is null, we use the session data already stored
            we save order into the the var to load the view with the param already selected       
            */

            if($Total !== 0){
                $filter_session_data['manufacture_selected'] = $Total;
            }else{
                $Total = $this->session->userdata('manufacture_selected');
            }
            $data['manufacture_selected'] = $Total;

            if($search_string){
                $filter_session_data['search_string_selected'] = $search_string;
            }else{
                $search_string = $this->session->userdata('search_string_selected');
            }
            $data['search_string_selected'] = $search_string;

            if($order){
                $filter_session_data['order'] = $order;
            }
            else{
                $order = $this->session->userdata('order');
            }
            $data['order'] = $order;

            //save session data into the session
            $this->session->set_userdata($filter_session_data);

            //fetch manufacturers data into arrays
           

            $data['count_inward']= $this->inward_model->count_purchase($Total, $search_string, $order);
            $config['total_rows'] = $data['count_inward'];

            //fetch sql data into arrays
            if($search_string){
                if($order){
                    $data['inward'] = $this->inward_model->get_purchase($Total, $search_string, $order, $order_type, $config['per_page'],$limit_end);        
                }else{
                    $data['inward'] = $this->inward_model->get_purchase($Total, $search_string, '', $order_type, $config['per_page'],$limit_end);           
                }
            }else{
                if($order){
                    $data['inward'] = $this->inward_model->get_purchase($Total, '', $order, $order_type, $config['per_page'],$limit_end);        
                }else{
                    $data['inward'] = $this->inward_model->get_purchase($Total, '', '', $order_type, $config['per_page'],$limit_end);        
                }
            }

        }else{

            //clean filter data inside section
            $filter_session_data['manufacture_selected'] = null;
            $filter_session_data['search_string_selected'] = null;
            $filter_session_data['order'] = null;
            $filter_session_data['order_type'] = null;
            $this->session->set_userdata($filter_session_data);

            //pre selected options
            $data['search_string_selected'] = '';
            $data['manufacture_selected'] = 0;
            $data['order'] = 'id';

            $data['count_purchase']= $this->inward_model->count_inward();
            $data['capri_purchase_inward'] = $this->inward_model->get_inward('', '', '', $order_type, $config['per_page'],$limit_end);        
            $config['total_rows'] = $data['count_purchase'];

        }//!isset($Total) && !isset($search_string) && !isset($order)

        //initializate the panination helper 
        $this->pagination->initialize($config);   

        //load the view
        $data['main_content'] = 'admin/inward/list';
        $this->load->view('includes/template', $data);  

    }//index

    public function add()
    {

        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {
            //form validation
            $this->form_validation->set_rules('inward_Id', 'inward_number');
            $this->form_validation->set_rules('inward_number', 'inward_number', 'required');
            $this->form_validation->set_rules('inwardDate', 'inwardDate', 'required');
            $this->form_validation->set_rules('BillNumber', 'BillNumber', 'required');               
            $this->form_validation->set_rules('BillDate', 'BillDate','required');
            $this->form_validation->set_rules('Tax_type', 'Tax_type', 'required');
            $this->form_validation->set_rules('Tax', 'Tax', 'required');
            $this->form_validation->set_rules('TaxAmount', 'TaxAmount', 'required');
            $this->form_validation->set_rules('GrossTotal', 'GrossTotal', 'required');
           
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            

            //if the form has passed through the validation
            if ($this->form_validation->run())
            {

                $data_to_store = array(                    
                    'inward_number' => $this->input->post('inward_number'),
                    'inwardDate' => date("Y-m-d :H:i:s",strtotime($this->input->post('inwardDate'))),
                    'BillNumber' => $this->input->post('BillNumber'),
                    'BillDate' => date("Y-m-d :H:i:s",strtotime($this->input->post('BillDate'))),
                    'inwardDate' => $this->input->post('inwardDate'),
                    'Order_number' => $this->input->post('Order_number'),
                    'OrderDate' => $this->input->post('OrderDate'),                                        
                    'PlaceOfDelivery' => $this->input->post('PlaceOfDelivery'),
                    'Supplier_Id' => $this->input->post('Supplier_Id'),                    
                    'Total' => $this->input->post('Total'),
                    'Tax_type' => $this->input->post('Tax_type'),
                    'Tax' => $this->input->post('Tax'),
                    'TaxAmount' => $this->input->post('TaxAmount'),
                    'packingAndForward' => $this->input->post('packingAndForward'),
                    'servicetax' => $this->input->post('servicetax'),
                    'Otherchargs' => $this->input->post('Otherchargs'),
                    'GrossTotal' => $this->input->post('GrossTotal'),
                    'AdvancePayment' => $this->input->post('AdvancePayment'),
                    'Balance' => $this->input->post('Balance'),
                    'Paid' => "0",
                    'CreatedBy' => $this->session->userdata('user_id'),
                    'CreatedDate' => date("Y-m-d H:i:s")

                    
                );
                //if the insert has returned true then we show the flash message
                if($this->inward_model->store_manufacture($data_to_store)){
                    $data['flash_message'] = TRUE; 


        $id = $this->inward_model->maxvalue();
       /* echo $id->row('inward_Id');*/
       
        $product = $_POST['product'];
        $unit = $_POST['unit'];
        $qun = $_POST['qun'];
        $tot = $_POST['tot'];
        $price = $_POST['price'];
        $order = $id->row('inward_Id');


        for($i=0; $i<sizeof($product); $i++)
        {
        $data1['ProductName']= $product[$i];
        $data1['ProductUnit']= $unit[$i];
        $data1['Quantity']= $qun[$i];
        $data1['ProductPrice']= $price[$i];
        $data1['Total']= $tot[$i];
        $data1['inward_Id']= $order;
        $data1['CreatedBy'] = $this->session->userdata('user_id');
        $data1['CreatedDate'] = date("Y-m-d H:i:s");
            
        $this->value_model->getinward($data1);

      }
                }else{
                    $data['flash_message'] = FALSE; 
                }

            }

        }
        //fetch manufactures data to populate the select field
        $data['purchaseOrderDropdown'] = $this->inward_model->getordernumber();
        $product = $this->purchase_model->getproductname();
        //load the view       
        $data['ProductName'] =$product;
        $data['main_content'] = 'admin/inward/add';
        $this->load->view('includes/template', $data);  
 
    }       

    /**
    * Update item by his id
    * @return void
    */

public function get()
{

    $Product_Id=$_GET['Product_Id'];
    $this->purchase_model->getproductdetail('Product_Id');
    $productdetail["rows"] = $this->purchase_model->getproductdetail($Product_Id);
        echo json_encode($productdetail);

}

public function getsupplierdata()
{

    $Supplier_Id=$_GET['Supplier_Id'];
    $this->purchase_model->getsupplierdetail('Supplier_Id');
     $supplierdetail["rows"] = $this->purchase_model->getsupplierdetail($Supplier_Id);
        echo json_encode($supplierdetail);

}

    public function update()
    {
        //product id 
        $id = $this->uri->segment(4);
  
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {
            //form validation
            $this->form_validation->set_rules('inward_Id', 'inward_number');
            $this->form_validation->set_rules('inward_number', 'inward_number', 'required');
            $this->form_validation->set_rules('inwardDate', 'inwardDate', 'required');
            $this->form_validation->set_rules('BillNumber', 'BillNumber', 'required');               
            $this->form_validation->set_rules('BillDate', 'BillDate','required');
            $this->form_validation->set_rules('Tax_type', 'Tax_type', 'required');
            $this->form_validation->set_rules('Tax', 'Tax', 'required');
            $this->form_validation->set_rules('TaxAmount', 'TaxAmount', 'required');
            $this->form_validation->set_rules('GrossTotal', 'GrossTotal', 'required');
          
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
    
                 $data_to_store = array(                    
                                
                    'Total' => $this->input->post('Total'),
                    'Tax_type' => $this->input->post('Tax_type'),
                    'Tax' => $this->input->post('Tax'),
                    'TaxAmount' => $this->input->post('TaxAmount'),
                    'packingAndForward' => $this->input->post('packingAndForward'),
                    'servicetax' => $this->input->post('servicetax'),
                    'Otherchargs' => $this->input->post('Otherchargs'),
                    'GrossTotal' => $this->input->post('GrossTotal'),
                    'AdvancePayment' => $this->input->post('AdvancePayment'),
                    'Balance' => $this->input->post('GrossTotal'),
                    'Paid' => "0",
                    'CreatedBy' => $this->session->userdata('user_id'),
                    'CreatedDate' => date("Y-m-d H:i:s")

                );
                //if the insert has returned true then we show the flash message
                if($this->inward_model->update_manufacture($id, $data_to_store) == TRUE){
                    $this->session->set_flashdata('flash_message', 'updated');
                }else{
                    $this->session->set_flashdata('flash_message', 'not_updated');
                }
                redirect('admin/inward/');

            }//validation run

        }

        //if we are updating, and the data did not pass trough the validation
        //the code below wel reload the current data

     
        $data['manufacture'] = $this->inward_model->get_manufacture_by_inward_Id($id);
        
        $order=$this->inward_model->getsupplierDetails($id);
        $orderpro=$this->inward_model->getPurchaseInwardDetails($id);
        $product = $this->purchase_model->getproductname();

        
        //load the view    
        $data['ProductName'] =$product;
        $data['po']=$order;    
        $data['pop']=$orderpro;

        //load the view
        $data['main_content'] = 'admin/inward/edit';
        $this->load->view('includes/template', $data);            

    }//update

    /**
    * Delete product by his id
    * @return void
    */
    public function view()
    {
        //product id 
        $id = $this->uri->segment(4);

        //product data 
        $data['manufacture'] = $this->inward_model->get_manufacture_by_inward_Id($id);
        
        $order=$this->inward_model->getsupplierDetails($id);
        $orderpro=$this->inward_model->getPurchaseInwardDetails($id);
        
        //load the view    
        $data['po']=$order;    
        $data['pop']=$orderpro;
        //load the view
        $data['main_content'] = 'admin/inward/view';
        $this->load->view('includes/template', $data);            

    }//update

   /* Purchase order details get like supplier details*/
    public function  selectedOrderDetails()
    {
 
        $Id = $_GET['id'];        
 
        $resdata["row"] = array('order' =>$this->inward_model->getOrderDetails($Id) , 'details' => $this->purchase_model->getPurchaseOrderProductDetails($Id) );
        echo json_encode($resdata);

    }

    public function  inwardDetailsInsert()
    {
 
        
        $product = $_GET['product'];
        $unit = $_GET['unit'];
        $qun = $_GET['qun'];
        $price = $_GET['price'];
        $tot = $_GET['tot'];
        $inward = $_GET['inward_Id'];
        
        $data['ProductName']= $product;
        $data['ProductUnit']= $unit;
        $data['Quantity']= $qun;
        $data['ProductPrice']= $price;
        $data['Total']= $tot;
        $data['inward_Id']= $inward;
        $data['CreatedBy'] = $this->session->userdata('user_id');
        $data['CreatedDate'] = date("Y-m-d H:i:s");
            
        $resdata["row"] = $this->Value_model->getinward($data);
         

        echo json_encode($resdata);

    }



    public function  inwardDetailsDelete()
    {
 
        $pro = $_GET['id'];        
        
        $this->inward_model->delete_inward_details($pro);
        $deleteProduct = array(
        'message' => true
        );
        $resdata["row"] = $deleteProduct;
        echo json_encode($resdata);

     }
     public function  inwardDetailsMaxValue()
    {
       $id = $this->inward_model->inward_details_maxvalue();

       $sam = array(
        'Id' => $id->row('Id')
        );
       $resdata["row"] = $sam;
        echo json_encode($resdata);
    }
    /**
    * Delete product by his id
    * @return void
    */
    public function delete()
    {
        //product id 
        $id = $this->uri->segment(4);
        $this->purchase_model->purchase_order_delete_details($id);
        

         if($this->purchase_model->delete_manufacture($id) == TRUE){
                    $this->session->set_flashdata('flash_message', 'updated');
                }else{
                    $this->session->set_flashdata('flash_message', 'not_updated');
                }
        redirect('admin/purchase');
    }//edit

}