 <?php
class Admin_salesinvoice extends CI_Controller {

    /**
    * name of the folder responsible for the views 
    * which are manipulated by this controller
    * @constant string
    */
    const VIEW_FOLDER = 'admin/salesinvoice';
 
    /**
    * Responsable for auto load the model
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('salesinvoice_model');
         $this->load->model('customer_model');
         $this->load->model('product_model');
         $this->load->model('order_model');
         $this->load->model('value_model');
         $this->load->library('mpdf');

        if(!$this->session->userdata('is_logged_in')){
            redirect('admin/login');
        }
    }
 
    /**
    * Load the main view with all the current model model's data.
    * @return void
    */
     public function index()
    {
        /*$id = $this->salesinvoice_model->maxvalue();
        echo "";
        echo "jdhfjkfjhasjhf jahsfjhasf ahsjfhasf ahsfjaf =========>".$id->row('Order_Id');
        */
        //all the posts sent by the view
        $Reason = $this->input->post('Reason');        
        $search_string = $this->input->post('search_string');        
        $order = $this->input->post('order'); 
        $order_type = $this->input->post('order_type'); 

        //pagination settings
        $config['per_page'] = 20;
        $config['base_url'] = base_url().'admin/salesinvoice';
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = 20;
        $config['full_tag_open'] = '<ul>';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';

        //limit end
        $page = $this->uri->segment(3);

        //math to get the initial record to be select in the database
        $limit_end = ($page * $config['per_page']) - $config['per_page'];
        if ($limit_end < 0){
            $limit_end = 0;
        } 

        //if order type was changed
        if($order_type){
            $filter_session_data['order_type'] = $order_type;
        }
        else{
            //we have something stored in the session? 
            if($this->session->userdata('order_type')){
                $order_type = $this->session->userdata('order_type');    
            }else{
                //if we have nothing inside session, so it's the default "Asc"
                $order_type = 'Asc';    
            }
        }
        //make the data type var avaible to our view
        $data['order_type_selected'] = $order_type;        


        //we must avoid a page reload with the previous session data
        //if any filter post was sent, then it's the first time we load the content
        //in this case we clean the session filter data
        //if any filter post was sent but we are in some page, we must load the session data

        //filtered && || paginated
        if($Reason !== false && $search_string !== false && $order !== false || $this->uri->segment(3) == true){ 
           
            /*
            The comments here are the same for line 79 until 99

            if post is not null, we store it in session data array
            if is null, we use the session data already stored
            we save order into the the var to load the view with the param already selected       
            */

            if($Reason !== 0){
                $filter_session_data['manufacture_selected'] = $Reason;
            }else{
                $Reason = $this->session->userdata('manufacture_selected');
            }
            $data['manufacture_selected'] = $Reason;

            if($search_string){
                $filter_session_data['search_string_selected'] = $search_string;
            }else{
                $search_string = $this->session->userdata('search_string_selected');
            }
            $data['search_string_selected'] = $search_string;

            if($order){
                $filter_session_data['order'] = $order;
            }
            else{
                $order = $this->session->userdata('order');
            }
            $data['order'] = $order;

            //save session data into the session
            $this->session->set_userdata($filter_session_data);

            //fetch manufacturers data into arrays
            $data['manufactures'] = $this->salesinvoice_model->get_manufacturers();

            $data['salesinvoice'] = $this->product_model->get_manufacturers();

            $data['count_salesinvoice']= $this->salesinvoice_model->count_salesinvoice($Reason, $search_string, $order);
            $config['Reason_rows'] = $data['count_salesinvoice'];

            //fetch sql data into arrays
            if($search_string){
                if($order){
                    $data['salesinvoice'] = $this->salesinvoice_model->get_salesinvoice($Reason, $search_string, $order, $order_type, $config['per_page'],$limit_end);        
                }else{
                    $data['salesinvoice'] = $this->salesinvoice_model->get_salesinvoice($Reason, $search_string, '', $order_type, $config['per_page'],$limit_end);           
                }
            }else{
                if($order){
                    $data['salesinvoice'] = $this->salesinvoice_model->get_salesinvoice($Reason, '', $order, $order_type, $config['per_page'],$limit_end);        
                }else{
                    $data['salesinvoice'] = $this->salesinvoice_model->get_salesinvoice($Reason, '', '', $order_type, $config['per_page'],$limit_end);        
                }
            }

        }else{

            //clean filter data inside section
            $filter_session_data['manufacture_selected'] = null;
            $filter_session_data['search_string_selected'] = null;
            $filter_session_data['order'] = null;
            $filter_session_data['order_type'] = null;
            $this->session->set_userdata($filter_session_data);

            //pre selected options
            $data['search_string_selected'] = '';
            $data['manufacture_selected'] = 0;
            $data['order'] = 'id';

            //fetch sql data into arrays
            /*$data['salesinvoice'] = $this->salesinvoice_model->get_salesinvoice();*/

            $data['manufactures'] = $this->customer_model->get_customer();
            $data['count_salesinvoice']= $this->salesinvoice_model->count_salesinvoice();
            $data['capri_salesinvoice'] = $this->salesinvoice_model->get_salesinvoice('', '', '', $order_type, $config['per_page'],$limit_end);        
            $config['Reason_rows'] = $data['count_salesinvoice'];

        }//!isset($Reason) && !isset($search_string) && !isset($order)

        //initializate the panination helper 
        $this->pagination->initialize($config);   

        //load the view
        $data['main_content'] = 'admin/salesinvoice/list';
        $this->load->view('includes/template', $data);  

    }//index

    public function add()
    {

        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {

            //form validation
             $this->form_validation->set_rules('Invoice_Id', 'Invoice_Id');
            $this->form_validation->set_rules('InvoiceNumber', 'InvoiceNumber', 'required');
            $this->form_validation->set_rules('InvoiceDate', 'InvoiceDate');
             $this->form_validation->set_rules('CustomerName', 'CustomerName','required');
            
            $this->form_validation->set_rules('DateOfDelivery', 'DateOfDelivery');
            $this->form_validation->set_rules('dispatchThru', 'dispatchThru');
            $this->form_validation->set_rules('PlaceOfDelivery', 'PlaceOfDelivery');
            $this->form_validation->set_rules('packingAndForward', 'packingAndForward');
            $this->form_validation->set_rules('serviceCharge', 'serviceCharge');
            $this->form_validation->set_rules('Othercharges', 'Othercharges');
            $this->form_validation->set_rules('Quality', 'Quality');
            $this->form_validation->set_rules('AdvancePayment', 'AdvancePayment');
            $this->form_validation->set_rules('tax', 'tax');
            $this->form_validation->set_rules('TotalAmount', 'TotalAmount');
            $this->form_validation->set_rules('Balance', 'Balance');
            $this->form_validation->set_rules('Discount', 'Discount');
           $this->form_validation->set_rules('PaymentTerm', 'PaymentTerm');
            
           
           


            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            

            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
                $data_to_store = array(
                    'Invoice_Id' => $this->input->post('Invoice_Id'),
                    'InvoiceNumber' => $this->input->post('InvoiceNumber'),
                    'InvoiceDate' => date("Y-m-d :H:i:s",strtotime($this->input->post('InvoiceDate'))),
                   
                    'Po_Number' => $this->input->post('Po_Number'),
                     'PoDate' => date("Y-m-d :H:i:s",strtotime($this->input->post('PoDate'))),
                    
                    'Dc_Number' => $this->input->post('Dc_Number'),
                    'DcDate' => date("Y-m-d :H:i:s",strtotime($this->input->post('DcDate'))),
                    'CustomerName' => $this->input->post('CustomerName'),
                    'BillingAddress' => $this->input->post('BillingAddress'),
                    'CST' => $this->input->post('CST'),
                    'TIN' => $this->input->post('TIN'),

                    'Othercharges' => $this->input->post('Othercharges'),
                    'packingAndForward' => $this->input->post('packingAndForward'),
                    'serviceCharge' => $this->input->post('serviceCharge'),
                    
                    
                    'DateOfDelivery' => $this->input->post('DateOfDelivery'),
                    'dispatchThru' => $this->input->post('dispatchThru'),
                    'PlaceOfDelivery' => $this->input->post('PlaceOfDelivery'),
                    'Quality' => $this->input->post('Quality'),
                    'Tax' => $this->input->post('Tax'),
                    'TaxAmount' => $this->input->post('TaxAmount'),
                    'Tax_type' => $this->input->post('Tax_type'),
                    'Discount' => $this->input->post('Discount'),
                    'PaymentTerm' => $this->input->post('PaymentTerm'),
                    'TotalAmount' => $this->input->post('TotalAmount'),
                    'AdvancePayment' => $this->input->post('AdvancePayment'),
                    'Balance' => $this->input->post('Balance'),
                    'PaidAmount' => "0",
                    'CreatedBy' => $this->session->userdata('user_id'),
                    'CreatedDate' => date("Y-m-d H:i:s")

                    
                    
                );
                //if the insert has returned true then we show the flash message
                if($this->salesinvoice_model->store_manufacture($data_to_store)){
                    $data['flash_message'] = TRUE; 


        $id = $this->salesinvoice_model->maxvalue();

        $pro = $_POST['id'];
        $product = $_POST['product'];
        $unit = $_POST['unit'];
        $qun = $_POST['qun'];
        $tot = $_POST['tot'];
        $price = $_POST['price'];
        $order = $id->row('Invoice_Id');


        for($i=0; $i<sizeof($pro); $i++)
        {
        $data1['ProductName']= $product[$i];
        $data1['ProductUnit']= $unit[$i];
        $data1['Quantity']= $qun[$i];
        $data1['ProductPrice']= $price[$i];
        $data1['Total']= $tot[$i];
        $data1['Invoice_Id']= $order;
        $data1['CreatedBy'] = $this->session->userdata('user_id');
        $data1['CreatedDate'] = date("Y-m-d H:i:s");
            
        $this->value_model->getsalesinvoice($data1);

    }
                }else{
                    $data['flash_message'] = FALSE; 
                }

            }

        }
        //fetch manufactures data to populate the select field
        $data['manufactures'] = $this->customer_model->get_customer();
        $data['salesinvoiceorder'] = $this->product_model->get_product();
        $data['price'] = $this->product_model->get_product();
        $data['product'] = $this->product_model->get_product();

        $customer = $this->salesinvoice_model->getcustomername();
        $product = $this->salesinvoice_model->getproductname();
        /*$getProduct=$this->salesinvoice_model->getproductdetail('Product_Id');*/

        //load the view
        $data['CustomerName'] =$customer;
        $data['ProductName'] =$product;
        $data['main_content'] = 'admin/salesinvoice/add';
        $this->load->view('includes/template', $data);  
 
    }       

    /**
    * Update item by his id
    * @return void
    */

public function get()
{

    $Product_Id=$_GET['Product_Id'];
    $this->salesinvoice_model->getproductdetail('Product_Id');
     $productdetail["rows"] = $this->salesinvoice_model->getproductdetail($Product_Id);
        echo json_encode($productdetail);

}

public function getcustomerdata()
{

    $Customer_Id=$_GET['Customer_Id'];
    $this->salesinvoice_model->getcustomerdetail('Customer_Id');
     $customerdetail["rows"] = $this->salesinvoice_model->getcustomerdetail($Customer_Id);
        echo json_encode($customerdetail);

}
public function salesinvoiceorder_submit()
 {
     $order_id=$POST['Order_Id'];
     $ProformaNumer=$POST['ProformaNumer'];
     $EnqDate=$POST['EnqDate'];
     $PhoneNumber=$POST['PhoneNumber'];
     $date=$POST['Email'];
     $place=$POST['packingAndForward'];
     $qunt=$POST['serviceCharge'];
     $Status=$POST['Status'];
     $pay=$POST['Customer_Id'];
     $Reason=$POST['Reason'];
     $taxtype=$POST['Tax_Type'];
     $tax=$POST['Tax'];
     $gReason=$POST['GrossReason'];

    $this->salesinvoice_model->save($data);

    if($flash_message == true)
    {

        $id = $this->salesinvoice_model->maxvalue();

        $pro = $_POST['id'];
        $qtn = $_POST['unit'];
        $tot = $_POST['tot'];
        $order = $id->row('Order_Id');


        for($i=0; $i<sizeof($pro); $i++)
        {
        $data1['Product_Id']= $pro[$i];
        $data1['Qunantity']= $qtn[$i];
        $data1['Reason']= $tot[$i];
        $data1['Order_Id']= $order;
            
        $this->salesinvoiceOrder_product_model->getProduct($data1);

        }
    }
     
 }



    


    public function update()
    {
        //product id 
        $id = $this->uri->segment(4);
  
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {
            //form validation
             $this->form_validation->set_rules('Invoice_Id', 'Invoice_Id');
            $this->form_validation->set_rules('InvoiceNumber', 'InvoiceNumber', 'required');
            $this->form_validation->set_rules('InvoiceDate', 'InvoiceDate');
             $this->form_validation->set_rules('CustomerName', 'CustomerName','required');
            
            $this->form_validation->set_rules('DateOfDelivery', 'DateOfDelivery');
            $this->form_validation->set_rules('dispatchThru', 'dispatchThru');
            $this->form_validation->set_rules('PlaceOfDelivery', 'PlaceOfDelivery');
            $this->form_validation->set_rules('packingAndForward', 'packingAndForward');
            $this->form_validation->set_rules('serviceCharge', 'serviceCharge');
            $this->form_validation->set_rules('Othercharges', 'Othercharges');
            $this->form_validation->set_rules('Quality', 'Quality');
            $this->form_validation->set_rules('AdvancePayment', 'AdvancePayment');
            $this->form_validation->set_rules('tax', 'tax');
            $this->form_validation->set_rules('TotalAmount', 'TotalAmount');
            $this->form_validation->set_rules('Balance', 'Balance');
            $this->form_validation->set_rules('Discount', 'Discount');
           $this->form_validation->set_rules('PaymentTerm', 'PaymentTerm');
           
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
    
                $data_to_store = array(
                     'Invoice_Id' => $this->input->post('Invoice_Id'),
                    'InvoiceNumber' => $this->input->post('InvoiceNumber'),
                    'InvoiceDate' => $this->input->post('InvoiceDate'),
                     'Po_Number' => $this->input->post('Po_Number'),
                     'PoDate' => date("Y-m-d :H:i:s",strtotime($this->input->post('PoDate'))),
                    
                    'Dc_Number' => $this->input->post('Dc_Number'),
                    'DcDate' => date("Y-m-d :H:i:s",strtotime($this->input->post('DcDate'))),
                    'CustomerName' => $this->input->post('CustomerName'),
                    'BillingAddress' => $this->input->post('BillingAddress'),
                    'CST' => $this->input->post('CST'),
                    'TIN' => $this->input->post('TIN'),
                    'Othercharges' => $this->input->post('Othercharges'),
                    'packingAndForward' => $this->input->post('packingAndForward'),
                    'serviceCharge' => $this->input->post('serviceCharge'),
                    'DateOfDelivery' => $this->input->post('DateOfDelivery'),
                    'dispatchThru' => $this->input->post('dispatchThru'),
                    'PlaceOfDelivery' => $this->input->post('PlaceOfDelivery'),
                    'Quality' => $this->input->post('Quality'),
                    'Tax' => $this->input->post('Tax'),
                    'TaxAmount' => $this->input->post('TaxAmount'),
                    'Tax_type' => $this->input->post('Tax_type'),
                    'Discount' => $this->input->post('Discount'),
                    'PaymentTerm' => $this->input->post('PaymentTerm'),
                    'TotalAmount' => $this->input->post('TotalAmount'),
                    'AdvancePayment' => $this->input->post('AdvancePayment'),
                    'Balance' => $this->input->post('Balance'),
                    'PaidAmount' => "0",
                    'CreatedBy' => $this->session->userdata('user_id'),
                    'CreatedDate' => date("Y-m-d H:i:s")

                );
                //if the insert has returned true then we show the flash message
                if($this->salesinvoice_model->update_manufacture($id, $data_to_store) == TRUE){
                    $this->session->set_flashdata('flash_message', 'updated');
                }else{
                    $this->session->set_flashdata('flash_message', 'not_updated');
                }
                redirect('admin/salesinvoice/update/'.$id.'');

            }//validation run

        }

        //if we are updating, and the data did not pass trough the validation
        //the code below wel reload the current data

        //product data 
        $data['manufacture'] = $this->salesinvoice_model->get_manufacture_by_Invoice_Id($id);
        

        $supplier = $this->salesinvoice_model->getcustomername();
        $product = $this->salesinvoice_model->getproductname();


        $order=$this->salesinvoice_model->getsalesinvoiceDetails($id);
        $orderpro=$this->salesinvoice_model->getPurchaseOrderproductDetails($id);
        


        /*$getProduct=$this->salesinvoice_model->getproductdetail('Product_Id');*/
       
        //load the view
        $data['CustomerName'] =$supplier;
        $data['ProductName'] =$product;
        $data['po']=$order;
        $data['pop']=$orderpro;

        //load the view
        $data['main_content'] = 'admin/salesinvoice/edit';
        $this->load->view('includes/template', $data);            

    }//update

    /**
    * Delete product by his id
    * @return void
    */
    public function view()
    {
        //product id 
        $id = $this->uri->segment(4);
  
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {
           //form validation
            

            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
    
                $data_to_store = array(
                   'Order_Id' => $this->input->post('Order_Id'),
                    'ProformaNumer' => $this->input->post('ProformaNumer'),
                    'Quantity' => $this->input->post('Quantity'),
                    'Reason' => $this->input->post('Reason'),
                    'Email' => $this->input->post('Email'),
                    'packingAndForward' => $this->input->post('packingAndForward'),
                    'serviceCharge' => $this->input->post('serviceCharge'),
                    'Status' => $this->input->post('Status'),
                   
                     'Reason' => $this->input->post('Reason'),
                    'Tax_type' => $this->input->post('Tax_type'),
                    'Tax' => $this->input->post('Tax'),
                    'GrossReason' => $this->input->post('GrossReason'),
                );
                //if the insert has returned true then we show the flash message
                if($this->salesinvoice_model->update_manufacture($id, $data_to_store) == TRUE){
                    $this->session->set_flashdata('flash_message', 'updated');
                }else{
                    $this->session->set_flashdata('flash_message', 'not_updated');
                }
                redirect('admin/salesinvoice/view/'.$id.'');

            }//validation run

        }

        //if we are updating, and the data did not pass trough the validation
        //the code below wel reload the current data

        //product data 
        $data['manufacture'] = $this->salesinvoice_model->get_manufacture_by_Invoice_Id($id);
        
         $order=$this->salesinvoice_model->getsalesinvoiceDetails($id);
        $orderpro=$this->salesinvoice_model->getPurchaseOrderproductDetails($id);

        $data['po']=$order;
        $data['pop']=$orderpro;



        //load the view
        $data['main_content'] = 'admin/salesinvoice/view';
        $this->load->view('includes/template', $data);            

    }//update

    /**
    * Delete product by his id
    * @return void
    */


public function  orderdetailsinsert()
    {
 
        
        $product = $_GET['product'];
        $unit = $_GET['unit'];
        $qun = $_GET['qun'];
        $price = $_GET['price'];
        $tot = $_GET['tot'];
        $order = $_GET['Invoice_Id'];

        
        $data['ProductName']= $product;
        $data['ProductUnit']= $unit;
        $data['Quantity']= $qun;
        $data['ProductPrice']= $price;
        $data['Total']= $tot;
        $data['Invoice_Id']= $order;
        $data['CreatedBy'] = $this->session->userdata('user_id');
        $data['CreatedDate'] = date("Y-m-d H:i:s");
            
        $resdata["row"] = $this->value_model->getsalesinvoice($data);
        echo json_encode($resdata);

    }

    public function  orderdetailsdelete()
    {
 
        $pro = $_GET['id'];        
        
        $this->salesinvoice_model->delete_order_details($pro);
        $deleteProduct = array(
        'message' => true
        );
        $resdata["row"] = $deleteProduct;
        echo json_encode($resdata);

     }
     public function  orderdetailsmaxvalue()
    {
       $id = $this->salesinvoice_model->maxvalue();

       $sam = array(
        'Id' => $id->row('Id')
        );
       $resdata["row"] = $sam;
        echo json_encode($resdata);
    }
    /**
    * Delete product by his id
    * @return void
    */
    public function delete()
    {
        //product id 
        $id = $this->uri->segment(4);
        $this->salesinvoice_model->purchase_order_delete_details($id);
        

         if($this->salesinvoice_model->delete_manufacture($id) == TRUE){
                    $this->session->set_flashdata('flash_message', 'updated');
                }else{
                    $this->session->set_flashdata('flash_message', 'not_updated');
                }
        redirect('admin/salesinvoice');
    }//edit
 public function report()
    {


        $id = $this->uri->segment(3);

       /* redirect('admin/purchase/report/'.$id.'');*/
        $data['manufacture'] = $this->salesinvoice_model->get_manufacture_by_Invoice_Id($id);
        
         $order=$this->salesinvoice_model->getsalesinvoiceDetails($id);
        $orderpro=$this->salesinvoice_model->getPurchaseOrderproductDetails($id);

        $data['po']=$order;
        $data['pop']=$orderpro;
        $html = $this->load->view('admin/salesinvoice/report', $data, TRUE);

        $mpdf=new mPDF();

        $mpdf=new mPDF('c','A4','','',32,25,27,25,16,13);

        $mpdf->WriteHTML($html,2);

        $mpdf->Output('mpdf.pdf','I');
    }
  
}