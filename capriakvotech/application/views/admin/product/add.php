<script src="<?php echo base_url(); ?>js/jquery-1.11.0.min.js"></script>
<script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
  
  <script>
$(document).ready(function() {
    $( "#unit" ).change(function () {
     $("#ProductUn").val($( "#unit option:selected").text());
     console.log($( "#unit option:selected").text());
   });
   }); 
    
   </script> 
    <div class="container top">
       <div class="row">
        <div class="span10 columns">
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url("admin"); ?>">
            <?php echo ucfirst($this->uri->segment(1));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li>
          <a href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>">
            <?php echo ucfirst($this->uri->segment(2));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
          <a href="#">New</a>
        </li>
      </ul>
      
      <div class="page-header">
        <h2>
          Adding <?php echo ucfirst($this->uri->segment(2));?>
        </h2>
      </div>

      <?php
      //flash messages
      if(isset($flash_message)){
        if($flash_message == TRUE)
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Well done!</strong> New Product created with success.';
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
          echo '</div>';          
        }
      }
      ?>
      
      <?php
      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '');

      //form validation
      echo validation_errors();
      
      echo form_open('admin/product/add', $attributes);
      ?>
        <fieldset>
          <!-- <div class="control-group">
            <label for="inputError" class="control-label">Product Id</label>
            <div class="controls">
              <input type="text" id="" name="Product_Id" value="<?php echo set_value('Product_Id'); ?>" >
              
            </div>
          </div> -->
          <div class="control-group">
            <label for="inputError" class="control-label">Product Code</label>
            <div class="controls">
              <input type="text" id="" name="ProductCode" value="<?php echo set_value('ProductCode'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Product Name</label>
            <div class="controls">
              <input type="text" id="" name="ProductName" value="<?php echo set_value('ProductName'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Product Unit</label>
          <select id="unit">
            <option value="">select</option>
                      <?php
                        foreach ($UnitName as $row) {
                          echo "<option value=".$UnitName = $row->UnitId.">".$UnitName = $row->UnitName."</option>"; 
                        }
                      ?>
                      </select>
                      <input type="hidden" id="ProductUn" name="ProductUnit">
                    </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Product Price</label>
            <div class="controls">
              <input type="text" id="" name="ProductPrice" value="<?php echo set_value('ProductPrice'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label"></label>
            <div class="controls">
              <input type="hidden" id="" name="Quantity" value="<?php echo set_value('Quantity'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label"></label>
            <div class="controls">
              <input type="hidden" id="" name="incentive" value="<?php echo set_value('incentive'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="form-actions">
            <button class="btn btn-primary" type="submit">Save changes</button>
            <button class="btn" type="reset" onclick='alert("Product cancel successfully")'>Cancel</button>
          </div>
        </fieldset>

      <?php echo form_close(); ?>

    </div>
     