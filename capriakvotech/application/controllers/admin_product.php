<?php
class Admin_product extends CI_Controller {

    /**
    * name of the folder responsible for the views 
    * which are manipulated by this controller
    * @constant string
    */
    const VIEW_FOLDER = 'admin/product';
 
    /**
    * Responsable for auto load the model
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('product_model');

        if(!$this->session->userdata('is_logged_in')){
            redirect('admin/login');
        }
    }
 
    /**
    * Load the main view with all the current model model's data.
    * @return void
    */
    public function index()
    {

        //all the posts sent by the view
        $search_string = $this->input->post('search_string');        
        $order = $this->input->post('order'); 
        $order_type = $this->input->post('order_type'); 

        //pagination settings
        $config['per_page'] = 5;

        $config['base_url'] = base_url().'admin/product';
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = 20;
        $config['full_tag_open'] = '<ul>';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';

        //limit end
        $page = $this->uri->segment(3);

        //math to get the initial record to be select in the database
        $limit_end = ($page * $config['per_page']) - $config['per_page'];
        if ($limit_end < 0){
            $limit_end = 0;
        } 

        //if order type was changed
        if($order_type){
            $filter_session_data['order_type'] = $order_type;
        }
        else{
            //we have something stored in the session? 
            if($this->session->userdata('order_type')){
                $order_type = $this->session->userdata('order_type');    
            }else{
                //if we have nothing inside session, so it's the default "Asc"
                $order_type = 'Asc';    
            }
        }
        //make the data type var avaible to our view
        $data['order_type_selected'] = $order_type;        


        //we must avoid a page reload with the previous session data
        //if any filter post was sent, then it's the first time we load the content
        //in this case we clean the session filter data
        //if any filter post was sent but we are in some page, we must load the session data

        //filtered && || paginated
        if($search_string !== false && $order !== false || $this->uri->segment(3) == true){ 
           
            /*
            The comments here are the same for line 79 until 99

            if post is not null, we store it in session data array
            if is null, we use the session data already stored
            we save order into the the var to load the view with the param already selected       
            */
            if($search_string){
                $filter_session_data['search_string_selected'] = $search_string;
            }else{
                $search_string = $this->session->userdata('search_string_selected');
            }
            $data['search_string_selected'] = $search_string;

            if($order){
                $filter_session_data['order'] = $order;
            }
            else{
                $order = $this->session->userdata('order');
            }
            $data['order'] = $order;

            //save session data into the session
            if(isset($filter_session_data)){
              $this->session->set_userdata($filter_session_data);    
            }
            
            //fetch sql data into arrays
            $data['count_products']= $this->product_model->count_product($search_string, $order);
            $config['total_rows'] = $data['count_products'];

            //fetch sql data into arrays
            if($search_string){
                if($order){
                    $data['product'] = $this->product_model->get_product($search_string, $order, $order_type, $config['per_page'],$limit_end);        
                }else{
                    $data['product'] = $this->product_model->get_product($search_string, '', $order_type, $config['per_page'],$limit_end);           
                }
            }else{
                if($order){
                    $data['product'] = $this->product_model->get_product('', $order, $order_type, $config['per_page'],$limit_end);        
                }else{
                    $data['product'] = $this->product_model->get_product('', '', $order_type, $config['per_page'],$limit_end);        
                }
            }

        }else{

            //clean filter data inside section
            $filter_session_data['manufacture_selected'] = null;
            $filter_session_data['search_string_selected'] = null;
            $filter_session_data['order'] = null;
            $filter_session_data['order_type'] = null;
            $this->session->set_userdata($filter_session_data);

            //pre selected options
            $data['search_string_selected'] = '';
            $data['order'] = 'id';

            //fetch sql data into arrays
            $data['count_products']= $this->product_model->count_product();
            $data['product'] = $this->product_model->get_product('', '', $order_type, $config['per_page'],$limit_end);        
            $config['total_rows'] = $data['count_products'];

        }//!isset($search_string) && !isset($order)
         
        //initializate the panination helper 
        $this->pagination->initialize($config);   

        //load the view
        $data['main_content'] = 'admin/product/list';
        $this->load->view('includes/template', $data);  

    }//index
 
    public function add()
    {
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {

            //form validation
            
            $this->form_validation->set_rules('ProductCode', 'ProductCode');
            $this->form_validation->set_rules('ProductName', 'ProductName');
            $this->form_validation->set_rules('ProductUnit', 'ProductUnit');
            $this->form_validation->set_rules('ProductPrice', 'ProductPrice');
            $this->form_validation->set_rules('incentive', 'incentive');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            

            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
                $data_to_store = array(
                    
                    'ProductCode' => $this->input->post('ProductCode'),
                    'ProductName' => $this->input->post('ProductName'),
                    'ProductUnit' => $this->input->post('ProductUnit'),
                    'ProductPrice' => $this->input->post('ProductPrice'),
                    'Quantity' => "0",
                    'incentive' => $this->input->post('incentive'),
                    'CreatedBy' => $this->session->userdata('user_id'),
                    'CreatedDate' => date("Y-m-d H:i:s")
                );
                //if the insert has returned true then we show the flash message
                if($this->product_model->store_manufacture($data_to_store)){
                    $data['flash_message'] = TRUE; 
                }else{
                    $data['flash_message'] = FALSE; 
                }

            }

        }

        $productunit = $this->product_model->getunitname();

        $data['UnitName'] =$productunit;
        //load the view
        $data['main_content'] = 'admin/product/add';
        $this->load->view('includes/template', $data);  
    }       

    /**
    * Update item by his id
    * @return void
    */
    public function update()
    {
        //product id 
        $id = $this->uri->segment(4);
  
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {
            //form validation
           $this->form_validation->set_rules('Product_Id', 'Product_Id');
            $this->form_validation->set_rules('ProductCode', 'ProductCode');
            $this->form_validation->set_rules('ProductName', 'ProductName');
            $this->form_validation->set_rules('ProductUnit', 'ProductUnit');
            $this->form_validation->set_rules('ProductPrice', 'ProductPrice');
            
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
    
                $data_to_store = array(
                    
                    'ProductCode' => $this->input->post('ProductCode'),
                    'ProductName' => $this->input->post('ProductName'),
                    'ProductUnit' => $this->input->post('ProductUnit'),
                    'ProductPrice' => $this->input->post('ProductPrice'),
                    'incentive' => $this->input->post('incentive'),
                );
                //if the insert has returned true then we show the flash message
                if($this->product_model->update_manufacture($id, $data_to_store) == TRUE){
                    $this->session->set_flashdata('flash_message', 'updated');
                }else{
                    $this->session->set_flashdata('flash_message', 'not_updated');
                }

                
                
                redirect('admin/product/update/'.$id.'');

            }//validation run

        }

        //if we are updating, and the data did not pass trough the validation
        //the code below wel reload the current data

        //product data 
        $data['manufacture'] = $this->product_model->get_manufacture_by_Product_Id($id);


        $unit = $this->product_model->getunitname();

                $data['Unit'] = $unit;

        //load the view
        $data['main_content'] = 'admin/product/edit';
        $this->load->view('includes/template', $data);            

    }//update

    /**
    * Delete product by his id
    * @return void
    */
     public function view()
    {
        //product id 
        $id = $this->uri->segment(4);
  
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {
            //form validation
           $this->form_validation->set_rules('Product_Id', 'Product_Id', 'required');
            $this->form_validation->set_rules('ProductCode', 'ProductCode', 'required');
            $this->form_validation->set_rules('ProductName', 'ProductName', 'required');
            $this->form_validation->set_rules('ProductUnit', 'ProductUnit', 'required|numeric');
            $this->form_validation->set_rules('ProductPrice', 'ProductPrice', 'required|numeric');
            $this->form_validation->set_rules('incentive', 'incentive', 'required|numeric');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
    
                $data_to_store = array(
                    'Product_Id' => $this->input->post('Product_Id'),
                    'ProductCode' => $this->input->post('ProductCode'),
                    'ProductName' => $this->input->post('ProductName'),
                    'ProductUnit' => $this->input->post('ProductUnit'),
                    'ProductPrice' => $this->input->post('ProductPrice'),
                    'incentive' => $this->input->post('incentive'),
                );
                //if the insert has returned true then we show the flash message
                if($this->product_model->update_manufacture($id, $data_to_store) == TRUE){
                    $this->session->set_flashdata('flash_message', 'updated');
                }else{
                    $this->session->set_flashdata('flash_message', 'not_updated');
                }
                redirect('admin/product/view/'.$id.'');

            }//validation run

        }

        //if we are updating, and the data did not pass trough the validation
        //the code below wel reload the current data

        //product data 
        $data['manufacture'] = $this->product_model->get_manufacture_by_Product_Id($id);
        //load the view
        $data['main_content'] = 'admin/product/view';
        $this->load->view('includes/template', $data);            

    }//update

    /**
    * Delete product by his id
    * @return void
    */
    public function delete()
    {
        //product id 
        $id = $this->uri->segment(4);
        $this->product_model->delete_manufacture($id);
        redirect('admin/product');
    }//edit

}