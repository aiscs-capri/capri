<?php
class salesreport_model extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return voInvoice_Id
    */
    public function __construct()
    {
        $this->load->database();
    }

    /**
    * Get product by his is
    * @param int $product_Invoice_Id 
    * @return array
    */
    public function get_manufacture_by_Invoice_Id($Invoice_Id)
    {
		$this->db->select('*');
		$this->db->from('capri_sale_invoice');
		$this->db->where('Invoice_Id', $Invoice_Id);
		$query = $this->db->get();
		return $query->result_array(); 
    }    


public function save($Invoice_Id)
    {
        $insert = $this->db->insert('capri_sale_invoice', $data);
        return $insert;
    }   

    public function maxvalue()
    {
        $this->db->select_MAX('Invoice_Id');
        $this->db->from('capri_sale_invoice');
        $query = $this->db->get();
        return $query;

    }


   public function getsalesreportDetails($lead_order)
    {
       $this->db->select('capri_sale_invoice.InvoiceNumber, capri_master_customer.CustomerName,capri_master_customer.BillingAddress,capri_master_customer.TIN,capri_master_customer.CST');
        $this->db->from('capri_sale_invoice');
        $this->db->join('capri_master_customer','capri_sale_invoice.CustomerName = capri_master_customer.CustomerName','inner');
        $this->db->where('capri_sale_invoice.Invoice_Id',$lead_order);
        $query = $this->db->get();
        return $query->result();
    }
    public function getPurchaseOrderproductDetails($purchase_order_product)
    {
        $this->db->select('Id,ProductName,ProductUnit,ProductPrice,Quantity,Total');
        $this->db->from('capri_sale_invoice_details ');       
        $this->db->where('capri_sale_invoice_details.Invoice_Id',$purchase_order_product);
        $query = $this->db->get();
        return $query->result();
    }

    /**
    * Fetch salesreport_master data from the database
    * possibility to mix search, filter and order
    * @param string $search_string 
    * @param strong $order
    * @param string $order_type 
    * @param int $limit_start
    * @param int $limit_end
    * @return array
    */
    public function get_salesreport($Customer_Id=null, $search_string=null, $order=null, $order_type='Asc', $limit_start, $limit_end)
    {
        
        $this->db->select('capri_sale_invoice.Invoice_Id');
        $this->db->select('capri_sale_invoice.InvoiceNumber');
        $this->db->select('capri_sale_invoice.InvoiceDate');
        $this->db->select('capri_sale_invoice.Po_Number');
        $this->db->select('capri_sale_invoice.PoDate');
        $this->db->select('capri_sale_invoice.Dc_Number');
        $this->db->select('capri_sale_invoice.DcDate');

        $this->db->select('capri_sale_invoice.CustomerName');
        $this->db->select('capri_sale_invoice.TIN');
        $this->db->select('capri_sale_invoice.BillingAddress');
        $this->db->select('capri_sale_invoice.CST');
        $this->db->select('capri_sale_invoice.DateOfDelivery');
        $this->db->select('capri_sale_invoice.dispatchThru');
        $this->db->select('capri_sale_invoice.PlaceOfDelivery');
        $this->db->select('capri_sale_invoice.Quality');
        $this->db->select('capri_sale_invoice.packingAndForward');
        $this->db->select('capri_sale_invoice.serviceCharge');
        $this->db->select('capri_sale_invoice.Othercharges');
        $this->db->select('capri_sale_invoice.TotalAmount');
        $this->db->select('capri_sale_invoice.AdvancePayment');
        $this->db->select('capri_sale_invoice.Balance');
        $this->db->select('capri_sale_invoice.PaidAmount');
        $this->db->select('capri_sale_invoice.Tax_type');
        $this->db->select('capri_sale_invoice.Tax');
        $this->db->select('capri_sale_invoice.TaxAmount');
        $this->db->select('capri_sale_invoice.Discount');
        $this->db->select('capri_sale_invoice.PaymentTerm');
           
        $this->db->from('capri_sale_invoice');
        
        

        $this->db->group_by('capri_sale_invoice.Invoice_Id');

        


        $this->db->limit($limit_start, $limit_end);
        //$this->db->limit('4', '4');


        $query = $this->db->get();
        
        return $query->result_array();  
    }
    /**
    * Count the number of rows
    * @param int $search_string
    * @param int $order
    * @return int
    */

    public function getcustomername()
{

    $this->db->select('Customer_Id,CustomerName');
    $this -> db -> from('capri_master_customer');  
    $query = $this -> db -> get();
    return $query->result();
}
    public function getproductname()
{

    $this->db->select('Product_Id,ProductName');
    $this -> db -> from('capri_master_product');

    $query = $this -> db -> get();
    return $query->result();
}
public function getproductdetail($Product_Id)
{

    $this->db->select('Product_Id,ProductPrice,ProductUnit');
    $this->db->where('Product_Id', $Product_Id);
    $this->db->from('capri_master_product');
    $query=$this->db->get();
    return $query->result();
}

/*$this->db->select('concat(BillStreet,',',BillVillage,',',BillCity,',',billstate,',',billpincode) as address,CST,TIN');*/
public function getcustomerdetail($Customer_Id)
{

    $this->db->select('BillingAddress');
    $this->db->select('CST,TIN');
    $this->db->from('capri_master_customer');
    $this->db->where('Customer_Id', $Customer_Id);
    $query=$this->db->get();
    return $query->result();
}

     public function get_order($Product_Id=null, $search_string=null, $order=null, $order_type='Asc', $limit_start, $limit_end)
    {
        
        $this->db->select('capri_sale_invoice_details.Id');
        $this->db->select('capri_sale_invoice_details.Product_Id');
        $this->db->select('capri_sale_invoice_details.Quantity');
        $this->db->select('capri_sale_invoice_details.Total');
         $this->db->select('capri_sale_invoice_details.Invoice_Id');
        $this->db->select('capri_sale_invoice_details.Product_Id');
        $this->db->select('capri_master_product.Product_Id as Product_Id');
        $this->db->from('capri_sale_invoice_details');
        if($Product_Id != null && $Product_Id != 0){
            $this->db->where('Product_Id', $Product_Id);
        }
        if($search_string){
            $this->db->like('Product_Id', $search_string);
        }

        $this->db->join('capri_master_product', 'capri_sale_invoice_details.Product_Id = capri_master_product.Product_Id', 'left');
       
        
        $this->db->group_by('capri_sale_invoice_details.Id');

        if($order){
            $this->db->order_by($order, $order_type);
        }else{
            $this->db->order_by('Id', $order_type);
        }


        $this->db->limit($limit_start, $limit_end);
        //$this->db->limit('4', '4');


        $query = $this->db->get();
        
        return $query->result_array();  
    }
    /**
    * Count the number of rows
    * @param int $search_string
    * @param int $order
    * @return int
    */
   function count_salesreport($Customer_Id=null, $search_string=null, $order=null)
    {
        $this->db->select('*');
        $this->db->from('capri_sale_invoice');
        if($Customer_Id != null && $Customer_Id != 0){
            $this->db->where('Customer_Id', $Customer_Id);
        }
        
        if($order){
            $this->db->order_by($order, 'Asc');
        }else{
            $this->db->order_by('Invoice_Id', 'Asc');
        }
        $query = $this->db->get();
        return $query->num_rows();        
    }


    /**
    * Store the new item into the database
    * @param array $data - associative array with data to store
    * @return boolean 
    */
    function store_manufacture($data)
    {
		$insert = $this->db->insert('capri_sale_invoice', $data);
	    return $insert;
	}

    /**
    * Update manufacture
    * @param array $data - associative array with data to store
    * @return boolean
    */
    function update_manufacture($Invoice_Id, $data)
    {
		$this->db->where('Invoice_Id', $Invoice_Id);
		$this->db->update('capri_sale_invoice', $data);
		$report = array();
		$report['error'] = $this->db->_error_number();
		$report['message'] = $this->db->_error_message();
		if($report !== 0){
			return true;
		}else{
			return false;
		}
	}

    /**
    * Delete manufacturer
    * @param int $Invoice_Id - manufacture Invoice_Id
    * @return boolean
    */


function delete_order_details($Id){

    $this->db->where('Id',$Id);
    $this->db->delete('capri_sale_invoice_details');
}


function insert_order_details($data)
    {
        $insert = $this->db->insert('capri_sale_invoice_details', $data);
        return $insert;
    }

    function purchase_order_delete_details($Id){

    $this->db->where('Invoice_Id',$Id);
    $this->db->delete('capri_sale_invoice_details');
 }


public function order_details_maxvalue()
    {
        $this->db->select_MAX('Id');
        $this->db->from('capri_sale_invoice_details');
        $query = $this->db->get();
        return $query;

    }








	function delete_manufacture($Invoice_Id){
		$this->db->where('Invoice_Id', $Invoice_Id);
		$this->db->delete('capri_sale_invoice'); 
	}
 
}
	
