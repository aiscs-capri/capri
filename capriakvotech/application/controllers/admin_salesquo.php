 <?php
class Admin_salesquo extends CI_Controller {

    /**
    * name of the folder responsible for the views 
    * which are manipulated by this controller
    * @constant string
    */
    const VIEW_FOLDER = 'admin/salesquotation';
 
    /**
    * Responsable for auto load the model
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('salesquo_model');
         $this->load->model('customer_model');
         $this->load->model('product_model');
         $this->load->model('order_model');
         $this->load->model('value_model');
         $this->load->library('mpdf');

        if(!$this->session->userdata('is_logged_in')){
            redirect('admin/login');
        }
    }
 
    /**
    * Load the main view with all the current model model's data.
    * @return void
    */
     public function index()
    {
        /*$id = $this->salesquo_model->maxvalue();
        echo "";
        echo "jdhfjkfjhasjhf jahsfjhasf ahsjfhasf ahsfjaf =========>".$id->row('Order_Id');
        */
        //all the posts sent by the view
        $Total = $this->input->post('Total');        
        $search_string = $this->input->post('search_string');        
        $order = $this->input->post('order'); 
        $order_type = $this->input->post('order_type'); 

        //pagination settings
        $config['per_page'] = 20;
        $config['base_url'] = base_url().'admin/salesquotation';
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = 20;
        $config['full_tag_open'] = '<ul>';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';

        //limit end
        $page = $this->uri->segment(3);

        //math to get the initial record to be select in the database
        $limit_end = ($page * $config['per_page']) - $config['per_page'];
        if ($limit_end < 0){
            $limit_end = 0;
        } 

        //if order type was changed
        if($order_type){
            $filter_session_data['order_type'] = $order_type;
        }
        else{
            //we have something stored in the session? 
            if($this->session->userdata('order_type')){
                $order_type = $this->session->userdata('order_type');    
            }else{
                //if we have nothing inside session, so it's the default "Asc"
                $order_type = 'Asc';    
            }
        }
        //make the data type var avaible to our view
        $data['order_type_selected'] = $order_type;        


        //we must avoid a page reload with the previous session data
        //if any filter post was sent, then it's the first time we load the content
        //in this case we clean the session filter data
        //if any filter post was sent but we are in some page, we must load the session data

        //filtered && || paginated
        if($Total !== false && $search_string !== false && $order !== false || $this->uri->segment(3) == true){ 
           
            /*
            The comments here are the same for line 79 until 99

            if post is not null, we store it in session data array
            if is null, we use the session data already stored
            we save order into the the var to load the view with the param already selected       
            */

            if($Total !== 0){
                $filter_session_data['manufacture_selected'] = $Total;
            }else{
                $Total = $this->session->userdata('manufacture_selected');
            }
            $data['manufacture_selected'] = $Total;

            if($search_string){
                $filter_session_data['search_string_selected'] = $search_string;
            }else{
                $search_string = $this->session->userdata('search_string_selected');
            }
            $data['search_string_selected'] = $search_string;

            if($order){
                $filter_session_data['order'] = $order;
            }
            else{
                $order = $this->session->userdata('order');
            }
            $data['order'] = $order;

            //save session data into the session
            $this->session->set_userdata($filter_session_data);

            //fetch manufacturers data into arrays
            /*$data['manufactures'] = $this->salesquo_model->get_quotation($id);*/

            /*$data['purchase'] = $this->salesquo_model->get_quotation();*/

            $data['count_purchase']= $this->salesquo_model->count_quotation($Total, $search_string, $order);
            $config['total_rows'] = $data['count_purchase'];

            //fetch sql data into arrays
            if($search_string){
                if($order){
                    $data['purchase'] = $this->salesquo_model->get_quotation($Total, $search_string, $order, $order_type, $config['per_page'],$limit_end);        
                }else{
                    $data['purchase'] = $this->salesquo_model->get_quotation($Total, $search_string, '', $order_type, $config['per_page'],$limit_end);           
                }
            }else{
                if($order){
                    $data['purchase'] = $this->salesquo_model->get_quotation($Total, '', $order, $order_type, $config['per_page'],$limit_end);        
                }else{
                    $data['purchase'] = $this->salesquo_model->get_quotation($Total, '', '', $order_type, $config['per_page'],$limit_end);        
                }
            }

        }else{

            //clean filter data inside section
            $filter_session_data['manufacture_selected'] = null;
            $filter_session_data['search_string_selected'] = null;
            $filter_session_data['order'] = null;
            $filter_session_data['order_type'] = null;
            $this->session->set_userdata($filter_session_data);

            //pre selected options
            $data['search_string_selected'] = '';
            $data['manufacture_selected'] = 0;
            $data['order'] = 'id';

            //fetch sql data into arrays
            /*$data['purchase'] = $this->product_model->get_product();*/

            /*$data['manufactures'] = $this->salesquo_model->get_quotation();*/
            $data['count_purchase']= $this->salesquo_model->count_quotation();
            $data['capri_purchase_order'] = $this->salesquo_model->get_quotation('', '', '', $order_type, $config['per_page'],$limit_end);        
            $config['total_rows'] = $data['count_purchase'];

        }//!isset($Total) && !isset($search_string) && !isset($order)

        //initializate the panination helper 
        $this->pagination->initialize($config);   

        //load the view
        $data['main_content'] = 'admin/salesquotation/list';
        $this->load->view('includes/template', $data);  

    }//index

    public function add()
    {

        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {

            //form validation
             $this->form_validation->set_rules('Qtn_Id', 'Qtn_Id');
            $this->form_validation->set_rules('QtnNumber', 'QtnNumber', 'required');
            $this->form_validation->set_rules('QtnDate', 'QtnDate', 'required');
            $this->form_validation->set_rules('CustomerName', 'CustomerName', 'required');
            $this->form_validation->set_rules('validityQtn', 'validityQtn');

            $this->form_validation->set_rules('DateOfDelivery', 'DateOfDelivery');
            $this->form_validation->set_rules('dispatchThru', 'dispatchThru');

            $this->form_validation->set_rules('PlaceOfDelivery', 'PlaceOfDelivery');
            $this->form_validation->set_rules('Quality', 'Quality');
            $this->form_validation->set_rules('PaymentTerm', 'PaymentTerm');
            $this->form_validation->set_rules('Tax', 'Tax');
            $this->form_validation->set_rules('Discount', 'Discount');
           
           


            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            

            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
                $data_to_store = array(
                    'Qtn_Id' => $this->input->post('Qtn_Id'),
                    'QtnNumber' => $this->input->post('QtnNumber'),
                     'QtnDate' => date("Y-m-d :H:i:s",strtotime($this->input->post('QtnDate'))),
                     'KindAttn' => $this->input->post('KindAttn'),
                      'OurRef' => $this->input->post('OurRef'),
                    
                    'CustomerName' => $this->input->post('CustomerName'),
                    'BillingAddress' => $this->input->post('BillingAddress'),
                    'TIN' => $this->input->post('TIN'),
                    'CST' => $this->input->post('CST'),
                    'validityQtn' => $this->input->post('validityQtn'),
                    'DateOfDelivery' => $this->input->post('DateOfDelivery'),
                    'dispatchThru' => $this->input->post('dispatchThru'),
                    'PlaceOfDelivery' => $this->input->post('PlaceOfDelivery'),
                    'Quality' => $this->input->post('Quality'),
                    'Tax' => $this->input->post('Tax'),
                    'Tax_type' => $this->input->post('Tax_type'),
                    'Total' => $this->input->post('Total'),
                    'TaxAmount' => $this->input->post('TaxAmount'),
                    'Discount' => $this->input->post('Discount'),
                    'TotalAmount' => $this->input->post('TotalAmount'),
                    'PaymentTerm' => $this->input->post('PaymentTerm'),
                     'CreatedBy' => $this->session->userdata('user_id'),
                    'CreatedDate' => date("Y-m-d H:i:s")
                    
                );
                //if the insert has returned true then we show the flash message
                if($this->salesquo_model->store_manufacture($data_to_store)){
                    $data['flash_message'] = TRUE; 


        $id = $this->salesquo_model->maxvalue();

        $pro = $_POST['id'];
        $product = $_POST['product'];
        $unit = $_POST['unit'];
        $qun = $_POST['qun'];
        $tot = $_POST['tot'];
        $price = $_POST['price'];
        $order = $id->row('Qtn_Id');


        for($i=0; $i<sizeof($pro); $i++)
        {
        $data1['ProductName']= $product[$i];
        $data1['ProductUnit']= $unit[$i];
        $data1['Quantity']= $qun[$i];
        $data1['ProductPrice']= $price[$i];
        $data1['Total']= $tot[$i];
        $data1['Qtn_Id']= $order;
        $data1['CreatedBy'] = $this->session->userdata('user_id');
        $data1['CreatedDate'] = date("Y-m-d H:i:s");
            
        $this->value_model->getsalesquo($data1);

    }
                }else{
                    $data['flash_message'] = FALSE; 
                }

            }

        }
        //fetch manufactures data to populate the select field
        $data['manufactures'] = $this->customer_model->get_customer();
        $data['purchaseorder'] = $this->product_model->get_product();
        $data['price'] = $this->product_model->get_product();
        $data['product'] = $this->product_model->get_product();

        $customer = $this->salesquo_model->getcustomername();
        $product = $this->salesquo_model->getproductname();
        

        //load the view
        $data['CustomerName'] =$customer;
        $data['ProductName'] =$product;
        $data['main_content'] = 'admin/salesquotation/add';
        $this->load->view('includes/template', $data);  
 
    }       

    /**
    * Update item by his id
    * @return void
    */

public function get()
{

    $Product_Id=$_GET['Product_Id'];
    $this->salesquo_model->getproductdetail('Product_Id');
     $productdetail["rows"] = $this->salesquo_model->getproductdetail($Product_Id);
        echo json_encode($productdetail);

}

public function getcustomerdata()
{

    $Customer_Id=$_GET['Customer_Id'];
    $this->salesquo_model->getcustomerdetail('Customer_Id');
     $customerdetail["rows"] = $this->salesquo_model->getcustomerdetail($Customer_Id);
        echo json_encode($customerdetail);

}
public function purchaseorder_submit()
 {
     $order_id=$POST['Order_Id'];
     $order_number=$POST['Order_number'];
     $orderdate=$POST['OrderDate'];
     $customer_id=$POST['customer_Id'];
     $date=$POST['DateOfDelivery'];
     $place=$POST['PlaceOfDelivery'];
     $qunt=$POST['Quality'];
     $note=$POST['Note'];
     $pay=$POST['PaymentTerm'];
     $total=$POST['Total'];
     $taxtype=$POST['Tax_Type'];
     $tax=$POST['Tax'];
     $gtotal=$POST['GrossTotal'];

    $this->salesquo_model->save($data);

    if($flash_message == true)
    {

        $id = $this->salesquo_model->maxvalue();

        $pro = $_POST['id'];
        $qtn = $_POST['unit'];
        $tot = $_POST['tot'];
        $order = $id->row('Order_Id');


        for($i=0; $i<sizeof($pro); $i++)
        {
        $data1['Product_Id']= $pro[$i];
        $data1['Qunantity']= $qtn[$i];
        $data1['Total']= $tot[$i];
        $data1['Order_Id']= $order;
            
        $this->purchaseOrder_product_model->getProduct($data1);

        }
    }
     
 }



    


    public function update()
    {
        //product id 
        $id = $this->uri->segment(4);
  
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {
            //form validation
         $this->form_validation->set_rules('Qtn_Id', 'Qtn_Id');
            $this->form_validation->set_rules('QtnNumber', 'QtnNumber');
            $this->form_validation->set_rules('QtnDate', 'QtnDate', 'required');
            $this->form_validation->set_rules('CustomerName', 'CustomerName');
            $this->form_validation->set_rules('validityQtn', 'validityQtn');

            $this->form_validation->set_rules('DateOfDelivery', 'DateOfDelivery');
            $this->form_validation->set_rules('dispatchThru', 'dispatchThru');

            $this->form_validation->set_rules('PlaceOfDelivery', 'PlaceOfDelivery');
            $this->form_validation->set_rules('Quality', 'Quality');
            $this->form_validation->set_rules('PaymentTerm', 'PaymentTerm');
            $this->form_validation->set_rules('Tax', 'Tax');
            $this->form_validation->set_rules('Discount', 'Discount');
           
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
    
                $data_to_store = array(
                    'Qtn_Id' => $this->input->post('Qtn_Id'),
                    'QtnNumber' => $this->input->post('QtnNumber'),
                     'QtnDate' => date("Y-m-d :H:i:s",strtotime($this->input->post('QtnDate'))),
                      'KindAttn' => $this->input->post('KindAttn'),
                      'OurRef' => $this->input->post('OurRef'),
                    
                    'CustomerName' => $this->input->post('CustomerName'),
                    'BillingAddress' => $this->input->post('BillingAddress'),
                    'TIN' => $this->input->post('TIN'),
                    'CST' => $this->input->post('CST'),
                    'validityQtn' => $this->input->post('validityQtn'),
                    'DateOfDelivery' => $this->input->post('DateOfDelivery'),
                    'dispatchThru' => $this->input->post('dispatchThru'),
                    'PlaceOfDelivery' => $this->input->post('PlaceOfDelivery'),
                    'Quality' => $this->input->post('Quality'),
                    'Tax' => $this->input->post('Tax'),
                    'Tax_type' => $this->input->post('Tax_type'),
                    'TaxAmount' => $this->input->post('TaxAmount'),
                    'Total' => $this->input->post('Total'),
                    'Discount' => $this->input->post('Discount'),
                    'TotalAmount' => $this->input->post('TotalAmount'),
                    'PaymentTerm' => $this->input->post('PaymentTerm'),
                     'CreatedBy' => $this->session->userdata('user_id'),
                    'CreatedDate' => date("Y-m-d H:i:s")
                );
                //if the insert has returned true then we show the flash message
                if($this->salesquo_model->update_manufacture($id, $data_to_store) == TRUE){
                    $this->session->set_flashdata('flash_message', 'updated');
                }else{
                    $this->session->set_flashdata('flash_message', 'not_updated');
                }
                redirect('admin/salesquotation/update/'.$id.'');
              
            }//validation run

        }


        //if we are updating, and the data did not pass trough the validation
        //the code below wel reload the current data

        //product data 
        $data['manufacture'] = $this->salesquo_model->get_manufacture_by_Qtn_Id($id);
        $order=$this->salesquo_model->getquotationOrderDetails($id);
        $orderpro=$this->salesquo_model->getquotationproductDetails($id);
        
        $customer = $this->salesquo_model->getcustomername();
        $product = $this->salesquo_model->getproductname();


        $data['po']=$order;
        $data['pop']=$orderpro;
   

        //load the view
        $data['CustomerName'] =$customer;
        $data['ProductName'] =$product;
        //load the view
        $data['main_content'] = 'admin/salesquotation/edit';
        $this->load->view('includes/template', $data);            

    }//update

    /**
    * Delete product by his id
    * @return void
    */
    public function view()
    {
        //product id 
        $id = $this->uri->segment(4);
  
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {
           //form validation
            

            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
    
                $data_to_store = array(
                   
                    'Total' => $this->input->post('Total'),
                    'DateOfDelivery' => $this->input->post('DateOfDelivery'),
                    'PlaceOfDelivery' => $this->input->post('PlaceOfDelivery'),
                    'Quality' => $this->input->post('Quality'),
                    'Note' => $this->input->post('Note'),
                    'PaymentTerm' => $this->input->post('PaymentTerm'),
                     'Total' => $this->input->post('Total'),
                    'Tax_type' => $this->input->post('Tax_type'),
                    'Tax' => $this->input->post('Tax'),
                    'GrossTotal' => $this->input->post('GrossTotal'),
                );
                //if the insert has returned true then we show the flash message
                if($this->salesquo_model->update_manufacture($id, $data_to_store) == TRUE){
                    $this->session->set_flashdata('flash_message', 'updated');
                }else{
                    $this->session->set_flashdata('flash_message', 'not_updated');
                }
                redirect('admin/purchase/view/'.$id.'');

            }//validation run

        }

        //if we are updating, and the data did not pass trough the validation
        //the code below wel reload the current data

        //product data 
        $data['manufacture'] = $this->salesquo_model->get_manufacture_by_Qtn_Id($id);
        $order=$this->salesquo_model->getquotationOrderDetails($id);
        $orderpro=$this->salesquo_model->getquotationproductDetails($id);
        
        $data['po']=$order;
        $data['pop']=$orderpro;

        //load the view
        $data['main_content'] = 'admin/salesquotation/view';
        $this->load->view('includes/template', $data);            

    }//update

    /**
    * Delete product by his id
    * @return void
    */
    public function delete()
    {
        //product id 
        $id = $this->uri->segment(4);
        $this->salesquo_model->delete_manufacture($id);
        redirect('admin/salesquotation');
    }//edit
public function report()
    {


        $id = $this->uri->segment(3);

       /* redirect('admin/purchase/report/'.$id.'');*/

       $data['manufacture'] = $this->salesquo_model->get_manufacture_by_Qtn_Id($id);
        $order=$this->salesquo_model->getquotationOrderDetails($id);
        $orderpro=$this->salesquo_model->getquotationproductDetails($id);
        
        $customer = $this->salesquo_model->getcustomername();
        $product = $this->salesquo_model->getproductname();


        $data['po']=$order;
        $data['pop']=$orderpro;
   
        $data['ProductName'] =$product;
        $html = $this->load->view('admin/salesquotation/report', $data, TRUE);
         /*$data['main_content'] = 'admin/enquery/report';
         $html = $this->load->view('includes/template', $data,TRUE);  */



$mpdf=new mPDF();

$mpdf=new mPDF('c','A4','','',32,25,27,25,16,13);

$mpdf->WriteHTML($html,2);

$mpdf->Output('mpdf.pdf','I');
    }

}