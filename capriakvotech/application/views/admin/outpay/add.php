<script src="<?php echo base_url(); ?>js/jquery-1.11.1.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery-ui.css"> 
<link rel="stylesheet" href="<?php echo base_url(); ?>css/style.css"> 
<script src="<?php echo base_url(); ?>js/jquery-1.11.0.min.js"></script>
<script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
<script src="<?php echo base_url(); ?>js/config.js"></script>
<script>
$(document).ready(function() {

     //max
    $( "#datepickermax1").datepicker({ dateFormat: "yy-M-dd"
         }).datepicker("setDate","0");
    

//get customer data
  $( "#customer" ).change(function () {
    var str = "";
    var str1 = "";
    $( "#customer option:selected" ).each(function() {
      str += $( this ).val() + " ";
      str1 += $( this ).text()+ "";
    });

    $.ajax({
            type : "get",
            url: serverurl+"/admin_outpay/get",
            dataType: "json",
            data: {
                SupplierName : str
               },
            success: function(data) { 
              var i = 0;
              var len = data.rows.length;
              for(i;i<len;i++) 
              {
               $("#bil").val(data.rows[i].BillingAddress),
               $("#cst").val(data.rows[i].CST),
               $("#tin").val(data.rows[i].TIN);               
               $("#myTable").append("<tr><td class=invoId style=display:none;>"+data.rows[i].inward_Id+"</td><td class=updateInv>"+data.rows[i].inward_number+"</td><td class=updateCus>"+str1+"</td><td class=totamt>"+data.rows[i].GrossTotal+"</td><td>"+data.rows[i].AdvancePayment+"</td><td  class=totpai>"+data.rows[i].Paid+"</td><td class=amt >"+data.rows[i].Balance+"</td><td><input type=checkbox onClick=display(this)></input></td></tr>");
              } 
               
            } ,
            error: function(data, errorThrown)
          {
              alert('request failed :'+errorThrown);
          }
      });    
  }).change();

});

function display(obj)
{
  var total = parseFloat($("#tamt").val());  
  if($(obj).prop('checked'))
  {
    var Balance = parseFloat($(obj).parent().siblings(".amt").text());
    total = total + Balance;
    var id = $(obj).parent().siblings(".invoId").text();
    var invoiceNo = $(obj).parent().siblings(".updateInv").text();
    var customerNa = $(obj).parent().siblings(".updateCus").text();
    var totamt = $(obj).parent().siblings(".totamt").text();
    var totpai = $(obj).parent().siblings(".totpai").text();
    $(obj).parent().parent().append("<td class=updateid style=display:none;><input type=hidden name=updateId[] value="+id+"><input type=hidden name=updateInv[] value="+invoiceNo+"><input type=hidden name=updateCus[] value="+customerNa+"><input type=hidden name=updateBalance[] value="+Balance+"><input type=hidden name=updateTotal[] value="+totamt+"><input type=hidden name=updatePaid[] value="+totpai+"></td>");
  } 
  else
  {
    total = total - parseFloat($(obj).parent().siblings(".amt").text());
    $(obj).parent().siblings(".updateid").remove();
  }  
  $("#tamt").val(total);
}
</script>

<div class="container top">
  <div class="row">
    <div class="span10 columns">
    <ul class="breadcrumb">
      <li>
      <a href="<?php echo site_url("admin"); ?>">
      <?php echo ucfirst($this->uri->segment(1));?>
      </a> 
      <span class="divider">/</span>
      </li>
      <li>
      <a href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>">
      <?php echo ucfirst($this->uri->segment(2));?>
      </a> 
      <span class="divider">/</span>
      </li>
      <li class="active">
      <a href="#">New</a>
      </li>
    </ul>
    </div>
  </div>
</div>
<div class="page-header">
  <h2>Adding <?php echo ucfirst($this->uri->segment(2));?></h2>
</div>
<?php
  //flash messages
  if(isset($flash_message)){
    if($flash_message == TRUE)
    {
       redirect(current_url());
      echo '<div class="alert alert-success">';
        echo '<a class="close" data-dismiss="alert">×</a>';
        echo '<strong>Well done!</strong> New purchase created with success.';            
      echo '</div>';         
         
    }else{
      redirect(current_url());
      echo '<div class="alert alert-error">';
        echo '<a class="close" data-dismiss="alert">×</a>';
        echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
      echo '</div>';            
              
    }
  }
?>

<?php
      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '');
          //form validation
      echo validation_errors();
      
      echo form_open('admin/outpay/add', $attributes);
?>
<div class="container">
  <div class="row">
    <div class="col-md-5">
      <div class="control-group">      
        <div class="controls">
          <input type="hidden" id="box1" name="Bill_Id" value="<?php echo set_value('Order_Id'); ?>" >
        </div>
      </div>
      <div class="control-group">
        <label for="inputError" class="control-label">Bill Number</label>
        <div class="controls">
          <input type="text" id="box2" name="BillNumber" value="<?php echo set_value('Order_number'); ?>"  >
        </div>
      </div>
      <div class="control-group">
        <label for="inputError" class="control-label">Bill Date</label>
        <div class="controls">
          <input type="text" id="datepickermax1" name="BillDate">
        </div>
      </div>
    </div>
    <div class="col-md-5">
      <div class="control-group">
        <label for="inputError" class="control-label">Supplier Name</label>
        <select id="customer" name="CustomerName">
         <option value="">select</option>
            <?php
              foreach ($SupplierName as $row) {
                echo "<option value=".$Supplier_Id = $row->Supplier_Id.">".$SupplierName = $row->SupplierName."</option>"; 
              }
            ?>
            </select>
        <br><br>
      </div>
      <div class="control-group">
        <label for="inputError" class="control-label"> Address</label>
        <div class="controls">
          <textarea rows="5" cols="20" id="bil" name="BillingAddress" readonly></textarea>
        </div>
      </div>
      <div class="control-group">
        <label for="inputError" class="control-label" >CST</label>
        <div class="controls">
          <input type="text" id="cst" name="CST" readonly>
        </div>
      </div>
      <div class="control-group">
        <label for="inputError" class="control-label" >TIN</label>
        <div class="controls">
          <input type="text" id="tin" name="TIN" readonly>
        </div>
      </div>                    
    </div>
  </div>
</div>
<hr>
<div class="row">  
  <div class="col-md-10">
    <table id="myTable" class="table table-striped table-bordered table-condensed">
      <tr>
        <td style=display:none;>id</td>
        <td>InwardNumber</td>
        <td>SupplierName</td>
        <td>Total Amount</td>
        <td>AdvancePayment</td>
        <td>PaidAmount</td>
        <td>Balance</td>
        <td>select</td>
      </tr>
    </table>
  </div>
</div>
<hr>
<div class="row">
  <div class="col-md-5">            
    <div class="control-group">
      <label for="inputError" class="control-label">Total Amount</label>
      <div class="controls">
        <input type="text" id="tamt" name="Total" value="0" readonly>
      </div>
    </div>
    <div class="control-group">
      <label for="inputError" class="control-label">To Pay</label>
      <div class="controls">
        <input type="text" id="" name="topay" value="<?php echo set_value('topay'); ?>" >
      </div>
    </div>
    <div class="control-group">
      <label for="inputError" class="control-label">Bank Name</label>
      <div class="controls">
        <input type="text" id="" name="BankName" value="<?php echo set_value('BankName'); ?>" >
      </div>
    </div>
    <div class="control-group">
      <label for="inputError" class="control-label">Pay Mode</label>
      <div class="controls">
        <select type="text" id="" name="Paymode">
          <option>Select Type</option>
          <option value="Cheque">Cheque</option>
          <option value="Cash">Cash</option>
        </select>
      </div>
    </div>
    <div class="control-group">
      <label for="inputError" class="control-label">Cheque No</label>
      <div class="controls">
        <input type="text" id="" name="chequeno" value="<?php echo set_value('chequeno'); ?>" >
      </div>
    </div>
  </div>  
</div>
<div class="row">
  <div class="col-md-10">
    <div class="form-actions">
      <center><a href="admin/purchase"><button class="btn btn-primary" type="submit" onClick="clearform();">Save</button></a>
      <button class="btn" type="reset">Cancel</button></center>
    </div>
  </div>

</div>

<?php echo form_close(); ?>
