    

    <div class="container top">
       <div class="row">
        <div class="span10 columns">
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url("admin"); ?>">
            <?php echo ucfirst($this->uri->segment(1));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li>
          <a href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>">
            <?php echo ucfirst($this->uri->segment(2));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
          <a href="#">New</a>
        </li>
      </ul>
      
      <div class="page-header">
        <h2>
          Adding <?php echo ucfirst($this->uri->segment(2));?>
        </h2>
      </div>

      <?php
      //flash messages
      if(isset($flash_message)){
        if($flash_message == TRUE)
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Well done!</strong> New Employee created with success.';
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
          echo '</div>';          
        }
      }
      ?>
      
      <?php
      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '');

      //form validation
      echo validation_errors();
      
      echo form_open('admin/employee/add', $attributes);
      ?>
     
        <fieldset>
          <div class="container">
          <div class="row">
            <div class="col-md-5">

          <div class="control-group">
            <label for="inputError" class="control-label"></label>
            <div class="controls">
              <input type="hidden" id="" name="Employee_Id" value="<?php echo set_value('Employee_Id'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Employee Code</label>
            <div class="controls">
              <input type="text" id="" name="EmployeeCode" value="<?php echo set_value('EmployeeCode'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label">Employee Name</label>
            <div class="controls">
              <input type="text" id="" name="EmployeeName" value="<?php echo set_value('EmployeeName'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
            <div class="control-group">
            <label for="inputError" class="control-label">Pan Number</label>
            <div class="controls">
              <input type="text" id="" name="PanNumber" value="<?php echo set_value('PanNumber'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div></div>
          <div class="col-md-5">
          <div class="control-group">
            <label for="inputError" class="control-label">Date Of Birth</label>
            <div class="controls">
              <input type="date" id="" name="Date_of_birth" value="<?php echo set_value('Date_of_birth'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
         <div class="control-group">
            <label for="inputError" class="control-label">Join Date</label>
            <div class="controls">
              <input type="date" id="" name="Joindate" value="<?php echo set_value('Joindate'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Gross Salary</label>
            <div class="controls">
              <input type="text" id="" name="Grosssalary" value="<?php echo set_value('Grosssalary'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div></div><hr></div>
          <div class="row">
          <div class="col-md-5">
          <div class="control-group">
            <label for="inputError" class="control-label">Father Name</label>
            <div class="controls">
              <input type="text" id="" name="FatherName" value="<?php echo set_value('FatherName'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>

           <!-- <div class="control-group">
            <label for="inputError" class="control-label">Gender</label>
            <div class="controls">
              <input type="text" id="" name="Gender" value="<?php echo set_value('Gender'); ?>" >
              <!span class="help-inline">Woohoo!</span>
            </div>
          </div>  -->


                <div class="control-group">
                <label class="control-label" for="selectError3">Gender</label>
                <div class="controls">
                  <select id="selectError3" name="Gender" value="<?php echo set_value('Gender'); ?>">
                  <option>Select</option>
                  <option>Male</option>
                  <option>FeMale</option>
                  
                  </select>
                </div>
                </div>  
                <div class="control-group">
                <label class="control-label" for="selectError3">Marital Status</label>
                <div class="controls">
                  <select id="selectError3" name="MaritalStatus" value="<?php echo set_value('MaritalStatus'); ?>">
                  <option>Select</option>
                  <option>Married</option>
                  <option>UnMarried</option>
                  
                  </select>
                </div>
                </div>  
           <!-- <div class="control-group">
            <label for="inputError" class="control-label">Marital Status</label>
            <div class="controls">
              <input type="text" id="" name="MaritalStatus" value="<?php echo set_value('MaritalStatus'); ?>" >
              <!<span class="help-inline">Woohoo!</span>
            </div> 
          </div>-->
           <div class="control-group">
            <label for="inputError" class="control-label">Nationality</label>
            <div class="controls">
              <input type="text" id="" name="Nationality" value="<?php echo set_value('Nationality'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>

        </div>
          <div class="col-md-5">
           <div class="control-group">
            <label for="inputError" class="control-label">Emp Street </label>
            <div class="controls">
              <input type="text" id="" name="EmpStreet" value="<?php echo set_value('EmpStreet'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Emp Village</label>
            <div class="controls">
              <input type="text" id="" name="EmpVillage" value="<?php echo set_value('EmpVillage'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Emp City</label>
            <div class="controls">
              <input type="text" id="" name="EmpCity" value="<?php echo set_value('EmpCity'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Emp State</label>
            <div class="controls">
              <input type="text" id="" name="EmpState" value="<?php echo set_value('EmpState'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          </div><hr>
          </div>

          <div class="row">
            <div class="col-md-5">
          <div class="control-group">
            <label for="inputError" class="control-label">Emp Pincode</label>
            <div class="controls">
              <input type="number" id="" name="EmpPincode" value="<?php echo set_value('EmpPincode'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label">Phone Number</label>
            <div class="controls">
              <input type="text" id="" name="PhoneNumber" value="<?php echo set_value('PhoneNumber'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label">Mobile Number</label>
            <div class="controls">
              <input type="text" id="" name="MobileNumber" value="<?php echo set_value('MobileNumber'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">EmergencyNumber</label>
            <div class="controls">
              <input type="text" id="" name="EmergencyNumber" value="<?php echo set_value('EmergencyNumber'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
           </div>
        <div class="col-md-5">
           <div class="control-group">
            <label for="inputError" class="control-label">Email </label>
            <div class="controls">
              <input type="text" id="" name="Email" value="<?php echo set_value('Email'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label">Blood Group </label>
            <div class="controls">
              <input type="text" id="" name="BloodGroup" value="<?php echo set_value('BloodGroup'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Education  </label>
            <div class="controls">
              <input type="text" id="" name="Education" value="<?php echo set_value('Education'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Total Experience </label>
            <div class="controls">
              <input type="text" id="" name="TotalExperience" value="<?php echo set_value('TotalExperience'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Photo</label>
            <div class="controls">
              <input type="file" id="" name="Photo" value="<?php echo set_value('Photo'); ?>" >
              
            </div>
          </div>
           </div> </div>

          <div class="form-actions">
            <button class="btn btn-primary" type="submit">Save changes</button>
            <button class="btn" type="reset" onclick='alert("Employee Detail cancel successfully")'>Cancel</button>
          </div>
        </fieldset>

      <?php echo form_close(); ?>

    </div>
     </div>
   </div>