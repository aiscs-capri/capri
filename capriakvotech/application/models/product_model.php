<?php
class product_model extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return void
    */
    public function __construct()
    {
        $this->load->database();
    }

    /**
    * Get product by his is
    * @param int $product_id 
    * @return array
    */
    public function get_manufacture_by_Product_Id($Product_Id)
    {
		$this->db->select('*');
		$this->db->from('capri_master_product');
		$this->db->where('Product_Id', $Product_Id);
		$query = $this->db->get();
		return $query->result_array(); 
    }    

//get UnitName

 public function getunitname()
{

    $this->db->select('Unit_Id,UnitName');
    $this -> db -> from('capri_master_unit');  
    $query = $this -> db -> get();
    return $query->result();
}
    /**
    * Fetch product data from the database
    * possibility to mix search, filter and order
    * @param string $search_string 
    * @param strong $order
    * @param string $order_type 
    * @param int $limit_start
    * @param int $limit_end
    * @return array
    */
        public function get_unitname()
        {

            $this->db->select('capri_master_product.Product_Id,capri_master_product.ProductCode,capri_master_product.ProductName,capri_master_unit.UnitName,capri_master_product.ProductPrice');
        $this->db->from('capri_master_product'); 
        $this->db->join('capri_master_unit', 'capri_master_unit.Unit_Id = capri_master_product.ProductUnit', 'inner');
        }

    public function get_product($search_string=null, $order=null, $order_type='Asc', $limit_start=null, $limit_end=null)
    {
	    
		$this->db->select('*');
		$this->db->from('capri_master_product'); 
       


		if($search_string){
			$this->db->like('ProductCode', $search_string);
		}
		$this->db->group_by('Product_Id','ProductPrice');

		if($order){
			$this->db->order_by($order, $order_type);
		}else{
		    $this->db->order_by('Product_Id', $order_type);
		}

        if($limit_start && $limit_end){
          $this->db->limit($limit_start, $limit_end);	
        }

        if($limit_start != null){
          $this->db->limit($limit_start, $limit_end);    
        }
        
		$query = $this->db->get();
		
		return $query->result_array(); 	
    }

    /**
    * Count the number of rows
    * @param int $search_string
    * @param int $order
    * @return int
    */
    function count_product($search_string=null, $order=null)
    {
		$this->db->select('*');
		$this->db->from('capri_master_product');
		if($search_string){
			$this->db->like('ProductCode', $search_string);
		}
		if($order){
			$this->db->order_by($order, 'Asc');
		}else{
		    $this->db->order_by('Product_Id', 'Asc');
		}
		$query = $this->db->get();
		return $query->num_rows();        
    }

    /**
    * Store the new item into the database
    * @param array $data - associative array with data to store
    * @return boolean 
    */
    function store_manufacture($data)
    {
		$insert = $this->db->insert('capri_master_product', $data);
	    return $insert;
	}

    /**
    * Update manufacture
    * @param array $data - associative array with data to store
    * @return boolean
    */
    function update_manufacture($id, $data)
    {
		$this->db->where('Product_Id', $id);
		$this->db->update('capri_master_product', $data);
		$report = array();
		$report['error'] = $this->db->_error_number();
		$report['message'] = $this->db->_error_message();
		if($report !== 0){
			return true;
		}else{
			return false;
		}
	}

    /**
    * Delete manufacturer
    * @param int $id - manufacture id
    * @return boolean
    */
	function delete_manufacture($Product_Id){
		$this->db->where('Product_Id', $Product_Id);
		$this->db->delete('capri_master_product'); 
	}
 
}
	
