<?php
class value_model extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return voOrder_Id
    */
    public function __construct()
    {
        $this->load->database();
    }

    /**
    * Get product by his is
    * @param int $product_Order_Id 
    * @return array
    */
    public function getproduct($data)
    {
		$insert = $this->db->insert('capri_purchase_order_details',$data);
        return $insert;
    }    

public function getsalesquo($data2)
    {
        $insert = $this->db->insert('capri_sale_quotation_detail',$data2);
        return $insert;
    }   

    public function getlead($data1)
    {
        $insert = $this->db->insert('capri_sale_quotation_lead_product',$data1);
        return $insert;
    }   
    public function getperformance($data)
    {
        $insert = $this->db->insert('capri_sale_proforma_invoice_product',$data);
        return $insert;
    }     
     public function getinward($data)
    {
        $insert = $this->db->insert('capri_purchase_inward_details',$data);
        return $insert;
    }

    public function getsalesinvoice($data)
    {
        $insert = $this->db->insert('capri_sale_invoice_details',$data);
        return $insert;
    }


 public function getproforma($data)
    {
        $insert = $this->db->insert('capri_proforma_invoice_details',$data);
        return $insert;
    }    
public function getenquiry($data)
    {
        $insert = $this->db->insert('capri_customer_enquiry_details',$data);
        return $insert;
    }    
public function getdebit($data) 
    {
        $insert = $this->db->insert('capri_debit_details',$data);
        return $insert;
    }    
    public function getcredit($data) 
    {
        $insert = $this->db->insert('capri_credit_details',$data);
        return $insert;
    }   
    public function getstocktransfer($data) 
    {
        $insert = $this->db->insert('capri_stock_transfer_details',$data);
        return $insert;
    }    
}

