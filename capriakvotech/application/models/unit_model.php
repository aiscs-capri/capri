<?php
class unit_model extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return voUnit_Id
    */
    public function __construct()
    {
        $this->load->database();
    }

    /**
    * Get product by his is
    * @param int $product_Unit_Id 
    * @return array
    */
    public function get_manufacture_by_Unit_Id($Unit_Id)
    {
		$this->db->select('*');
		$this->db->from('capri_master_unit');
		$this->db->where('Unit_Id', $Unit_Id);
		$query = $this->db->get();
		return $query->result_array(); 
    }    

    /**
    * Fetch unit data from the database
    * possibility to mix search, filter and order
    * @param string $search_string 
    * @param strong $order
    * @param string $order_type 
    * @param int $limit_start
    * @param int $limit_end
    * @return array
    */
    public function get_unit($search_string=null, $order=null, $order_type='Asc', $limit_start=null, $limit_end=null)
    {
	    
		$this->db->select('*');
		$this->db->from('capri_master_unit');

		if($search_string){
			$this->db->like('UnitName', $search_string);
		}
		$this->db->group_by('Unit_Id');

		if($order){
			$this->db->order_by($order, $order_type);
		}else{
		    $this->db->order_by('Unit_Id', $order_type);
		}

        if($limit_start && $limit_end){
          $this->db->limit($limit_start, $limit_end);	
        }

        if($limit_start != null){
          $this->db->limit($limit_start, $limit_end);    
        }
        
		$query = $this->db->get();
		
		return $query->result_array(); 	
    }

    /**
    * Count the number of rows
    * @param int $search_string
    * @param int $order
    * @return int
    */
    function count_unit($search_string=null, $order=null)
    {
		$this->db->select('*');
		$this->db->from('capri_master_unit');
		if($search_string){
			$this->db->like('UnitName', $search_string);
		}
		if($order){
			$this->db->order_by($order, 'Asc');
		}else{
		    $this->db->order_by('Unit_Id', 'Asc');
		}
		$query = $this->db->get();
		return $query->num_rows();        
    }

    /**
    * Store the new item into the database
    * @param array $data - associative array with data to store
    * @return boolean 
    */
    function store_manufacture($data)
    {
		$insert = $this->db->insert('capri_master_unit', $data);
	    return $insert;
	}

    /**
    * Update manufacture
    * @param array $data - associative array with data to store
    * @return boolean
    */
    function update_manufacture($Unit_Id, $data)
    {
		$this->db->where('Unit_Id', $Unit_Id);
		$this->db->update('capri_master_unit', $data);
		$report = array();
		$report['error'] = $this->db->_error_number();
		$report['message'] = $this->db->_error_message();
		if($report !== 0){
			return true;
		}else{
			return false;
		}
	}

    /**
    * Delete manufacturer
    * @param int $Unit_Id - manufacture Unit_Id
    * @return boolean
    */
	function delete_unit($Unit_Id){
		$this->db->where('Unit_Id', $Unit_Id);
		$this->db->delete('capri_master_unit'); 
	}
 
}

