<?php
class salesquo_model extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return voQtn_Id
    */
    public function __construct()
    {
        $this->load->database();
    }

    /**
    * Get product by his is
    * @param int $product_Qtn_Id 
    * @return array
    */
    public function get_manufacture_by_Qtn_Id($Qtn_Id)
    {
		$this->db->select('*');
		$this->db->from('capri_sale_quotation');
		$this->db->where('Qtn_Id', $Qtn_Id);
		$query = $this->db->get();
		return $query->result_array(); 
    }    


public function save($Qtn_Id)
    {
        $insert = $this->db->insert('capri_sale_quotation', $data);
        return $insert;
    }   

    public function maxvalue()
    {
        $this->db->select_MAX('Qtn_Id');
        $this->db->from('capri_sale_quotation');
        $query = $this->db->get();
        return $query;

    }


    public function getquotationOrderDetails($lead_order)
    {
       $this->db->select('capri_sale_quotation.QtnNumber,capri_master_customer.CustomerName,capri_master_customer.BillingAddress,capri_master_customer.TIN,capri_master_customer.CST');
        $this->db->from('capri_sale_quotation');
        $this->db->join('capri_master_customer','capri_sale_quotation.CustomerName = capri_master_customer.CustomerName','inner');
        $this->db->where('capri_sale_quotation.Qtn_Id',$lead_order);
        $query = $this->db->get();
        return $query->result();
    }
   public function getquotationproductDetails($purchase_order_product)
    {
        $this->db->select('Id,ProductName,ProductUnit,ProductPrice,Quantity,Total');
        $this->db->from('capri_sale_quotation_detail ');       
        $this->db->where('capri_sale_quotation_detail.Qtn_Id',$purchase_order_product);
        $query = $this->db->get();
        return $query->result();
    }


    /**
    * Fetch purchase_master data from the database
    * possibility to mix search, filter and order
    * @param string $search_string 
    * @param strong $order
    * @param string $order_type 
    * @param int $limit_start
    * @param int $limit_end
    * @return array
    */
    public function get_quotation($search_string=null, $order=null, $order_type='Asc', $limit_start, $limit_end)
    {
        
        $this->db->select('capri_sale_quotation.Qtn_Id');
        $this->db->select('capri_sale_quotation.QtnNumber');
        $this->db->select('capri_sale_quotation.QtnDate');
        $this->db->select('capri_sale_quotation.OurRef');
        $this->db->select('capri_sale_quotation.KindAttn');
        $this->db->select('capri_sale_quotation.CustomerName');
        $this->db->select('capri_sale_quotation.TIN');
        $this->db->select('capri_sale_quotation.BillingAddress');
        $this->db->select('capri_sale_quotation.CST');
        $this->db->select('capri_sale_quotation.DateOfDelivery');
        $this->db->select('capri_sale_quotation.PlaceOfDelivery');
        $this->db->select('capri_sale_quotation.Quality');
        $this->db->select('capri_sale_quotation.Discount');
        $this->db->select('capri_sale_quotation.PaymentTerm');
        $this->db->select('capri_sale_quotation.validityQtn');
        $this->db->select('capri_sale_quotation.dispatchThru');
        $this->db->select('capri_sale_quotation.Tax');
        $this->db->select('capri_sale_quotation.Tax_type');
        $this->db->select('capri_sale_quotation.Total');
        $this->db->select('capri_sale_quotation.TaxAmount');
        $this->db->from('capri_sale_quotation');
        $query = $this->db->get();
        return $query->result_array();  
    }
    /**
    * Count the number of rows
    * @param int $search_string
    * @param int $order
    * @return int
    */

    public function getCustomername()
{

    $this->db->select('Customer_Id,CustomerName');
    $this -> db -> from('capri_master_Customer');  
    $query = $this -> db -> get();
    return $query->result();
}
    public function getproductname()
{

    $this->db->select('Product_Id,ProductName');
    $this -> db -> from('capri_master_product');

    $query = $this -> db -> get();
    return $query->result();
}
public function getproductdetail($Product_Id)
{

    $this->db->select('Product_Id,ProductPrice,ProductUnit');
    $this->db->where('Product_Id', $Product_Id);
    $this->db->from('capri_master_product');
    $query=$this->db->get();
    return $query->result();
}

/*$this->db->select('concat(BillStreet,',',BillVillage,',',BillCity,',',billstate,',',billpincode) as address,CST,TIN');*/
public function getcustomerdetail($Customer_Id)
{

    $this->db->select('BillingAddress');
    $this->db->select('CST,TIN');
    $this->db->from('capri_master_customer');
    $this->db->where('Customer_Id', $Customer_Id);
    $query=$this->db->get();
    return $query->result();
}

     public function get_order($Product_Id=null, $search_string=null, $order=null, $order_type='Asc', $limit_start, $limit_end)
    {
        
        $this->db->select('capri_sale_quotation_product.Id');
        $this->db->select('capri_sale_quotation_product.Product_Id');
        $this->db->select('capri_sale_quotation_product.Quantity');
        $this->db->select('capri_sale_quotation_product.Total');
         $this->db->select('capri_sale_quotation_product.Qtn_Id');
        $this->db->select('capri_sale_quotation_product.Product_Id');
        $this->db->select('capri_master_product.Product_Id as Product_Id');
        $this->db->from('capri_sale_quotation_product');
        if($Product_Id != null && $Product_Id != 0){
            $this->db->where('Product_Id', $Product_Id);
        }
        if($search_string){
            $this->db->like('Product_Id', $search_string);
        }

        $this->db->join('capri_master_product', 'capri_sale_quotation_product.Product_Id = capri_master_product.Product_Id', 'left');
       
        
        $this->db->group_by('capri_sale_quotation_product.Id');

        if($order){
            $this->db->order_by($order, $order_type);
        }else{
            $this->db->order_by('Id', $order_type);
        }


        $this->db->limit($limit_start, $limit_end);
        //$this->db->limit('4', '4');


        $query = $this->db->get();
        
        return $query->result_array();  
    }
    /**
    * Count the number of rows
    * @param int $search_string
    * @param int $order
    * @return int
    */
   function count_quotation($search_string=null, $order=null)
    {
        $this->db->select('*');
        $this->db->from('capri_sale_quotation');
       
        if($search_string){
            $this->db->like('QtnNumber', $search_string);
        }
        if($order){
            $this->db->order_by($order, 'Asc');
        }else{
            $this->db->order_by('Qtn_Id', 'Asc');
        }
        $query = $this->db->get();
        return $query->num_rows();        
    }
    /**
    * Store the new item into the database
    * @param array $data - associative array with data to store
    * @return boolean 
    */
    function store_manufacture($data)
    {
		$insert = $this->db->insert('capri_sale_quotation', $data);
	    return $insert;
	}

    /**
    * Update manufacture
    * @param array $data - associative array with data to store
    * @return boolean
    */
    function update_manufacture($Qtn_Id, $data)
    {
		$this->db->where('Qtn_Id', $Qtn_Id);
		$this->db->update('capri_sale_quotation', $data);
		$report = array();
		$report['error'] = $this->db->_error_number();
		$report['message'] = $this->db->_error_message();
		if($report !== 0){
			return true;
		}else{
			return false;
		}
	}

    /**
    * Delete manufacturer
    * @param int $Qtn_Id - manufacture Qtn_Id
    * @return boolean
    */
	function delete_manufacture($Qtn_Id){
		$this->db->where('Qtn_Id', $Qtn_Id);
		$this->db->delete('capri_sale_quotation'); 
	}
 
}
	
