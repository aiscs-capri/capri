<?php
class marketing_model extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return voId
    */
    public function __construct()
    {
        $this->load->database();
    }

    /**
    * Get product by his is
    * @param int $product_Id 
    * @return array
    */
    public function get_marketing_by_Id($Id)
    {
		$this->db->select('*');
		$this->db->from('capri_marketing_schedule');
		$this->db->where('Id', $Id);
		$query = $this->db->get();
		return $query->result_array(); 
    }

    /**
    * Fetch capri_marketing_schedule data from the database
    * possibility to mix search, filter and order
    * @param int $manufacuture_Id 
    * @param string $search_string 
    * @param strong $order
    * @param string $order_type 
    * @param int $limit_start
    * @param int $limit_end
    * @return array
    */
    public function get_marketing($Employee_Id=null, $search_string=null, $order=null, $order_type='Asc', $limit_start, $limit_end)
    {
	    
		$this->db->select('capri_marketing_schedule.Id');
		$this->db->select('capri_marketing_schedule.ScheduleDate');
		$this->db->select('capri_marketing_schedule.ScheduleTime');
		$this->db->select('capri_marketing_schedule.Name');
		$this->db->select('capri_marketing_schedule.Address');
		$this->db->select('capri_marketing_schedule.Phone');
		$this->db->select('capri_marketing_schedule.ContactPerson');
		$this->db->select('capri_marketing_schedule.Mobile');
		$this->db->select('capri_marketing_schedule.Status');
		$this->db->select('capri_marketing_schedule.message');
		$this->db->select('capri_marketing_schedule.EmployeeName');
		
		$this->db->from('capri_marketing_schedule');
		if($Employee_Id != null && $Employee_Id != 0){
			$this->db->where('Employee_Id', $Employee_Id);
		}
		if($search_string){
			$this->db->like('name', $search_string);
		}

		

		$this->db->group_by('capri_marketing_schedule.Id');

		if($order){
			$this->db->order_by($order, $order_type);
		}else{
		    $this->db->order_by('Id', $order_type);
		}


		$this->db->limit($limit_start, $limit_end);
		//$this->db->limit('4', '4');


		$query = $this->db->get();
		
		return $query->result_array(); 	
    }

    /**
    * Count the number of rows
    * @param int $Employee_Id
    * @param int $search_string
    * @param int $order
    * @return int
    */

   
    function count_marketing($Employee_Id=null, $search_string=null, $order=null)
    {
		$this->db->select('*');
		$this->db->from('capri_marketing_schedule');
		if($Employee_Id != null && $Employee_Id != 0){
			$this->db->where('Employee_Id', $Employee_Id);
		}
		if($search_string){
			$this->db->like('Name', $search_string);
		}
		if($order){
			$this->db->order_by($order, 'Asc');
		}else{
		    $this->db->order_by('Id', 'Asc');
		}
		$query = $this->db->get();
		return $query->num_rows();        
    }

    /**
    * Store the new item into the database
    * @param array $data - associative array with data to store
    * @return boolean 
    */
    function store_marketing($data)
    {
		$insert = $this->db->insert('capri_marketing_schedule', $data);
	    return $insert;
	}

 public function getemployeename()
{

    $this->db->select('Employee_Id,EmployeeName');
    $this -> db -> from('capri_master_employee');  
    $query = $this -> db -> get();
    return $query->result();
}
    /**
    * Update product
    * @param array $data - associative array with data to store
    * @return boolean
    */
    function update_product($Id, $data)
    {
		$this->db->where('Id', $Id);
		$this->db->update('capri_marketing_schedule', $data);
		$report = array();
		$report['error'] = $this->db->_error_number();
		$report['message'] = $this->db->_error_message();
		if($report !== 0){
			return true;
		}else{
			return false;
		}
	}

    /**
    * Delete product
    * @param int $Id - product Id
    * @return boolean
    */
	function delete_marketing($Id){
		$this->db->where('Id', $Id);
		$this->db->delete('capri_marketing_schedule'); 
	}
 
}
	
