    <div class="container top">
     <div class="row">
        <div class="span10 columns">
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url("admin"); ?>">
            <?php echo ucfirst($this->uri->segment(1));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
          <?php echo ucfirst($this->uri->segment(2));?>
        </li>
      </ul>

      <div class="page-header users-header">
        <h2>
          <?php echo ucfirst($this->uri->segment(2));?> 
          <a  href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>/add" class="btn btn-success">Add Pay</a>
        </h2>
      </div>
    </div>
  </div>
</div>
      
        <div class="span10 columns">
          <div class="well">
           
            <?php
           
            $attributes = array('class' => 'form-inline reset-margin', 'id' => 'myform');
           
            //save the columns names in a array that we will use as filter         
            $options_outpay = array();    
            foreach ($outpay as $array) {
              foreach ($array as $key => $value) {
                $options_outpay[$key] = $key;
              }
              break;
            }

            echo form_open('admin/outpay', $attributes);
     
              echo form_label('Search:', 'search_string');
              echo form_input('search_string', $search_string_selected);

              echo form_label('Order by:', 'order');
              echo form_dropdown('order', $options_outpay, $order, 'class="span2"');

              $data_submit = array('name' => 'mysubmit', 'class' => 'btn btn-primary', 'value' => 'Go');

              $options_order_type = array('Asc' => 'Asc', 'Desc' => 'Desc');
              echo form_dropdown('order_type', $options_order_type, $order_type_selected, 'class="span2"');

              echo form_submit($data_submit);

            echo form_close();
            ?>

          </div>

          <table class="table table-striped table-bordered table-condensed">
            <thead>
              <tr>              
              <th class="yellow header headerSortDown">BillNumber</th>
              <th class="yellow header headerSortDown">BillDate</th>
              <th class="yellow header headerSortDown">InwardNumber</th>
              <th class="yellow header headerSortDown">SupplierName</th>              
              </tr>
            </thead>
            <tbody>
              <?php
              foreach($outpay as $row)
              {
                echo '<tr>';                
                echo '<td>'.$row['BillNumber'].'</td>';
                echo '<td>'.date('Y-M-d',strtotime($row['BillDate'])).'</td>';
                echo '<td>'.$row['InwardNumber'].'</td>';
                echo '<td>'.$row['SupplierName'].'</td>';                
                echo '<td class="crud-actions">                  
                  <a href="'.site_url("admin").'/outpay/delete/'.$row['Id'].'" ><button class="btn btn-xs btn-danger"><i class="fa fa-trash-o fa-fw"></i> </button></a>
                  
                </td>';
                echo '</tr>';
              }
              ?>      
            </tbody>
          </table>
          <?php echo '<div class="pagination">'.$this->pagination->create_links().'</div>'; ?>
      </div>
  
 