<?php
class Admin_outpay extends CI_Controller {

    /**
    * name of the folder responsible for the views 
    * which are manipulated by this controller
    * @constant string
    */
    const VIEW_FOLDER = 'admin/outpay';
 
    /**
    * Responsable for auto load the model
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('outpay_model');
        $this->load->model('customer_model');
        $this->load->model('salesinvoice_model');
        if(!$this->session->userdata('is_logged_in')){
            redirect('admin/outpay');
        }
    }
 
    /**
    * Load the main view with all the current model model's data.
    * @return void
    */
    public function index()
    {

        //all the posts sent by the view
        $search_string = $this->input->post('search_string');        
        $order = $this->input->post('order'); 
        $order_type = $this->input->post('order_type'); 

        //pagination settings
        $config['per_page'] = 5;

        $config['base_url'] = base_url().'admin/outpay';
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = 20;
        $config['full_tag_open'] = '<ul>';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';

        //limit end
        $page = $this->uri->segment(3);

        //math to get the initial record to be select in the database
        $limit_end = ($page * $config['per_page']) - $config['per_page'];
        if ($limit_end < 0){
            $limit_end = 0;
        } 

        //if order type was changed
        if($order_type){
            $filter_session_data['order_type'] = $order_type;
        }
        else{
            //we have something stored in the session? 
            if($this->session->userdata('order_type')){
                $order_type = $this->session->userdata('order_type');    
            }else{
                //if we have nothing inside session, so it's the default "Asc"
                $order_type = 'Asc';    
            }
        }
        //make the data type var avaible to our view
        $data['order_type_selected'] = $order_type;        


        //we must avoid a page reload with the previous session data
        //if any filter post was sent, then it's the first time we load the content
        //in this case we clean the session filter data
        //if any filter post was sent but we are in some page, we must load the session data

        //filtered && || paginated
        if($search_string !== false && $order !== false || $this->uri->segment(3) == true){ 
           
            /*
            The comments here are the same for line 79 until 99

            if post is not null, we store it in session data array
            if is null, we use the session data already stored
            we save order into the the var to load the view with the param already selected       
            */
            if($search_string){
                $filter_session_data['search_string_selected'] = $search_string;
            }else{
                $search_string = $this->session->userdata('search_string_selected');
            }
            $data['search_string_selected'] = $search_string;

            if($order){
                $filter_session_data['order'] = $order;
            }
            else{
                $order = $this->session->userdata('order');
            }
            $data['order'] = $order;

            //save session data into the session
            if(isset($filter_session_data)){
              $this->session->set_userdata($filter_session_data);    
            }
            
            //fetch sql data into arrays
            $data['count_outpays']= $this->outpay_model->count_income($search_string, $order);
            $config['total_rows'] = $data['count_outpays'];

            //fetch sql data into arrays
            if($search_string){
                if($order){
                    $data['income'] = $this->outpay_model->get_outpay($search_string, $order, $order_type, $config['per_page'],$limit_end);        
                }else{
                    $data['income'] = $this->outpay_model->get_outpay($search_string, '', $order_type, $config['per_page'],$limit_end);           
                }
            }else{
                if($order){
                    $data['income'] = $this->outpay_model->get_outpay('', $order, $order_type, $config['per_page'],$limit_end);        
                }else{
                    $data['income'] = $this->outpay_model->get_outpay('', '', $order_type, $config['per_page'],$limit_end);        
                }
            }

        }else{

            //clean filter data inside section
            $filter_session_data['manufacture_selected'] = null;
            $filter_session_data['search_string_selected'] = null;
            $filter_session_data['order'] = null;
            $filter_session_data['order_type'] = null;
            $this->session->set_userdata($filter_session_data);

            //pre selected options
            $data['search_string_selected'] = '';
            $data['order'] = 'id';

            //fetch sql data into arrays
            $data['count_outpay']= $this->outpay_model->count_outpay();
            $data['outpay'] = $this->outpay_model->get_outpay('', '', $order_type, $config['per_page'],$limit_end);        
            $config['total_rows'] = $data['count_outpay'];

        }//!isset($search_string) && !isset($order)
         
        //initializate the panination helper 
        $this->pagination->initialize($config);   

        //load the view
        $data['main_content'] = 'admin/outpay/list';
        $this->load->view('includes/template', $data);  

    }//index
 
    public function add()
    {
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {

            //form validation
            $this->form_validation->set_rules('Id', 'Id');
            $this->form_validation->set_rules('BillNumber', 'BillNumber', 'required');
            $this->form_validation->set_rules('BillDate', 'BillDate', 'required');
            $this->form_validation->set_rules('Paymode', 'Paymode', 'required');
            $this->form_validation->set_rules('topay', 'topay', 'required|numeric');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            

            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
                
              /*  //if the insert has returned true then we show the flash message
                if($this->outpay_model->store_income($data_to_store)){
                    $data['flash_message'] = TRUE;
*/
                    $payId = $_POST['updateId'];
                    $invNum = $_POST['updateInv'];
                    $cus = $_POST['updateCus'];
                    $balAmt = $_POST['updateBalance'];
                    $totPai = $_POST['updatePaid'];
                    $totAmt = $_POST['updateTotal'];
                    $topay = $_POST['topay'];
                    

                    
                    for($i=0; $i<sizeof($payId); $i++)
                    {
                        if($topay>=$balAmt[$i])
                        {
                            $bal = 0;
                            $paid = $totPai[$i] + $balAmt[$i];
                            $topay = $topay - $balAmt[$i];
                            $data_to_store = array(
                                'Id' => $this->input->post('Id'),
                                'BillNumber' => $this->input->post('BillNumber'),
                                'BillDate' => date("Y-m-d",strtotime($this->input->post('BillDate'))),
                                'InwardNumber' => $invNum[$i],
                                'SupplierName' => $cus[$i],
                                'Total' => $balAmt[$i],
                                'Pay' => $balAmt[$i],
                                'Balance' => $bal,
                                'Paymode' => $this->input->post('Paymode'),
                                'Chequeno' => $this->input->post('Chequeno'),
                                'BankName' => $this->input->post('BankName'),
                                'CreatedBy' => $this->session->userdata('user_id'),
                                'CreatedDate' => date("Y-m-d H:i:s")                    
                                );
                            $this->outpay_model->store_outcome($data_to_store);
                            $data1 = array('Paid'=>$paid,'Balance'=>$bal);
                            $this->outpay_model->getUpdateDetail($payId[$i],$data1);
                        }   
                        else
                        {
                            $bal = $balAmt[$i] - $topay;
                            $paid = $totPai[$i]+$topay;

                            $data_to_store = array(
                                'Id' => $this->input->post('Id'),
                                'BillNumber' => $this->input->post('BillNumber'),
                                'BillDate' => date("Y-m-d",strtotime($this->input->post('BillDate'))),
                                'InwardNumber' => $invNum[$i],
                                'SupplierName' => $cus[$i],
                                'Total' => $balAmt[$i],
                                'Pay' => $topay,
                                'Balance' => $bal,
                                'Paymode' => $this->input->post('Paymode'),
                                'Chequeno' => $this->input->post('Chequeno'),
                                'BankName' => $this->input->post('BankName'),
                                'CreatedBy' => $this->session->userdata('user_id'),
                                'CreatedDate' => date("Y-m-d H:i:s")                    
                                );
                            $this->outpay_model->store_outcome($data_to_store);
                            $data1 = array('Paid'=>$paid,'Balance'=>$bal);
                            $this->outpay_model->getUpdateDetail($payId[$i],$data1);
                        }        
                    
                    }

            }

        }

        $supplier = $this->outpay_model->getsuppliername();
        //load the view
        $data['SupplierName'] =$supplier;
        $data['main_content'] = 'admin/outpay/add';
        $this->load->view('includes/template', $data);  
    } 
    public function get()
    {

        $SupplierId =$_GET['SupplierName'];        
        $incomedetail["rows"] = $this->outpay_model->getincomedetail($SupplierId);
        echo json_encode($incomedetail);

    }  
    /**
    * Delete outpay by his id
    * @return void
    */
     public function view()
    {
        //outpay id 
        $id = $this->uri->segment(4);
  
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {
            //form validation
            $this->form_validation->set_rules('Id', 'Id', 'required');
            $this->form_validation->set_rules('PaymentMode', 'PaymentMode', 'required');
            $this->form_validation->set_rules('ChequeorDraftNumber', 'ChequeorDraftNumber', 'required');
            $this->form_validation->set_rules('BankName', 'BankName', 'required');
            $this->form_validation->set_rules('topay', 'topay', 'required|numeric');
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
    
                $data_to_store = array(
                    'Id' => $this->input->post('Id'),
                    'PaymentMode' => $this->input->post('PaymentMode'),
                    'ChequeorDraftNumber' => $this->input->post('ChequeorDraftNumber'),
                    'BankName' => $this->input->post('BankName'),
                    'topay' => $this->input->post('topay'),
                );
                //if the insert has returned true then we show the flash message
                if($this->outpay_model->update_manufacture($id, $data_to_store) == TRUE){
                    $this->session->set_flashdata('flash_message', 'updated');
                }else{
                    $this->session->set_flashdata('flash_message', 'not_updated');
                }
                redirect('admin/outpay/view/'.$id.'');

            }//validation run

        }

        //if we are updating, and the data did not pass trough the validation
        //the code below wel reload the current data

        //outpay data 
        $data['manufacture'] = $this->outpay_model->get_manufacture_by_outpay_Id($id);
        //load the view
        $data['main_content'] = 'admin/outpay/view';
        $this->load->view('includes/template', $data);            

    }//update

    /**
    * Delete outpay by his id
    * @return void
    */
    public function delete()
    {
        //outpay id 
        $id = $this->uri->segment(4);
        //$incomedetail[] = $this->outpay_model->getIncomeDetails($id);
        $incomedetail = $this->outpay_model->getIncomeDetails($id);
        foreach ($incomedetail as $key => $value) {
            $InwardNumber = $value->InwardNumber;
            $Pay = $value->Pay;
            $data =  $this->outpay_model->getPaidInvoiceDetail($value->InwardNumber);

            foreach ($data as $key => $value) {
                $paidamt = $value->Paid;
                $balance = $value->Balance;
                $updateInward = array('Paid' => ($paidamt-$Pay), 
                    'Balance'=>($Pay+$balance)); 
                $this->outpay_model->getUpdateInvoiceDetail($InwardNumber,$updateInward);
                $this->outpay_model->getDeleteIncomeDetails($id);
            }
        }

        redirect('admin/outpay');
    }//edit

}


?>