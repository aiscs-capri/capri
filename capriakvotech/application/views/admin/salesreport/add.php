    
 <script src="<?php echo base_url(); ?>js/jquery-1.11.1.js"></script>
 <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
<script src="<?php echo base_url(); ?>js/config.js"></script>
<script>
$(document).ready(function() {
    $( "#product" ).change(function () {

    var str = "";
    $( "#product option:selected" ).each(function() {
      str += $( this ).val() + " ";
    });

  $.ajax({
            type : "get",
            url: serverurl+"/admin_purchase/get", 
            dataType: "json",
            data: {
                Product_Id : str
               },
            success: function(data){                          
               console.log(data.rows[0].ProductPrice);
               $("#pid").val(data.rows[0].Product_Id),
                $("#price").val(data.rows[0].ProductPrice),
                $("#unit").val(data.rows[0].ProductUnit);
            } ,
            error: function(data, errorThrown)
          {
              alert('request failed :'+errorThrown);
          }
        });    

  }).change(); 

      $("#addRecord").click(function(){
      console.log('test');
        var id = $("#product option:selected").val();
        var product = $("#product option:selected").text();
        var unit = $("#unit").val();
        var qun = $("#qun").val();
        var price = $("#price").val();
        var tot = $("#tot").val();
        
        $("#myTable").append("<tr><td style=display:none;>"+id+"<input type=hidden name=id[] value="+id+"></td><td>"+product+"<input type=hidden name=product[] value="+product+"></td><td>"+unit+"<input type=hidden name=unit[] value="+unit+"></td><td>"+qun+"<input type=hidden name=qun[] value="+qun+"></td><td>"+price+"<input type=hidden name=price[] value="+price+"></td><td class=amt >"+tot+"<input type=hidden name=tot[] value="+tot+"></td><td><input type=checkbox></input></td></tr>");
        totalDisplay();
        $("#product").prop('selectedIndex',0);       
        $("#unit").val("");
        $("#qun").val("");
        $("#price").val("");
         $("#tot").val("");

    });

    
 $("#delete").click(function(){
        $("input[type=checkbox]:checked").each(function() {
        console.log(  $(this).parent().parent().remove());
        totalDisplay();
      });
  });

//Balance calculation

 $("#ap").on("keyup", function() {
    var val = +this.value || 0;
    $("#bal").val($("#gtot").val() - val);
});



 //get customer data

 $( "#customer" ).change(function () {

    var str = "";
    $( "#customer option:selected" ).each(function() {
      str += $( this ).val() + " ";
    });

  $.ajax({
            type : "get",
            url: serverurl+"/admin_salesinvoice/getcustomerdata", 
            dataType: "json",
            data: {
                Customer_Id : str
               },
            success: function(data){                          
               console.log(data.rows[0].BillState);
               $("#bil").val(data.rows[0].BillingAddress),
                $("#cst").val(data.rows[0].CST),
                $("#tin").val(data.rows[0].TIN);
            } ,
            error: function(data, errorThrown)
          {
              alert('request failed :'+errorThrown);
          }
        });    
  }).change(); 


//get customer name store in db
 $( "#customer" ).change(function () {
     $("#customername").val($( "#customer option:selected").text());
     console.log($( "#customer option:selected").text());
   });

});

    

//get table total values
$("#total").click(function(){
var $table = $('#myTable'),
    $tr = $table.find('tbody tr'),
    total = 0;

$tr.each(function(i, el){
    total += parseInt($(this).find('td:last').html(), 10);
});

$table.find('tfoot td:last').html(total);
 });



function totalDisplay(){
  var totalHours=0;
  $(".amt").each(function(){
    totalHours +=parseInt($(this).text());


  });
  $("#total").val(totalHours);
}

function sum() {
            var txtFirstNumberValue = document.getElementById('price').value;
            var txtSecondNumberValue = document.getElementById('qun').value;
            var result = parseInt(txtFirstNumberValue) * parseInt(txtSecondNumberValue);
            if (!isNaN(result)) {
                document.getElementById('tot').value = result;
            }
        }

function gtotal() {
            var txtFirstNumberValue = document.getElementById('total').value;
            var txtSecondNumberValue = document.getElementById('tax').value;
            var txtThirdNumberValue = document.getElementById('paf').value;
            var txtFourthNumberValue = document.getElementById('sc').value;
            var txtFifthNumberValue = document.getElementById('oc').value;
            
            var result =(parseInt(txtFirstNumberValue) * parseInt(txtSecondNumberValue)/100);
            var result1 = parseInt(txtFirstNumberValue)+(parseInt(txtFirstNumberValue) * parseInt(txtSecondNumberValue)/100);
            var result2 = (parseInt(txtFifthNumberValue)+parseInt(txtFourthNumberValue)+parseInt(txtThirdNumberValue)+result1);
            if (!isNaN(result)) {
                document.getElementById('atax').value = result;
                document.getElementById('gtot').value = result2;
            }
        }

</script>
    <div class="container top">
         <div class="row">
        <div class="span10 columns">
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url("admin"); ?>">
            <?php echo ucfirst($this->uri->segment(1));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li>
          <a href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>">
            <?php echo ucfirst($this->uri->segment(2));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
          <a href="#">New</a>
        </li>
      </ul>
    </div></div></div>
      
      <div class="page-header">
        <h2>
          Adding <?php echo ucfirst($this->uri->segment(2));?>
        </h2>
      
    </div>
      <?php
      //flash messages
      if(isset($flash_message)){
        if($flash_message == TRUE)
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Well done!</strong> New performance created with success.';
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
          echo '</div>';          
        }
      }
      ?>
      
      <?php
      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '');
          //form validation
      echo validation_errors();
      
      echo form_open('admin/salesinvoice/add', $attributes);
      ?>
       <div class="container">
           <div class="row">
               <div class="col-md-5">
                <div class="control-group">
            <label for="inputError" class="control-label"></label>
            <div class="controls">
              <input type="hidden" id="box1" name="Invoice_Id" value="<?php echo set_value('Invoice_Id'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Invoice Numer</label>
            <div class="controls">
              <input type="text" id="box2" name="InvoiceNumber" value="<?php echo set_value('InvoiceNumber'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Invoice Date</label>
            <div class="controls">
              <input type="date" id="result" name="InvoiceDate" value="<?php echo set_value('InvoiceDate'); ?>"  >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Po Number</label>
            <div class="controls">
              <input type="text" id="result" name="Po_Number" value="<?php echo set_value('Po_Number'); ?>"  >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Po Date</label>
            <div class="controls">
              <input type="date" id="result" name="PoDate" value="<?php echo set_value('PoDate'); ?>"  >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Dc Number</label>
            <div class="controls">
              <input type="text" id="result" name="Dc_Number" value="<?php echo set_value('Dc_Number'); ?>"  >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Dc Date</label>
            <div class="controls">
              <input type="date" id="result" name="DcDate" value="<?php echo set_value('DcDate'); ?>"  >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          </div>
          
      <div class="col-md-5">
      <div class="control-group">
            <label for="inputError" class="control-label">Customer Name</label>
            <select id="customer">
            <option value="">select</option>
                      <?php
                        foreach ($CustomerName as $row) {
                          echo "<option value=".$CustomerName = $row->Customer_Id.">".$CustomerName = $row->CustomerName."</option>"; 
                        }
                      ?>
                      </select>
                      <input type="hidden" id="customername" name="CustomerName">
                    </div>

            <div class="control-group">
            <label for="inputError" class="control-label"> Address</label>
            <div class="controls">
           <textarea rows="5" cols="20" id="bil" name="BillingAddress" readonly></textarea>
           </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label" >CST</label>
            <div class="controls">
              <input type="text" id="cst" name="CST" readonly>
              </div>
          </div>
          
           <div class="control-group">
            <label for="inputError" class="control-label" >TIN</label>
            <div class="controls">
              <input type="text" id="tin" name="TIN" readonly>
              </div>
          </div>
                    

          </div>
        </div></div>
        <hr>
       
     

       <div class="container">
           <div class="row">
               <div class="col-md-5">
              <div class="control-group">
            <label for="inputError" class="control-label">Product Name</label>
            <select id="product" name="Product_Id">
              <option value="">select</option>
                      <?php
                        foreach ($ProductName as $row) {
                          echo "<option value=".$Product_Id = $row->Product_Id.">".$ProductName = $row->ProductName."</option>"; 
                        }
                      ?>
                      </select>
                    </div>
          <div class="control-group">
            <!-- <label for="inputError" class="control-label">product Id</label> -->
            <div class="controls">
              <input type="hidden" id="pid" name="Product_Id" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>

          <div class="control-group">
            <label for="inputError" class="control-label" >Product Price</label>
            <div class="controls">
              <input type="text" id="price" name="ProductPrice"onkeyup="sum();" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div></div>
          <div class="col-md-5">
           <div class="control-group">
            <label for="inputError" class="control-label" >Product Unit</label>
            <div class="controls">
              <input type="text" id="unit" name="ProductUnit" readonly>
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
                <div class="control-group">
            <label for="inputError" class="control-label">Quantity</label>
            <div class="controls">
              <input type="text" id="qun" name="Quantity" value="<?php echo set_value('Quantity'); ?>" onkeyup="sum();">
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>

          <div class="control-group">
            <label for="inputError" class="control-label">Amount</label>
            <div class="controls">
              <input type="text" id="tot" name="Total" value="<?php echo set_value('Total'); ?>" readonly>
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div></div></div>
          <div class="row">
         <div class="col-md-10">
          <center>
          <div class="btn btn-success" id="addRecord">Add</div>
           <div class="btn btn-danger" id="delete">Delete</div></center>
           </div></div>
         <br><br>
         <div class="row">
         <div class="col-md-10">
          <table id="myTable" class="table table-striped table-bordered table-condensed">
            <tr>
            <!-- <td>id</td> -->
            <td>Product Name</td>
            <td>Product Unit</td>
            <td>Product Quantity</td>
            <td>Product Price</td>
            <td>Total</td>
            <td>select</td>
        </tr>
          </table>                  
  
     

    </div>
            </div><hr>
<div class="row">
  <div class="col-md-5">
  
           <div class="control-group">
            <label for="inputError" class="control-label">Date Of Delivery</label>
            <div class="controls">
              <input type="date" id="" name="DateOfDelivery" value="<?php echo set_value('DateOfDelivery'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Dispatch Thro</label>
            <div class="controls">
              <input type="text" id="" name="dispatchThru" value="<?php echo set_value('dispatchThru'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label">Place Of Delivery</label>
            <div class="controls">
              <input type="text" id="" name="PlaceOfDelivery" value="<?php echo set_value('PlaceOfDelivery'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Quality</label>
            <div class="controls">
              <input type="text" id="" name="Quality" value="<?php echo set_value('Quality'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
         <!--  <div class="control-group">
            <label for="inputError" class="control-label">Tax</label>
            <div class="controls">
              <input type="text" id="" name="Tax" value="<?php echo set_value('tax'); ?>" >
              
            </div> -->
          
           <div class="control-group">
            <label for="inputError" class="control-label">Discount</label>
            <div class="controls">
              <input type="text" id="" name="Discount" value="<?php echo set_value('Discount'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">PaymentTerms</label>
            <div class="controls">
              <input type="text" id="" name="PaymentTerm" value="<?php echo set_value('PaymentTerm'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          </div>

<div class="col-md-5">
          <div class="control-group">
            <label for="inputError" class="control-label" >Total</label>
            <div class="controls">
              <input type="text" id="total" name="Total" value="<?php echo set_value('Total'); ?>" onkeyup="gtotal();" readonly>
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Tax Type</label>
            <div class="controls">
              <select type="text" id="" name="Tax_type" value="<?php echo set_value('Tax_type'); ?>" >
              <option>Select Type</option>
              <option>VAT</option>
              <option>CST</option>
            </select>
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Tax</label>
            <div class="controls">
              <input type="text" id="tax" name="Tax" value="<?php echo set_value('Tax'); ?>" onkeyup="gtotal();">
                          
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label">Tax Amount</label>
            <div class="controls">
              <input  type="text" id="atax" name="TaxAmount" value="<?php echo set_value('TaxAmount'); ?>"onkeyup="gtotal();">
              
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label" >Packing And Forward</label>
            <div class="controls">
              <input type="text" id="paf" name="packingAndForward" value="<?php echo set_value('packingAndForward'); ?>" onkeyup="gtotal();">
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label" >service Charge</label>
            <div class="controls">
              <input type="text" id="sc" name="serviceCharge" value="<?php echo set_value('serviceCharge'); ?>" onkeyup="gtotal();">
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label" >Other Charges</label>
            <div class="controls">
              <input type="text" id="oc" name="Othercharges" value="<?php echo set_value('Othercharges'); ?>" onkeyup="gtotal();">
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label" >Total Amount</label>
            <div class="controls">
              <input type="text" id="gtot" name="TotalAmount" value="<?php echo set_value('TotalAmount'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label" >Advance Payment</label>
            <div class="controls">
              <input type="text" id="ap" name="AdvancePayment" value="<?php echo set_value('AdvancePayment'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label" >Balance</label>
            <div class="controls">
              <input type="text" id="bal" name="Balance" value="<?php echo set_value('Balance'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
    </div></div>
   
    
      </div>
      <div class="row">
         <div class="col-md-10">
             <div class="form-actions">
           <center><button class="btn btn-primary" type="submit">Save changes</button>
            <button class="btn" type="reset">Cancel</button></center>
          </div>
        </div>
      </div>
      <?php echo form_close(); ?>

    </div>
            </div>
          </div>
            