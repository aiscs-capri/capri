    <div class="container top">
    
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url("admin"); ?>">
            <?php echo ucfirst($this->uri->segment(1));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li>
          <a href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>">
            <?php echo ucfirst($this->uri->segment(2));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
          <a href="#">Update</a>
        </li>
      </ul>
      
      <div class="page-header">
        <h2>
          Updating <?php echo ucfirst($this->uri->segment(2));?>
        </h2>
      </div>

 
      <?php
      //flash messages
      if($this->session->flashdata('flash_message')){
        if($this->session->flashdata('flash_message') == 'updated')
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Well done!</strong> Product updated with success.';
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
          echo '</div>';          
        }
      }
      ?>
      
      <?php
      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '');

      //form validation
      echo validation_errors();

      echo form_open('admin/product/update/'.$this->uri->segment(4).'', $attributes);
      ?>
        <fieldset>
          <div class="control-group">
            <label for="inputError" class="control-label"></label>
            <div class="controls">
              <input type="hidden" id="" name="Product_Id" value="<?php echo $manufacture[0]['Product_Id']; ?>" >
            
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label">Product Code</label>
            <div class="controls">
              <input type="text" id="" name="ProductCode" value="<?php echo $manufacture[0]['ProductCode']; ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label">Product Name</label>
            <div class="controls">
              <input type="text" id="" name="ProductName" value="<?php echo $manufacture[0]['ProductName']; ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>

         <div class="control-group">
            <label for="inputError" class="control-label">Product Unit</label>
            
          <select id="" name="ProductUnit">

                      <?php
                        foreach ($Unit as $row) {
                          echo "<option value=".$UnitName = $row->UnitName.">".$UnitName = $row->UnitName."</option>"; 
                         
                        }
                      ?>
                      </select>

           <!-- <div class="control-group">
            <label for="inputError" class="control-label">Product Unit</label>
            <div class="controls">
              <input type="text" id="" name="ProductUnit" value="<?php echo $manufacture[0]['ProductUnit']; ?>" readonly>
             
            </div> -->
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Product Price</label>
            <div class="controls">
              <input type="text" id="" name="ProductPrice" value="<?php echo $manufacture[0]['ProductPrice']; ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
           <!-- <div class="control-group">
            <label for="inputError" class="control-label"></label>
            <div class="controls">
              <input type="hidden" id="" name="incentive" value="<?php echo $manufacture[0]['incentive']; ?>" >
              
            </div> 
          </div>-->
          <div class="form-actions">
            <button class="btn btn-primary" type="submit">Save changes</button>
            <button class="btn" type="reset">Cancel</button>
          </div>
        </fieldset>

      <?php echo form_close(); ?>

    </div>
     