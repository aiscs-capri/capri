    
 <script src="<?php echo base_url(); ?>js/jquery-1.11.1.js"></script>
 <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery-ui.css"> 
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/style.css"> 
  <script src="<?php echo base_url(); ?>js/jquery-1.11.0.min.js"></script>
  <script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
  <script src="<?php echo base_url(); ?>js/config.js"></script>
  <script>
  $(function() {
    //min
    $( ".datepickermin").datepicker({dateFormat: "yy-mm-dd",
      minDate: new Date()
    });
    //max
    $( ".datepickermax").datepicker({ dateFormat: "yy-M-dd"
         }).datepicker("setDate","0");
    
  });
    
  
  </script>
  <script>
$(document).ready(function() {
    $( "#product" ).change(function () {

    var str = "";
    $( "#product option:selected" ).each(function() {
      str += $( this ).val() + " ";
    });

  $.ajax({
            type : "get",
            url: serverurl+"/admin_purchase/get", 
            dataType: "json",
            data: {
                Product_Id : str
               },
            success: function(data){                          
               console.log(data.rows[0].ProductPrice);
               $("#pid").val(data.rows[0].Product_Id),
                $("#price").val(data.rows[0].ProductPrice),
                $("#unit").val(data.rows[0].ProductUnit);
            } ,
            error: function(data, errorThrown)
          {
              alert('request failed :'+errorThrown);
          }
        });    

  }).change(); 

     $("#addRecord").click(function(){
      console.log('test');
        var id = $("#product option:selected").val();
        var product = $("#product option:selected").text();
        var unit = $("#unit").val();
        var qun = $("#qun").val();
       /* var price = $("#price").val();
        var tot = $("#tot").val();*/
        var orderID = $("#box1").val();

   /* insert product details */
  
    $.ajax({
            type : "get",
            url: serverurl+"/admin_credit/orderdetailsinsert", 
            dataType: "json",
            data: {               
                product : product,
                unit : unit,
                qun : qun,
               /* price : price,
                tot : tot,*/
                Credit_Id : orderID 
               },
            success: function(data){                          
              
               /* get max record of prdoct details*/
                $.ajax({
            type : "get",
            url: serverurl+"/admin_credit/orderdetailsmaxvalue", 
            dataType: "json",                  
            success: function(data){                          
               console.log(data.row.Id);
                $("#myTable").append("<tr><td style=display:none;></td><td>"+product+"</td><td>"+unit+"</td><td>"+qun+"</td><td><input type=checkbox value="+data.row.Id+"></input></td></tr>");
                totalDisplay();
            } ,
            error: function(data, errorThrown)
          {
              alert('request failed :'+errorThrown);
          }
        });  

            } ,
            error: function(data, errorThrown)
          {
              alert('request failed :'+errorThrown);
          }
        });

  
        
      
    });

    
 $("#delete").click(function(){
        $("input[type=checkbox]:checked").each(function() {
        //console.log( );
        var str = $(this).val();
        $(this).parent().parent().remove(); 
        $.ajax({
            type : "get",
            url: serverurl+"/admin_credit/orderdetailsdelete", 
            dataType: "json",
            data: {
                id : str
               },
            success: function(data){                                      
               totalDisplay();
            } ,
            error: function(data, errorThrown)
          {
              alert('request failed :'+errorThrown);
          }
        });  

        
      });
  });
 //get supplier data

 
 $( "#customer" ).change(function () {

    var str = "";
    $( "#customer option:selected" ).each(function() {
      str += $( this ).val() + " ";
    });

  $.ajax({
            type : "get",
            url: serverurl+"/admin_credit/getcustomerdata", 
            dataType: "json",
            data: {
                Customer_Id : str
               },
            success: function(data){                          
               console.log(data.rows[0].BillState);
               $("#bil").val(data.rows[0].BillingAddress),
                $("#cst").val(data.rows[0].CST),
                $("#tin").val(data.rows[0].TIN);
            } ,
            error: function(data, errorThrown)
          {
              alert('request failed :'+errorThrown);
          }
        });    
  }).change(); 
});

    

//get table total values
/*$("#total").click(function(){
var $table = $('#myTable'),
    $tr = $table.find('tbody tr'),
    total = 0;

$tr.each(function(i, el){
    total += parseInt($(this).find('td:last').html(), 10);
});

$table.find('tfoot td:last').html(total);
 });
*/







</script>



    <div class="container top">
         <div class="row">
        <div class="span10 columns">
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url("admin"); ?>">
            <?php echo ucfirst($this->uri->segment(1));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li>
          <a href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>">
            <?php echo ucfirst($this->uri->segment(2));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
          <a href="#">update</a>
        </li>
      </ul>
    </div></div></div>
      
      <div class="page-header">
        <h2>
          Adding <?php echo ucfirst($this->uri->segment(2));?>
        </h2>
      
    </div>
      <?php
      //flash messages
      if(isset($flash_message)){
        if($flash_message == TRUE)
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Well done!</strong> New purchase created with success.';
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
          echo '</div>';          
        }
      }
      ?>
      
      <?php
      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '');
          //form validation
      echo validation_errors();
      
      echo form_open('admin/credit/update/'.$this->uri->segment(4).'', $attributes);
      ?>
       <div class="container">
           <div class="row">
               <div class="col-md-5">
                <div class="control-group">
            <label for="inputError" class="control-label"></label>
            <div class="controls">
              <input type="hidden" id="box1" name="Credit_Id" value="<?php echo $manufacture[0]['Credit_Id']; ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          

        </div>
          


          <div class="col-md-5">
         <div class="control-group">

            <label for="inputError" class="control-label">Customer Name</label>
            <div class="controls">
           <input type="text" id="cst" name="CustomerName" value="<?php echo $manufacture[0]['CustomerName']; ?>" readonly>
                  </div>
          </div>
                      <div class="control-group">
            <label for="inputError" class="control-label"> Address</label>
            <div class="controls">
              <textarea rows="5" cols="20" id="bil" name="BillingAddress"  readonly><?php echo $manufacture[0]['BillingAddress']; ?></textarea>
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>

          <div class="control-group">
            <label for="inputError" class="control-label" >CST</label>
            <div class="controls">
              <input type="text" id="cst" name="CST" value="<?php echo $manufacture[0]['CST']; ?>" readonly>
              
            </div>
          </div>
          
           <div class="control-group">
            <label for="inputError" class="control-label" >TIN</label>
            <div class="controls">
              <input type="text" id="tin" name="TIN" value="<?php echo $manufacture[0]['TIN']; ?>" readonly>
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
                    

          </div>
        </div>
        <hr>
         
            <div class="container">
         <div class="row">
               <div class="col-md-5">
              <div class="control-group">
            <label for="inputError" class="control-label">Product Name</label>
            <select id="product" name="Product_Id">
              <option value="">select</option>
                      <?php
                        foreach ($ProductName as $row) {
                          echo "<option value=".$Product_Id = $row->Product_Id.">".$ProductName = $row->ProductName."</option>"; 
                        
                        }


                      ?>
                      </select>
                    </div>
          <div class="control-group">
            <!-- <label for="inputError" class="control-label">product Id</label> -->
            <div class="controls">
              <input type="hidden" id="pid" name="Product_Id" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>

         <!--  <div class="control-group">
            <label for="inputError" class="control-label" >product price</label>
            <div class="controls">
              <input type="text" id="price" name="ProductPrice"onkeyup="sum();" >
              
            </div>
          </div>
 -->
        </div>
          <div class="col-md-5">
           <div class="control-group">
            <label for="inputError" class="control-label" >product Unit</label>
            <div class="controls">
              <input type="text" id="unit" name="ProductUnit" readonly>
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
                <div class="control-group">
            <label for="inputError" class="control-label">Qunatity</label>
            <div class="controls">
              <input type="text" id="qun" name="Quantity" value="<?php echo set_value('Quantity'); ?>" onkeyup="sum();">
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>

          <!-- <div class="control-group">
            <label for="inputError" class="control-label">Amount</label>
            <div class="controls">
              <input type="text" id="tot" name="Total" value="<?php echo set_value('Total'); ?>" readonly>
              
            </div>
          </div>
 -->


        </div></div>
          <div class="row">
         <div class="col-md-10">
          <center>
          <div class="btn btn-success" id="addRecord">Add</div>
           <div class="btn btn-danger" id="delete">Delete</div></center>
           </div></div>
         <br><br>
        <div class="row">
         <div class="col-md-10">
          <table id="myTable" class="table table-striped table-bordered table-condensed">
            <tr>
            <td style=display:none;>id</td>
            <td>Product Name</td>
            <td>Product Unit</td>
            <td>Product Qunatity</td>
            
            <td>select</td>
        </tr>
        <tbody>
               <?php
              $i =1;
              foreach($pop as $row)
              {                
                echo '<tr>';
                echo '<td style=display:none;>'.$row->Id.'</td>';
                echo '<td>'.$row->ProductName.'</td>';
                echo '<td>'.$row->ProductUnit.'</td>';
                echo '<td>'.$row->Quantity.'</td>';
                
                echo '<td><input type=checkbox value='.$row->Id.'></input></td>';
                echo '</tr>';
                $i++;   
              }
              ?>     
             
           </tbody>
          </table>                  
  
      

    </div>
            </div><hr>
<div class="row">

          
           <div class="col-md-5">
    <div class="control-group">
            <label for="inputError" class="control-label">Reson For Debit</label>
            <div class="controls">
              <input type="text" id="box2" name="Reson" value="<?php echo $manufacture[0]['Reson']; ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
    <div class="control-group">
            <label for="inputError" class="control-label">Invoice Numer</label>
            <div class="controls">
              <input type="text" id="box2" name="InvoiceNumber" value="<?php echo $manufacture[0]['InvoiceNumber']; ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Invoice Date</label>
            <div class="controls">
              <input type="text" id="result" name="InvoiceDate"  value="<?php $date = $manufacture[0]['InvoiceDate'];  echo date('Y-M-d',strtotime($date)); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
  <div class="control-group">
            <label for="inputError" class="control-label">Return Date</label>
            <div class="controls">
              <input type="text" id="result" name="ReturnDate" value="<?php $date = $manufacture[0]['ReturnDate'];  echo date('Y-M-d',strtotime($date)); ?>"  >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
  
           
          </div>
      </div>
      <div class="row">
         <div class="col-md-10">
             <div class="form-actions">
           <center><button class="btn btn-primary" type="submit">Save changes</button>
            <button class="btn" type="reset">Cancel</button></center>
          </div>
        </div>
      </div>
      <?php echo form_close(); ?>

    </div>
            </div>
          </div>
            