 <div class="container top">
      <div class="row">
     <div class="span10 columns">
        <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url("admin"); ?>">
            <?php echo ucfirst($this->uri->segment(1));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
          <?php echo ucfirst($this->uri->segment(2));?>
        </li>
      </ul>

      <div class="page-header users-header">
        <h2>
          Purchase Report
          
        </h2>
      </div>
      
      <?php
      //flash messages
      if(isset($flash_message)){
        if($flash_message == TRUE)
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Well done!</strong> New purchase created with success.';
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
          echo '</div>';          
        }
      }
      ?>

    </div>
   </div>
       <div class="span10 columns">
          <div class="well">
           
             <?php
           
            $attributes = array('class' => 'form-inline reset-margin', 'id' => 'myform');
                      
            //save the columns names in a array that we will use as filter         
            $options_purchase = array();    
            foreach ($capri_purchase_inward as $array) {
              foreach ($array as $key => $value) {
                $options_purchase[$key] = $key;
              }
              break;
            }

            echo form_open('admin/purchasereport', $attributes);
     
             /* echo form_label('Search:', 'search_string');
              echo form_input('search_string', $search_string_selected, 'style="width: 170px;
height: 26px;"');*/

             /* echo form_label('Filter by manufacturer:', 'Supplier_Id');
              echo form_dropdown('Supplier_Id', $options_manufacture, $manufacture_selected, 'class="span2"');
*/
              echo form_label('Order by:', 'order');
              echo form_dropdown('order', $options_purchase, $order, 'class="span3"');

              $data_submit = array('name' => 'mysubmit', 'class' => 'btn btn-primary', 'value' => 'Go');

              $options_order_type = array('Asc' => 'Asc', 'Desc' => 'Desc');
              echo form_dropdown('order_type', $options_order_type, $order_type_selected, 'class="span3"');

              echo form_submit($data_submit);

            echo form_close();
            ?>

          </div>

          <table class="table table-striped table-bordered table-condensed">
            <thead>
              <tr>
               <!--  <th class="header">Order Id</th> -->
                <th class="yellow header headerSortDown">Inward Number</th>
                <th class="yellow header headerSortDown">Inward Date</th>
                <!-- <th class="yellow header headerSortDown">SupplierName</th> -->
                 <th class="yellow header headerSortDown">Tax Amount</th>
                 <th class="yellow header headerSortDown">Gross Total</th>
                <th class="yellow header headerSortDown">AdvancePayment</th>
                <th class="yellow header headerSortDown">Balance</th>
                
                
              </tr>
            </thead>
            <tbody>
              <?php
              foreach($capri_purchase_inward as $row)
              {
                echo '<tr>';
                /*echo '<td>'.$row['Order_Id'].'</td>';*/
                echo '<td>'.$row['inward_number'].'</td>';
                echo '<td>'.date('Y-M-d',strtotime($row['inwardDate'])).'</td>';
                /*echo '<td>'.$row['SupplierName'].'</td>';*/
                echo '<td>'.$row['TaxAmount'].'</td>';
                echo '<td>'.$row['GrossTotal'].'</td>';
                echo '<td>'.$row['AdvancePayment'].'</td>';
                echo '<td>'.$row['Balance'].'</td>';
                
                  echo '</tr>';
              }
              ?>      
            </tbody>
          </table>

          <?php echo '<div class="pagination">'.$this->pagination->create_links().'</div>'; ?>
       </div>
   
  
    </div>