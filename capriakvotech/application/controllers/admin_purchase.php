<?php
class Admin_purchase extends CI_Controller {

    /**
    * name of the folder responsible for the views 
    * which are manipulated by this controller
    * @constant string
    */
    const VIEW_FOLDER = 'admin/purchase';
 
    /**
    * Responsable for auto load the model
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('purchase_model');
         $this->load->model('supplier_model');
         $this->load->model('product_model');
         $this->load->model('order_model');
         $this->load->model('value_model');
          $this->load->library('mpdf');


        if(!$this->session->userdata('is_logged_in')){
            redirect('admin/login');
        }
    }
 
    /**
    * Load the main view with all the current model model's data.
    * @return void
    */
     public function index()
    {
       
        //all the posts sent by the view
        $Total = $this->input->post('Total');        
        $search_string = $this->input->post('search_string');        
        $order = $this->input->post('order'); 
        $order_type = $this->input->post('order_type'); 

        //pagination settings
        $config['per_page'] = 20;
        $config['base_url'] = base_url().'admin/purchase';
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = 20;
        $config['full_tag_open'] = '<ul>';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';

        //limit end
        $page = $this->uri->segment(3);

        //math to get the initial record to be select in the database
        $limit_end = ($page * $config['per_page']) - $config['per_page'];
        if ($limit_end < 0){
            $limit_end = 0;
        } 

        //if order type was changed
        if($order_type){
            $filter_session_data['order_type'] = $order_type;
        }
        else{
            //we have something stored in the session? 
            if($this->session->userdata('order_type')){
                $order_type = $this->session->userdata('order_type');    
            }else{
                //if we have nothing inside session, so it's the default "Asc"
                $order_type = 'Asc';    
            }
        }
        //make the data type var avaible to our view
        $data['order_type_selected'] = $order_type;        


        //we must avoid a page reload with the previous session data
        //if any filter post was sent, then it's the first time we load the content
        //in this case we clean the session filter data
        //if any filter post was sent but we are in some page, we must load the session data

        //filtered && || paginated
        if($Total !== false && $search_string !== false && $order !== false || $this->uri->segment(3) == true){ 
           
            /*
            The comments here are the same for line 79 until 99

            if post is not null, we store it in session data array
            if is null, we use the session data already stored
            we save order into the the var to load the view with the param already selected       
            */

            if($Total !== 0){
                $filter_session_data['manufacture_selected'] = $Total;
            }else{
                $Total = $this->session->userdata('manufacture_selected');
            }
            $data['manufacture_selected'] = $Total;

            if($search_string){
                $filter_session_data['search_string_selected'] = $search_string;
            }else{
                $search_string = $this->session->userdata('search_string_selected');
            }
            $data['search_string_selected'] = $search_string;

            if($order){
                $filter_session_data['order'] = $order;
            }
            else{
                $order = $this->session->userdata('order');
            }
            $data['order'] = $order;

            //save session data into the session
            $this->session->set_userdata($filter_session_data);

            //fetch manufacturers data into arrays
           // $data['manufactures'] = $this->purchase_model->get_manufacturers();

           // $data['purchase'] = $this->product_model->get_manufacturers();

            $data['count_purchase']= $this->purchase_model->count_purchase($Total, $search_string, $order);
            $config['total_rows'] = $data['count_purchase'];

            //fetch sql data into arrays
            if($search_string){
                if($order){
                    $data['purchase'] = $this->purchase_model->get_purchase($Total, $search_string, $order, $order_type, $config['per_page'],$limit_end);        
                }else{
                    $data['purchase'] = $this->purchase_model->get_purchase($Total, $search_string, '', $order_type, $config['per_page'],$limit_end);           
                }
            }else{
                if($order){
                    $data['purchase'] = $this->purchase_model->get_purchase($Total, '', $order, $order_type, $config['per_page'],$limit_end);        
                }else{
                    $data['purchase'] = $this->purchase_model->get_purchase($Total, '', '', $order_type, $config['per_page'],$limit_end);        
                }
            }

        }else{

            //clean filter data inside section
            $filter_session_data['manufacture_selected'] = null;
            $filter_session_data['search_string_selected'] = null;
            $filter_session_data['order'] = null;
            $filter_session_data['order_type'] = null;
            $this->session->set_userdata($filter_session_data);

            //pre selected options
            $data['search_string_selected'] = '';
            $data['manufacture_selected'] = 0;
            $data['order'] = 'id';

            //fetch sql data into arrays
            $data['purchase'] = $this->product_model->get_product();

            $data['manufactures'] = $this->supplier_model->get_supplier();
            $data['count_purchase']= $this->purchase_model->count_purchase();
            $data['capri_purchase_order'] = $this->purchase_model->get_purchase('', '', '', $order_type, $config['per_page'],$limit_end);        
            $config['total_rows'] = $data['count_purchase'];

        }//!isset($Total) && !isset($search_string) && !isset($order)

        //initializate the panination helper 
        $this->pagination->initialize($config);   

        //load the view
        $data['main_content'] = 'admin/purchase/list';
        $this->load->view('includes/template', $data);  

    }//index

    public function add()
    {

        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {

            //form validation
             $this->form_validation->set_rules('Order_Id', 'Order_Id');
            $this->form_validation->set_rules('Order_number', 'Order_number', 'required');
            $this->form_validation->set_rules('OrderDate', 'OrderDate', 'required');
            $this->form_validation->set_rules('Supplier_Id', 'Supplier_Id', 'required');
   
            $this->form_validation->set_rules('DateOfDelivery', 'DateOfDelivery');
            $this->form_validation->set_rules('PlaceOfDelivery', 'PlaceOfDelivery');
            $this->form_validation->set_rules('Quality', 'Quality');
            $this->form_validation->set_rules('PaymentTerm', 'PaymentTerm');
            $this->form_validation->set_rules('Note', 'Note');
            $this->form_validation->set_rules('Total', 'Total', 'required');
            $this->form_validation->set_rules('Tax_type', 'Tax_type', 'required');
            $this->form_validation->set_rules('Tax', 'Tax', 'required');
            $this->form_validation->set_rules('TaxAmount', 'Tax', 'required');
            $this->form_validation->set_rules('GrossTotal', 'GrossTotal', 'required');
           
           


            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            

            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
                /* $dat = $this->input->post('OrderDate');
                 $date = date_create_from_format('d/M/Y:H:i:s', $dat);
                 echo $date;
                 echo date("Y-m-d :H:i:s",strtotime($dat));
                 echo "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb".$this->input->post('OrderDate');
                 echo "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa".$dat;exit;
*/
                $data_to_store = array(
                    'Order_Id' => $this->input->post('Order_Id'),
                    'Order_number' => $this->input->post('Order_number'),
                    'OrderDate' => date("Y-m-d",strtotime($this->input->post('OrderDate'))),
                    'OurRef' => $this->input->post('OurRef'),
                    'Supplier_Id' => $this->input->post('Supplier_Id'),
                    'DateOfDelivery' => date("Y-m-d :H:i:s",strtotime($this->input->post('DateOfDelivery'))),
                    'PlaceOfDelivery' => $this->input->post('PlaceOfDelivery'),
                    'Quality' => $this->input->post('Quality'),
                    'Note' => $this->input->post('Note'),
                    'PaymentTerm' => $this->input->post('PaymentTerm'),
                    'Total' => $this->input->post('Total'),
                    'Tax_type' => $this->input->post('Tax_type'),
                    'Tax' => $this->input->post('Tax'),
                    'TaxAmount' => $this->input->post('TaxAmount'),
                    'GrossTotal' => $this->input->post('GrossTotal'),
                    'CreatedBy' => $this->session->userdata('user_id'),
                    'CreatedDate' => date("Y-m-d H:i:s")

                    
                );
                //if the insert has returned true then we show the flash message
                if($this->purchase_model->store_manufacture($data_to_store)){
                    $data['flash_message'] = TRUE; 


        $id = $this->purchase_model->maxvalue();

        $pro = $_POST['id'];
        $product = $_POST['product'];
        $unit = $_POST['unit'];
        $qun = $_POST['qun'];
        $tot = $_POST['tot'];
        $price = $_POST['price'];
        $order = $id->row('Order_Id');


        for($i=0; $i<sizeof($pro); $i++)
        {
        $data1['ProductName']= $product[$i];
        $data1['ProductUnit']= $unit[$i];
        $data1['Quantity']= $qun[$i];
        $data1['ProductPrice']= $price[$i];
        $data1['Total']= $tot[$i];
        $data1['Order_Id']= $order;
        $data1['CreatedBy'] = $this->session->userdata('user_id');
        $data1['CreatedDate'] = date("Y-m-d H:i:s");
            
        $this->value_model->getProduct($data1);

    }
                }else{
                    $data['flash_message'] = FALSE; 
                }

            }

        }
        //fetch manufactures data to populate the select field
        $data['manufactures'] = $this->supplier_model->get_supplier();
        $data['purchaseorder'] = $this->product_model->get_product();
        $data['price'] = $this->product_model->get_product();
        $data['product'] = $this->product_model->get_product();

        $supplier = $this->purchase_model->getsuppliername();
        $product = $this->purchase_model->getproductname();
        /*$getProduct=$this->purchase_model->getproductdetail('Product_Id');*/

        //load the view
        $data['SupplierName'] =$supplier;
        $data['ProductName'] =$product;
        $data['main_content'] = 'admin/purchase/add';
        $this->load->view('includes/template', $data);  
 
    }       

    /**
    * Update item by his id
    * @return void
    */

public function get()
{

    $Product_Id=$_GET['Product_Id'];
    $this->purchase_model->getproductdetail('Product_Id');
    $productdetail["rows"] = $this->purchase_model->getproductdetail($Product_Id);
        echo json_encode($productdetail);

}

public function getsupplierdata()
{

    $Supplier_Id=$_GET['Supplier_Id'];
    $this->purchase_model->getsupplierdetail('Supplier_Id');
     $supplierdetail["rows"] = $this->purchase_model->getsupplierdetail($Supplier_Id);
        echo json_encode($supplierdetail);

}




    


    public function update()
    {
        //product id 
        $id = $this->uri->segment(4);
  
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {
            //form validation
             $this->form_validation->set_rules('Order_Id', 'Order_Id', 'required|numeric');
            $this->form_validation->set_rules('Order_number', 'Order_number', 'required');
            $this->form_validation->set_rules('Total', 'Total', 'required');
            $this->form_validation->set_rules('DateOfDelivery', 'DateOfDelivery');
            $this->form_validation->set_rules('PlaceOfDelivery', 'PlaceOfDelivery');
            $this->form_validation->set_rules('Quality', 'Quality');
            $this->form_validation->set_rules('Note', 'Note');
            $this->form_validation->set_rules('PaymentTerm', 'PaymentTerm');
            $this->form_validation->set_rules('Total', 'Total', 'required');
            $this->form_validation->set_rules('Tax_type', 'Tax_type', 'required');
            $this->form_validation->set_rules('Tax', 'Tax', 'required');
            $this->form_validation->set_rules('TaxAmount', 'TaxAmount', 'required');
            $this->form_validation->set_rules('GrossTotal', 'GrossTotal', 'required');
           
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
    
                $data_to_store = array(
                    'Order_Id' => $this->input->post('Order_Id'),
                    'Order_number' => $this->input->post('Order_number'),
                    'OrderDate' => date("Y-m-d",strtotime($this->input->post('OrderDate'))),
                    'OurRef' => $this->input->post('OurRef'),
                    'Supplier_Id' => $this->input->post('Supplier_Id'),
                    'DateOfDelivery' => date("Y-m-d :H:i:s",strtotime($this->input->post('DateOfDelivery'))),
                    'PlaceOfDelivery' => $this->input->post('PlaceOfDelivery'),
                    'Quality' => $this->input->post('Quality'),
                    'Note' => $this->input->post('Note'),
                    'PaymentTerm' => $this->input->post('PaymentTerm'),
                    'Total' => $this->input->post('Total'),
                    'Tax_type' => $this->input->post('Tax_type'),
                    'Tax' => $this->input->post('Tax'),
                    'TaxAmount' => $this->input->post('TaxAmount'),
                    'GrossTotal' => $this->input->post('GrossTotal'),
                    'CreatedBy' => $this->session->userdata('user_id'),
                    'CreatedDate' => date("Y-m-d H:i:s")

                );
                //if the insert has returned true then we show the flash message
                if($this->purchase_model->update_manufacture($id, $data_to_store) == TRUE){
                    $this->session->set_flashdata('flash_message', 'updated');
                }else{
                    $this->session->set_flashdata('flash_message', 'not_updated');
                }
                redirect('admin/purchase/update/'.$id.'');

            }//validation run

        }

        //if we are updating, and the data did not pass trough the validation
        //the code below wel reload the current data

        //product data 
        $data['manufacture'] = $this->purchase_model->get_manufacture_by_Order_Id($id);
        

       

        $order=$this->purchase_model->getPurchaseOrderDetails($id);
        $orderpro=$this->purchase_model->getPurchaseOrderProductDetails($id);
        $product = $this->purchase_model->getproductname();


        
        
        //load the view
        
        $data['po']=$order;
        $data['pop']=$orderpro;
        $data['ProductName'] =$product;
        //load the view
        $data['main_content'] = 'admin/purchase/edit';
        $this->load->view('includes/template', $data);            

    }//update

    /**
    * Delete product by his id
    * @return void
    */
    public function view()
    {
        //product id 
        $id = $this->uri->segment(4);
  
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {
           //form validation
             /*$this->form_validation->set_rules('Order_Id', 'Order_Id');
            $this->form_validation->set_rules('Order_number', 'Order_number', 'required');
            $this->form_validation->set_rules('Quantity', 'Quantity', 'required');
            $this->form_validation->set_rules('Total', 'Total', 'required');
            $this->form_validation->set_rules('DateOfDelivery', 'DateOfDelivery');
            $this->form_validation->set_rules('PlaceOfDelivery', 'PlaceOfDelivery');
            $this->form_validation->set_rules('Quality', 'Quality');
            $this->form_validation->set_rules('Note', 'Note');
            $this->form_validation->set_rules('PaymentTerm', 'PaymentTerm');
            $this->form_validation->set_rules('Total', 'Total', 'required');
            $this->form_validation->set_rules('Tax_type', 'Tax_type', 'required');
            $this->form_validation->set_rules('Tax', 'Tax', 'required');
            $this->form_validation->set_rules('TaxAmount', 'TaxAmount', 'required');
            $this->form_validation->set_rules('GrossTotal', 'GrossTotal', 'required');*/

            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
    
                $data_to_store = array(
                   'Order_Id' => $this->input->post('Order_Id'),
                    'Order_number' => $this->input->post('Order_number'),
                    'Quantity' => $this->input->post('Quantity'),
                    'Total' => $this->input->post('Total'),
                    'DateOfDelivery' => $this->input->post('DateOfDelivery'),
                    'PlaceOfDelivery' => $this->input->post('PlaceOfDelivery'),
                    'Quality' => $this->input->post('Quality'),
                    'Note' => $this->input->post('Note'),
                    'PaymentTerm' => $this->input->post('PaymentTerm'),
                     'Total' => $this->input->post('Total'),
                    'Tax_type' => $this->input->post('Tax_type'),
                    'Tax' => $this->input->post('Tax'),
                    'TaxAmount' => $this->input->post('TaxAmount'),
                    'GrossTotal' => $this->input->post('GrossTotal'),
                );
                //if the insert has returned true then we show the flash message
                if($this->purchase_model->update_manufacture($id, $data_to_store) == TRUE){
                    $this->session->set_flashdata('flash_message', 'updated');
                }else{
                    $this->session->set_flashdata('flash_message', 'not_updated');
                }
                redirect('admin/purchase/view/'.$id.'');

            }//validation run

        }

        //if we are updating, and the data did not pass trough the validation
        //the code below wel reload the current data

        //product data 
        $data['manufacture'] = $this->purchase_model->get_manufacture_by_Order_Id($id);
        
        $order=$this->purchase_model->getPurchaseOrderDetails($id);
        $orderpro=$this->purchase_model->getPurchaseOrderProductDetails($id);
        
        /*$getProduct=$this->purchase_model->getproductdetail('Product_Id');*/

        //load the view
        $data['po']=$order;
        $data['pop']=$orderpro;
        //load the view
        $data['main_content'] = 'admin/purchase/view';
        $this->load->view('includes/template', $data);            

    }//update

 public function  orderdetailsinsert()
    {
 
        
        $product = $_GET['product'];
        $unit = $_GET['unit'];
        $qun = $_GET['qun'];
        $price = $_GET['price'];
        $tot = $_GET['tot'];
        $order = $_GET['Order_Id'];

        
        $data['ProductName']= $product;
        $data['ProductUnit']= $unit;
        $data['Quantity']= $qun;
        $data['ProductPrice']= $price;
        $data['Total']= $tot;
        $data['Order_Id']= $order;
        $data['CreatedBy'] = $this->session->userdata('user_id');
        $data['CreatedDate'] = date("Y-m-d H:i:s");
            
        $resdata["row"] = $this->Value_model->getProduct($data);
        echo json_encode($resdata);

    }

    public function  orderdetailsdelete()
    {
 
        $pro = $_GET['id'];        
        
        $this->purchase_model->delete_order_details($pro);
        $deleteProduct = array(
        'message' => true
        );
        $resdata["row"] = $deleteProduct;
        echo json_encode($resdata);

     }
     public function  orderdetailsmaxvalue()
    {
       $id = $this->purchase_model->order_details_maxvalue();

       $sam = array(
        'Id' => $id->row('Id')
        );
       $resdata["row"] = $sam;
        echo json_encode($resdata);
    }
    /**
    * Delete product by his id
    * @return void
    */
    public function delete()
    {
        //product id 
        $id = $this->uri->segment(4);
        $this->purchase_model->purchase_order_delete_details($id);
        

         if($this->purchase_model->delete_manufacture($id) == TRUE){
                    $this->session->set_flashdata('flash_message', 'updated');
                }else{
                    $this->session->set_flashdata('flash_message', 'not_updated');
                }
        redirect('admin/purchase');
    }//edit

    public function report()
    {


        $id = $this->uri->segment(3);

       /* redirect('admin/purchase/report/'.$id.'');*/
        $data['manufacture'] = $this->purchase_model->get_manufacture_by_Order_Id($id);
        
        $order=$this->purchase_model->getPurchaseOrderDetails($id);
        $orderpro=$this->purchase_model->getPurchaseOrderProductDetails($id);
        
        //load the view
        $data['po']=$order;
        $data['pop']=$orderpro;
        $html = $this->load->view('admin/purchase/report', $data, TRUE);

        $mpdf=new mPDF();

        $mpdf=new mPDF('c','A4','','',32,25,27,25,16,13);

        $mpdf->WriteHTML($html,2);

        $mpdf->Output('mpdf.pdf','I');
    }

}
