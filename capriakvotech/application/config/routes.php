<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the login_user guide for complete details:
|
|	http://codeigniter.com/login_user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
$route['default_controller'] = 'login_user/index';
$route['404_override'] = '';

/*admin*/
$route['admin'] = 'login_user/index';
$route['admin/signup'] = 'login_user/signup';
$route['admin/create_member'] = 'login_user/create_member';
$route['admin/login'] = 'login_user/index';
$route['admin/logout'] = 'login_user/logout';
$route['admin/login/validate_credentials'] = 'login_user/validate_credentials';

$route['admin/products'] = 'admin_products/index';
$route['admin/products/add'] = 'admin_products/add';
$route['admin/products/update'] = 'admin_products/update';
$route['admin/products/update/(:any)'] = 'admin_products/update/$1';
$route['admin/products/delete/(:any)'] = 'admin_products/delete/$1';
$route['admin/products/(:any)'] = 'admin_products/index/$1'; //$1 = page number

$route['admin/unit'] = 'admin_unit/index';
$route['admin/unit/add'] = 'admin_unit/add';
$route['admin/unit/update'] = 'admin_unit/update';
$route['admin/unit/update/(:any)'] = 'admin_unit/update/$1';
$route['admin/unit/delete/(:any)'] = 'admin_unit/delete/$1';
$route['admin/unit/(:any)'] = 'admin_unit/index/$1'; //$1 = page number

$route['admin/product'] = 'admin_product/index';
$route['admin/product/add'] = 'admin_product/add';
$route['admin/product/update'] = 'admin_product/update';
$route['admin/product/update/(:any)'] = 'admin_product/update/$1';
$route['admin/product/delete/(:any)'] = 'admin_product/delete/$1';
$route['admin/product/view/(:any)'] = 'admin_product/view/$1';
$route['admin/product/(:any)'] = 'admin_product/index/$1'; //$1 = page number


$route['admin/customer'] = 'admin_customer/index';
$route['admin/customer/add'] = 'admin_customer/add'; 
$route['admin/customer/update'] = 'admin_customer/update';
$route['admin/customer/update/(:any)'] = 'admin_customer/update/$1';
$route['admin/customer/delete/(:any)'] = 'admin_customer/delete/$1';
$route['admin/customer/view/(:any)'] = 'admin_customer/view/$1';
$route['admin/customer/(:any)'] = 'admin_customer/index/$1'; //$1 = page number

$route['admin/supplier'] = 'admin_supplier/index';
$route['admin/supplier/add'] = 'admin_supplier/add';
$route['admin/supplier/update'] = 'admin_supplier/update';
$route['admin/supplier/update/(:any)'] = 'admin_supplier/update/$1';
$route['admin/supplier/delete/(:any)'] = 'admin_supplier/delete/$1';
$route['admin/supplier/view/(:any)'] = 'admin_supplier/view/$1';
$route['admin/supplier/(:any)'] = 'admin_supplier/index/$1'; //$1 = page number


$route['admin/employee'] = 'admin_employee/index';
$route['admin/employee/add'] = 'admin_employee/add';
$route['admin/employee/update'] = 'admin_employee/update';
$route['admin/employee/update/(:any)'] = 'admin_employee/update/$1';
$route['admin/employee/delete/(:any)'] = 'admin_employee/delete/$1';
$route['admin/employee/view/(:any)'] = 'admin_employee/view/$1';
$route['admin/employee/(:any)'] = 'admin_employee/index/$1'; //$1 = page number


$route['admin/purchase'] = 'admin_purchase/index';
$route['admin/purchase/add'] = 'admin_purchase/add'; 
$route['admin/purchase/order'] = 'admin_purchase/order';
$route['admin/purchase/update'] = 'admin_purchase/update';
$route['admin/purchase/update/(:any)'] = 'admin_purchase/update/$1';
$route['admin/purchase/delete/(:any)'] = 'admin_purchase/delete/$1';
$route['admin/purchase/view/(:any)'] = 'admin_purchase/view/$1';
$route['admin/purchase/(:any)'] = 'admin_purchase/index/$1'; //$1 = page number


$route['admin/inward'] = 'admin_inward/index';
$route['admin/inward/add'] = 'admin_inward/add'; 
$route['admin/inward/update'] = 'admin_inward/update';
$route['admin/inward/update/(:any)'] = 'admin_inward/update/$1';
$route['admin/inward/delete/(:any)'] = 'admin_inward/delete/$1';
$route['admin/inward/view/(:any)'] = 'admin_inward/view/$1';
$route['admin/inward/(:any)'] = 'admin_inward/index/$1'; //$1 = page number


$route['admin/outpay'] = 'admin_outpay/index';
$route['admin/outpay/add'] = 'admin_outpay/add';
$route['admin/outpay/update'] = 'admin_outpay/update';
$route['admin/outpay/update/(:any)'] = 'admin_outpay/update/$1';
$route['admin/outpay/delete/(:any)'] = 'admin_outpay/delete/$1';
$route['admin/outpay/view/(:any)'] = 'admin_outpay/view/$1';
$route['admin/outpay/(:any)'] = 'admin_outpay/index/$1'; //$1 = page number


$route['admin/marketing'] = 'admin_marketing/index';
$route['admin/marketing/add'] = 'admin_marketing/add';
$route['admin/marketing/update'] = 'admin_marketing/update';
$route['admin/marketing/update/(:any)'] = 'admin_marketing/update/$1';
$route['admin/marketing/delete/(:any)'] = 'admin_marketing/delete/$1';
$route['admin/marketing/view/(:any)'] = 'admin_marketing/view/$1';
$route['admin/marketing/(:any)'] = 'admin_marketing/index/$1'; //$1 = page number

$route['admin/lead'] = 'admin_lead/index';
$route['admin/lead/add'] = 'admin_lead/add'; 
$route['admin/lead/order'] = 'admin_lead/order';
$route['admin/lead/update'] = 'admin_lead/update';
$route['admin/lead/update/(:any)'] = 'admin_lead/update/$1';
$route['admin/lead/delete/(:any)'] = 'admin_lead/delete/$1';
$route['admin/lead/view/(:any)'] = 'admin_lead/view/$1';
$route['admin/lead/(:any)'] = 'admin_lead/index/$1'; //$1 = page number


$route['admin/salesquotation'] = 'admin_salesquo/index';
$route['admin/salesquotation/add'] = 'admin_salesquo/add'; 
$route['admin/salesquotation/order'] = 'admin_salesquo/order';
$route['admin/salesquotation/update'] = 'admin_salesquo/update';
$route['admin/salesquotation/update/(:any)'] = 'admin_salesquo/update/$1';
$route['admin/salesquotation/delete/(:any)'] = 'admin_salesquo/delete/$1';
$route['admin/salesquotation/view/(:any)'] = 'admin_salesquo/view/$1';
$route['admin/salesquotation/(:any)'] = 'admin_salesquo/index/$1'; //$1 = page number

$route['admin/proforma'] = 'admin_proforma/index';
$route['admin/proforma/add'] = 'admin_proforma/add'; 
$route['admin/proforma/order'] = 'admin_proforma/order';
$route['admin/proforma/update'] = 'admin_proforma/update';
$route['admin/proforma/update/(:any)'] = 'admin_proforma/update/$1';
$route['admin/proforma/delete/(:any)'] = 'admin_proforma/delete/$1';
$route['admin/proforma/view/(:any)'] = 'admin_proforma/view/$1';
$route['admin/proforma/(:any)'] = 'admin_proforma/index/$1'; //$1 = page number


$route['admin/salesinvoice'] = 'admin_salesinvoice/index';
$route['admin/salesinvoice/add'] = 'admin_salesinvoice/add'; 
$route['admin/salesinvoice/order'] = 'admin_salesinvoice/order';
$route['admin/salesinvoice/update'] = 'admin_salesinvoice/update';
$route['admin/salesinvoice/update/(:any)'] = 'admin_salesinvoice/update/$1';
$route['admin/salesinvoice/delete/(:any)'] = 'admin_salesinvoice/delete/$1';
$route['admin/salesinvoice/view/(:any)'] = 'admin_salesinvoice/view/$1';
$route['admin/salesinvoice/(:any)'] = 'admin_salesinvoice/index/$1'; //$1 = page number


$route['admin/enquiry'] = 'admin_enquiry/index';
$route['admin/enquiry/add'] = 'admin_enquiry/add'; 
$route['admin/enquiry/order'] = 'admin_enquiry/order';
$route['admin/enquiry/update'] = 'admin_enquiry/update';
$route['admin/enquiry/update/(:any)'] = 'admin_enquiry/update/$1';
$route['admin/enquiry/delete/(:any)'] = 'admin_enquiry/delete/$1';
$route['admin/enquiry/view/(:any)'] = 'admin_enquiry/view/$1';
$route['admin/enquiry/(:any)'] = 'admin_enquiry/index/$1'; //$1 = page number


$route['admin/delivery'] = 'admin_delivery/index';
$route['admin/delivery/add'] = 'admin_delivery/add'; 
$route['admin/delivery/order'] = 'admin_delivery/order';
$route['admin/delivery/update'] = 'admin_delivery/update';
$route['admin/delivery/update/(:any)'] = 'admin_delivery/update/$1';
$route['admin/delivery/delete/(:any)'] = 'admin_delivery/delete/$1';
$route['admin/delivery/view/(:any)'] = 'admin_delivery/view/$1';
$route['admin/delivery/(:any)'] = 'admin_delivery/index/$1'; //$1 = page number


$route['admin/income'] = 'admin_income/index';
$route['admin/income/add'] = 'admin_income/add'; 
$route['admin/income/order'] = 'admin_income/order';
$route['admin/income/update'] = 'admin_income/update';
$route['admin/income/update/(:any)'] = 'admin_income/update/$1';
$route['admin/income/delete/(:any)'] = 'admin_income/delete/$1';
$route['admin/income/view/(:any)'] = 'admin_income/view/$1';
$route['admin/income/(:any)'] = 'admin_income/index/$1'; //$1 = page number

$route['admin/debit'] = 'admin_debit/index';
$route['admin/debit/add'] = 'admin_debit/add'; 
$route['admin/debit/order'] = 'admin_debit/order';
$route['admin/debit/update'] = 'admin_debit/update';
$route['admin/debit/update/(:any)'] = 'admin_debit/update/$1';
$route['admin/debit/delete/(:any)'] = 'admin_debit/delete/$1';
$route['admin/debit/view/(:any)'] = 'admin_debit/view/$1';
$route['admin/debit/(:any)'] = 'admin_debit/index/$1'; //$1 = page number


$route['admin/credit'] = 'admin_credit/index';
$route['admin/credit/add'] = 'admin_credit/add'; 
$route['admin/credit/order'] = 'admin_credit/order';
$route['admin/credit/update'] = 'admin_credit/update';
$route['admin/credit/update/(:any)'] = 'admin_credit/update/$1';
$route['admin/credit/delete/(:any)'] = 'admin_credit/delete/$1';
$route['admin/credit/view/(:any)'] = 'admin_credit/view/$1';
$route['admin/credit/(:any)'] = 'admin_credit/index/$1'; //$1 = page number

$route['admin/salesreport'] = 'admin_salesreport/index';
$route['admin/salesreport/add'] = 'admin_salesreport/add'; 

$route['admin/purchasereport'] = 'admin_purchasereport/index';
$route['admin/purchasereport/add'] = 'admin_purchasereport/add'; 


$route['admin/stocktransfer'] = 'admin_stocktransfer/index';
$route['admin/stocktransfer/add'] = 'admin_stocktransfer/add'; 
$route['admin/stocktransfer/order'] = 'admin_stocktransfer/order';
$route['admin/stocktransfer/update'] = 'admin_stocktransfer/update';
$route['admin/stocktransfer/update/(:any)'] = 'admin_stocktransfer/update/$1';
$route['admin/stocktransfer/delete/(:any)'] = 'admin_stocktransfer/delete/$1';
$route['admin/stocktransfer/view/(:any)'] = 'admin_stocktransfer/view/$1';
$route['admin/stocktransfer/(:any)'] = 'admin_stocktransfer/index/$1'; //$1 = page number

/* End of file routes.php */
/* Location: ./application/config/routes.php */