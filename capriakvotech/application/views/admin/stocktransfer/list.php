    <div class="container top">
      <div class="row">
     <div class="span10 columns">
        <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url("admin"); ?>">
            <?php echo ucfirst($this->uri->segment(1));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
          <?php echo ucfirst($this->uri->segment(2));?>
        </li>
      </ul>

      <div class="page-header users-header">
        <h2>
          <?php echo ucfirst($this->uri->segment(2));?> 
          <a  href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>/add" class="btn btn-success">Add Stock Transfer</a>
        </h2>
      </div>
      
    </div>
   </div>













     





       <div class="span10 columns">
          <div class="well">
           
             <?php
           
            $attributes = array('class' => 'form-inline reset-margin', 'id' => 'myform');
           
            /*$options_manufacture = array(0 => "all");
            foreach ($manufactures as $row)
            {
              $options_manufacture[$row['Customer_Id']] = $row['Customer_Id'];
            }
            //save the columns names in a array that we will use as filter         
            $options_purchase = array();    
            foreach ($capri_salesinvoice as $array) {
              foreach ($array as $key => $value) {
                $options_purchase[$key] = $key;
              }
              break;
            }*/

            echo form_open('admin/stocktransfer', $attributes);
     
             /* echo form_label('Search:', 'search_string');
              echo form_input('search_string', $search_string_selected, 'style="width: 170px;
height: 26px;"');

             /* echo form_label('Filter by manufacturer:', 'Supplier_Id');
              echo form_dropdown('Supplier_Id', $options_manufacture, $manufacture_selected, 'class="span2"');
*/
             /* echo form_label('Order by:', 'order');
              echo form_dropdown('order', $options_purchase, $order, 'class="span2"');

              $data_submit = array('name' => 'mysubmit', 'class' => 'btn btn-primary', 'value' => 'Go');

              $options_order_type = array('Asc' => 'Asc', 'Desc' => 'Desc');
              echo form_dropdown('order_type', $options_order_type, $order_type_selected, 'class="span2"');

              echo form_submit($data_submit);
*/
            echo form_close();
            ?>

          </div>

          <table class="table table-striped table-bordered table-condensed">
            <thead>
              <tr>
                <!-- <th class="header">Invoice_Id</th> -->
                <th class="yellow header headerSortDown">Stock Number</th>
                <th class="yellow header headerSortDown">Stock Date</th>
                <th class="yellow header headerSortDown">Customer Name</th>
                <th class="yellow header headerSortDown">Total Amount</th>
              </tr>
            </thead>
            <tbody>
              <?php
              foreach($capri_stocktransfer as $row)
              {
                echo '<tr>';
                /*echo '<td>'.$row['Stock_Id'].'</td>';*/
                echo '<td>'.$row['StockNumber'].'</td>';
                echo '<td>'.$row['StockDate'].'</td>';
                echo '<td>'.$row['CustomerName'].'</td>';
                echo '<td>'.$row['TotalAmount'].'</td>';
                echo '<td class="crud-actions">
                  <a href="'.site_url("admin").'/stocktransfer/update/'.$row['Stock_Id'].'"><button class="btn btn-xs btn-warning"><i class="fa fa-pencil"></i> </button></a>  
                  <a href="'.site_url("admin").'/stocktransfer/delete/'.$row['Stock_Id'].'"><button class="btn btn-xs btn-danger"><i class="fa fa-times"></i> </button></a>
                  <a href="'.site_url("admin").'/stocktransfer/view/'.$row['Stock_Id'].'"><button class="btn btn-xs btn-primary"><i class="fa fa-th-list"></i> </button></a>

                </td>';
                echo '</tr>';
              }
              ?>      
            </tbody>
          </table>

          <?php echo '<div class="pagination">'.$this->pagination->create_links().'</div>'; ?>
       </div>
   
  
    </div>