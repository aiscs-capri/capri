    
 <script src="<?php echo base_url(); ?>js/jquery-1.11.1.js"></script>
 <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>


    <div class="container top">
         <div class="row">
        <div class="span10 columns">
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url("admin"); ?>">
            <?php echo ucfirst($this->uri->segment(1));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li>
          <a href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>">
            <?php echo ucfirst($this->uri->segment(2));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
          <a href="#">View</a>
        </li>
      </ul>
    </div></div></div>
      
      <div class="page-header">
        <h2>
          Adding <?php echo ucfirst($this->uri->segment(2));?>
        </h2>
      
    </div>
      <?php
      //flash messages
      if(isset($flash_message)){
        if($flash_message == TRUE)
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Well done!</strong> New purchase created with success.';
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
          echo '</div>';          
        }
      }
      ?>
      
      <?php
      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '');
          //form validation
      echo validation_errors();
      
      echo form_open('admin/salesquotation/view', $attributes);
      ?>
       <div class="container">
           <div class="row">
               <div class="col-md-5">
                <div class="control-group">
            <label for="inputError" class="control-label"></label>
            <div class="controls">
              <input type="hidden" id="box1" name="Qtn_Id" value="<?php echo $manufacture[0]['Qtn_Id']; ?>"  readonly >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Qtn Number</label>
            <div class="controls">
              <input type="text" id="box2" name="QtnNumber" value="<?php echo $manufacture[0]['QtnNumber']; ?>"  readonly >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Qtn Date</label>
            <div class="controls">
              <input type="text" id="result" name="QtnDate" value="<?php $date = $manufacture[0]['QtnDate'];  echo date('Y-M-d',strtotime($date)); ?>"  readonly >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Our Ref</label>
            <div class="controls">
              <input type="text" id="result" name="OurRef" value="<?php echo $manufacture[0]['OurRef']; ?>"  >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Kind Attn</label>
            <div class="controls">
              <input type="text" id="result" name="KindAttn" value="<?php echo $manufacture[0]['KindAttn']; ?>"  >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>



        </div>
          
          <div class="col-md-5">
         <div class="control-group">

            <?php
              foreach ($po as $row) {
              ?>
            
                  
            <label for="inputError" class="control-label">Customer Name</label>

            <?php 
            echo "<input type=text id=customer name=Customer_Id  readonly value=".$row->CustomerName.">";
            ?>
                      
                      <br><br>
                      <div class="control-group">
            <label for="inputError" class="control-label"> Address</label>
            <div class="controls">
              <textarea rows="5" cols="20" id="bil" name="Bill" readonly><?php 
                  echo $row->BillingAddress;
                  
                  ?>
              </textarea>
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>

          <div class="control-group">
            <label for="inputError" class="control-label" >CST</label>
            <div class="controls">
            <?php 
            echo "<input type=text name=cst readonly value=".$row->CST.">";
            ?>
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div></div>
          
           <div class="control-group">
            <label for="inputError" class="control-label" >TIN</label>
            <div class="controls">
               <?php 
            echo "<input type=text name=tin readonly value=".$row->TIN.">";
            ?>
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
             <?php 
            }
            ?>
          </div>
                    

          </div>
        </div></div>
        <hr>
       
      <!-- <div class="page-header">
        <h2>
          Purchase Order
        </h2>
      
    </div>
       -->
     

       <div class="container">
        <div class="row">
         <div class="col-md-10">
          <table id="myTable" class="table table-striped table-bordered table-condensed">
            <tr>
            <td>id</td>
            <td>Product Name</td>
            <td>Product Unit</td>
            <td>Product Qunatity</td>
            <td>Product Price</td>
            <td>Total</td>
            <td>select</td>
        </tr>
        <tbody>
               <?php
              $i =1;
              foreach($pop as $row)
              {
                
                echo '<tr>';
                echo '<td>'.$i.'</td>';
                echo '<td>'.$row->ProductName.'</td>';
                echo '<td>'.$row->ProductUnit.'</td>';
                echo '<td>'.$row->Quantity.'</td>';
                echo '<td>'.$row->ProductPrice.'</td>';
                echo '<td>'.$row->Total.'</td>';
                echo '<td><input type=checkbox></input></td>';
                echo '</tr>';
                $i++;   
              }
              ?>     
           </tbody>
          </table>                        
  
     

    </div>
            </div><hr>
<div class="row">
 <div class="col-md-5">
  <div class="control-group">
            <label for="inputError" class="control-label">validity Qtn</label>
            <div class="controls">
              <input type="text" id="" name="validityQtn" value="<?php echo $manufacture[0]['validityQtn']; ?>"  readonly >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label">Date Of Delivery</label>
            <div class="controls">
              <input type="text" id="" name="DateOfDelivery" value="<?php $date = $manufacture[0]['DateOfDelivery'];  echo date('Y-M-d',strtotime($date)); ?>"  readonly >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">DispatchThro</label>
            <div class="controls">
              <input type="text" id="" name="dispatchThru" value="<?php echo $manufacture[0]['dispatchThru']; ?>"  readonly>
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label">Place Of Delivery</label>
            <div class="controls">
              <input type="text" id="" name="PlaceOfDelivery" value="<?php echo $manufacture[0]['PlaceOfDelivery']; ?>"  readonly>
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Quality</label>
            <div class="controls">
              <input type="text" id="" name="Quality" value="<?php echo $manufacture[0]['Quality']; ?>"  readonly >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Tax</label>
            <div class="controls">
              <input type="text" id="" name="Tax" value="<?php echo $manufacture[0]['Tax']; ?>"  readonly >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label">Discount</label>
            <div class="controls">
              <input type="text" id="" name="Discount" value="<?php echo $manufacture[0]['Discount']; ?>"  readonly >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">PaymentTerms</label>
            <div class="controls">
              <input type="text" id="" name="PaymentTerm" value="<?php echo $manufacture[0]['PaymentTerm']; ?>"  readonly >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
</div>
<div class="col-md-5">
          <div class="control-group">
            <label for="inputError" class="control-label" >Total</label>
            <div class="controls">
              <input type="text" id="total" name="Total" value="<?php echo $manufacture[0]['Total']; ?>"  readonly>
             
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Tax Type</label>
            <div class="controls">
              <input type="text" id="" name="Tax_type" value="<?php echo $manufacture[0]['Tax_type']; ?>"  readonly>
              
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Tax</label>
            <div class="controls">
              <input type="text" id="tax" name="" value="<?php echo $manufacture[0]['Tax']; ?>"  readonly>
            
             
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Tax Amount</label>
            <div class="controls">
              <input type="text" id="tax" name="TaxAmount" value="<?php echo $manufacture[0]['TaxAmount']; ?>"  readonly>
            
             
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label" >Gross Total</label>
            <div class="controls">
              <input type="text" id="gtot" name="" value="<?php echo $manufacture[0]['Tax']; ?>"  readonly>
             
            </div>
          </div>
    </div>

  </div>
   
    
      </div>
      <div class="row">
         <div class="col-md-10">
             <div class="form-actions">
           <center><a href="<?php echo base_url(); ?>admin_salesquo/report/<?php echo $manufacture[0]['Qtn_Id']; ?>">Print</a>
            <button class="btn" type="reset">Cancel</button></center>
          </div>
        </div>
      </div>
      <?php echo form_close(); ?>

    </div>
            </div>
          </div>
            