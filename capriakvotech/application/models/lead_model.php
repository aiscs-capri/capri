<?php
class lead_model extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return voLead_Id
    */
    public function __construct()
    {
        $this->load->database();
    }

    /**
    * Get product by his is
    * @param int $product_Lead_Id 
    * @return array
    */
    public function get_manufacture_by_Lead_Id($Lead_Id)
    {
		$this->db->select('*');
		$this->db->from('capri_lead');
		$this->db->where('Lead_Id', $Lead_Id);
		$query = $this->db->get();
		return $query->result_array(); 
    }    


public function save($Lead_Id)
    {
        $insert = $this->db->insert('capri_lead', $data);
        return $insert;
    }   

    public function maxvalue()
    {
        $this->db->select_MAX('Lead_Id');
        $this->db->from('capri_lead');
        $query = $this->db->get();
        return $query;

    }




    

    public function getleadOrderDetails($lead_order)
    {
       $this->db->select('capri_lead.Enqnumer, capri_master_customer.CustomerName,capri_master_customer.BillingAddress,capri_master_customer.TIN,capri_master_customer.CST');
        $this->db->from('capri_lead');
        $this->db->join('capri_master_customer','capri_lead.Customer_Id = capri_master_customer.Customer_Id','inner');
        $this->db->where('capri_lead.Lead_Id',$lead_order);
        $query = $this->db->get();
        return $query->result();
    }
    public function getleadOrderproductDetails($purchase_order_product)
    {
        $this->db->select('capri_master_product.ProductName,capri_master_product.ProductUnit,capri_master_product.ProductPrice,capri_lead_product.Quantity, capri_lead_product.Total');
        $this->db->from('capri_lead_product ');
        $this->db->join('capri_master_product ','capri_master_product.Product_Id =capri_lead_product.Product_Id','inner');
        $this->db->where('capri_lead_product.Lead_Id',$purchase_order_product);
        $query = $this->db->get();
        return $query->result();
    }

    /**
    * Fetch lead_master data from the database
    * possibility to mix search, filter and order
    * @param string $search_string 
    * @param strong $order
    * @param string $order_type 
    * @param int $limit_start
    * @param int $limit_end
    * @return array
    */
    public function get_lead($Customer_Id=null, $search_string=null, $order=null, $order_type='Asc', $limit_start, $limit_end)
    {
        
        $this->db->select('capri_lead.Lead_Id');
        $this->db->select('capri_lead.LeadDate');
        $this->db->select('capri_lead.CustomerName');
        $this->db->select('capri_lead.PhoneNumber');
        $this->db->select('capri_lead.BillingAddress');
        $this->db->select('capri_lead.ContactPerson');
        $this->db->select('capri_lead.Email');
        $this->db->select('capri_lead.MobileNumber1');
               
        $this->db->from('capri_lead');
                

        $query = $this->db->get();
        
        return $query->result_array();  
    }
    /**
    * Count the number of rows
    * @param int $search_string
    * @param int $order
    * @return int
    */

    public function getcustomername()
{

    $this->db->select('Customer_Id,CustomerName','BillingAddress');
    $this -> db -> from('capri_master_customer');  
    $query = $this -> db -> get();
    return $query->result();
}

public function getempname()
{

    $this->db->select('Employee_Id,EmployeeName');
    $this -> db -> from('capri_master_employee');  
    $query = $this -> db -> get();
    return $query->result();
}
    public function getproductname()
{

    $this->db->select('Product_Id,ProductName');
    $this -> db -> from('capri_master_product');

    $query = $this -> db -> get();
    return $query->result();
}
public function getproductdetail($Product_Id)
{

    $this->db->select('Product_Id,ProductPrice,ProductUnit');
    $this->db->where('Product_Id', $Product_Id);
    $this->db->from('capri_master_product');
    $query=$this->db->get();
    return $query->result();
}

/*$this->db->select('concat(BillStreet,',',BillVillage,',',BillCity,',',billstate,',',billpincode) as address,CST,TIN');*/
public function getcustomerdetail($Customer_Id)
{

    $this->db->select('BillingAddress,ContactPerson,Email,MobileNumber1,PhoneNumber');
    $this->db->select('CST,TIN');
    $this->db->from('capri_master_customer');
    $this->db->where('Customer_Id', $Customer_Id);
    $query=$this->db->get();
    return $query->result();
}

     public function get_order($Product_Id=null, $search_string=null, $order=null, $order_type='Asc', $limit_start, $limit_end)
    {
        
        $this->db->select('capri_lead_product.Id');
        $this->db->select('capri_lead_product.Product_Id');
        $this->db->select('capri_lead_product.Quantity');
        $this->db->select('capri_lead_product.Total');
         $this->db->select('capri_lead_product.Lead_Id');
        $this->db->select('capri_lead_product.Product_Id');
        $this->db->select('capri_master_product.Product_Id as Product_Id');
        $this->db->from('capri_lead_product');
        if($Product_Id != null && $Product_Id != 0){
            $this->db->where('Product_Id', $Product_Id);
        }
        if($search_string){
            $this->db->like('Product_Id', $search_string);
        }

        $this->db->join('capri_master_product', 'capri_lead_product.Product_Id = capri_master_product.Product_Id', 'left');
       
        
        $this->db->group_by('capri_lead_product.Id');

        if($order){
            $this->db->order_by($order, $order_type);
        }else{
            $this->db->order_by('Id', $order_type);
        }


        $this->db->limit($limit_start, $limit_end);
        //$this->db->limit('4', '4');


        $query = $this->db->get();
        
        return $query->result_array();  
    }
    /**
    * Count the number of rows
    * @param int $search_string
    * @param int $order
    * @return int
    */
   function count_lead($Customer_Id=null, $search_string=null, $order=null)
    {
        $this->db->select('*');
        $this->db->from('capri_lead');
        if($Customer_Id != null && $Customer_Id != 0){
            $this->db->where('Customer_Id', $Customer_Id);
        }
        if($search_string){
            $this->db->like('EnqNumer', $search_string);
        }
        if($order){
            $this->db->order_by($order, 'Asc');
        }else{
            $this->db->order_by('Lead_Id', 'Asc');
        }
        $query = $this->db->get();
        return $query->num_rows();        
    }


    /**
    * Store the new item into the database
    * @param array $data - associative array with data to store
    * @return boolean 
    */
    function store_manufacture($data)
    {
		$insert = $this->db->insert('capri_lead', $data);
	    return $insert;
	}

    /**
    * Update manufacture
    * @param array $data - associative array with data to store
    * @return boolean
    */
    function update_manufacture($Lead_Id, $data)
    {
		$this->db->where('Lead_Id', $Lead_Id);
		$this->db->update('capri_lead', $data);
		$report = array();
		$report['error'] = $this->db->_error_number();
		$report['message'] = $this->db->_error_message();
		if($report !== 0){
			return true;
		}else{
			return false;
		}
	}

    /**
    * Delete manufacturer
    * @param int $Lead_Id - manufacture Lead_Id
    * @return boolean
    */
	function delete_manufacture($Lead_Id){
		$this->db->where('Lead_Id', $Lead_Id);
		$this->db->delete('capri_lead'); 
	}
 
}
	
