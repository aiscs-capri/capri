    <div class="container top">
     <div class="row">
        <div class="span10 columns">
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url("admin"); ?>">
            <?php echo ucfirst($this->uri->segment(1));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
          <?php echo ucfirst($this->uri->segment(2));?>
        </li>
      </ul>

      <div class="page-header users-header">
        <h2>
          <?php echo ucfirst($this->uri->segment(2));?> 
          <a  href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>/add" class="btn btn-success">Add Product</a>
        </h2>
      </div>
    </div>
  </div>
</div>
      
        <div class="span10 columns">
          <div class="well">
           
            <?php
           
            $attributes = array('class' => 'form-inline reset-margin', 'id' => 'myform');
           
            //save the columns names in a array that we will use as filter         
            /*$options_product = array();    
            foreach ($product as $array) {
              foreach ($array as $key => $value) {
                $options_product[$key] = $key;
              }
              break;
            }*/

            echo form_open('admin/product', $attributes);
     
             /* echo form_label('Search:', 'search_string');
              echo form_input('search_string', $search_string_selected);*/

              /*echo form_label('Order by:', 'order');
              echo form_dropdown('order', $options_product, $order, 'class="span3"');

              $data_submit = array('name' => 'mysubmit', 'class' => 'btn btn-primary', 'value' => 'Go');

              $options_order_type = array('Asc' => 'Asc', 'Desc' => 'Desc');
              echo form_dropdown('order_type', $options_order_type, $order_type_selected, 'class="span3"');

              echo form_submit($data_submit);*/

            echo form_close();
            ?>

          </div>

          <table class="table table-striped table-bordered table-condensed">
            <thead>
              <tr>
                <!-- <th class="header">Product Id</th> -->
                <th class="yellow header headerSortDown">Product Code</th>
                 <th class="yellow header headerSortDown">Product Name</th>
                  <th class="yellow header headerSortDown">Product Unit</th>
                   <th class="yellow header headerSortDown">Product Price</th>
                    <!-- <th class="yellow header headerSortDown">Incentive</th> -->
                    <th class="yellow header headerSortDown">Control</th>
              </tr>
            </thead>
            <tbody>
              <?php
              foreach($product as $row)
              {
                echo '<tr>';
                /*echo '<td>'.$row['Product_Id'].'</td>';*/
                echo '<td>'.$row['ProductCode'].'</td>';
                echo '<td>'.$row['ProductName'].'</td>';
                
                echo '<td>'.$row['ProductUnit'].'</td>';

                echo '<td>'.$row['ProductPrice'].'</td>';
                /*echo '<td>'.$row['incentive'].'</td>';*/
                echo '<td class="crud-actions">
                  <a href="'.site_url("admin").'/product/update/'.$row['Product_Id'].'" ><button class="btn btn-xs btn-warning"><i class="fa fa-pencil"></i> </button></a>  
                  <a href="'.site_url("admin").'/product/delete/'.$row['Product_Id'].'" ><button class="btn btn-xs btn-danger"><i class="fa fa-trash-o fa-fw"></i> </button></a>
                  <a href="'.site_url("admin").'/product/view/'.$row['Product_Id'].'"><button class="btn btn-xs btn-primary"><i class="fa fa-th-list"></i> </button></a>
                </td>';
                echo '</tr>';
              }
              ?>      
            </tbody>
          </table>

          <?php echo '<div class="pagination">'.$this->pagination->create_links().'</div>'; ?>

      </div>
  
 