    <div class="container top">
        <div class="row">
        <div class="span10 columns">
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url("admin"); ?>">
            <?php echo ucfirst($this->uri->segment(1));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
          <?php echo ucfirst($this->uri->segment(2));?>
        </li>
      </ul>

      <div class="page-header users-header">
        <h2>
          <?php echo ucfirst($this->uri->segment(2));?> 
          <a  href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>/add" class="btn btn-success">Add Employee</a>
        </h2>
      </div>
       </div>
     </div>
      <div class="row">
        <div class="span10 columns">
          <div class="well">
           
            <?php
           
            $attributes = array('class' => 'form-inline reset-margin', 'id' => 'myform');
           
            //save the columns names in a array that we will use as filter         
            /*$options_employee = array();    
            foreach ($employee as $array) {
              foreach ($array as $key => $value) {
                $options_employee[$key] = $key;
              }
              break;
            }*/

            echo form_open('admin/employee', $attributes);
     
              /*echo form_label('Search:', 'search_string');
              echo form_input('search_string', $search_string_selected);

              echo form_label('Order by:', 'order');
              echo form_dropdown('order', $options_employee, $order, 'class="span2"');

              $data_submit = array('name' => 'mysubmit', 'class' => 'btn btn-primary', 'value' => 'Go');

              $options_order_type = array('Asc' => 'Asc', 'Desc' => 'Desc');
              echo form_dropdown('order_type', $options_order_type, $order_type_selected, 'class="span2"');

              echo form_submit($data_submit);*/

            echo form_close();
            ?>

          </div>

          <table class="table table-striped table-bordered table-condensed">
            <thead>
              <tr>
                <!-- <th class="header">#</th> -->
                <th class="yellow header headerSortDown">Employee Name</th>
                <th class="yellow header headerSortDown">Emp State</th>
                <th class="yellow header headerSortDown">Mobile</th>
                <th class="yellow header headerSortDown">Email</th>
                <th class="yellow header headerSortDown">Control</th>
              </tr>
            </thead>
            <tbody>
              <?php
              foreach($employee as $row)
              {
                echo '<tr>';
               /* echo '<td>'.$row['Employee_Id'].'</td>';*/
                echo '<td>'.$row['EmployeeName'].'</td>';
                echo '<td>'.$row['EmpState'].'</td>';
                echo '<td>'.$row['MobileNumber'].'</td>';
                echo '<td>'.$row['Email'].'</td>';
                echo '<td class="crud-actions">
                  <a href="'.site_url("admin").'/employee/update/'.$row['Employee_Id'].'"><button class="btn btn-xs btn-warning"><i class="fa fa-pencil"></i> </button></a>  
                  <a href="'.site_url("admin").'/employee/delete/'.$row['Employee_Id'].'"><button class="btn btn-xs btn-danger"><i class="fa fa-times"></i> </button></a>
                  <a href="'.site_url("admin").'/employee/view/'.$row['Employee_Id'].'"><button class="btn btn-xs btn-primary"><i class="fa fa-th-list"></i> </button></a>
                </td>';
                echo '</tr>';
              }
              ?>      
            </tbody>
          </table>

          <?php echo '<div class="pagination">'.$this->pagination->create_links().'</div>'; ?>

      </div>
    </div>