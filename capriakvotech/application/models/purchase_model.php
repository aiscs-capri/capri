<?php
class purchase_model extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return voOrder_Id
    */
    public function __construct()
    {
        $this->load->database();
    }

    /**
    * Get product by his is
    * @param int $product_Order_Id 
    * @return array
    */
    public function get_manufacture_by_Order_Id($Order_Id)
    {
        $this->db->select('*');
        $this->db->from('capri_purchase_order');
        $this->db->where('Order_Id', $Order_Id);
        $query = $this->db->get();
        return $query->result_array(); 
    }    


public function save($Order_Id)
    {
        $insert = $this->db->insert('capri_purchase_order', $data);
        return $insert;
    }   

    public function maxvalue()
    {
        $this->db->select_MAX('Order_Id');
        $this->db->from('capri_purchase_order');
        $query = $this->db->get();
        return $query;

    }

  /* view order and supplier*/
    public function getPurchaseOrderDetails($purchase_order)
    {
        $this->db->select('capri_purchase_order.Order_number,capri_purchase_order.Total,capri_purchase_order.OrderDate,capri_purchase_order.Supplier_Id, capri_master_supplier.SupplierName, capri_master_supplier.BillingAddress, capri_master_supplier.TIN,capri_master_supplier.CST');
        $this->db->from('capri_purchase_order');
        $this->db->join('capri_master_supplier','capri_purchase_order.Supplier_Id = capri_master_supplier.Supplier_Id','inner');
        $this->db->where('capri_purchase_order.Order_Id',$purchase_order);
        $query = $this->db->get();
        return $query->result();
    }

/* show the product item details*/
public function getPurchaseOrderproductDetails($purchase_order_product)
    {
        $this->db->select('Id,ProductName,ProductUnit,ProductPrice,Quantity,Total');
        $this->db->from('capri_purchase_order_details ');       
        $this->db->where('capri_purchase_order_details.Order_Id',$purchase_order_product);
        $query = $this->db->get();
        return $query->result();
    }


    
    
    /**
    * Fetch purchase_master data from the database
    * possibility to mix search, filter and order
    * @param string $search_string 
    * @param strong $order
    * @param string $order_type 
    * @param int $limit_start
    * @param int $limit_end
    * @return array
    */
    public function get_purchase($Supplier_Id=null, $search_string=null, $order=null, $order_type='Asc', $limit_start, $limit_end)
    {
        
        $this->db->select('capri_purchase_order.Order_Id');
        $this->db->select('capri_purchase_order.Order_number');
        $this->db->select('capri_purchase_order.OurRef');
        $this->db->select('capri_purchase_order.OrderDate');
        $this->db->select('capri_purchase_order.DateOfDelivery');
        $this->db->select('capri_purchase_order.PlaceOfDelivery');
        $this->db->select('capri_purchase_order.Quality');
        $this->db->select('capri_purchase_order.Note');
        $this->db->select('capri_purchase_order.PaymentTerm');
        $this->db->select('capri_purchase_order.Total');
        $this->db->select('capri_purchase_order.Tax_type');
        $this->db->select('capri_purchase_order.Tax');
        $this->db->select('capri_purchase_order.GrossTotal');
        $this->db->select('capri_purchase_order.Supplier_Id');
        $this->db->select('capri_master_supplier.Supplier_Id as Supplier_Id');
        $this->db->from('capri_purchase_order');
        if($Supplier_Id != null && $Supplier_Id != 0){
            $this->db->where('Supplier_Id', $Supplier_Id);
        }
        if($search_string){
            $this->db->like('Order_number', $search_string);
        }

        $this->db->join('capri_master_supplier', 'capri_purchase_order.Supplier_Id = capri_master_supplier.Supplier_Id', 'left');

        $this->db->group_by('capri_purchase_order.Order_Id');

        if($order){
            $this->db->order_by($order, $order_type);
        }else{
            $this->db->order_by('Order_Id', $order_type);
        }


        $this->db->limit($limit_start, $limit_end);
        //$this->db->limit('4', '4');


        $query = $this->db->get();
        
        return $query->result_array();  
    }
    /**
    * Count the number of rows
    * @param int $search_string
    * @param int $order
    * @return int
    */

    public function getsuppliername()
{

    $this->db->select('Supplier_Id,SupplierName');
    $this -> db -> from('capri_master_supplier');  
    $query = $this -> db -> get();
    return $query->result();
}


    public function getproductname()
{

    $this->db->select('Product_Id,ProductName');
    $this -> db -> from('capri_master_product');

    $query = $this -> db -> get();
    return $query->result();
}
public function getproductdetail($Product_Id)
{

    $this->db->select('Product_Id,ProductPrice,ProductUnit');
    $this->db->where('Product_Id', $Product_Id);
    $this->db->from('capri_master_product');
    $query=$this->db->get();
    return $query->result();
}

/*$this->db->select('concat(BillStreet,',',BillVillage,',',BillCity,',',billstate,',',billpincode) as address,CST,TIN');*/
public function getsupplierdetail($Supplier_Id)
{

    $this->db->select('BillingAddress,CST,TIN');
    $this->db->from('capri_master_supplier');
    $this->db->where('Supplier_Id', $Supplier_Id);
    $query=$this->db->get();
    return $query->result();
}

     public function get_order($Product_Id=null, $search_string=null, $order=null, $order_type='Asc', $limit_start, $limit_end)
    {
        
        $this->db->select('capri_purchase_order_details.Id');
        $this->db->select('capri_purchase_order_details.Product_Id');
        $this->db->select('capri_purchase_order_details.Quantity');
        $this->db->select('capri_purchase_order_details.Total');
         $this->db->select('capri_purchase_order_details.Order_Id');
        $this->db->select('capri_purchase_order_details.Product_Id');
        $this->db->select('capri_master_product.Product_Id as Product_Id');
        $this->db->from('capri_purchase_order_details');
        if($Product_Id != null && $Product_Id != 0){
            $this->db->where('Product_Id', $Product_Id);
        }
        if($search_string){
            $this->db->like('Product_Id', $search_string);
        }

        $this->db->join('capri_master_product', 'capri_purchase_order_details.Product_Id = capri_master_product.Product_Id', 'left');
       
        
        $this->db->group_by('capri_purchase_order_details.Id');

        if($order){
            $this->db->order_by($order, $order_type);
        }else{
            $this->db->order_by('Id', $order_type);
        }


        $this->db->limit($limit_start, $limit_end);
        //$this->db->limit('4', '4');


        $query = $this->db->get();
        
        return $query->result_array();  
    }
    /**
    * Count the number of rows
    * @param int $search_string
    * @param int $order
    * @return int
    */
   function count_purchase($Supplier_Id=null, $search_string=null, $order=null)
    {
        $this->db->select('*');
        $this->db->from('capri_purchase_order');
        if($Supplier_Id != null && $Supplier_Id != 0){
            $this->db->where('Supplier_Id', $Supplier_Id);
        }
        if($search_string){
            $this->db->like('Order_number', $search_string);
        }
        if($order){
            $this->db->order_by($order, 'Asc');
        }else{
            $this->db->order_by('Order_Id', 'Asc');
        }
        $query = $this->db->get();
        return $query->num_rows();        
    }


    /**
    * Store the new item into the database
    * @param array $data - associative array with data to store
    * @return boolean 
    */
    function store_manufacture($data)
    {
        $insert = $this->db->insert('capri_purchase_order', $data);
        return $insert;
    }

    /**
    * Update manufacture
    * @param array $data - associative array with data to store
    * @return boolean
    */
    function update_manufacture($Order_Id, $data)
    {
        $this->db->where('Order_Id', $Order_Id);
        $this->db->update('capri_purchase_order', $data);
        $report = array();
        $report['error'] = $this->db->_error_number();
        $report['message'] = $this->db->_error_message();
        if($report !== 0){
            return true;
        }else{
            return false;
        }
    }

    /**
    * Delete manufacturer
    * @param int $Order_Id - manufacture Order_Id
    * @return boolean
    */
    function delete_manufacture($Order_Id){
        $this->db->where('Order_Id', $Order_Id);
        $this->db->delete('capri_purchase_order'); 
    }
 
function delete_order_details($Id){

    $this->db->where('Id',$Id);
    $this->db->delete('capri_purchase_order_details');
}


function insert_order_details($data)
    {
        $insert = $this->db->insert('capri_purchase_order_details', $data);
        return $insert;
    }

    function purchase_order_delete_details($Id){

    $this->db->where('Order_Id',$Id);
    $this->db->delete('capri_purchase_order_details');
 }


public function order_details_maxvalue()
    {
        $this->db->select_MAX('Id');
        $this->db->from('capri_purchase_order_details');
        $query = $this->db->get();
        return $query;

    }


}
