﻿<?php
class Admin_credit extends CI_Controller {

     /**
    * name of the folder responsible for the views 
    * which are manipulated by this controller
    * @constant string
    */
    const VIEW_FOLDER = 'admin/credit';
 
    /**
    * Responsable for auto load the model
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('credit_model');
         $this->load->model('customer_model');
         $this->load->model('product_model');
         $this->load->model('order_model');
         $this->load->model('value_model');

        if(!$this->session->userdata('is_logged_in')){
            redirect('admin/login');
        }
    }
 
    /**
    * Load the main view with all the current model model's data.
    * @return void
    */
     public function index()
    {
        /*$id = $this->credit_model->maxvalue();
        echo "";
        echo "jdhfjkfjhasjhf jahsfjhasf ahsjfhasf ahsfjaf =========>".$id->row('Order_Id');
        */
        //all the posts sent by the view
        $Reason = $this->input->post('Reason');        
        $search_string = $this->input->post('search_string');        
        $order = $this->input->post('order'); 
        $order_type = $this->input->post('order_type'); 

        //pagination settings
        $config['per_page'] = 20;
        $config['base_url'] = base_url().'admin/credit';
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = 20;
        $config['full_tag_open'] = '<ul>';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';

        //limit end
        $page = $this->uri->segment(3);

        //math to get the initial record to be select in the database
        $limit_end = ($page * $config['per_page']) - $config['per_page'];
        if ($limit_end < 0){
            $limit_end = 0;
        } 

        //if order type was changed
        if($order_type){
            $filter_session_data['order_type'] = $order_type;
        }
        else{
            //we have something stored in the session? 
            if($this->session->userdata('order_type')){
                $order_type = $this->session->userdata('order_type');    
            }else{
                //if we have nothing inside session, so it's the default "Asc"
                $order_type = 'Asc';    
            }
        }
        //make the data type var avaible to our view
        $data['order_type_selected'] = $order_type;        


        //we must avoid a page reload with the previous session data
        //if any filter post was sent, then it's the first time we load the content
        //in this case we clean the session filter data
        //if any filter post was sent but we are in some page, we must load the session data

        //filtered && || paginated
        if($Reason !== false && $search_string !== false && $order !== false || $this->uri->segment(3) == true){ 
           
            /*
            The comments here are the same for line 79 until 99

            if post is not null, we store it in session data array
            if is null, we use the session data already stored
            we save order into the the var to load the view with the param already selected       
            */

            if($Reason !== 0){
                $filter_session_data['manufacture_selected'] = $Reason;
            }else{
                $Reason = $this->session->userdata('manufacture_selected');
            }
            $data['manufacture_selected'] = $Reason;

            if($search_string){
                $filter_session_data['search_string_selected'] = $search_string;
            }else{
                $search_string = $this->session->userdata('search_string_selected');
            }
            $data['search_string_selected'] = $search_string;

            if($order){
                $filter_session_data['order'] = $order;
            }
            else{
                $order = $this->session->userdata('order');
            }
            $data['order'] = $order;

            //save session data into the session
            $this->session->set_userdata($filter_session_data);

            //fetch manufacturers data into arrays
            $data['manufactures'] = $this->credit_model->get_manufacturers();

            

            $data['count_credit']= $this->credit_model->count_credit($Reason, $search_string, $order);
            $config['Reason_rows'] = $data['count_credit'];

            //fetch sql data into arrays
            if($search_string){
                if($order){
                    $data['credit'] = $this->credit_model->get_credit($Reason, $search_string, $order, $order_type, $config['per_page'],$limit_end);        
                }else{
                    $data['credit'] = $this->credit_model->get_credit($Reason, $search_string, '', $order_type, $config['per_page'],$limit_end);           
                }
            }else{
                if($order){
                    $data['credit'] = $this->credit_model->get_credit($Reason, '', $order, $order_type, $config['per_page'],$limit_end);        
                }else{
                    $data['credit'] = $this->credit_model->get_credit($Reason, '', '', $order_type, $config['per_page'],$limit_end);        
                }
            }

        }else{

            //clean filter data inside section
            $filter_session_data['manufacture_selected'] = null;
            $filter_session_data['search_string_selected'] = null;
            $filter_session_data['order'] = null;
            $filter_session_data['order_type'] = null;
            $this->session->set_userdata($filter_session_data);

            //pre selected options
            $data['search_string_selected'] = '';
            $data['manufacture_selected'] = 0;
            $data['order'] = 'id';

            //fetch sql data into arrays
            /*$data['salesinvoice'] = $this->credit_model->get_salesinvoice();*/
            $data['count_credit']= $this->credit_model->count_credit();
            $data['get_credit'] = $this->credit_model->get_credit('', '', '', $order_type, $config['per_page'],$limit_end);        
            $config['Reason_rows'] = $data['count_credit'];
        }//!isset($Reason) && !isset($search_string) && !isset($order)

        //initializate the panination helper 
        $this->pagination->initialize($config);   

        //load the view
        $data['main_content'] = 'admin/credit/list';
        $this->load->view('includes/template', $data);  

    }//index

    public function add()
    {

        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {

            //form validation
            $this->form_validation->set_rules('Credit_Id', 'Credit_Id');
            $this->form_validation->set_rules('CustomerName','CustomerName');
            $this->form_validation->set_rules('BillingAddress', 'BillingAddress');
            $this->form_validation->set_rules('TIN', 'TIN');
            $this->form_validation->set_rules('CST', 'CST');
            $this->form_validation->set_rules('Reson', 'Reson');
            $this->form_validation->set_rules('InvoiceNumber', 'InvoiceNumber');
            $this->form_validation->set_rules('InvoiceDate', 'InvoiceDate');
            $this->form_validation->set_rules('ReturnDate', 'ReturnDate');
      
           
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            

            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
                $data_to_store = array(
                    'Credit_Id' => $this->input->post('Credit_Id'),
                    'CustomerName' => $this->input->post('CustomerName'),
                    'BillingAddress' => $this->input->post('BillingAddress'),
                    'TIN' => $this->input->post('TIN'),
                    'CST' => $this->input->post('CST'),
                    'Reson' => $this->input->post('Reson'),
                    'InvoiceNumber' => $this->input->post('InvoiceNumber'),
                    'InvoiceDate' => date("Y-m-d :H:i:s",strtotime($this->input->post('InvoiceDate'))),
                    'ReturnDate' => date("Y-m-d :H:i:s",strtotime($this->input->post('ReturnDate'))),
                    'CreatedBy' => $this->session->userdata('user_id'),
                    'CreatedDate' => date("Y-m-d H:i:s")

                    
                    
                );
                //if the insert has returned true then we show the flash message
                if($this->credit_model->store_manufacture($data_to_store)){
                    $data['flash_message'] = TRUE; 


        $id = $this->credit_model->maxvalue();

        $pro = $_POST['id'];
        $product = $_POST['product'];
        $unit = $_POST['unit'];
        $qun = $_POST['qun'];
     /*   $tot = $_POST['tot'];
        $price = $_POST['price'];*/
        $order = $id->row('Credit_Id');


        for($i=0; $i<sizeof($pro); $i++)
        {
        $data1['ProductName']= $product[$i];
        $data1['ProductUnit']= $unit[$i];
        $data1['Quantity']= $qun[$i];
      /*  $data1['ProductPrice']= $price[$i];
        $data1['Total']= $tot[$i];*/
        $data1['Credit_Id']= $order;
        $data1['CreatedBy'] = $this->session->userdata('user_id');
        $data1['CreatedDate'] = date("Y-m-d H:i:s");
            
        $this->value_model->getcredit($data1);

    }
                }else{
                    $data['flash_message'] = FALSE; 
                }

            }

        }
        //fetch manufactures data to populate the select field
        $data['manufactures'] = $this->customer_model->get_customer();
        $data['salesinvoiceorder'] = $this->product_model->get_product();
        $data['price'] = $this->product_model->get_product();
        $data['product'] = $this->product_model->get_product();

        $customer = $this->credit_model->getcustomername();
        $product = $this->credit_model->getproductname();
        $getProduct=$this->credit_model->getproductdetail('Product_Id');
        $getcustomer=$this->credit_model->getcustomerdetail('Customer_Id');

        //load the view
        $data['CustomerName'] =$customer;
        $data['ProductName'] =$product;
        $data['main_content'] = 'admin/credit/add';
        $this->load->view('includes/template', $data);  
 
    }       

    /**
    * Update item by his id
    * @return void
    */

public function get()
{

    $Product_Id=$_GET['Product_Id'];
    $this->credit_model->getproductdetail('Product_Id');
     $productdetail["rows"] = $this->credit_model->getproductdetail($Product_Id);
        echo json_encode($productdetail);

}

public function getcustomerdata()
{

    $Customer_Id=$_GET['Customer_Id'];
    $this->credit_model->getcustomerdetail('Customer_Id');
     $customerdetail["rows"] = $this->credit_model->getcustomerdetail($Customer_Id);
        echo json_encode($customerdetail);

}
/*public function salesinvoiceorder_submit()
 {
     $order_id=$POST['Order_Id'];
     $ProformaNumer=$POST['ProformaNumer'];
     $EnqDate=$POST['EnqDate'];
     $PhoneNumber=$POST['PhoneNumber'];
     $date=$POST['Email'];
     $place=$POST['packingAndForward'];
     $qunt=$POST['serviceCharge'];
     $Status=$POST['Status'];
     $pay=$POST['Customer_Id'];
     $Reason=$POST['Reason'];
     $taxtype=$POST['Tax_Type'];
     $tax=$POST['Tax'];
     $gReason=$POST['GrossReason'];

    $this->credit_model->save($data);

    if($flash_message == true)
    {

        $id = $this->credit_model->maxvalue();

        $pro = $_POST['id'];
        $qtn = $_POST['unit'];
        $tot = $_POST['tot'];
        $order = $id->row('Order_Id');


        for($i=0; $i<sizeof($pro); $i++)
        {
        $data1['Product_Id']= $pro[$i];
        $data1['Qunantity']= $qtn[$i];
        $data1['Reason']= $tot[$i];
        $data1['Order_Id']= $order;
            
        $this->salesinvoiceOrder_product_model->getProduct($data1);

        }
    }
     
 }
*/


    


    public function update()
    {
        //product id 
        $id = $this->uri->segment(4);
  
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {
            //form validation
            
           
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
    
                $data_to_store = array(
                    'Credit_Id' => $this->input->post('Credit_Id'),
                    'CustomerName' => $this->input->post('CustomerName'),
                    'BillingAddress' => $this->input->post('BillingAddress'),
                    'TIN' => $this->input->post('TIN'),
                    'CST' => $this->input->post('CST'),
                    'Reson' => $this->input->post('Reson'),
                    'InvoiceNumber' => $this->input->post('InvoiceNumber'),
                    'InvoiceDate' => date("Y-m-d :H:i:s",strtotime($this->input->post('InvoiceDate'))),
                    'ReturnDate' => date("Y-m-d :H:i:s",strtotime($this->input->post('ReturnDate'))),
                    'CreatedBy' => $this->session->userdata('user_id'),
                    'CreatedDate' => date("Y-m-d H:i:s")

                );
                //if the insert has returned true then we show the flash message
                if($this->credit_model->update_manufacture($id, $data_to_store) == TRUE){
                    $this->session->set_flashdata('flash_message', 'updated');
                }else{
                    $this->session->set_flashdata('flash_message', 'not_updated');
                }
                redirect('admin/credit/update/'.$id.'');

            }//validation run

        }

        //if we are updating, and the data did not pass trough the validation
        //the code below wel reload the current data

        //product data 
        $data['manufacture'] = $this->credit_model->get_manufacture_by_Credit_Id($id);
        

        $supplier = $this->credit_model->getcustomername();
        $product = $this->credit_model->getproductname();


        $order=$this->credit_model->getCreditentryDetails($id);
        $orderpro=$this->credit_model->getCreditproductDetails($id);
        $getProduct=$this->credit_model->getproductdetail('Product_Id');
       
        //load the view
        $data['CustomerName'] =$supplier;
        $data['ProductName'] =$product;
        $data['po']=$order;
        $data['pop']=$orderpro;

        //load the view
        $data['main_content'] = 'admin/credit/edit';
        $this->load->view('includes/template', $data);            

    }//update
public function  orderdetailsinsert()
    {
        $product = $_GET['product'];
        $unit = $_GET['unit'];
        $qun = $_GET['qun'];
       /* $price = $_GET['price'];
        $tot = $_GET['tot'];*/
        $order = $_GET['Credit_Id'];

        
        $data['ProductName']= $product;
        $data['ProductUnit']= $unit;
        $data['Quantity']= $qun;
        /*$data['ProductPrice']= $price;
        $data['Total']= $tot;*/
        $data['Credit_Id']= $order;
        $data['CreatedBy'] = $this->session->userdata('user_id');
        $data['CreatedDate'] = date("Y-m-d H:i:s");
            
        $resdata["row"] = $this->value_model->getcredit($data);
        echo json_encode($resdata);

    }

    public function  orderdetailsdelete()
    {
 
        $pro = $_GET['id'];        
        
        $this->credit_model->delete_order_details($pro);
        $deleteProduct = array(
        'message' => true
        );
        $resdata["row"] = $deleteProduct;
        echo json_encode($resdata);

     }
     public function  orderdetailsmaxvalue()
    {
       $id = $this->credit_model->maxvalue();

       $sam = array(
        'Id' => $id->row('Id')
        );
       $resdata["row"] = $sam;
        echo json_encode($resdata);
    }
    /**
    * Delete product by his id
    * @return void
    */
    public function view()
    {
        //product id 
        $id = $this->uri->segment(4);
  
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {
           //form validation
            

            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
    
                $data_to_store = array(
                   'Order_Id' => $this->input->post('Order_Id'),
                    'ProformaNumer' => $this->input->post('ProformaNumer'),
                    'Quantity' => $this->input->post('Quantity'),
                    'Reason' => $this->input->post('Reason'),
                    'Email' => $this->input->post('Email'),
                    'packingAndForward' => $this->input->post('packingAndForward'),
                    'serviceCharge' => $this->input->post('serviceCharge'),
                    'Status' => $this->input->post('Status'),
                   
                     'Reason' => $this->input->post('Reason'),
                    'Tax_type' => $this->input->post('Tax_type'),
                    'Tax' => $this->input->post('Tax'),
                    'GrossReason' => $this->input->post('GrossReason'),
                );
                //if the insert has returned true then we show the flash message
                if($this->credit_model->update_manufacture($id, $data_to_store) == TRUE){
                    $this->session->set_flashdata('flash_message', 'updated');
                }else{
                    $this->session->set_flashdata('flash_message', 'not_updated');
                }
                redirect('admin/credit/view/'.$id.'');

            }//validation run

        }

        //if we are updating, and the data did not pass trough the validation
        //the code below wel reload the current data

        //product data 
        $data['manufacture'] = $this->credit_model->get_manufacture_by_Credit_Id($id);
        
         $order=$this->credit_model->getCreditentryDetails($id);
        $orderpro=$this->credit_model->getCreditproductDetails($id);

        $data['po']=$order;
        $data['pop']=$orderpro;



        //load the view
        $data['main_content'] = 'admin/credit/view';
        $this->load->view('includes/template', $data);            

    }//update

    /**
    * Delete product by his id
    * @return void
    */
    
    /**
    * Delete product by his id
    * @return void
    */
    public function delete()
    {
        //product id 
        $id = $this->uri->segment(4);
        $this->credit_model->credit_product_delete_details($id);
        

         if($this->credit_model->delete_manufacture($id) == TRUE){
                    $this->session->set_flashdata('flash_message', 'updated');
                }else{
                    $this->session->set_flashdata('flash_message', 'not_updated');
                }
        redirect('admin/credit');
    }//edit








   
}