<?php
class Admin_lead extends CI_Controller {

    /**
    * name of the folder responsible for the views 
    * which are manipulated by this controller
    * @constant string
    */
    const VIEW_FOLDER = 'admin/lead';
 
    /**
    * Responsable for auto load the model
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('lead_model');
         $this->load->model('supplier_model');
         $this->load->model('product_model');
         $this->load->model('order_model');
         $this->load->model('value_model');

        if(!$this->session->userdata('is_logged_in')){
            redirect('admin/login');
        }
    }
 
    /**
    * Load the main view with all the current model model's data.
    * @return void
    */
     public function index()
    {
        /*$id = $this->lead_model->maxvalue();
        echo "";
        echo "jdhfjkfjhasjhf jahsfjhasf ahsjfhasf ahsfjaf =========>".$id->row('Order_Id');
        */
        //all the posts sent by the view
        $Reason = $this->input->post('Reason');        
        $search_string = $this->input->post('search_string');        
        $order = $this->input->post('order'); 
        $order_type = $this->input->post('order_type'); 

        //pagination settings
        $config['per_page'] = 20;
        $config['base_url'] = base_url().'admin/lead';
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = 20;
        $config['full_tag_open'] = '<ul>';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';

        //limit end
        $page = $this->uri->segment(3);

        //math to get the initial record to be select in the database
        $limit_end = ($page * $config['per_page']) - $config['per_page'];
        if ($limit_end < 0){
            $limit_end = 0;
        } 

        //if order type was changed
        if($order_type){
            $filter_session_data['order_type'] = $order_type;
        }
        else{
            //we have something stored in the session? 
            if($this->session->userdata('order_type')){
                $order_type = $this->session->userdata('order_type');    
            }else{
                //if we have nothing inside session, so it's the default "Asc"
                $order_type = 'Asc';    
            }
        }
        //make the data type var avaible to our view
        $data['order_type_selected'] = $order_type;        


        //we must avoid a page reload with the previous session data
        //if any filter post was sent, then it's the first time we load the content
        //in this case we clean the session filter data
        //if any filter post was sent but we are in some page, we must load the session data

        //filtered && || paginated
        if($Reason !== false && $search_string !== false && $order !== false || $this->uri->segment(3) == true){ 
           
            /*
            The comments here are the same for line 79 until 99

            if post is not null, we store it in session data array
            if is null, we use the session data already stored
            we save order into the the var to load the view with the param already selected       
            */

            if($Reason !== 0){
                $filter_session_data['manufacture_selected'] = $Reason;
            }else{
                $Reason = $this->session->userdata('manufacture_selected');
            }
            $data['manufacture_selected'] = $Reason;

            if($search_string){
                $filter_session_data['search_string_selected'] = $search_string;
            }else{
                $search_string = $this->session->userdata('search_string_selected');
            }
            $data['search_string_selected'] = $search_string;

            if($order){
                $filter_session_data['order'] = $order;
            }
            else{
                $order = $this->session->userdata('order');
            }
            $data['order'] = $order;

            //save session data into the session
            $this->session->set_userdata($filter_session_data);

            //fetch manufacturers data into arrays
            
            $data['count_lead']= $this->lead_model->count_lead($Reason, $search_string, $order);
            $config['Reason_rows'] = $data['count_lead'];

            //fetch sql data into arrays
            if($search_string){
                if($order){
                    $data['lead'] = $this->lead_model->get_lead($Reason, $search_string, $order, $order_type, $config['per_page'],$limit_end);        
                }else{
                    $data['lead'] = $this->lead_model->get_lead($Reason, $search_string, '', $order_type, $config['per_page'],$limit_end);           
                }
            }else{
                if($order){
                    $data['lead'] = $this->lead_model->get_lead($Reason, '', $order, $order_type, $config['per_page'],$limit_end);        
                }else{
                    $data['lead'] = $this->lead_model->get_lead($Reason, '', '', $order_type, $config['per_page'],$limit_end);        
                }
            }

        }else{

            //clean filter data inside section
            $filter_session_data['manufacture_selected'] = null;
            $filter_session_data['search_string_selected'] = null;
            $filter_session_data['order'] = null;
            $filter_session_data['order_type'] = null;
            $this->session->set_userdata($filter_session_data);

            //pre selected options
            $data['search_string_selected'] = '';
            $data['manufacture_selected'] = 0;
            $data['order'] = 'id';

            //fetch sql data into arrays
            $data['lead'] = $this->product_model->get_product();

            
            $data['count_lead']= $this->lead_model->count_lead();
            $data['capri_lead_order'] = $this->lead_model->get_lead('', '', '', $order_type, $config['per_page'],$limit_end);        
            $config['Reason_rows'] = $data['count_lead'];

        }//!isset($Reason) && !isset($search_string) && !isset($order)

        //initializate the panination helper 
        $this->pagination->initialize($config);   

        //load the view
        $data['main_content'] = 'admin/lead/list';
        $this->load->view('includes/template', $data);  

    }//index

    public function add()
    {

        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {

            //form validation
             $this->form_validation->set_rules('Lead_Id', 'Lead_Id', 'required');
            $this->form_validation->set_rules('LeadDate', 'LeadDate');
            $this->form_validation->set_rules('CustomerName', 'CustomerName', 'required');
            $this->form_validation->set_rules('PhoneNumber', 'PhoneNumber');
   
            $this->form_validation->set_rules('Email', 'Email');
            $this->form_validation->set_rules('ContactPerson', 'ContactPerson');
            /*$this->form_validation->set_rules('EnqAbout', 'EnqAbout', 'required');
            $this->form_validation->set_rules('Customer_Id', 'Customer_Id', 'required');
            $this->form_validation->set_rules('Status', 'Status', 'required');
            $this->form_validation->set_rules('Reason', 'Reason', 'required');
            */
           
           


            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            

            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
                $data_to_store = array(
                    'Lead_Id' => $this->input->post('Lead_Id'),
                     'LeadDate' => date("Y-m-d :H:i:s",strtotime($this->input->post('LeadDate'))),
                    'CustomerName' => $this->input->post('CustomerName'),
                    'PhoneNumber' => $this->input->post('PhoneNumber'),
                    'Email' => $this->input->post('Email'),
                    'BillingAddress' => $this->input->post('BillingAddress'),
                    'ContactPerson' => $this->input->post('ContactPerson'),
                    'MobileNumber1' => $this->input->post('MobileNumber1'),
                    'Leadassign' => $this->input->post('Leadassign'),
                    'Leadcategory' => $this->input->post('Leadcategory'),
                    'Leadstage' => $this->input->post('Leadstage'),
                    'Leadsource' => $this->input->post('Leadsource'),
                    'CustomerBudget' => $this->input->post('CustomerBudget'),
                    'NextAction' => $this->input->post('NextAction'),
                    'PriofAction' => $this->input->post('PriofAction'),
                    'Leadsubject' => $this->input->post('Leadsubject'),
                    'Leaddetails' => $this->input->post('Leaddetails'),
                    'Approvalstatus' => $this->input->post('Approvalstatus'),
                    'Remark' => $this->input->post('Remark'),
                    'LeadStatus' => $this->input->post('LeadStatus'),
                    'CreatedBy' => $this->session->userdata('user_id'),
                    'CreatedDate' => date("Y-m-d H:i:s")
                    
                );
                //if the insert has returned true then we show the flash message
                if($this->lead_model->store_manufacture($data_to_store)){
                    $data['flash_message'] = TRUE; 


            }
                else{
                    $data['flash_message'] = FALSE; 
                }

            }

        }
        //fetch manufactures data to populate the select field
        $data['manufactures'] = $this->supplier_model->get_supplier();
        $data['leadorder'] = $this->product_model->get_product();
        $data['price'] = $this->product_model->get_product();
        $data['product'] = $this->product_model->get_product();

        $customer = $this->lead_model->getcustomername();
        $product = $this->lead_model->getproductname();
        $employee = $this->lead_model->getempname();
        /*$getProduct=$this->lead_model->getproductdetail('Product_Id');*/

        //load the view
        $data['CustomerName'] =$customer;
        $data['EmpName'] =$employee;
        $data['ProductName'] =$product;
        $data['main_content'] = 'admin/lead/add';
        $this->load->view('includes/template', $data);  
 
    }       

    /**
    * Update item by his id
    * @return void
    */

public function get()
{

    $Product_Id=$_GET['Product_Id'];
    $this->lead_model->getproductdetail('Product_Id');
     $productdetail["rows"] = $this->lead_model->getproductdetail($Product_Id);
        echo json_encode($productdetail);

}

public function getcustomerdata()
{

    $Customer_Id=$_GET['Customer_Id'];
    $this->lead_model->getcustomerdetail('Customer_Id');
     $customerdetail["rows"] = $this->lead_model->getcustomerdetail($Customer_Id);
        echo json_encode($customerdetail);

}
public function leadorder_submit()
 {
     $order_id=$POST['Order_Id'];
     $EnqNumer=$POST['EnqNumer'];
     $EnqDate=$POST['EnqDate'];
     $PhoneNumber=$POST['PhoneNumber'];
     $date=$POST['Email'];
     $place=$POST['capturethelead'];
     $qunt=$POST['EnqAbout'];
     $Status=$POST['Status'];
     $pay=$POST['Customer_Id'];
     $Reason=$POST['Reason'];
     $taxtype=$POST['Tax_Type'];
     $tax=$POST['Tax'];
     $gReason=$POST['GrossReason'];

    $this->lead_model->save($data);

    if($flash_message == true)
    {

        $id = $this->lead_model->maxvalue();

        $pro = $_POST['id'];
        $qtn = $_POST['unit'];
        $tot = $_POST['tot'];
        $order = $id->row('Order_Id');


        for($i=0; $i<sizeof($pro); $i++)
        {
        $data1['Product_Id']= $pro[$i];
        $data1['Qunantity']= $qtn[$i];
        $data1['Reason']= $tot[$i];
        $data1['Order_Id']= $order;
            
        $this->leadOrder_product_model->getProduct($data1);

        }
    }
     
 }



    


    public function update()
    {
        //product id 
        $id = $this->uri->segment(4);
  
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {
            //form validation
             $this->form_validation->set_rules('Lead_Id', 'Lead_Id', 'required');
            $this->form_validation->set_rules('LeadDate', 'LeadDate');
            $this->form_validation->set_rules('CustomerName', 'CustomerName', 'required');
            $this->form_validation->set_rules('PhoneNumber', 'PhoneNumber');
            $this->form_validation->set_rules('BillingAddress', 'BillingAddress');
            $this->form_validation->set_rules('Email', 'Email');
            $this->form_validation->set_rules('ContactPerson', 'ContactPerson');
           
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
    
                $data_to_store = array(
                   'Lead_Id' => $this->input->post('Lead_Id'),
                    'LeadDate' => $this->input->post('LeadDate'),
                    'CustomerName' => $this->input->post('CustomerName'),
                    'PhoneNumber' => $this->input->post('PhoneNumber'),
                    'Email' => $this->input->post('Email'),
                    'BillingAddress' => $this->input->post('BillingAddress'),
                    'ContactPerson' => $this->input->post('ContactPerson'),
                    'MobileNumber1' => $this->input->post('MobileNumber1'),
                    'Leadassign' => $this->input->post('Leadassign'),
                    'Leadcategory' => $this->input->post('Leadcategory'),
                    'Leadstage' => $this->input->post('Leadstage'),
                    'Leadsource' => $this->input->post('Leadsource'),
                    'CustomerBudget' => $this->input->post('CustomerBudget'),
                    'NextAction' => $this->input->post('NextAction'),
                    'PriofAction' => $this->input->post('PriofAction'),
                    'Leadsubject' => $this->input->post('Leadsubject'),
                    'Leaddetails' => $this->input->post('Leaddetails'),
                    'Approvalstatus' => $this->input->post('Approvalstatus'),
                    'Remark' => $this->input->post('Remark'),
                    'LeadStatus' => $this->input->post('LeadStatus'),
                    'CreatedBy' => $this->session->userdata('user_id'),
                    'CreatedDate' => date("Y-m-d H:i:s")
                    
                );
                //if the insert has returned true then we show the flash message
                if($this->lead_model->update_manufacture($id, $data_to_store) == TRUE){
                    $this->session->set_flashdata('flash_message', 'updated');
                }else{
                    $this->session->set_flashdata('flash_message', 'not_updated');
                }
                redirect('admin/lead/update/'.$id.'');

            }//validation run

        }

        //if we are updating, and the data did not pass trough the validation
        //the code below wel reload the current data

        //product data 
        $data['manufacture'] = $this->lead_model->get_manufacture_by_Lead_Id($id);
        

        

       
        $customer = $this->lead_model->getcustomername();
        $product = $this->lead_model->getproductname();


        /*$getProduct=$this->lead_model->getproductdetail('Product_Id');*/
        
        //load the view
       
        $data['CustomerName'] =$customer;
        $data['ProductName'] =$product;
        //load the view
        $data['main_content'] = 'admin/lead/edit';
        $this->load->view('includes/template', $data);            

    }//update

    /**
    * Delete product by his id
    * @return void
    */
    public function view()
    {
        //product id 
        $id = $this->uri->segment(4);
  
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {
           //form validation
            /* $this->form_validation->set_rules('Order_Id', 'Order_Id', 'required|numeric');
            $this->form_validation->set_rules('EnqNumer', 'EnqNumer', 'required');
            $this->form_validation->set_rules('Quantity', 'Quantity', 'required');
            $this->form_validation->set_rules('Reason', 'Reason', 'required');
            $this->form_validation->set_rules('Email', 'Email', 'required');
            $this->form_validation->set_rules('capturethelead', 'capturethelead', 'required');
            $this->form_validation->set_rules('EnqAbout', 'EnqAbout', 'required');
            $this->form_validation->set_rules('Status', 'Status', 'required');
            $this->form_validation->set_rules('Customer_Id', 'Customer_Id', 'required');
            $this->form_validation->set_rules('Reason', 'Reason', 'required');
            $this->form_validation->set_rules('Tax_type', 'Tax_type', 'required');
            $this->form_validation->set_rules('Tax', 'Tax', 'required');
            $this->form_validation->set_rules('GrossReason', 'GrossReason', 'required');
*/
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
    
                $data_to_store = array(
                   /*'Enq_Id' => $this->input->post('Enq_Id'),
                    'EnqNumer' => $this->input->post('EnqNumer'),
                    'EnqDate' => $this->input->post('EnqDate'),
                    'PhoneNumber' => $this->input->post('PhoneNumber'),
                    'Email' => $this->input->post('Email'),
                    'capturethelead' => $this->input->post('capturethelead'),
                    'EnqAbout' => $this->input->post('EnqAbout'),
                    'Status' => $this->input->post('Status'),
                    'Customer_Id' => $this->input->post('Customer_Id'),
                    'Reason' => $this->input->post('Reason'),*/
                );
                //if the insert has returned true then we show the flash message
                if($this->lead_model->update_manufacture($id, $data_to_store) == TRUE){
                    $this->session->set_flashdata('flash_message', 'updated');
                }else{
                    $this->session->set_flashdata('flash_message', 'not_updated');
                }




                redirect('admin/lead/view/'.$id.'');

            }//validation run

        }

        //if we are updating, and the data did not pass trough the validation
        //the code below wel reload the current data

        //product data 
       
        
    $data['manufacture'] = $this->lead_model->get_manufacture_by_Lead_Id($id);
        

$customer = $this->lead_model->getcustomername();
       


        
        
        //load the view
       
        $data['CustomerName'] =$customer;
       
        
        /*$getProduct=$this->lead_model->getproductdetail('Product_Id');*/

        //load the view
         

        //load the view
        $data['main_content'] = 'admin/lead/view';
        $this->load->view('includes/template', $data);            

    }//update

    /**
    * Delete product by his id
    * @return void
    */
    public function delete()
    {
        //product id 
        $id = $this->uri->segment(4);
        $this->lead_model->delete_manufacture($id);
        redirect('admin/lead');
    }//edit

}