    <div class="container top">
      <div class="row">
     <div class="span10 columns">
        <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url("admin"); ?>">
            <?php echo ucfirst($this->uri->segment(1));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
          <?php echo ucfirst($this->uri->segment(2));?>
        </li>
      </ul>

      <div class="page-header users-header">
        <h2>
          Sales Report
          
        </h2>
      </div>
      
    </div>
   </div>

 <div class="span10 columns">
          <div class="well">
           
             <?php
           
            $attributes = array('class' => 'form-inline reset-margin', 'id' => 'myform');
            echo form_open('admin/salesinvoice', $attributes);
     
            echo form_close();
            ?>

          </div>

          

          <table class="table table-striped table-bordered table-condensed">
            <thead>
              <tr>
                <!-- <th class="header">Invoice_Id</th> -->
                <th class="yellow header headerSortDown">Invoice Number</th>
                <th class="yellow header headerSortDown">Invoice Date</th>
                <th class="yellow header headerSortDown">Customer Name</th>
                
                <th class="yellow header headerSortDown">Tax Amount</th>
                <th class="yellow header headerSortDown">Discount</th>
                <th class="yellow header headerSortDown">TotalAmount</th>
                <th class="yellow header headerSortDown">AdvancePayment</th>
                <th class="yellow header headerSortDown">Balance</th>
                
                
              </tr>
            </thead>
            <tbody>
              <?php
              foreach($capri_salesreport as $row)
              {
                echo '<tr>';
                /*echo '<td>'.$row['Invoice_Id'].'</td>';*/
                echo '<td>'.$row['InvoiceNumber'].'</td>';
                echo '<td>'.date('Y-M-d',strtotime($row['InvoiceDate'])).'</td>';
                echo '<td>'.$row['CustomerName'].'</td>';
                echo '<td>'.$row['TaxAmount'].'</td>';
                echo '<td>'.$row['Discount'].'</td>';
                echo '<td>'.$row['TotalAmount'].'</td>';
                echo '<td>'.$row['AdvancePayment'].'</td>';
                echo '<td>'.$row['Balance'].'</td>';
               
                echo '</tr>';
              }
              ?>      
            </tbody>
          </table>

          <?php echo '<div class="pagination">'.$this->pagination->create_links().'</div>'; ?>
       </div>
   
  
    </div>