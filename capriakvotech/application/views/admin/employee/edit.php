    <div class="container top">
       <div class="row">
        <div class="span10 columns">
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url("admin"); ?>">
            <?php echo ucfirst($this->uri->segment(1));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li>
          <a href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>">
            <?php echo ucfirst($this->uri->segment(2));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
          <a href="#">Update</a>
        </li>
      </ul>
      
      <div class="page-header">
        <h2>
          Updating <?php echo ucfirst($this->uri->segment(2));?>
        </h2>
      </div>
   </div>
 </div>
 
      <?php
      //flash messages
      if($this->session->flashdata('flash_message')){
        if($this->session->flashdata('flash_message') == 'updated')
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Well done!</strong> Employee updated with success.';
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
          echo '</div>';          
        }
      }
      ?>
      
      <?php
      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '');

      //form validation
      echo validation_errors();

      echo form_open('admin/employee/update/'.$this->uri->segment(4).'', $attributes);
      ?>
       <div class="row">
        <div class="span10 columns">
        <fieldset>
         
          <div class="control-group">
            <label for="inputError" class="control-label">Employee Id</label>
            <div class="controls">
              <input type="text" id="" name="Employee_Id" value="<?php echo $manufacture[0]['Employee_Id']; ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Employee Code</label>
            <div class="controls">
              <input type="text" id="" name="EmployeeCode" value="<?php echo $manufacture[0]['EmployeeCode']; ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <form>
          <div class="control-group">
            <label for="inputError" class="control-label">Employee Name</label>
            <div class="controls">
             <input type="text" id="" name="EmployeeName" value="<?php echo $manufacture[0]['EmployeeName']; ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Pan Number</label>
            <div class="controls">
              <input type="text" id="" name="PanNumber" value="<?php echo $manufacture[0]['PanNumber']; ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Date Of Birth</label>
            <div class="controls">
              <input type="text" id="" name="Date_of_birth" value="<?php echo $manufacture[0]['Date_of_birth']; ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Join date</label>
            <div class="controls">
              <input type="text" id="" name="Joindate" value="<?php echo $manufacture[0]['Joindate']; ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Gross Salary</label>
            <div class="controls">
              <input type="text" id="" name="Grosssalary" value="<?php echo $manufacture[0]['Grosssalary']; ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Father Name</label>
            <div class="controls">
             <input type="text" id="" name="FatherName" value="<?php echo $manufacture[0]['FatherName']; ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>

           <!-- <div class="control-group">
            <label for="inputError" class="control-label">Gender</label>
            <div class="controls">
              <input type="text" id="" name="Gender" value="<?php echo $manufacture[0]['Gender']; ?>" >
              
            </div>
          </div>   -->

<div class="control-group">
                <label class="control-label" for="selectError3">Gender</label>
                <div class="controls">
                  <input type="text" id="selectError3" name="Gender" value="<?php echo $manufacture[0]['Gender']; ?>">
                  
                </div>
                </div>  
           </form>
           <!-- <div class="control-group">
            <label for="inputError" class="control-label">Marital Status</label>
            <div class="controls">
              <input type="text" id="" name="MaritalStatus" value="<?php echo $manufacture[0]['MaritalStatus']; ?>" >
              
            </div>
          </div> -->
          <div class="control-group">
                <label class="control-label" for="selectError3">Marital Status</label>
                <div class="controls">
                  <input type="text" id="selectError3" name="MaritalStatus" value="<?php echo $manufacture[0]['MaritalStatus']; ?>">
                  
                </div>
                </div>  
           <div class="control-group">
            <label for="inputError" class="control-label">Nationality</label>
            <div class="controls">
              <input type="text" id="" name="Nationality" value="<?php echo $manufacture[0]['Nationality']; ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label">Emp Street</label>
            <div class="controls">
              <input type="text" id="" name="EmpStreet" value="<?php echo $manufacture[0]['EmpStreet']; ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label">Emp Village</label>
            <div class="controls">
              <input type="text" id="" name="EmpVillage" value="<?php echo $manufacture[0]['EmpVillage']; ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label">Emp City</label>
            <div class="controls">
              <input type="text" id="" name="EmpCity" value="<?php echo $manufacture[0]['EmpCity']; ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label">Emp State</label>
            <div class="controls">
              <input type="text" id="" name="EmpState" value="<?php echo $manufacture[0]['EmpState']; ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label">Emp Pincode</label>
            <div class="controls">
              <input type="text" id="" name="EmpPincode" value="<?php echo $manufacture[0]['EmpPincode']; ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label">Phone Number</label>
            <div class="controls">
              <input type="text" id="" name="PhoneNumber" value="<?php echo $manufacture[0]['PhoneNumber']; ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label">Mobile Number</label>
            <div class="controls">
              <input type="text" id="" name="MobileNumber" value="<?php echo $manufacture[0]['MobileNumber']; ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label">Email</label>
            <div class="controls">
             <input type="text" id="" name="Email" value="<?php echo $manufacture[0]['Email']; ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label">Blood Group</label>
            <div class="controls">
              <input type="text" id="" name="BloodGroup" value="<?php echo $manufacture[0]['BloodGroup']; ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Education</label>
            <div class="controls">
              <input type="text" id="" name="Education" value="<?php echo $manufacture[0]['Education']; ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Total Experience</label>
            <div class="controls">
              <input type="text" id="" name="TotalExperience" value="<?php echo $manufacture[0]['TotalExperience']; ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Photo</label>
            <div class="controls">
              <input type="file" id="" name="Photo" value="<?php echo $manufacture[0]['Photo']; ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="form-actions">
            <button class="btn btn-primary" type="submit">Save changes</button>
            <button class="btn" type="reset">Cancel</button>
          </div>
        </fieldset>
          </div>
          </div>
      <?php echo form_close(); ?>

    </div>
     