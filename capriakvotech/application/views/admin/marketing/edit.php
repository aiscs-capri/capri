       <div class="container top">
        <div class="row">
        <div class="span10 columns">
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url("admin"); ?>">
            <?php echo ucfirst($this->uri->segment(1));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li>
          <a href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>">
            <?php echo ucfirst($this->uri->segment(2));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
          <a href="#">New</a>
        </li>
      </ul>
      
      <div class="page-header">
        <h2>
          Adding <?php echo ucfirst($this->uri->segment(2));?>
        </h2>
      </div>
  </div>
</div>
      <?php
      //flash messages
      if(isset($flash_message)){
        if($flash_message == TRUE)
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Well done!</strong> new product created with success.';
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
          echo '</div>';          
        }
      }
      ?>
      
      <?php
      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '');
      

      //form validation
      echo validation_errors();
      
      echo form_open('admin/marketing/edit', $attributes);
      ?>
        <div class="row">
        <div class="span10 columns">
        <fieldset>
          <div class="control-group">
            <label for="inputError" class="control-label">ScheduleDate</label>
            <div class="controls">
              <input type="text" id="" name="ScheduleDate" value="<?php $date = $manufacture[0]['ScheduleDate'];  echo date('Y-M-d',strtotime($date)); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <!-- <div class="control-group">
            <label for="inputError" class="control-label">ScheduleTime</label>
            <div class="controls">
              <input type="text" id="" name="ScheduleTime" value="<?php $time = $manufacture[0]['ScheduleTime'];  echo time('H:i:s',strtotime($time)); ?>" >
              
            </div>
          </div>    -->       
          <div class="control-group">
            <label for="inputError" class="control-label">Name</label>
            <div class="controls">
              <input type="text" id="" name="Name" value="<?php echo $manufacture[0]['Name']; ?>" >
              <!--<span class="help-inline">Cost Price</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Address</label>
            <div class="controls">
              <input type="text" name="Address" value="<?php echo $manufacture[0]['Address']; ?>" >
              <!--<span class="help-inline">OOps</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Phone</label>
            <div class="controls">
              <input type="text" name="Phone" value="<?php echo $manufacture[0]['Phone']; ?>" >
              <!--<span class="help-inline">OOps</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">ContactPerson</label>
            <div class="controls">
              <input type="text" name="ContactPerson" value="<?php echo $manufacture[0]['ContactPerson']; ?>" >
              <!--<span class="help-inline">OOps</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Mobile</label>
            <div class="controls">
              <input type="text" name="Mobile" value="<?php echo $manufacture[0]['Mobile']; ?>" >
              <!--<span class="help-inline">OOps</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Status</label>
            <div class="controls">
              <input type="text" name="Status" value="<?php echo $manufacture[0]['Status']; ?>" >
              
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Message</label>
            <div class="controls">
              <input type="text" name="message" value="<?php echo $manufacture[0]['message']; ?>" >
              <!--<span class="help-inline">OOps</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Employee Name</label>
            <div class="controls">
              <input type="text" name="EmployeeName" value="<?php echo $manufacture[0]['EmployeeName']; ?>" >
              <!--<span class="help-inline">OOps</span>-->
            </div>
          </div>
          
          </div>
        </div>
        <div class="row">
             <div class=col-md-10>
          <div class="form-actions">
          <button class="btn btn-primary" type="submit">Save changes</button>
            <button class="btn" type="reset">Cancel</button>
          </div>
          </div>
          </div>
        </fieldset>

      <?php echo form_close(); ?>

    </div>
     