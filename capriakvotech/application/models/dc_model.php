<?php
class dc_model extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return voDc_Id
    */
    public function __construct()
    {
        $this->load->database();
    }

    /**
    * Get product by his is
    * @param int $product_Dc_Id 
    * @return array
    */
    public function get_manufacture_by_Dc_Id($Dc_Id)
    {
        $this->db->select('*');
        $this->db->from('capri_dc_entry');
        $this->db->where('Dc_Id', $Dc_Id);
        $query = $this->db->get();
        return $query->result_array(); 
    }    


public function save($Dc_Id)
    {
        $insert = $this->db->insert('capri_dc_entry', $data);
        return $insert;
    }   

    public function maxvalue()
    {
        $this->db->select_MAX('Dc_Id');
        $this->db->from('capri_dc_entry');
        $query = $this->db->get();
        return $query;

    }

  /* view order and supplier*/
     public function getdcOrderDetails($purchase_order)
    {
        $this->db->select('capri_customer_enquiry.enquiry_number, capri_customer_enquiry.CustomerName, capri_master_customer.CustomerName, capri_master_customer.BillingAddress, capri_master_customer.TIN,capri_master_customer.CST');
        $this->db->from('capri_customer_enquiry');
        $this->db->join('capri_master_customer','capri_customer_enquiry.CustomerName = capri_master_customer.CustomerName','inner');
        $this->db->where('capri_customer_enquiry.enquiry_Id',$purchase_order);
        $query = $this->db->get();
        return $query->result();
    }


/* show the product item details*/
public function getdcOrderproductDetails($dc_order_product)
    {
        $this->db->select('Id,ProductName,ProductUnit,ProductPrice,Quantity,Total');
        $this->db->from('capri_dc_details');       
        $this->db->where('capri_dc_details.Dc_Id',$dc_order_product);
        $query = $this->db->get();
        return $query->result();
    }


    
    
    /**
    * Fetch dc_master data from the database
    * possibility to mix search, filter and order
    * @param string $search_string 
    * @param strong $order
    * @param string $order_type 
    * @param int $limit_start
    * @param int $limit_end
    * @return array
    */
    public function get_dc($Supplier_Id=null, $search_string=null, $order=null, $order_type='Asc', $limit_start, $limit_end)
    {
        
        $this->db->select('capri_dc_entry.Dc_Id');
        $this->db->select('capri_dc_entry.Dc_number');
        $this->db->select('capri_dc_entry.DcDate');
        $this->db->select('capri_dc_entry.podate');
        $this->db->select('capri_dc_entry.po_number');
        $this->db->select('capri_dc_entry.CustomerName');
        $this->db->select('capri_dc_entry.Address');
        $this->db->select('capri_dc_entry.TIN');
        $this->db->select('capri_dc_entry.CST');
        $this->db->select('capri_dc_entry.Remark');
        $this->db->select('capri_dc_entry.ourref');
        $this->db->from('capri_dc_entry');
        
        

        $this->db->group_by('capri_dc_entry.Dc_Id');

        if($order){
            $this->db->order_by($order, $order_type);
        }else{
            $this->db->order_by('Dc_Id', $order_type);
        }


        $this->db->limit($limit_start, $limit_end);
        //$this->db->limit('4', '4');


        $query = $this->db->get();
        
        return $query->result_array();  
    }
    /**
    * Count the number of rows
    * @param int $search_string
    * @param int $order
    * @return int
    */

    public function getcustomername()
{

    $this->db->select('Customer_Id,CustomerName');
    $this -> db -> from('capri_master_customer');  
    $query = $this -> db -> get();
    return $query->result();
}


    public function getproductname()
{

    $this->db->select('Product_Id,ProductName');
    $this -> db -> from('capri_master_product');

    $query = $this -> db -> get();
    return $query->result();
}
public function getproductdetail($Product_Id)
{

    $this->db->select('Product_Id,ProductPrice,ProductUnit');
    $this->db->where('Product_Id', $Product_Id);
    $this->db->from('capri_master_product');
    $query=$this->db->get();
    return $query->result();
}

/*$this->db->select('concat(BillStreet,',',BillVillage,',',BillCity,',',billstate,',',billpincode) as address,CST,TIN');*/
public function getcustomerdetail($Customer_Id)
{

    $this->db->select('BillingAddress,CST,TIN');
    $this->db->from('capri_master_customer');
    $this->db->where('Customer_Id', $Customer_Id);
    $query=$this->db->get();
    return $query->result();
}

     public function get_order($Product_Id=null, $search_string=null, $order=null, $order_type='Asc', $limit_start, $limit_end)
    {
        
        $this->db->select('capri_dc_entry_details.Id');
        $this->db->select('capri_dc_entry_details.Product_Id');
        $this->db->select('capri_dc_entry_details.Quantity');
        $this->db->select('capri_dc_entry_details.Total');
         $this->db->select('capri_dc_entry_details.Dc_Id');
        $this->db->select('capri_dc_entry_details.Product_Id');
        $this->db->select('capri_master_product.Product_Id as Product_Id');
        $this->db->from('capri_dc_entry_details');
        if($Product_Id != null && $Product_Id != 0){
            $this->db->where('Product_Id', $Product_Id);
        }
        if($search_string){
            $this->db->like('Product_Id', $search_string);
        }

        $this->db->join('capri_master_product', 'capri_dc_entry_details.Product_Id = capri_master_product.Product_Id', 'left');
       
        
        $this->db->group_by('capri_dc_entry_details.Id');

        if($order){
            $this->db->order_by($order, $order_type);
        }else{
            $this->db->order_by('Id', $order_type);
        }


        $this->db->limit($limit_start, $limit_end);
        //$this->db->limit('4', '4');


        $query = $this->db->get();
        
        return $query->result_array();  
    }
    /**
    * Count the number of rows
    * @param int $search_string
    * @param int $order
    * @return int
    */
   function count_dc($Supplier_Id=null, $search_string=null, $order=null)
    {
        $this->db->select('*');
        $this->db->from('capri_dc_entry');
        if($Supplier_Id != null && $Supplier_Id != 0){
            $this->db->where('Supplier_Id', $Supplier_Id);
        }
        if($search_string){
            $this->db->like('Order_number', $search_string);
        }
        if($order){
            $this->db->order_by($order, 'Asc');
        }else{
            $this->db->order_by('Dc_Id', 'Asc');
        }
        $query = $this->db->get();
        return $query->num_rows();        
    }


    /**
    * Store the new item into the database
    * @param array $data - associative array with data to store
    * @return boolean 
    */
    function store_manufacture($data)
    {
        $insert = $this->db->insert('capri_dc_entry', $data);
        return $insert;
    }

    /**
    * Update manufacture
    * @param array $data - associative array with data to store
    * @return boolean
    */
    function update_manufacture($Dc_Id, $data)
    {
        $this->db->where('Dc_Id', $Dc_Id);
        $this->db->update('capri_dc_entry', $data);
        $report = array();
        $report['error'] = $this->db->_error_number();
        $report['message'] = $this->db->_error_message();
        if($report !== 0){
            return true;
        }else{
            return false;
        }
    }

    /**
    * Delete manufacturer
    * @param int $Dc_Id - manufacture Dc_Id
    * @return boolean
    */
    function delete_manufacture($Dc_Id){
        
        $this->db->where('Dc_Id', $Dc_Id);
        $this->db->delete('capri_dc_entry'); 
    }
 
function delete_order_details($Id){

    $this->db->where('Id',$Id);
    $this->db->delete('capri_dc_details');
}


function insert_order_details($data)
    {
        $insert = $this->db->insert('capri_dc_details', $data);
        return $insert;
    }

    function dc_order_delete_details($Id){

    $this->db->where('Dc_Id',$Id);
    $this->db->delete('capri_dc_details');
 }


public function order_details_maxvalue()
    {
        $this->db->select_MAX('Id');
        $this->db->from('capri_dc_details');
        $query = $this->db->get();
        return $query;

    }


}
