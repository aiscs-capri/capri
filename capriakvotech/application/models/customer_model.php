<?php
class customer_model extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return voCustomer_Id
    */
    public function __construct()
    {
        $this->load->database();
    }

    /**
    * Get product by his is
    * @param int $product_Customer_Id 
    * @return array
    */
    public function get_manufacture_by_Customer_Id($Customer_Id)
    {
		$this->db->select('*');
		$this->db->from('capri_master_customer');
		$this->db->where('Customer_Id', $Customer_Id);
		$query = $this->db->get();
		return $query->result_array(); 
    }    

    /**
    * Fetch customer_master data from the database
    * possibility to mix search, filter and order
    * @param string $search_string 
    * @param strong $order
    * @param string $order_type 
    * @param int $limit_start
    * @param int $limit_end
    * @return array
    */
    public function get_customer($search_string=null, $order=null, $order_type='Asc', $limit_start=null, $limit_end=null)
    {
	    
		$this->db->select('*');
		$this->db->from('capri_master_customer');

		if($search_string){
			$this->db->like('CustomerCode', $search_string);
		}
		$this->db->group_by('Customer_Id');

		if($order){
			$this->db->order_by($order, $order_type);
		}else{
		    $this->db->order_by('Customer_Id', $order_type);
		}

        if($limit_start && $limit_end){
          $this->db->limit($limit_start, $limit_end);	
        }

        if($limit_start != null){
          $this->db->limit($limit_start, $limit_end);    
        }
        
		$query = $this->db->get();
		
		return $query->result_array(); 	
    }

    /**
    * Count the number of rows
    * @param int $search_string
    * @param int $order
    * @return int
    */
    function count_customer($search_string=null, $order=null)
    {
		$this->db->select('*');
		$this->db->from('capri_master_customer');
		if($search_string){
			$this->db->like('CustomerCode', $search_string);
		}
		if($order){
			$this->db->order_by($order, 'Asc');
		}else{
		    $this->db->order_by('Customer_Id', 'Asc');
		}
		$query = $this->db->get();
		return $query->num_rows();        
    }

    /**
    * Store the new item into the database
    * @param array $data - associative array with data to store
    * @return boolean 
    */
    function store_manufacture($data)
    {
		$insert = $this->db->insert('capri_master_customer', $data);
	    return $insert;
	}

    /**
    * Update manufacture
    * @param array $data - associative array with data to store
    * @return boolean
    */
    function update_manufacture($Customer_Id, $data)
    {
		$this->db->where('Customer_Id', $Customer_Id);
		$this->db->update('capri_master_customer', $data);
		$report = array();
		$report['error'] = $this->db->_error_number();
		$report['message'] = $this->db->_error_message();
		if($report !== 0){
			return true;
		}else{
			return false;
		}
	}

    /**
    * Delete manufacturer
    * @param int $Customer_Id - manufacture Customer_Id
    * @return boolean
    */
	function delete_manufacture($Customer_Id){
		$this->db->where('Customer_Id', $Customer_Id);
		$this->db->delete('capri_master_customer'); 
	}
 
}
	
