<?php
class Admin_employee extends CI_Controller {

    /**
    * name of the folder responsible for the views 
    * which are manipulated by this controller
    * @constant string
    */
    const VIEW_FOLDER = 'admin/employee';
 
    /**
    * Responsable for auto load the model
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('employee_model');

        if(!$this->session->userdata('is_logged_in')){
            redirect('admin/login');
        }
    }
 
    /**
    * Load the main view with all the current model model's data.
    * @return void
    */
    public function index()
    {

        //all the posts sent by the view
        $search_string = $this->input->post('search_string');        
        $order = $this->input->post('order'); 
        $order_type = $this->input->post('order_type'); 

        //pagination settings
        $config['per_page'] = 5;

        $config['base_url'] = base_url().'admin/employee';
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = 20;
        $config['full_tag_open'] = '<ul>';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';

        //limit end
        $page = $this->uri->segment(3);

        //math to get the initial record to be select in the database
        $limit_end = ($page * $config['per_page']) - $config['per_page'];
        if ($limit_end < 0){
            $limit_end = 0;
        } 

        //if order type was changed
        if($order_type){
            $filter_session_data['order_type'] = $order_type;
        }
        else{
            //we have something stored in the session? 
            if($this->session->userdata('order_type')){
                $order_type = $this->session->userdata('order_type');    
            }else{
                //if we have nothing inside session, so it's the default "Asc"
                $order_type = 'Asc';    
            }
        }
        //make the data type var avaible to our view
        $data['order_type_selected'] = $order_type;        


        //we must avoid a page reload with the previous session data
        //if any filter post was sent, then it's the first time we load the content
        //in this case we clean the session filter data
        //if any filter post was sent but we are in some page, we must load the session data

        //filtered && || paginated
        if($search_string !== false && $order !== false || $this->uri->segment(3) == true){ 
           
            /*
            The comments here are the same for line 79 until 99

            if post is not null, we store it in session data array
            if is null, we use the session data already stored
            we save order into the the var to load the view with the param already selected       
            */
            if($search_string){
                $filter_session_data['search_string_selected'] = $search_string;
            }else{
                $search_string = $this->session->userdata('search_string_selected');
            }
            $data['search_string_selected'] = $search_string;

            if($order){
                $filter_session_data['order'] = $order;
            }
            else{
                $order = $this->session->userdata('order');
            }
            $data['order'] = $order;

            //save session data into the session
            if(isset($filter_session_data)){
              $this->session->set_userdata($filter_session_data);    
            }
            
            //fetch sql data into arrays
            $data['count_employee']= $this->employee_model->count_employee($search_string, $order);
            $config['total_rows'] = $data['count_employee'];

            //fetch sql data into arrays
            if($search_string){
                if($order){
                    $data['employee'] = $this->employee_model->get_employee($search_string, $order, $order_type, $config['per_page'],$limit_end);        
                }else{
                    $data['employee'] = $this->employee_model->get_employee($search_string, '', $order_type, $config['per_page'],$limit_end);           
                }
            }else{
                if($order){
                    $data['employee'] = $this->employee_model->get_employee('', $order, $order_type, $config['per_page'],$limit_end);        
                }else{
                    $data['employee'] = $this->employee_model->get_employee('', '', $order_type, $config['per_page'],$limit_end);        
                }
            }

        }else{

            //clean filter data inside section
            $filter_session_data['manufacture_selected'] = null;
            $filter_session_data['search_string_selected'] = null;
            $filter_session_data['order'] = null;
            $filter_session_data['order_type'] = null;
            $this->session->set_userdata($filter_session_data);

            //pre selected options
            $data['search_string_selected'] = '';
            $data['order'] = 'id';

            //fetch sql data into arrays
            $data['count_employee']= $this->employee_model->count_employee();
            $data['employee'] = $this->employee_model->get_employee('', '', $order_type, $config['per_page'],$limit_end);        
            $config['total_rows'] = $data['count_employee'];

        }//!isset($search_string) && !isset($order)
         
        //initializate the panination helper 
        $this->pagination->initialize($config);   

        //load the view
        $data['main_content'] = 'admin/employee/list';
        $this->load->view('includes/template', $data);  

    }//index


    public function add()
    {
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {

            //form validation
            
            $this->form_validation->set_rules('Employee_Id', 'Employee_Id');
            $this->form_validation->set_rules('EmployeeCode', 'EmployeeCode', 'required');
            $this->form_validation->set_rules('EmployeeName', 'EmployeeName', 'required');
            $this->form_validation->set_rules('PanNumber', 'PanNumber', 'required');
            $this->form_validation->set_rules('Date_of_birth', 'Date_of_birth', 'required');
            $this->form_validation->set_rules('Joindate', 'Joindate', 'required');
            $this->form_validation->set_rules('Grosssalary', 'Grosssalary', 'required|numeric');
            $this->form_validation->set_rules('FatherName', 'FatherName', 'required');
            $this->form_validation->set_rules('Gender', 'Gender', 'required');
            $this->form_validation->set_rules('MaritalStatus', 'MaritalStatus', 'required');
            $this->form_validation->set_rules('Nationality', 'Nationality', 'required');
            $this->form_validation->set_rules('EmpStreet', 'EmpStreet', 'required');
            $this->form_validation->set_rules('EmpVillage', 'EmpVillage', 'required');
            $this->form_validation->set_rules('EmpCity', 'EmpCity', 'required');
            $this->form_validation->set_rules('EmpState', 'EmpState', 'required');
            $this->form_validation->set_rules('EmpPincode', 'EmpPincode', 'required');
            $this->form_validation->set_rules('PhoneNumber', 'PhoneNumber', 'required|numeric');
            $this->form_validation->set_rules('MobileNumber', 'MoblieNumber', 'required|numeric');
             $this->form_validation->set_rules('EmergencyNumber', 'EmergencyNumber', 'required|numeric');
            $this->form_validation->set_rules('Email', 'Email', 'required|valid mail');
            $this->form_validation->set_rules('BloodGroup', 'BloodGroup', 'required');
            $this->form_validation->set_rules('Education', 'Education', 'required');
            $this->form_validation->set_rules('TotalExperience', 'TotalExperience', 'required');
            $this->form_validation->set_rules('Photo', 'Photo', 'required');


            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            

            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
                $data_to_store = array(
                    
                    'Employee_Id' => $this->input->post('Employee_Id'),
                    'EmployeeCode' => $this->input->post('EmployeeCode'),
                    'EmployeeName' => $this->input->post('EmployeeName'),
                    'PanNumber' => $this->input->post('PanNumber'),
                    'Date_of_birth' => $this->input->post('Date_of_birth'),
                    'Joindate' => $this->input->post('Joindate'),
                    'Grosssalary' => $this->input->post('Grosssalary'),
                    'FatherName' => $this->input->post('FatherName'),
                    'Gender' => $this->input->post('Gender'),
                    'MaritalStatus' => $this->input->post('MaritalStatus'),
                    'Nationality' => $this->input->post('Nationality'),
                    'EmpStreet' => $this->input->post('EmpStreet'),
                    'EmpVillage' => $this->input->post('EmpVillage'),
                    'EmpCity' => $this->input->post('EmpCity'),
                    'EmpState' => $this->input->post('EmpState'),
                    'EmpPincode' => $this->input->post('EmpPincode'),
                    'PhoneNumber' => $this->input->post('PhoneNumber'),
                    'MobileNumber' => $this->input->post('MobileNumber'),
                    'EmergencyNumber' => $this->input->post('EmergencyNumber'),
                    'Email' => $this->input->post('Email'),
                    'BloodGroup' => $this->input->post('BloodGroup'),
                    'Education' => $this->input->post('Education'),
                    'TotalExperience' => $this->input->post('TotalExperience'),
                    'Photo' => $this->input->post('Photo'),
                    'CreatedBy' => $this->session->userdata('user_id'),
                    'CreatedDate' => date("Y-m-d H:i:s")


                );
                //if the insert has returned true then we show the flash message
                if($this->employee_model->store_manufacture($data_to_store)){
                    $data['flash_message'] = TRUE; 
                }else{
                    $data['flash_message'] = FALSE; 
                }

            }

        }
        //load the view
        $data['main_content'] = 'admin/employee/add';
        $this->load->view('includes/template', $data);  
    }       

    /**
    * Update item by his id
    * @return void
    */
    public function update()
    {
        //product id 
        $id = $this->uri->segment(4);
  
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {
            //form validation
           $this->form_validation->set_rules('Employee_Id', 'Employee_Id', 'required');
            $this->form_validation->set_rules('EmployeeCode', 'EmployeeCode', 'required');
            $this->form_validation->set_rules('EmployeeName', 'EmployeeName', 'required');
            $this->form_validation->set_rules('PanNumber', 'PanNumber', 'required');
            $this->form_validation->set_rules('Date_of_birth', 'Date_of_birth', 'required');
            $this->form_validation->set_rules('Joindate', 'Joindate', 'required');
            $this->form_validation->set_rules('Grosssalary', 'Grosssalary', 'required|numeric');
            $this->form_validation->set_rules('FatherName', 'FatherName', 'required');
            $this->form_validation->set_rules('Gender', 'Gender', 'required');
            $this->form_validation->set_rules('MaritalStatus', 'MaritalStatus', 'required');
            $this->form_validation->set_rules('Nationality', 'Nationality', 'required');
            $this->form_validation->set_rules('EmpStreet', 'EmpStreet', 'required');
            $this->form_validation->set_rules('EmpVillage', 'EmpVillage', 'required');
            $this->form_validation->set_rules('EmpCity', 'EmpCity', 'required');
            $this->form_validation->set_rules('EmpState', 'EmpState', 'required');
            $this->form_validation->set_rules('EmpPincode', 'EmpPincode', 'required');
            $this->form_validation->set_rules('PhoneNumber', 'PhoneNumber', 'required|numeric');
            $this->form_validation->set_rules('MobileNumber', 'MoblieNumber', 'required|numeric');
             $this->form_validation->set_rules('EmergencyNumber', 'EmergencyNumber', 'required|numeric');
            $this->form_validation->set_rules('Email', 'Email', 'required|valid mail');
            $this->form_validation->set_rules('BloodGroup', 'BloodGroup', 'required');
            $this->form_validation->set_rules('Education', 'Education', 'required');
            $this->form_validation->set_rules('TotalExperience', 'TotalExperience', 'required');
            $this->form_validation->set_rules('Photo', 'Photo', 'required');



            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
    
                $data_to_store = array(
                   'Employee_Id' => $this->input->post('Employee_Id'),
                    'EmployeeCode' => $this->input->post('EmployeeCode'),
                    'EmployeeName' => $this->input->post('EmployeeName'),
                    'PanNumber' => $this->input->post('PanNumber'),
                    'Date_of_birth' => $this->input->post('Date_of_birth'),
                    'Joindate' => $this->input->post('Joindate'),
                    'Grosssalary' => $this->input->post('Grosssalary'),
                    'FatherName' => $this->input->post('FatherName'),
                    'Gender' => $this->input->post('Gender'),
                    'MaritalStatus' => $this->input->post('MaritalStatus'),
                    'Nationality' => $this->input->post('Nationality'),
                    'EmpStreet' => $this->input->post('EmpStreet'),
                    'EmpVillage' => $this->input->post('EmpVillage'),
                    'EmpCity' => $this->input->post('EmpCity'),
                    'EmpState' => $this->input->post('EmpState'),
                    'EmpPincode' => $this->input->post('EmpPincode'),
                    'PhoneNumber' => $this->input->post('PhoneNumber'),
                    'MobileNumber' => $this->input->post('MobileNumber'),
                    'EmergencyNumber' => $this->input->post('EmergencyNumber'),
                    'Email' => $this->input->post('Email'),
                    'BloodGroup' => $this->input->post('BloodGroup'),
                    'Education' => $this->input->post('Education'),
                    'TotalExperience' => $this->input->post('TotalExperience'),
                    'Photo' => $this->input->post('Photo'),


                );
                //if the insert has returned true then we show the flash message
                if($this->employee_model->update_manufacture($id, $data_to_store) == TRUE){
                    $this->session->set_flashdata('flash_message', 'updated');
                }else{
                    $this->session->set_flashdata('flash_message', 'not_updated');
                }
                redirect('admin/employee/update/'.$id.'');

            }//validation run

        }

        //if we are updating, and the data did not pass trough the validation
        //the code below wel reload the current data

        //product data 
        $data['manufacture'] = $this->employee_model->get_manufacture_by_Employee_Id($id);
        //load the view
        $data['main_content'] = 'admin/employee/edit';
        $this->load->view('includes/template', $data);            

    }//update

    /**
    * Delete product by his id
    * @return void
    */
    public function view()
    {
        //product id 
        $id = $this->uri->segment(4);
  
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {
            //form validation
           $this->form_validation->set_rules('Employee_Id', 'Employee_Id', 'required');
            $this->form_validation->set_rules('EmployeeCode', 'EmployeeCode', 'required');
            $this->form_validation->set_rules('EmployeeName', 'EmployeeName', 'required');
            $this->form_validation->set_rules('PanNumber', 'PanNumber', 'required');
            $this->form_validation->set_rules('Date_of_birth', 'Date_of_birth', 'required');
            $this->form_validation->set_rules('Joindate', 'Joindate', 'required');
            $this->form_validation->set_rules('Grosssalary', 'Grosssalary', 'required|numeric');
            $this->form_validation->set_rules('FatherName', 'FatherName', 'required');
            $this->form_validation->set_rules('Gender', 'Gender', 'required');
            $this->form_validation->set_rules('MaritalStatus', 'MaritalStatus', 'required');
            $this->form_validation->set_rules('Nationality', 'Nationality', 'required');
            $this->form_validation->set_rules('EmpStreet', 'EmpStreet', 'required');
            $this->form_validation->set_rules('EmpVillage', 'EmpVillage', 'required');
            $this->form_validation->set_rules('EmpCity', 'EmpCity', 'required');
            $this->form_validation->set_rules('EmpState', 'EmpState', 'required');
            $this->form_validation->set_rules('EmpPincode', 'EmpPincode', 'required');
            $this->form_validation->set_rules('PhoneNumber', 'PhoneNumber', 'required|numeric');
            $this->form_validation->set_rules('MobileNumber', 'MoblieNumber', 'required|numeric');
             $this->form_validation->set_rules('EmergencyNumber', 'EmergencyNumber', 'required|numeric');
            $this->form_validation->set_rules('Email', 'Email', 'required|valid mail');
            $this->form_validation->set_rules('BloodGroup', 'BloodGroup', 'required');
            $this->form_validation->set_rules('Education', 'Education', 'required');
            $this->form_validation->set_rules('TotalExperience', 'TotalExperience', 'required');
            $this->form_validation->set_rules('Photo', 'Photo', 'required');



            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
    
                $data_to_store = array(
                   'Employee_Id' => $this->input->post('Employee_Id'),
                    'EmployeeCode' => $this->input->post('EmployeeCode'),
                    'EmployeeName' => $this->input->post('EmployeeName'),
                    'PanNumber' => $this->input->post('PanNumber'),
                    'Date_of_birth' => $this->input->post('Date_of_birth'),
                    'Joindate' => $this->input->post('Joindate'),
                    'Grosssalary' => $this->input->post('Grosssalary'),
                    'FatherName' => $this->input->post('FatherName'),
                    'Gender' => $this->input->post('Gender'),
                    'MaritalStatus' => $this->input->post('MaritalStatus'),
                    'Nationality' => $this->input->post('Nationality'),
                    'EmpStreet' => $this->input->post('EmpStreet'),
                    'EmpVillage' => $this->input->post('EmpVillage'),
                    'EmpCity' => $this->input->post('EmpCity'),
                    'EmpState' => $this->input->post('EmpState'),
                    'EmpPincode' => $this->input->post('EmpPincode'),
                    'PhoneNumber' => $this->input->post('PhoneNumber'),
                    'MobileNumber' => $this->input->post('MobileNumber'),
                    'EmergencyNumber' => $this->input->post('EmergencyNumber'),
                    'Email' => $this->input->post('Email'),
                    'BloodGroup' => $this->input->post('BloodGroup'),
                    'Education' => $this->input->post('Education'),
                    'TotalExperience' => $this->input->post('TotalExperience'),
                    'Photo' => $this->input->post('Photo'),


                );
                //if the insert has returned true then we show the flash message
                if($this->employee_model->update_manufacture($id, $data_to_store) == TRUE){
                    $this->session->set_flashdata('flash_message', 'updated');
                }else{
                    $this->session->set_flashdata('flash_message', 'not_updated');
                }
                redirect('admin/employee/update/'.$id.'');

            }//validation run

        }

        //if we are updating, and the data did not pass trough the validation
        //the code below wel reload the current data

        //product data 
        $data['manufacture'] = $this->employee_model->get_manufacture_by_Employee_Id($id);
        //load the view
        $data['main_content'] = 'admin/employee/view';
        $this->load->view('includes/template', $data);            

    }//update

    /**
    * Delete product by his id
    * @return void
    */
    public function delete()
    {
        //product id 
        $id = $this->uri->segment(4);
        $this->employee_model->delete_manufacture($id);
        redirect('admin/employee');
    }//edit

}