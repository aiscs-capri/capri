    <div class="container top">
      
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url("admin"); ?>">
            <?php echo ucfirst($this->uri->segment(1));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li>
          <a href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>">
            <?php echo ucfirst($this->uri->segment(2));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
          <a href="#">Update</a>
        </li>
      </ul>
      
      <div class="page-header">
        <h2>
          Updating <?php echo ucfirst($this->uri->segment(2));?>
        </h2>
      </div>

 
      <?php
      //flash messages
      if($this->session->flashdata('flash_message')){
        if($this->session->flashdata('flash_message') == 'updated')
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Well done!</strong> Supplier updated with success.';
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
          echo '</div>';          
        }
      }
      ?>
      
      <?php
      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '');

      //form validation
      echo validation_errors();

      echo form_open('admin/supplier/update/'.$this->uri->segment(4).'', $attributes);
      ?>
        <fieldset>
         
          <div class="control-group">
            <label for="inputError" class="control-label">Supplier ID</label>
            <div class="controls">
              <input type="text" id="" name="Supplier_Id" value="<?php echo $manufacture[0]['Supplier_Id']; ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Supplier Code</label>
            <div class="controls">
              <input type="text" id="" name="SupplierCode" value="<?php echo $manufacture[0]['SupplierCode']; ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <form>
          <div class="control-group">
            <label for="inputError" class="control-label">Supplier Name</label>
            <div class="controls">
             <input type="text" id="" name="SupplierName" value="<?php echo $manufacture[0]['SupplierName']; ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Billing Address</label>
            <div class="controls">
              <input type="text" id="" name="BillingAddress" value="<?php echo $manufacture[0]['BillingAddress']; ?>" readonly>
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Shipping Address</label>
            <div class="controls">
              <input type="text" id="" name="ShippingAddress" value="<?php echo $manufacture[0]['ShippingAddress']; ?>" readonly>
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          
          
          <input type="checkbox" name="billingtoo" onclick="FillBilling(this.form)">
<em>same address</em>
         
         
          

           <div class="control-group">
            <label for="inputError" class="control-label">Phone Number</label>
            <div class="controls">
              <input type="text" id="" name="PhoneNumber" value="<?php echo $manufacture[0]['PhoneNumber']; ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>   </form>
           <div class="control-group">
            <label for="inputError" class="control-label">Contact Person</label>
            <div class="controls">
              <input type="text" id="" name="ContactPerson" value="<?php echo $manufacture[0]['ContactPerson']; ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label">Mobile Number1</label>
            <div class="controls">
              <input type="text" id="" name="MobileNumber1" value="<?php echo $manufacture[0]['MobileNumber1']; ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label">Mobile Number2</label>
            <div class="controls">
              <input type="text" id="" name="MobileNumber2" value="<?php echo $manufacture[0]['MobileNumber2']; ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label">Email</label>
            <div class="controls">
              <input type="text" id="" name="Email" value="<?php echo $manufacture[0]['Email']; ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label">Fax</label>
            <div class="controls">
              <input type="text" id="" name="Fax" value="<?php echo $manufacture[0]['Fax']; ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label">Tin Number</label>
            <div class="controls">
             <input type="text" id="" name="TIN" value="<?php echo $manufacture[0]['TIN']; ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label">CST Number</label>
            <div class="controls">
              <input type="text" id="" name="CST" value="<?php echo $manufacture[0]['CST']; ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label">ECC Number</label>
            <div class="controls">
              <input type="text" id="" name="ECC" value="<?php echo $manufacture[0]['ECC']; ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="form-actions">
            <button class="btn btn-primary" type="submit">Save changes</button>
            <button class="btn" type="reset">Cancel</button>
          </div>
        </fieldset>

      <?php echo form_close(); ?>

    </div>
     