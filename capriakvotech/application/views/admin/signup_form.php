<!DOCTYPE html> 
<html lang="en-US">
  <head>
    <title>CodeIgniter Admin Sample Project</title>
    <meta charset="utf-8">
    <link href="<?php echo base_url(); ?>assets/css/admin/global.css" rel="stylesheet" type="text/css">
  </head>
  <body>
<?php
//form validation
echo validation_errors();
?>  	
<div class="container login">
<?php
$attributes = array('class' => 'form-signin');   
echo form_open('admin/create_member', $attributes);
echo '<h2 class="form-signin-heading">Create an account</h2>';
echo form_input('FirstName', set_value('FirstName'), 'placeholder="First name"');
echo form_input('LastName', set_value('LastName'), 'placeholder="Last name"');
echo form_input('Email', set_value('Email'), 'placeholder="Email"');
echo form_input('Mobile', set_value('Mobile'), 'placeholder="Mobile"');

echo form_password('Password', '', 'placeholder="Password"');

echo form_hidden('Active','1', set_value('active'), 'placeholder="Active"');


echo form_submit('submit', 'submit', 'class="btn btn-large btn-primary"');
echo form_close();
?>
</div>
    <script src="<?php echo base_url(); ?>assets/js/jquery-1.7.1.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
  </body>
</html>    