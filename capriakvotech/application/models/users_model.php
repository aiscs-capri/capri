<?php

class Users_model extends CI_Model {

    /**
    * Validate the login's data with the database
    * @param string $user_name
    * @param string $password
    * @return void
    */
	function validate($Email, $Password)
	{
   $this -> db -> select('R_id,Email,Password');
   $this -> db -> from('capri_login_register');
   $this -> db -> where('Email', $Email);
   $this -> db -> where('Password', $Password);
   $this -> db -> where('Active', true);
   $this -> db -> limit(1);
	 
   $query = $this -> db -> get();

   if($query -> num_rows() == 1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
 }

    /**
    * Serialize the session data stored in the database, 
    * store it in a new array and return it to the controller 
    * @return array
    */
	function get_db_session_data()
	{
		$query = $this->db->select('user_data')->get('ci_sessions');
		$user = array(); /* array to store the user data we fetch */
		foreach ($query->result() as $row)
		{
		    $udata = unserialize($row->user_data);
		    /* put data in array using username as key */
		    $user['user_name'] = $udata['user_name']; 
		    $user['is_logged_in'] = $udata['is_logged_in']; 
		}
		return $user;
	}
	
    /**
    * Store the new user's data into the database
    * @return boolean - check the insert
    */	
	function create_member()
	{

		$this->db->where('Email', $this->input->post('Email'));
		$query = $this->db->get('capri_login_register');

        if($query->num_rows > 0){
        	echo '<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>';
			  echo "Email already taken";	
			echo '</strong></div>';
		}else{

			$new_member_insert_data = array(
				'FirstName' => $this->input->post('FirstName'),
				'LastName' => $this->input->post('LastName'),
				'Email' => $this->input->post('Email'),
				'Mobile' => $this->input->post('Mobile'),			
				'PassWord' => md5($this->input->post('Password')),
				
				'Active' => $this->input->post('Active')						
			);
			$insert = $this->db->insert('capri_login_register', $new_member_insert_data);
		    return $insert;
		}
	      
	}//create_member
}