<?php
class order_model extends CI_Model {
 
    /**
    * Responsable for auto load the database
    * @return voId
    */
    public function __construct()
    {
        $this->load->database();
    }

    /**
    * Get product by his is
    * @param int $product_Id 
    * @return array
    */
    public function get_manufacture_by_Id($Id)
    {
		$this->db->select('*');
		$this->db->from('capri_purchase_order_product');
		$this->db->where('Id', $Id);
		$query = $this->db->get();
		return $query->result_array(); 
    }    

    /**
    * Fetch purchase_master data from the database
    * possibility to mix search, filter and order
    * @param string $search_string 
    * @param strong $order
    * @param string $order_type 
    * @param int $limit_start
    * @param int $limit_end
    * @return array
    */
    public function get_order($Product_Id=null, $search_string=null, $order=null, $order_type='Asc', $limit_start, $limit_end)
    {
        
        $this->db->select('capri_purchase_order_product.Id');
        $this->db->select('capri_purchase_order_product.Product_Id');
        $this->db->select('capri_purchase_order_product.Quantity');
        $this->db->select('capri_purchase_order_product.Total');
         $this->db->select('capri_purchase_order_product.Order_Id');
        $this->db->select('capri_purchase_order_product.Product_Id');
        $this->db->select('capri_master_product.Product_Id as Product_Id');
        $this->db->from('capri_purchase_order_product');
        if($Product_Id != null && $Product_Id != 0){
            $this->db->where('Product_Id', $Product_Id);
        }
        if($search_string){
            $this->db->like('Product_Id', $search_string);
        }

        $this->db->join('capri_master_product', 'capri_purchase_order_product.Product_Id = capri_master_product.Product_Id', 'left');

        $this->db->group_by('capri_purchase_order_product.Id');

        if($order){
            $this->db->order_by($order, $order_type);
        }else{
            $this->db->order_by('Id', $order_type);
        }


        $this->db->limit($limit_start, $limit_end);
        //$this->db->limit('4', '4');


        $query = $this->db->get();
        
        return $query->result_array();  
    }
    /**
    * Count the number of rows
    * @param int $search_string
    * @param int $order
    * @return int
    */
   function count_purchase($Product_Id=null, $search_string=null, $order=null)
    {
        $this->db->select('*');
        $this->db->from('capri_purchase_order_product');
        if($Product_Id != null && $Product_Id != 0){
            $this->db->where('Product_Id', $Product_Id);
        }
        if($search_string){
            $this->db->like('Product_Id', $search_string);
        }
        if($order){
            $this->db->order_by($order, 'Asc');
        }else{
            $this->db->order_by('Id', 'Asc');
        }
        $query = $this->db->get();
        return $query->num_rows();        
    }


    /**
    * Store the new item into the database
    * @param array $data - associative array with data to store
    * @return boolean 
    */
    function store_order($data)
    {
		$insert = $this->db->insert('capri_purchase_order_product', $data);
	    return $insert;
	}

    /**
    * Update manufacture
    * @param array $data - associative array with data to store
    * @return boolean
    */
    function update_manufacture($Id, $data)
    {
		$this->db->where('Id', $Id);
		$this->db->update('capri_purchase_order_product', $data);
		$report = array();
		$report['error'] = $this->db->_error_number();
		$report['message'] = $this->db->_error_message();
		if($report !== 0){
			return true;
		}else{
			return false;
		}
	}

    /**
    * Delete manufacturer
    * @param int $Id - manufacture Id
    * @return boolean
    */
	function delete_manufacture($Id){
		$this->db->where('Id', $Id);
		$this->db->delete('capri_purchase_order_product'); 
	}
 
}
	
