 <div class="container top">
       <div class="row">
        <div class="span10 columns">
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url("admin"); ?>">
            <?php echo ucfirst($this->uri->segment(1));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
          <?php echo ucfirst($this->uri->segment(2));?>
        </li>
      </ul>
            
      <div class="page-header users-header">
        <h2>
          <?php echo ucfirst($this->uri->segment(2));?> 
          <a  href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>/add" class="btn btn-success">Add a new</a>
        </h2>
      </div>
      </div>
    </div>
      <div class="row">
        <div class="span10 columns">
          <div class="well">
           
            <?php
           
            $attributes = array('class' => 'form-inline reset-margin', 'id' => 'myform');
           
            //save the columns UnitNames in a array that we will use as filter         
           /* $options_unit = array();    
            foreach ($unit as $array) {
              foreach ($array as $key => $value) {
                $options_unit[$key] = $key;
              }
              break;
            }*/

            echo form_open('admin/unit', $attributes);
     
              /*echo form_label('Search:', 'search_string');
              echo form_input('search_string', $search_string_selected);*/

              /*echo form_label('Order by:', 'order');
              echo form_dropdown('order', $options_unit, $order, 'class="span3"');

              $data_submit = array('UnitName' => 'mysubmit', 'class' => 'btn btn-primary', 'value' => 'Go');

              $options_order_type = array('Asc' => 'Asc', 'Desc' => 'Desc');
              echo form_dropdown('order_type', $options_order_type, $order_type_selected, 'class="span3"');

              echo form_submit($data_submit);*/

            echo form_close();
            ?>

          </div>
 
          <table class="table table-striped table-bordered table-condensed">
            <thead>
              <tr>
                <th class="header">Unit Id</th>
                <!-- <th class="yellow header headerSortDown">Unit Code</th> -->
                <th class="yellow header headerSortDown"> Unit Name</th>
                <th class="yellow header headerSortDown"> Control</th>
              </tr>
            </thead>
            <tbody>
              <?php
              foreach($unit as $row)
              {
                echo '<tr>';
                /*echo '<td>'.$row['Unit_Id'].'</td>';*/
                 echo '<td>'.$row['UnitCode'].'</td>';
                echo '<td>'.$row['UnitName'].'</td>';
                echo '<td class="crud-actions">
                  <a href="'.site_url("admin").'/unit/update/'.$row['Unit_Id'].'" ><button class="btn btn-xs btn-warning"><i class="fa fa-pencil"></i> </button></a>  
                   <a href="'.site_url("admin").'/unit/delete/'.$row['Unit_Id'].'" > <button class="btn btn-xs btn-danger"><i class="fa fa-times"></i> </button></a>
                   
                </td>';
                echo '</tr>';
              }
              ?>      
            </tbody>
          </table>

          <?php echo '<div class="pagination">'.$this->pagination->create_links().'</div>'; ?>

      </div>
    </div>
  
