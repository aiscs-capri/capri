    
 <script src="<?php echo base_url(); ?>js/jquery-1.11.1.js"></script>
 <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>


    <div class="container top">
         <div class="row">
        <div class="span10 columns">
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url("admin"); ?>">
            <?php echo ucfirst($this->uri->segment(1));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li>
          <a href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>">
            <?php echo ucfirst($this->uri->segment(2));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
          <a href="#">View</a>
        </li>
      </ul>
    </div></div></div>
      
      <div class="page-header">
        <h2>
          Adding <?php echo ucfirst($this->uri->segment(2));?>
        </h2>
      
    </div>
      <?php
      //flash messages
      if(isset($flash_message)){
        if($flash_message == TRUE)
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Well done!</strong> New performance created with success.';
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
          echo '</div>';          
        }
      }
      ?>
      
      <?php
      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '');
          //form validation
      echo validation_errors();
      
      echo form_open('admin/salesinvoice/view', $attributes);
      ?>
       <div class="container">
           <div class="row">
               <div class="col-md-5">
                <div class="control-group">
            <label for="inputError" class="control-label"></label>
            <div class="controls">
              <input type="hidden" id="box1" name="Invoice_Id" value="<?php echo $manufacture[0]['Invoice_Id']; ?>" readonly >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Invoice Numer</label>
            <div class="controls">
              <input type="text" id="box2" name="InvoiceNumber" value="<?php echo $manufacture[0]['InvoiceNumber']; ?>" readonly >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Invoice Date</label>
            <div class="controls">
              <input type="text" id="result" name="InvoiceDate" value="<?php $date = $manufacture[0]['InvoiceDate'];  echo date('Y-M-d',strtotime($date)); ?>" readonly >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
         <div class="control-group">
            <label for="inputError" class="control-label">Po Number</label>
            <div class="controls">
              <input type="text" id="result" name="Po_Number" value="<?php echo $manufacture[0]['Po_Number']; ?>"readonly>
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Po Date</label>
            <div class="controls">
              <input type="text" id="result" name="PoDate" value="<?php $date = $manufacture[0]['PoDate'];  echo date('Y-M-d',strtotime($date)); ?>" readonly >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Dc Number</label>
            <div class="controls">
              <input type="text" id="result" name="Dc_Number" value="<?php echo $manufacture[0]['Dc_Number']; ?>" readonly  >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Dc Date</label>
            <div class="controls">
              <input type="text" id="result" name="DcDate" value="<?php $date = $manufacture[0]['DcDate'];  echo date('Y-M-d',strtotime($date)); ?>" readonly >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>

        </div>
          
          <div class="col-md-5">
           <div class="control-group">

            <?php
              foreach ($po as $row) {
              ?>
            
                  
            <label for="inputError" class="control-label">Customer Name</label>

            <?php 
            echo "<input type=text id=supplier name=CustomerName  readonly value=".$row->CustomerName.">";
            ?>
                      
                      <br><br>
                      <div class="control-group">
            <label for="inputError" class="control-label"> Address</label>
            <div class="controls">
              <textarea rows="5" cols="20" id="bil" name="BillingAddress" readonly><?php 
                  echo $row->BillingAddress;                  
                  ?>
              </textarea>
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>

          <div class="control-group">
            <label for="inputError" class="control-label" >CST</label>
            <div class="controls">
            <?php 
            echo "<input type=text name=cst readonly value=".$row->CST.">";
            ?>
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div></div>
          
           <div class="control-group">
            <label for="inputError" class="control-label" >TIN</label>
            <div class="controls">
               <?php 
            echo "<input type=text name=tin readonly value=".$row->TIN.">";
            ?>
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
             <?php 
            }
            ?>
          </div>
                    

          </div>
                    

          </div>
        
        <hr>
       
      
      
      

       <div class="container">
                        
         <div class="row">
         <div class="col-md-10">
          <table id="myTable" class="table table-striped table-bordered table-condensed">
            <tr>
            <td>id</td>
            <td>Product Name</td>
            <td>Product Unit</td>
            <td>Product Qunatity</td>
            <td>Product Price</td>
            <td>Total</td>
            <td>select</td>
        </tr>
        <tbody>
               <?php
              $i =1;
              foreach($pop as $row)
              {
                
                echo '<tr>';
                echo '<td>'.$i.'</td>';
                echo '<td>'.$row->ProductName.'</td>';
                echo '<td>'.$row->ProductUnit.'</td>';
                echo '<td>'.$row->Quantity.'</td>';
                echo '<td>'.$row->ProductPrice.'</td>';
                echo '<td>'.$row->Total.'</td>';
                echo '<td><input type=checkbox></input></td>';
                echo '</tr>';
                $i++;   
              }
              ?>     
           </tbody>
          </table>                  
  
     

    </div>
            </div><hr>
<div class="row">
  <div class="col-md-5">
  
           <div class="control-group">
            <label for="inputError" class="control-label">Date Of Delivery</label>
            <div class="controls">
              <input type="text" id="" name="DateOfDelivery" value="<?php $date = $manufacture[0]['DateOfDelivery'];  echo date('Y-M-d',strtotime($date)); ?>" readonly >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">dispatchThru</label>
            <div class="controls">
              <input type="text" id="" name="dispatchThru" value="<?php echo $manufacture[0]['dispatchThru']; ?>" readonly >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label">Place Of Delivery</label>
            <div class="controls">
              <input type="text" id="" name="PlaceOfDelivery" value="<?php echo $manufacture[0]['PlaceOfDelivery']; ?>" readonly >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Quality</label>
            <div class="controls">
              <input type="text" id="" name="Quality" value="<?php echo $manufacture[0]['Quality']; ?>" readonly >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">tax</label>
            <div class="controls">
              <input type="text" id="" name="Tax" value="<?php echo $manufacture[0]['Tax']; ?>" readonly >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label">Discount</label>
            <div class="controls">
              <input type="text" id="" name="Discount" value="<?php echo $manufacture[0]['Discount']; ?>" readonly >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">PaymentTerm</label>
            <div class="controls">
              <input type="text" id="" name="PaymentTerm" value="<?php echo $manufacture[0]['PaymentTerm']; ?>" readonly >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          
</div>
<div class="col-md-5">
          
           <div class="control-group">
            <label for="inputError" class="control-label" >PackingAndForward</label>
            <div class="controls">
              <input type="text" id="paf" name="packingAndForward" value="<?php echo $manufacture[0]['packingAndForward']; ?>" readonly>
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label" >serviceCharge</label>
            <div class="controls">
              <input type="text" id="sc" name="serviceCharge" value="<?php echo $manufacture[0]['serviceCharge']; ?>" readonly>
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label" >Otherchargs</label>
            <div class="controls">
              <input type="text" id="oc" name="Othercharges" value="<?php echo $manufacture[0]['Othercharges']; ?>" readonly>
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label" >Total Amount</label>
            <div class="controls">
              <input type="text" id="gtot" name="TotalAmount" value="<?php echo $manufacture[0]['TotalAmount']; ?>" readonly>
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label" >Advance Payment</label>
            <div class="controls">
              <input type="text" id="ap" name="AdvancePayment" value="<?php echo $manufacture[0]['AdvancePayment']; ?>" readonly >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label" >Net Payable</label>
            <div class="controls">
              <input type="text" id="bal" name="Balance" value="<?php echo $manufacture[0]['Balance']; ?>" readonly >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
    </div></div>
   
    
      </div>
      <div class="row">
         <div class="col-md-10">
             <div class="form-actions">
           <center><div class="btn btn-warning"><a href="<?php echo base_url(); ?>admin_salesinvoice/report/<?php echo $manufacture[0]['Invoice_Id']; ?>">Print</a></div>
            <button class="btn" type="reset">Cancel</button></center>
          </div>
        </div>
      </div>
      <?php echo form_close(); ?>

    </div>
            </div>
          </div>
            