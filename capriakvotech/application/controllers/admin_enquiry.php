 <?php
class Admin_enquiry extends CI_Controller {

    /**
    * name of the folder responsible for the views 
    * which are manipulated by this controller
    * @constant string
    */
    const VIEW_FOLDER = 'admin/enquiry';
 
    /**
    * Responsable for auto load the model
    * @return void
    */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('enquiry_model');
         $this->load->model('supplier_model');
         $this->load->model('product_model');
         $this->load->model('order_model');
         $this->load->model('value_model');
         $this->load->library('mpdf');

         

        if(!$this->session->userdata('is_logged_in')){
            redirect('admin/login');
        }
    }
 
    /**
    * Load the main view with all the current model model's data.
    * @return void
    */
     public function index()
    {
       
        //all the posts sent by the view
        $Total = $this->input->post('Total');        
        $search_string = $this->input->post('search_string');        
        $order = $this->input->post('order'); 
        $order_type = $this->input->post('order_type'); 

        //pagination settings
        $config['per_page'] = 20;
        $config['base_url'] = base_url().'admin/enquiry';
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = 20;
        $config['full_tag_open'] = '<ul>';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';

        //limit end
        $page = $this->uri->segment(3);

        //math to get the initial record to be select in the database
        $limit_end = ($page * $config['per_page']) - $config['per_page'];
        if ($limit_end < 0){
            $limit_end = 0;
        } 

        //if order type was changed
        if($order_type){
            $filter_session_data['order_type'] = $order_type;
        }
        else{
            //we have something stored in the session? 
            if($this->session->userdata('order_type')){
                $order_type = $this->session->userdata('order_type');    
            }else{
                //if we have nothing inside session, so it's the default "Asc"
                $order_type = 'Asc';    
            }
        }
        //make the data type var avaible to our view
        $data['order_type_selected'] = $order_type;        


        //we must avoid a page reload with the previous session data
        //if any filter post was sent, then it's the first time we load the content
        //in this case we clean the session filter data
        //if any filter post was sent but we are in some page, we must load the session data

        //filtered && || paginated
        if($Total !== false && $search_string !== false && $order !== false || $this->uri->segment(3) == true){ 
           
            /*
            The comments here are the same for line 79 until 99

            if post is not null, we store it in session data array
            if is null, we use the session data already stored
            we save order into the the var to load the view with the param already selected       
            */

            if($Total !== 0){
                $filter_session_data['manufacture_selected'] = $Total;
            }else{
                $Total = $this->session->userdata('manufacture_selected');
            }
            $data['manufacture_selected'] = $Total;

            if($search_string){
                $filter_session_data['search_string_selected'] = $search_string;
            }else{
                $search_string = $this->session->userdata('search_string_selected');
            }
            $data['search_string_selected'] = $search_string;

            if($order){
                $filter_session_data['order'] = $order;
            }
            else{
                $order = $this->session->userdata('order');
            }
            $data['order'] = $order;

            //save session data into the session
            $this->session->set_userdata($filter_session_data);

            //fetch manufacturers data into arrays
           // $data['manufactures'] = $this->enquiry_model->get_manufacturers();

           // $data['enquiry'] = $this->product_model->get_manufacturers();

            $data['count_enquiry']= $this->enquiry_model->count_enquiry($Total, $search_string, $order);
            $config['total_rows'] = $data['count_enquiry'];

            //fetch sql data into arrays
            if($search_string){
                if($order){
                    $data['enquiry'] = $this->enquiry_model->get_enquiry($Total, $search_string, $order, $order_type, $config['per_page'],$limit_end);        
                }else{
                    $data['enquiry'] = $this->enquiry_model->get_enquiry($Total, $search_string, '', $order_type, $config['per_page'],$limit_end);           
                }
            }else{
                if($order){
                    $data['enquiry'] = $this->enquiry_model->get_enquiry($Total, '', $order, $order_type, $config['per_page'],$limit_end);        
                }else{
                    $data['enquiry'] = $this->enquiry_model->get_enquiry($Total, '', '', $order_type, $config['per_page'],$limit_end);        
                }
            }

        }else{

            //clean filter data inside section
            $filter_session_data['manufacture_selected'] = null;
            $filter_session_data['search_string_selected'] = null;
            $filter_session_data['order'] = null;
            $filter_session_data['order_type'] = null;
            $this->session->set_userdata($filter_session_data);

            //pre selected options
            $data['search_string_selected'] = '';
            $data['manufacture_selected'] = 0;
            $data['order'] = 'id';

            //fetch sql data into arrays
            $data['enquiry'] = $this->product_model->get_product();

            $data['manufactures'] = $this->supplier_model->get_supplier();
            $data['count_enquiry']= $this->enquiry_model->count_enquiry();
            $data['capri_enquiry_order'] = $this->enquiry_model->get_enquiry('', '', '', $order_type, $config['per_page'],$limit_end);        
            $config['total_rows'] = $data['count_enquiry'];

        }//!isset($Total) && !isset($search_string) && !isset($order)

        //initializate the panination helper 
        $this->pagination->initialize($config);   

        //load the view
        $data['main_content'] = 'admin/enquiry/list';
        $this->load->view('includes/template', $data);  

    }//index

    public function add()
    {

        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {

            //form validation
             $this->form_validation->set_rules('Enquiry_Id', 'Enquiry_Id');
            $this->form_validation->set_rules('Enquiry_number', 'Enquiry_number', 'required');
            $this->form_validation->set_rules('EnquiryDate', 'EnquiryDate', 'required');
            $this->form_validation->set_rules('CustomerName', 'CustomerName', 'required');
            $this->form_validation->set_rules('Address', 'Address');
   
            $this->form_validation->set_rules('Quality', 'Quality');
            $this->form_validation->set_rules('PaymentTerm', 'PaymentTerm');
            $this->form_validation->set_rules('Note', 'Note');
            $this->form_validation->set_rules('Remark', 'Remark');
            $this->form_validation->set_rules('Total', 'Total', 'required');
            $this->form_validation->set_rules('Tax_type', 'Tax_type', 'required');
            $this->form_validation->set_rules('Tax', 'Tax', 'required');
            $this->form_validation->set_rules('TaxAmount', 'Tax', 'required');
            $this->form_validation->set_rules('GrossTotal', 'GrossTotal', 'required');
           
           


            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            

            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
                $data_to_store = array(
                    
                    'Enquiry_number' => $this->input->post('Enquiry_number'),
                    'EnquiryDate' => date("Y-m-d",strtotime($this->input->post('EnquiryDate'))),
                   
                    'CustomerName' => $this->input->post('CustomerName'),
                    'Address' => $this->input->post('Address'),
                    'TIN' => $this->input->post('TIN'),
                    'CST' => $this->input->post('CST'),
                    'Quality' => $this->input->post('Quality'),
                    'Note' => $this->input->post('Note'),
                    'Remark' => $this->input->post('Remark'),
                    'PaymentTerm' => $this->input->post('PaymentTerm'),
                    'Total' => $this->input->post('Total'),
                    'Tax_type' => $this->input->post('Tax_type'),
                    'Tax' => $this->input->post('Tax'),
                    'TaxAmount' => $this->input->post('TaxAmount'),
                    'GrossTotal' => $this->input->post('GrossTotal'),
                    'CreatedBy' => $this->session->userdata('user_id'),
                    'CreatedDate' => date("Y-m-d H:i:s")

                    
                );
                //if the insert has returned true then we show the flash message
                if($this->enquiry_model->store_manufacture($data_to_store)){
                    $data['flash_message'] = TRUE; 


        $id = $this->enquiry_model->maxvalue();

        $pro = $_POST['id'];
        $product = $_POST['product'];
        $unit = $_POST['unit'];
        $qun = $_POST['qun'];
        $tot = $_POST['tot'];
        $price = $_POST['price'];
        $order = $id->row('Enquiry_Id');


        for($i=0; $i<sizeof($pro); $i++)
        {
        $data1['ProductName']= $product[$i];
        $data1['ProductUnit']= $unit[$i];
        $data1['Quantity']= $qun[$i];
        $data1['ProductPrice']= $price[$i];
        $data1['Total']= $tot[$i];
        $data1['Enquiry_Id']= $order;
        $data1['CreatedBy'] = $this->session->userdata('user_id');
        $data1['CreatedDate'] = date("Y-m-d H:i:s");
            
        $this->value_model->getenquiry($data1);

    }
                }else{
                    $data['flash_message'] = FALSE; 
                }

            }

        }
        //fetch manufactures data to populate the select field
        $data['manufactures'] = $this->supplier_model->get_supplier();
        $data['enquiryorder'] = $this->product_model->get_product();
        $data['price'] = $this->product_model->get_product();
        $data['product'] = $this->product_model->get_product();

        $customer = $this->enquiry_model->getcustomername();
        $product = $this->enquiry_model->getproductname();
        /*$getProduct=$this->enquiry_model->getproductdetail('Product_Id');*/

        //load the view
        $data['CustomerName'] =$customer;
        $data['ProductName'] =$product;
        $data['main_content'] = 'admin/enquiry/add';
        $this->load->view('includes/template', $data);  
 
    }       

    /**
    * Update item by his id
    * @return void
    */

public function get()
{

    $Product_Id=$_GET['Product_Id'];
    $this->enquiry_model->getproductdetail('Product_Id');
    $productdetail["rows"] = $this->enquiry_model->getproductdetail($Product_Id);
        echo json_encode($productdetail);

}

public function getcustomerdata()
{

    $Customer_Id=$_GET['Customer_Id'];
    $this->enquiry_model->getcustomerdetail('Customer_Id');
     $customerdetail["rows"] = $this->enquiry_model->getcustomerdetail($Customer_Id);
        echo json_encode($customerdetail);

}




    


    public function update()
    {
        //product id 
        $id = $this->uri->segment(4);
  
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {
            //form validation
             $this->form_validation->set_rules('Enquiry_Id', 'Enquiry_Id', 'required|numeric');
            $this->form_validation->set_rules('Enquiry_number', 'Enquiry_number', 'required');
            $this->form_validation->set_rules('Total', 'Total', 'required');
            $this->form_validation->set_rules('DateOfDelivery', 'DateOfDelivery');
            $this->form_validation->set_rules('PlaceOfDelivery', 'PlaceOfDelivery');
            $this->form_validation->set_rules('Quality', 'Quality');
            $this->form_validation->set_rules('Note', 'Note');
            $this->form_validation->set_rules('PaymentTerm', 'PaymentTerm');
            $this->form_validation->set_rules('Total', 'Total', 'required');
            $this->form_validation->set_rules('Tax_type', 'Tax_type', 'required');
            $this->form_validation->set_rules('Tax', 'Tax', 'required');
            $this->form_validation->set_rules('TaxAmount', 'TaxAmount', 'required');
            $this->form_validation->set_rules('GrossTotal', 'GrossTotal', 'required');
           
            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
    
                $data_to_store = array(
                    
                   'Enquiry_number' => $this->input->post('Enquiry_number'),
                    'EnquiryDate' => $this->input->post('EnquiryDate'),
                    'CustomerName' => $this->input->post('CustomerName'),
                    'Address' => $this->input->post('Address'),
                    'TIN' => $this->input->post('TIN'),
                    'CST' => $this->input->post('CST'),
                    'Quality' => $this->input->post('Quality'),
                    'Note' => $this->input->post('Note'),
                    'Remark' => $this->input->post('Remark'),
                    'PaymentTerm' => $this->input->post('PaymentTerm'),
                    'Total' => $this->input->post('Total'),
                    'Tax_type' => $this->input->post('Tax_type'),
                    'Tax' => $this->input->post('Tax'),
                    'TaxAmount' => $this->input->post('TaxAmount'),
                    'GrossTotal' => $this->input->post('GrossTotal'),
                );
                //if the insert has returned true then we show the flash message
                if($this->enquiry_model->update_manufacture($id, $data_to_store) == TRUE){
                    $this->session->set_flashdata('flash_message', 'updated');
                }else{
                    $this->session->set_flashdata('flash_message', 'not_updated');
                }
                redirect('admin/enquiry/update/'.$id.'');

            }//validation run

        }

        //if we are updating, and the data did not pass trough the validation
        //the code below wel reload the current data

        //product data 
        $data['manufacture'] = $this->enquiry_model->get_manufacture_by_Enquiry_Id($id);
        

       

       $order=$this->enquiry_model->getenquiryOrderDetails($id);
        $orderpro=$this->enquiry_model->getenquiryOrderProductDetails($id);
        $product = $this->enquiry_model->getproductname();


        
        
        //load the view
        
        $data['po']=$order;
        $data['pop']=$orderpro;
        $data['ProductName'] =$product;
        //load the view
        $data['main_content'] = 'admin/enquiry/edit';
        $this->load->view('includes/template', $data);            

    }//update

    /**
    * Delete product by his id
    * @return void
    */
    public function view()
    {
        //product id 
        $id = $this->uri->segment(4);
  
        //if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {
           //form validation
            
            $this->form_validation->set_rules('Quantity', 'Quantity', 'required');
            $this->form_validation->set_rules('Total', 'Total', 'required');
            $this->form_validation->set_rules('DateOfDelivery', 'DateOfDelivery');
            $this->form_validation->set_rules('PlaceOfDelivery', 'PlaceOfDelivery');
            $this->form_validation->set_rules('Quality', 'Quality');
            $this->form_validation->set_rules('Note', 'Note');
            $this->form_validation->set_rules('PaymentTerm', 'PaymentTerm');
            $this->form_validation->set_rules('Total', 'Total', 'required');
            $this->form_validation->set_rules('Tax_type', 'Tax_type', 'required');
            $this->form_validation->set_rules('Tax', 'Tax', 'required');
            $this->form_validation->set_rules('TaxAmount', 'TaxAmount', 'required');
            $this->form_validation->set_rules('GrossTotal', 'GrossTotal', 'required');

            $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
    
                $data_to_store = array(
                   'Enquiry_number' => $this->input->post('Enquiry_number'),
                    'EnquiryDate' => $this->input->post('EnquiryDate'),
                    'CustomerName' => $this->input->post('CustomerName'),
                    'Address' => $this->input->post('Address'),
                    'TIN' => $this->input->post('TIN'),
                    'CST' => $this->input->post('CST'),
                    'Quality' => $this->input->post('Quality'),
                    'Note' => $this->input->post('Note'),
                    'Remark' => $this->input->post('Remark'),
                    'PaymentTerm' => $this->input->post('PaymentTerm'),
                    'Total' => $this->input->post('Total'),
                    'Tax_type' => $this->input->post('Tax_type'),
                    'Tax' => $this->input->post('Tax'),
                    'TaxAmount' => $this->input->post('TaxAmount'),
                    'GrossTotal' => $this->input->post('GrossTotal'),
                    
                );
                //if the insert has returned true then we show the flash message
                if($this->enquiry_model->update_manufacture($id, $data_to_store) == TRUE){
                    $this->session->set_flashdata('flash_message', 'updated');
                }else{
                    $this->session->set_flashdata('flash_message', 'not_updated');
                }
                redirect('admin/enquiry/view/'.$id.'');

            }//validation run

        }

        //if we are updating, and the data did not pass trough the validation
        //the code below wel reload the current data

        //product data 
        $data['manufacture'] = $this->enquiry_model->get_manufacture_by_Enquiry_Id($id);
        
        $order=$this->enquiry_model->getenquiryOrderDetails($id);
        $orderpro=$this->enquiry_model->getenquiryOrderProductDetails($id);
        
        /*$getProduct=$this->enquiry_model->getproductdetail('Product_Id');*/

        //load the view
        $data['po']=$order;
        $data['pop']=$orderpro;
        //load the view
        $data['main_content'] = 'admin/enquiry/view';
        $this->load->view('includes/template', $data);            

    }//update

 public function  orderdetailsinsert()
    {
 
        
        $product = $_GET['product'];
        $unit = $_GET['unit'];
        $qun = $_GET['qun'];
        $price = $_GET['price'];
        $tot = $_GET['tot'];
        $order = $_GET['Enquiry_Id'];

        
        $data['ProductName']= $product;
        $data['ProductUnit']= $unit;
        $data['Quantity']= $qun;
        $data['ProductPrice']= $price;
        $data['Total']= $tot;
        $data['Enquiry_Id']= $order;
        $data['CreatedBy'] = $this->session->userdata('user_id');
        $data['CreatedDate'] = date("Y-m-d H:i:s");
            
        $resdata["row"] = $this->Value_model->getenquiry($data);
        echo json_encode($resdata);

    }

    public function  orderdetailsdelete()
    {
 
        $pro = $_GET['id'];        
        
        $this->enquiry_model->delete_order_details($pro);
        $deleteProduct = array(
        'message' => true
        );
        $resdata["row"] = $deleteProduct;
        echo json_encode($resdata);

     }
     public function  orderdetailsmaxvalue()
    {
       $id = $this->enquiry_model->order_details_maxvalue();

       $sam = array(
        'Id' => $id->row('Id')
        );
       $resdata["row"] = $sam;
        echo json_encode($resdata);
    }
    /**
    * Delete product by his id
    * @return void
    */
    public function delete()
    {
        //product id 
        $id = $this->uri->segment(4);
        $this->enquiry_model->purchase_order_delete_details($id);
        

         if($this->enquiry_model->delete_manufacture($id) == TRUE){
                    $this->session->set_flashdata('flash_message', 'updated');
                }else{
                    $this->session->set_flashdata('flash_message', 'not_updated');
                }
        redirect('admin/enquiry');
    }//edit
public function report()
    {
     
      $data = array(
                               'name' => 'SATHIYARAJ'
                );

       $id = $this->uri->segment(4);
       $data['manufacture'] = $this->enquiry_model->get_manufacture_by_Enquiry_Id($id);
        
        $order=$this->enquiry_model->getenquiryOrderDetails($id);
        $orderpro=$this->enquiry_model->getenquiryOrderProductDetails($id);
        
        /*$getProduct=$this->enquiry_model->getproductdetail('Product_Id');*/

        //load the view
        $data['po']=$order;
        $data['pop']=$orderpro;

     $html = $this->load->view('admin/enquiry/report', $data, TRUE);
         /*$data['main_content'] = 'admin/enquiry/report';
         $html = $this->load->view('includes/template', $data,TRUE);  */



$mpdf=new mPDF();

$mpdf=new mPDF('c','A4','','',32,25,27,25,16,13);

$mpdf->WriteHTML($html,2);

$mpdf->Output('mpdf.pdf','I');
    }
}
