/* CRM Data Base*/
/* Data Base*/
CREATE DATABASE capriakvotech;

/* CRM Register & Login Details */

/* New register user detail */
CREATE TABLE capri_login_register (
R_Id BIGINT NOT NULL AUTO_INCREMENT ,
FirstName VARCHAR( 50 ) NOT NULL ,
LastName VARCHAR( 50 ) NOT NULL ,
Email VARCHAR( 80 ) NOT NULL ,
Mobile VARCHAR(12) NOT NULL,
Password VARCHAR(100) NOT NULL,
CreatedDate TIMESTAMP NOT NULL ,
Active BOOLEAN NOT NULL,
PRIMARY KEY (R_Id),
CONSTRAINT pk_login_register UNIQUE (Email)
);

/* Failure Login user details */
CREATE TABLE capri_login_failure (
F_Id BIGINT NOT NULL AUTO_INCREMENT,
Reg_Id BIGINT NOT NULL,
Ip VARCHAR(19) NOT NULL,
BrowserName VARCHAR(60) NOT NULL,
OsName VARCHAR(60) NOT NULL,
LoginTime TIMESTAMP NOT NULL,
LoginCount int NOT NULL,
PRIMARY KEY (F_Id),
CONSTRAINT fk_capri_login_failure_id FOREIGN KEY (Reg_Id) REFERENCES capri_login_register(R_Id)
);

/* Current Logged user details */
CREATE TABLE capri_login_current (
C_Id BIGINT NOT NULL AUTO_INCREMENT,
Reg_Id BIGINT NOT NULL,
Ip VARCHAR(19) NOT NULL,
BrowserName VARCHAR(60) NOT NULL,
OsName VARCHAR(60) NOT NULL,
LogInTime TIMESTAMP NOT NULL,
LogOutTime TIMESTAMP NOT NULL,
PRIMARY KEY (C_Id),
CONSTRAINT fk_capri_login_current_id FOREIGN KEY (Reg_Id) REFERENCES capri_login_register(R_Id)
);

/* History Logged user details */
CREATE TABLE capri_login_history (
H_Id BIGINT NOT NULL AUTO_INCREMENT,
Reg_Id BIGINT NOT NULL,
Ip VARCHAR(19) NOT NULL,
BrowserName VARCHAR(60) NOT NULL,
OsName VARCHAR(60) NOT NULL,
LogInTime TIMESTAMP NOT NULL,
LogOutTime TIMESTAMP NOT NULL,
TypeOfLogout VARCHAR(30) NOT NULL,
PRIMARY KEY (H_Id),
CONSTRAINT fk_capri_login_history_id FOREIGN KEY (Reg_Id) REFERENCES capri_login_register(R_Id)
);


/* Bloacked IP  details */
CREATE TABLE capri_login_block (
B_Id BIGINT NOT NULL AUTO_INCREMENT,
Ip VARCHAR(19) NOT NULL,
PRIMARY KEY (B_Id)
);

/* CRM Master Tables */

/* Unit Master */
CREATE TABLE capri_master_unit
(
Unit_Id BIGINT NOT NULL AUTO_INCREMENT,
UnitCode VARCHAR(30) NOT NULL,
UnitName VARCHAR(100) NOT NULL,
CreatedBy BIGINT NOT NULL,
CreatedDate TIMESTAMP NOT NULL,
UpdatedBy BIGINT,
UpdatedDate TIMESTAMP,
PRIMARY KEY (Unit_Id),
CONSTRAINT pk_unit_code UNIQUE (UnitCode),
CONSTRAINT fk_unit_created_by FOREIGN KEY (CreatedBy) REFERENCES capri_login_register(R_Id),
CONSTRAINT fk_unit_updated_by FOREIGN KEY (UpdatedBy) REFERENCES capri_login_register(R_Id)
);


CREATE TABLE capri_master_designation
(
Id BIGINT NOT NULL AUTO_INCREMENT,
designation VARCHAR(100) NOT NULL,
PRIMARY KEY (Id),
CONSTRAINT fk_designation_created_by FOREIGN KEY (CreatedBy) REFERENCES capri_login_register(R_Id),
CONSTRAINT fk_designation_updated_by FOREIGN KEY (UpdatedBy) REFERENCES capri_login_register(R_Id)
);
/* Product Master */
CREATE TABLE capri_master_product
(
Product_Id BIGINT NOT NULL AUTO_INCREMENT,
ProductCode VARCHAR(30) NOT NULL,
ProductName VARCHAR(100) NOT NULL,
ProductUnit VARCHAR(100) NOT NULL,
ProductPrice NUMERIC (10,2),
incentive BIGINT NULL,
CreatedBy BIGINT NOT NULL,
CreatedDate TIMESTAMP NOT NULL,
UpdatedBy BIGINT,
UpdatedDate TIMESTAMP,
PRIMARY KEY (Product_Id),
CONSTRAINT pk_product_code UNIQUE (ProductCode),
CONSTRAINT fk_product_created_by FOREIGN KEY (CreatedBy) REFERENCES capri_login_register(R_Id),
CONSTRAINT fk_product_updated_by FOREIGN KEY (UpdatedBy) REFERENCES capri_login_register(R_Id)
);


ALTER TABLE capri_master_product
  DROP FOREIGN KEY fk_Product_ProductUnit

/* Customer Master */
CREATE TABLE capri_master_customer
(
Customer_Id BIGINT NOT NULL AUTO_INCREMENT,
CustomerCode VARCHAR(30) NOT NULL,
CustomerName VARCHAR(100) NOT NULL,
BillingAddress VARCHAR(500) NULL,
ShippingAddress VARCHAR(500) NULL,
PhoneNumber VARCHAR(13) NULL,
ContactPerson VARCHAR(80) NULL,
MobileNumber1 VARCHAR(13) NULL,
MobileNumber2 VARCHAR(13) NULL,
Email VARCHAR(100) NULL,
Fax VARCHAR(20) NULL,
TIN VARCHAR(20) NULL,
CST VARCHAR(20) NULL,
ECC VARCHAR(20) NULL,
CreatedBy BIGINT NOT NULL,
CreatedDate TIMESTAMP NOT NULL,
UpdatedBy BIGINT,
UpdatedDate TIMESTAMP,
PRIMARY KEY (Customer_Id),
CONSTRAINT pk_customer_code UNIQUE (CustomerCode),
CONSTRAINT fk_customer_created_by FOREIGN KEY (CreatedBy) REFERENCES capri_login_register(R_Id),
CONSTRAINT fk_customer_updated_by FOREIGN KEY (UpdatedBy) REFERENCES capri_login_register(R_Id)
);


/* Supplier Master */
CREATE TABLE capri_master_supplier
(
Supplier_Id BIGINT NOT NULL AUTO_INCREMENT,
SupplierCode VARCHAR(30) NOT NULL,
SupplierName VARCHAR(100) NOT NULL,
BillingAddress VARCHAR(500) NULL,
ShippingAddress VARCHAR(500) NULL,
PhoneNumber VARCHAR(13) NULL,
ContactPerson VARCHAR(80) NULL,
MobileNumber1 VARCHAR(13) NULL,
MobileNumber2 VARCHAR(13) NULL,
Email VARCHAR(100) NULL,
Fax VARCHAR(20) NULL,
TIN VARCHAR(20) NULL,
CST VARCHAR(20) NULL,
ECC VARCHAR(20) NULL,
CreatedBy BIGINT NOT NULL,
CreatedDate TIMESTAMP NOT NULL,
UpdatedBy BIGINT,
UpdatedDate TIMESTAMP,
PRIMARY KEY (Supplier_Id),
CONSTRAINT pk_Supplier_code UNIQUE (SupplierCode),
CONSTRAINT fk_Supplier_created_by FOREIGN KEY (CreatedBy) REFERENCES capri_login_register(R_Id),
CONSTRAINT fk_Supplier_updated_by FOREIGN KEY (UpdatedBy) REFERENCES capri_login_register(R_Id)
);


/* Empolyee Master */
CREATE TABLE capri_master_employee
(
Employee_Id BIGINT NOT NULL AUTO_INCREMENT,
EmployeeCode VARCHAR(30) NOT NULL,
EmployeeName VARCHAR(100) NOT NULL,
PanNumber VARCHAR(15) NOT NULL,
Date_of_birth TIMESTAMP,
Joindate TIMESTAMP,
Grosssalary NUMERIC(10,3) NOT NULL,
FatherName VARCHAR(100) NOT NULL,
Gender VARCHAR(50) NOT NULL,
MaritalStatus VARCHAR(100) NOT NULL,
Nationality VARCHAR(100) NOT NULL,
EmpStreet VARCHAR(100) NOT NULL,
EmpVillage VARCHAR(100) NOT NULL,
EmpCity VARCHAR(100) NOT NULL,
EmpState VARCHAR(100) NOT NULL,
EmpPincode VARCHAR(6) NOT NULL,
PhoneNumber VARCHAR(13) NOT NULL,
MobileNumber VARCHAR(13) NOT NULL,
EmergencyNumber VARCHAR(13) NOT NULL,
Email VARCHAR(100) NOT NULL,
BloodGroup VARCHAR(10) NULL,
Education VARCHAR(100) NULL,
TotalExperience VARCHAR(5) NULL,
Photo VARCHAR(300) NOT NULL,
CreatedBy BIGINT NOT NULL,
CreatedDate TIMESTAMP NOT NULL,
UpdatedBy BIGINT,
UpdatedDate TIMESTAMP,
PRIMARY KEY (Employee_Id),
CONSTRAINT pk_Employee_code UNIQUE (EmployeeCode),
CONSTRAINT fk_Employee_created_by FOREIGN KEY (CreatedBy) REFERENCES capri_login_register(R_Id),
CONSTRAINT fk_Employee_updated_by FOREIGN KEY (UpdatedBy) REFERENCES capri_login_register(R_Id)
);


/* Purchase */
/* Purchase Order */
CREATE TABLE capri_purchase_order
(
Order_Id BIGINT NOT NULL AUTO_INCREMENT,
Order_number VARCHAR(30) NOT NULL,
OrderDate TIMESTAMP NOT NULL,
OurRef VARCHAR(200) NULL,
Supplier_Id BIGINT NOT NULL,
DateOfDelivery TIMESTAMP NULL,
PlaceOfDelivery VARCHAR(300) NULL,
Quality VARCHAR(100) NULL,
Note VARCHAR(200) NULL,
PaymentTerm VARCHAR(30) NULL,
Total NUMERIC(10,2) NOT NULL,
Tax_type VARCHAR(5) NOT NULL,
Tax NUMERIC(10,2) NULL,
TaxAmount NUMERIC(10,2) NULL,
GrossTotal NUMERIC(10,2) NOT NULL,
CreatedBy BIGINT NOT NULL,
CreatedDate TIMESTAMP NOT NULL,
UpdatedBy BIGINT,
UpdatedDate TIMESTAMP,
PRIMARY KEY (Order_Id),
CONSTRAINT fk_purchase_order_supplier_id FOREIGN KEY (Supplier_Id) REFERENCES capri_master_supplier(Supplier_Id),
CONSTRAINT fk_purchase_order_created_by FOREIGN KEY (CreatedBy) REFERENCES capri_login_register(R_Id),
CONSTRAINT fk_purchase_order_updated_by FOREIGN KEY (UpdatedBy) REFERENCES capri_login_register(R_Id)
);


/* Purchase Order with Product Details*/
CREATE TABLE capri_purchase_order_details
(
Id BIGINT NOT NULL AUTO_INCREMENT,
ProductName VARCHAR(150) NULL,
ProductUnit VARCHAR(150) NULL,
ProductPrice NUMERIC(10,2) NULL,
Quantity BIGINT NOT NULL,
Total NUMERIC (10,3) NOT NULL,
Order_Id BIGINT NOT NULL,
CreatedBy BIGINT NOT NULL,
CreatedDate TIMESTAMP NOT NULL,
UpdatedBy BIGINT,
UpdatedDate TIMESTAMP,
PRIMARY KEY (Id),
CONSTRAINT fk_purchase_order_details_order_id FOREIGN KEY (order_Id) REFERENCES capri_purchase_order(order_id),
CONSTRAINT fk_purchase_order_details_created_by FOREIGN KEY (CreatedBy) REFERENCES capri_login_register(R_Id),
CONSTRAINT fk_purchase_order_details_updated_by FOREIGN KEY (UpdatedBy) REFERENCES capri_login_register(R_Id)
);

CREATE TABLE capri_purchase_inward
(
inward_Id BIGINT NOT NULL AUTO_INCREMENT,
inward_number VARCHAR(30) NOT NULL,
inwardDate TIMESTAMP NOT NULL,
BillNumber VARCHAR(20) NOT NULL,
BillDate TIMESTAMP NOT NULL,
Order_number VARCHAR(20) NOT NULL,
OrderDate TIMESTAMP NOT NULL,
PlaceOfDelivery VARCHAR(200) NULL,
Supplier_Id BIGINT NOT NULL,
Total NUMERIC(10,2) NOT NULL,
Tax_type VARCHAR(5) NOT NULL,
Tax NUMERIC(10,2) NULL,
TaxAmount NUMERIC(10,2) NULL,
packingAndForward NUMERIC(10,2) NULL,
servicetax NUMERIC(10,2) NULL,
Otherchargs NUMERIC(10,2) NULL,
GrossTotal NUMERIC(10,2) NOT NULL,
AdvancePayment NUMERIC(10,2) NULL,
Balance NUMERIC(10,2) NULL,
Paid NUMERIC(10,2) NULL,
CreatedBy BIGINT NOT NULL,
CreatedDate TIMESTAMP NOT NULL,
UpdatedBy BIGINT,
UpdatedDate TIMESTAMP,
PRIMARY KEY (inward_Id),
CONSTRAINT fk_purchase_inward_supplier_id FOREIGN KEY (Supplier_Id) REFERENCES capri_master_supplier(Supplier_Id),
CONSTRAINT fk_purchase_inward_created_by FOREIGN KEY (CreatedBy) REFERENCES capri_login_register(R_Id),
CONSTRAINT fk_purchase_inward_updated_by FOREIGN KEY (UpdatedBy) REFERENCES capri_login_register(R_Id)
);


/* Purchase Order with Product Details*/
CREATE TABLE capri_purchase_inward_details
(
Id BIGINT NOT NULL AUTO_INCREMENT,
ProductName VARCHAR(150) NULL,
ProductUnit VARCHAR(150) NULL,
ProductPrice NUMERIC(10,2) NULL,
Quantity BIGINT NOT NULL,
Total NUMERIC (10,3) NOT NULL,
inward_Id BIGINT NOT NULL,
CreatedBy BIGINT NOT NULL,
CreatedDate TIMESTAMP NOT NULL,
UpdatedBy BIGINT,
UpdatedDate TIMESTAMP,
PRIMARY KEY (Id),
CONSTRAINT fk_purchase__inward_details_inward_Id FOREIGN KEY (inward_Id) REFERENCES capri_purchase_inward(inward_Id),
CONSTRAINT fk_purchase_order_product_inward_created_by FOREIGN KEY (CreatedBy) REFERENCES capri_login_register(R_Id),
CONSTRAINT fk_purchase_order_product_inward_updated_by FOREIGN KEY (UpdatedBy) REFERENCES capri_login_register(R_Id)
);




/* Dc Entry */
CREATE TABLE capri_dc_entry
(
Dc_Id BIGINT NOT NULL AUTO_INCREMENT,
Dc_number VARCHAR(30) NOT NULL,
DcDate TIMESTAMP NOT NULL,
podate TIMESTAMP NULL,
po_number VARCHAR(300) NULL,
CustomerName VARCHAR(100) NULL,
Address VARCHAR(500) NULL,
TIN VARCHAR(30) NULL,
CST VARCHAR(30) NULL,
Remark VARCHAR(200) NULL,
ourref VARCHAR(30) NULL,
CreatedBy BIGINT NOT NULL,
CreatedDate TIMESTAMP NOT NULL,
UpdatedBy BIGINT,
UpdatedDate TIMESTAMP,
PRIMARY KEY (Dc_Id),
CONSTRAINT fk_dc_entry_created_by FOREIGN KEY (CreatedBy) REFERENCES capri_login_register(R_Id),
CONSTRAINT fk_dc_entry_updated_by FOREIGN KEY (UpdatedBy) REFERENCES capri_login_register(R_Id)
);


/* Dc with Product Details*/
CREATE TABLE capri_dc_details
(
Id BIGINT NOT NULL AUTO_INCREMENT,
ProductName VARCHAR(150) NULL,
ProductUnit VARCHAR(150) NULL,
ProductPrice NUMERIC(10,2) NULL,
Quantity BIGINT NOT NULL,
Total NUMERIC (10,3) NOT NULL,
Dc_Id BIGINT NOT NULL,
CreatedBy BIGINT NOT NULL,
CreatedDate TIMESTAMP NOT NULL,
UpdatedBy BIGINT,
UpdatedDate TIMESTAMP,
PRIMARY KEY (Id),
CONSTRAINT fk_dc_details_Dc_Id FOREIGN KEY (Dc_Id) REFERENCES capri_dc_entry(Dc_Id),
CONSTRAINT fk_dc_details_created_by FOREIGN KEY (CreatedBy) REFERENCES capri_login_register(R_Id),
CONSTRAINT fk_dc_details_updated_by FOREIGN KEY (UpdatedBy) REFERENCES capri_login_register(R_Id)
);





























CREATE TABLE capri_purchase_outgoing_payment
(
Id BIGINT NOT NULL AUTO_INCREMENT,
inward_Id BIGINT NOT NULL,
PaymentMode VARCHAR(50) NOT NULL,
ChequeorDraftNumber VARCHAR(10) NULL,
BankName VARCHAR(100) NULL,
topay NUMERIC(10,2) NOT NULL,
CreateBy BIGINT NOT NULL,
CreatedDate TIMESTAMP NOT NULL,
UpdatedBy BIGINT,
UpdatedDate TIMESTAMP,
PRIMARY KEY (Id),
CONSTRAINT fk_purchase_outgoing_payment_inward_Id FOREIGN KEY (inward_Id) REFERENCES capri_purchase_inward(inward_Id),
CONSTRAINT fk_purchase_outgoing_payment_created_by FOREIGN KEY (CreateBy) REFERENCES capri_login_register(R_Id),
CONSTRAINT fk_purchase_outgoing_payment_updated_by FOREIGN KEY (UpdatedBy) REFERENCES capri_login_register(R_Id)
);

/* Marketing */

/* Maketing Schedule */
CREATE TABLE capri_marketing_schedule
(
Id BIGINT NOT NULL AUTO_INCREMENT,
ScheduleDate TIMESTAMP NOT NULL,
ScheduleTime TIMESTAMP NOT NULL,
Name VARCHAR(100) NOT NULL,
Address VARCHAR(300) NOT NULL,
Phome VARCHAR(13) NOT NULL,
ContactPerson VARCHAR(100) NOT NULL,
Mobile VARCHAR(13) NOT NULL,
Status VARCHAR(30) NOT NULL,
message VARCHAR(200) NOT NULL,
EmployeeName VARCHAR(100) NOT NULL,
CreatedBy BIGINT NOT NULL,
CreatedDate TIMESTAMP NOT NULL,
UpdatedBy BIGINT,
UpdatedDate TIMESTAMP,
PRIMARY KEY (Id),
CONSTRAINT fk_marketing_schedule_created_by FOREIGN KEY (CreatedBy) REFERENCES capri_login_register(R_Id),
CONSTRAINT fk_marketing_schedule_updated_by FOREIGN KEY (UpdatedBy) REFERENCES capri_login_register(R_Id)
);

/* Sales */
/* Customer Enquery */
CREATE TABLE capri_customer_enquiry
(
Enquiry_Id BIGINT NOT NULL AUTO_INCREMENT,
Enquiry_number VARCHAR(30) NOT NULL,
EnquiryDate TIMESTAMP NOT NULL,
CustomerName VARCHAR(150) NOT NULL,
Address VARCHAR(500) NULL,
TIN VARCHAR(30) NULL,
CST VARCHAR(30) NULL,
Quality VARCHAR(100) NULL,
Note VARCHAR(2000) NULL,
Remark VARCHAR(2000) NULL,
PaymentTerm VARCHAR(30) NULL,
Total NUMERIC(10,2) NOT NULL,
Tax_type VARCHAR(5) NOT NULL,
Tax NUMERIC(10,2) NULL,
TaxAmount NUMERIC(10,2) NULL,
GrossTotal NUMERIC(10,2) NOT NULL,
CreatedBy BIGINT NOT NULL,
CreatedDate TIMESTAMP NOT NULL,
UpdatedBy BIGINT,
UpdatedDate TIMESTAMP,
PRIMARY KEY (Enquery_Id),
CONSTRAINT fk_customer_enquiry_created_by FOREIGN KEY (CreatedBy) REFERENCES capri_login_register(R_Id),
CONSTRAINT fk_customer_enquiry_updated_by FOREIGN KEY (UpdatedBy) REFERENCES capri_login_register(R_Id)
);

/* Customer Enquery Details*/
CREATE TABLE capri_customer_enquiry_details
(
Id BIGINT NOT NULL AUTO_INCREMENT,
ProductName VARCHAR(150) NULL,
ProductUnit VARCHAR(150) NULL,
ProductPrice NUMERIC(10,2) NULL,
Quantity BIGINT NOT NULL,
Total NUMERIC (10,3) NOT NULL,
Enquiry_Id BIGINT NOT NULL,
CreatedBy BIGINT NOT NULL,
CreatedDate TIMESTAMP NOT NULL,
UpdatedBy BIGINT,
UpdatedDate TIMESTAMP,
PRIMARY KEY (Id),
CONSTRAINT fk_customer_enquiry_details_enquiry_id FOREIGN KEY (Enquiry_Id) REFERENCES capri_customer_enquiry(Enquiry_Id),
CONSTRAINT fk_customer_enquiry_details_created_by FOREIGN KEY (CreatedBy) REFERENCES capri_login_register(R_Id),
CONSTRAINT fk_customer_enquiry_details_updated_by FOREIGN KEY (UpdatedBy) REFERENCES capri_login_register(R_Id)
);

/* sale quotation */
CREATE TABLE capri_sale_quotation
(
Qtn_Id BIGINT NOT NULL AUTO_INCREMENT,
QtnNumber VARCHAR(30) NOT NULL,
QtnDate TIMESTAMP NOT NULL,
OurRef VARCHAR(100) NULL,
KindAttn VARCHAR(100) NULL,
CustomerName VARCHAR(150) NOT NULL,
BillingAddress VARCHAR(500) NULL,
TIN VARCHAR(30) NULL,
CST VARCHAR(30) NULL,
validityQtn VARCHAR(20) NULL,
DateOfDelivery TIMESTAMP NULL,
dispatchThru VARCHAR(100) NULL,
PlaceOfDelivery VARCHAR(300) NULL,
Quality VARCHAR(100) NULL,
Total NUMERIC(10,2) NOT NULL,
Tax_type VARCHAR(5) NOT NULL,
Tax NUMERIC(10,2) NULL,
TaxAmount NUMERIC(10,2) NULL,
Discount VARCHAR(10) NULL,
PaymentTerm VARCHAR(30) NULL,
CreatedBy BIGINT NOT NULL,
CreatedDate TIMESTAMP NOT NULL,
UpdatedBy BIGINT,
UpdatedDate TIMESTAMP,
PRIMARY KEY (Qtn_Id),
CONSTRAINT fk_sale_quotation_created_by FOREIGN KEY (CreatedBy) REFERENCES capri_login_register(R_Id),
CONSTRAINT fk_sale_quotation_updated_by FOREIGN KEY (UpdatedBy) REFERENCES capri_login_register(R_Id)
);

/* Sale quotaion product details */
CREATE TABLE capri_sale_quotation_detail
(
Id BIGINT NOT NULL AUTO_INCREMENT,
ProductName VARCHAR(150) NULL,
ProductUnit VARCHAR(150) NULL,
ProductPrice NUMERIC(10,2) NULL,
Quantity BIGINT NOT NULL,
Total NUMERIC (10,3) NOT NULL,
Qtn_Id BIGINT NOT NULL,
CreatedBy BIGINT NOT NULL,
CreatedDate TIMESTAMP NOT NULL,
UpdatedBy BIGINT,
UpdatedDate TIMESTAMP,
PRIMARY KEY (Id),
CONSTRAINT fk_sale_quotation_detail_qtn_id FOREIGN KEY (Qtn_Id) REFERENCES capri_sale_quotation(qtn_id),
CONSTRAINT fk_sale_quotation_detail_created_by FOREIGN KEY (CreatedBy) REFERENCES capri_login_register(R_Id),
CONSTRAINT fk_sale_quotation_detail_updated_by FOREIGN KEY (UpdatedBy) REFERENCES capri_login_register(R_Id)
);

/* Sale Invoice */
CREATE TABLE capri_sale_invoice
(
Invoice_Id BIGINT NOT NULL AUTO_INCREMENT,
InvoiceNumber VARCHAR(30) NOT NULL,
InvoiceDate TIMESTAMP NOT NULL,
Po_Number VARCHAR(30)  NULL,
PoDate TIMESTAMP  NULL,
Dc_Number VARCHAR(30) NULL,
DcDate TIMESTAMP  NULL,
CustomerName VARCHAR(150) NOT NULL,
BillingAddress VARCHAR(500) NULL,
TIN VARCHAR(30) NULL,
CST VARCHAR(30) NULL,
DateOfDelivery TIMESTAMP NULL,
dispatchThru VARCHAR(100) NULL,
PlaceOfDelivery VARCHAR(500) NULL,
Quality VARCHAR(100) NULL,
packingAndForward NUMERIC(10,2) NULL,
serviceCharge NUMERIC(10,2) NULL,
Othercharges NUMERIC(10,2) NULL,
TotalAmount NUMERIC(10,2) NULL,
AdvancePayment NUMERIC(10,2) NULL,
Balance NUMERIC(10,2) NULL,
PaidAmount NUMERIC(10,2) NULL,
Tax_type VARCHAR(5) NOT NULL,
Tax NUMERIC(10,2) NULL,
TaxAmount NUMERIC(10,2) NULL,
Discount NUMERIC(10,2) NULL,
PaymentTerm VARCHAR(30) NULL,
CreatedBy BIGINT NOT NULL,
CreatedDate TIMESTAMP NOT NULL,
UpdatedBy BIGINT,
UpdatedDate TIMESTAMP,
PRIMARY KEY (Invoice_Id),
CONSTRAINT fk_sale_invoice_created_by FOREIGN KEY (CreatedBy) REFERENCES capri_login_register(R_Id),
CONSTRAINT fk_sale_invoice_updated_by FOREIGN KEY (UpdatedBy) REFERENCES capri_login_register(R_Id)
);


/* Sale Invoice Details */
CREATE TABLE capri_sale_invoice_details
(
Id BIGINT NOT NULL AUTO_INCREMENT,
ProductName VARCHAR(150) NULL,
ProductUnit VARCHAR(150) NULL,
ProductPrice NUMERIC(10,2) NULL,
Quantity BIGINT NOT NULL,
Total NUMERIC (10,3) NOT NULL,
Invoice_Id BIGINT NOT NULL,
CreatedBy BIGINT NOT NULL,
CreatedDate TIMESTAMP NOT NULL,
UpdatedBy BIGINT,
UpdatedDate TIMESTAMP,
PRIMARY KEY (Id),
CONSTRAINT fk_sale_invoice_details_Invoice_Id FOREIGN KEY (Invoice_Id) REFERENCES capri_sale_invoice(Invoice_Id),
CONSTRAINT fk_sale_invoice_details_created_by FOREIGN KEY (CreatedBy) REFERENCES capri_login_register(R_Id),
CONSTRAINT fk_sale_invoice_details_updated_by FOREIGN KEY (UpdatedBy) REFERENCES capri_login_register(R_Id)
);

/* proforma  Invoice */
CREATE TABLE capri_proforma_invoice
(
prof_Id BIGINT NOT NULL AUTO_INCREMENT,
proformaNumber VARCHAR(30) NOT NULL,
proformaDate TIMESTAMP NOT NULL,
Po_Number VARCHAR(30)  NULL,
PoDate TIMESTAMP  NULL,
Dc_Number VARCHAR(30) NULL,
DcDate TIMESTAMP  NULL,
CustomerName VARCHAR(150) NOT NULL,
BillingAddress VARCHAR(500) NULL,
TIN VARCHAR(30) NULL,
CST VARCHAR(30) NULL,
DateOfDelivery TIMESTAMP NULL,
dispatchThru VARCHAR(100) NULL,
PlaceOfDelivery VARCHAR(500) NULL,
Quality VARCHAR(100) NULL,
packingAndForward NUMERIC(10,2) NULL,
serviceCharge NUMERIC(10,2) NULL,
Othercharges NUMERIC(10,2) NULL,
TotalAmount NUMERIC(10,2) NULL,
AdvancePayment NUMERIC(10,2) NULL,
Balance NUMERIC(10,2) NULL,
PaidAmount NUMERIC(10,2) NULL,
Tax_type VARCHAR(5) NOT NULL,
Tax NUMERIC(10,2) NULL,
TaxAmount NUMERIC(10,2) NULL,
Discount NUMERIC(10,2) NULL,
PaymentTerm VARCHAR(30) NULL,
CreatedBy BIGINT NOT NULL,
CreatedDate TIMESTAMP NOT NULL,
UpdatedBy BIGINT,
UpdatedDate TIMESTAMP,
PRIMARY KEY (prof_Id),
CONSTRAINT fk_proforma_invoice_created_by FOREIGN KEY (CreatedBy) REFERENCES capri_login_register(R_Id),
CONSTRAINT fk_proforma_invoice_updated_by FOREIGN KEY (UpdatedBy) REFERENCES capri_login_register(R_Id)
);


/* performa Invoice Details */
CREATE TABLE capri_proforma_invoice_details
(
Id BIGINT NOT NULL AUTO_INCREMENT,
ProductName VARCHAR(150) NULL,
ProductUnit VARCHAR(150) NULL,
ProductPrice NUMERIC(10,2) NULL,
Quantity BIGINT NOT NULL,
Total NUMERIC (10,3) NOT NULL,
prof_Id BIGINT NOT NULL,
CreatedBy BIGINT NOT NULL,
CreatedDate TIMESTAMP NOT NULL,
UpdatedBy BIGINT,
UpdatedDate TIMESTAMP,
PRIMARY KEY (Id),
CONSTRAINT fk_proforma_invoice_details_prof_Id FOREIGN KEY (prof_Id) REFERENCES capri_proforma_invoice(prof_Id),
CONSTRAINT fk_proforma_invoice_details_created_by FOREIGN KEY (CreatedBy) REFERENCES capri_login_register(R_Id),
CONSTRAINT fk_proforma_invoice_details_updated_by FOREIGN KEY (UpdatedBy) REFERENCES capri_login_register(R_Id)
);






CREATE TABLE capri_lead
(
Lead_Id BIGINT NOT NULL AUTO_INCREMENT,
LeadDate TIMESTAMP NOT NULL,
CustomerName VARCHAR(100) NOT NULL,
BillingAddress VARCHAR(500) NULL,
PhoneNumber VARCHAR(13) NULL,
ContactPerson VARCHAR(80) NULL,
Email VARCHAR(100) NULL,
MobileNumber1 VARCHAR(13) NULL,
Leadassign VARCHAR(200)  NULL,
Leadcategory VARCHAR(200)  NULL,
Leadstage VARCHAR(200)  NULL,
Leadsource VARCHAR(200)  NULL,
CustomerBudget NUMERIC(10,2)  NULL,
NextAction VARCHAR(200)  NULL,
PriofAction VARCHAR(200)  NULL,
Leadsubject VARCHAR(200)  NULL,
Leaddetails VARCHAR(200)  NULL,
Approvalstatus VARCHAR(200)  NULL,
Remark VARCHAR(200)  NULL,
LeadStatus VARCHAR(200)  NULL,
CreatedBy BIGINT NOT NULL,
CreatedDate TIMESTAMP NOT NULL,
UpdatedBy BIGINT,
UpdatedDate TIMESTAMP,
PRIMARY KEY (Lead_Id),
CONSTRAINT fk_lead_created_by FOREIGN KEY (CreatedBy) REFERENCES capri_login_register(R_Id),
CONSTRAINT fk_lead_updated_by FOREIGN KEY (UpdatedBy) REFERENCES capri_login_register(R_Id)
);




CREATE TABLE capri_incoming_payment
(
Id BIGINT NOT NULL AUTO_INCREMENT,
BillNumber VARCHAR(30) NOT NULL,
BillDate TIMESTAMP NOT NULL,
InvoiceNumber VARCHAR(30) NOT NULL,
CustomerName VARCHAR(150) NOT NULL,
Total NUMERIC(10,2) NULL,
Pay NUMERIC(10,2) NULL,
Balance NUMERIC(10,2) NULL,
BankName VARCHAR(5) NOT NULL,
Paymode VARCHAR(30) NULL,
Chequeno VARCHAR(30) NULL,
CreatedBy BIGINT NOT NULL,
CreatedDate TIMESTAMP NOT NULL,
UpdatedBy BIGINT,
UpdatedDate TIMESTAMP,
PRIMARY KEY (Id),
CONSTRAINT fk_incoming_payment_created_by FOREIGN KEY (CreatedBy) REFERENCES capri_login_register(R_Id),
CONSTRAINT fk_incoming_payment_updated_by FOREIGN KEY (UpdatedBy) REFERENCES capri_login_register(R_Id)
);

/*Debit Entry*/
CREATE TABLE capri_debit_entry
(
Debit_Id BIGINT NOT NULL AUTO_INCREMENT,
SupplierName VARCHAR(150) NOT NULL,
BillingAddress VARCHAR(500) NULL,
Reson VARCHAR(500) NULL,
TIN VARCHAR(30) NULL,
CST VARCHAR(30) NULL,
InvoiceNumber VARCHAR(30)  NULL,
InvoiceDate TIMESTAMP NULL,
ReturnDate TIMESTAMP  NULL,
CreatedBy BIGINT NOT NULL,
CreatedDate TIMESTAMP NOT NULL,
UpdatedBy BIGINT,
UpdatedDate TIMESTAMP,
PRIMARY KEY (Debit_Id),
CONSTRAINT fk_debit_entry_created_by FOREIGN KEY (CreatedBy) REFERENCES capri_login_register(R_Id),
CONSTRAINT fk_debit_entry_updated_by FOREIGN KEY (UpdatedBy) REFERENCES capri_login_register(R_Id)
);

/*Debit Entry Details*/
CREATE TABLE capri_debit_details
(
Id BIGINT NOT NULL AUTO_INCREMENT,
ProductName VARCHAR(150) NULL,
ProductUnit VARCHAR(150) NULL,
Quantity BIGINT NOT NULL,
Debit_Id BIGINT NOT NULL,
CreatedBy BIGINT NOT NULL,
CreatedDate TIMESTAMP NOT NULL,
UpdatedBy BIGINT,
UpdatedDate TIMESTAMP,
PRIMARY KEY (Id),
CONSTRAINT fk_debit_details_Debit_Id FOREIGN KEY (Debit_Id) REFERENCES capri_debit_entry(Debit_Id),
CONSTRAINT fk_debit_details_created_by FOREIGN KEY (CreatedBy) REFERENCES capri_login_register(R_Id),
CONSTRAINT fk_debit_details_updated_by FOREIGN KEY (UpdatedBy) REFERENCES capri_login_register(R_Id)
);




/*Credit Entry*/
CREATE TABLE capri_credit_entry
(
Credit_Id BIGINT NOT NULL AUTO_INCREMENT,
CustomerName VARCHAR(150) NOT NULL,
BillingAddress VARCHAR(500) NULL,
Reson VARCHAR(500) NULL,
TIN VARCHAR(30) NULL,
CST VARCHAR(30) NULL,
InvoiceNumber VARCHAR(30)  NULL,
InvoiceDate TIMESTAMP NULL,
ReturnDate TIMESTAMP  NULL,
CreatedBy BIGINT NOT NULL,
CreatedDate TIMESTAMP NOT NULL,
UpdatedBy BIGINT,
UpdatedDate TIMESTAMP,
PRIMARY KEY (Credit_Id),
CONSTRAINT fk_credit_entry_created_by FOREIGN KEY (CreatedBy) REFERENCES capri_login_register(R_Id),
CONSTRAINT fk_credit_entry_updated_by FOREIGN KEY (UpdatedBy) REFERENCES capri_login_register(R_Id)
);

/*Credit Entry Details*/
CREATE TABLE capri_Credit_details
(
Id BIGINT NOT NULL AUTO_INCREMENT,
ProductName VARCHAR(150) NULL,
ProductUnit VARCHAR(150) NULL,
Quantity BIGINT NOT NULL,
Credit_Id BIGINT NOT NULL,
CreatedBy BIGINT NOT NULL,
CreatedDate TIMESTAMP NOT NULL,
UpdatedBy BIGINT,
UpdatedDate TIMESTAMP,
PRIMARY KEY (Id),
CONSTRAINT fk_Credit_details_Debit_Id FOREIGN KEY (Credit_Id) REFERENCES capri_credit_entry(Credit_Id),
CONSTRAINT fk_Credit_details_created_by FOREIGN KEY (CreatedBy) REFERENCES capri_login_register(R_Id),
CONSTRAINT fk_Credit_details_updated_by FOREIGN KEY (UpdatedBy) REFERENCES capri_login_register(R_Id)
);


CREATE TABLE capri_outgoing_payment
(
Id BIGINT NOT NULL AUTO_INCREMENT,
BillNumber VARCHAR(30) NOT NULL,
BillDate TIMESTAMP NOT NULL,
InwardNumber VARCHAR(30) NOT NULL,
SupplierName VARCHAR(150) NOT NULL,
Total NUMERIC(10,2) NULL,
Pay NUMERIC(10,2) NULL,
Balance NUMERIC(10,2) NULL,
BankName VARCHAR(5) NOT NULL,
Paymode VARCHAR(30) NULL,
Chequeno VARCHAR(30) NULL,
CreatedBy BIGINT NOT NULL,
CreatedDate TIMESTAMP NOT NULL,
UpdatedBy BIGINT,
UpdatedDate TIMESTAMP,
PRIMARY KEY (Id),
CONSTRAINT fk_outgoing_payment_created_by FOREIGN KEY (CreatedBy) REFERENCES capri_login_register(R_Id),
CONSTRAINT fk_outgoing_payment_updated_by FOREIGN KEY (UpdatedBy) REFERENCES capri_login_register(R_Id)
);

/* Sale Transfer */
CREATE TABLE capri_stock_transfer
(
Stock_Id BIGINT NOT NULL AUTO_INCREMENT,
StockNumber VARCHAR(30) NOT NULL,
StockDate TIMESTAMP NOT NULL,
Po_Number VARCHAR(30)  NULL,
PoDate TIMESTAMP  NULL,
Dc_Number VARCHAR(30) NULL,
DcDate TIMESTAMP  NULL,
CustomerName VARCHAR(150) NOT NULL,
BillingAddress VARCHAR(500) NULL,
TIN VARCHAR(30) NULL,
CST VARCHAR(30) NULL,
Location VARCHAR(100) NULL,
DateOfDelivery TIMESTAMP NULL,
dispatchThru VARCHAR(100) NULL,
PlaceOfDelivery VARCHAR(500) NULL,
Quality VARCHAR(100) NULL,
packingAndForward NUMERIC(10,2) NULL,
serviceCharge NUMERIC(10,2) NULL,
Othercharges NUMERIC(10,2) NULL,
TotalAmount NUMERIC(10,2) NULL,
AdvancePayment NUMERIC(10,2) NULL,
Balance NUMERIC(10,2) NULL,
PaidAmount NUMERIC(10,2) NULL,
Tax_type VARCHAR(5) NOT NULL,
Tax NUMERIC(10,2) NULL,
TaxAmount NUMERIC(10,2) NULL,
Discount NUMERIC(10,2) NULL,
PaymentTerm VARCHAR(30) NULL,
CreatedBy BIGINT NOT NULL,
CreatedDate TIMESTAMP NOT NULL,
UpdatedBy BIGINT,
UpdatedDate TIMESTAMP,
PRIMARY KEY (Stock_Id),
CONSTRAINT fk_stock_transfer_created_by FOREIGN KEY (CreatedBy) REFERENCES capri_login_register(R_Id),
CONSTRAINT fk_stock_transfer_updated_by FOREIGN KEY (UpdatedBy) REFERENCES capri_login_register(R_Id)
);


/* Stock Transfer Details */
CREATE TABLE capri_stock_transfer_details
(
Id BIGINT NOT NULL AUTO_INCREMENT,
ProductName VARCHAR(150) NULL,
ProductUnit VARCHAR(150) NULL,
ProductPrice NUMERIC(10,2) NULL,
Quantity BIGINT NOT NULL,
Total NUMERIC (10,3) NOT NULL,
Stock_Id BIGINT NOT NULL,
CreatedBy BIGINT NOT NULL,
CreatedDate TIMESTAMP NOT NULL,
UpdatedBy BIGINT,
UpdatedDate TIMESTAMP,
PRIMARY KEY (Id),
CONSTRAINT fk_stock_transfer_details_Stock_Id FOREIGN KEY (Stock_Id) REFERENCES capri_stock_transfer(Stock_Id),
CONSTRAINT fk_stock_transfer_details_created_by FOREIGN KEY (CreatedBy) REFERENCES capri_login_register(R_Id),
CONSTRAINT fk_stock_transfer_details_updated_by FOREIGN KEY (UpdatedBy) REFERENCES capri_login_register(R_Id)
);