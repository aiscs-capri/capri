    

<script>

  function FillBilling(f) {
  if(f.billingtoo.checked == true) {
    f.Shippingaddress.value = f.Billingaddress.value;
    
  }
  else if (f.billingtoo.checked==false){

     f.Shippingaddress.value ='';
      
  }
}
 </script>

    <div class="container top">
         <div class="row">
        <div class="span10 columns">
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url("admin"); ?>">
            <?php echo ucfirst($this->uri->segment(1));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li>
          <a href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>">
            <?php echo ucfirst($this->uri->segment(2));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
          <a href="#">New</a>
        </li>
      </ul>
      
      <div class="page-header">
        <h2>
          Adding <?php echo ucfirst($this->uri->segment(2));?>
        </h2>
      
    </div>
      <?php
      //flash messages
      if(isset($flash_message)){
        if($flash_message == TRUE)
        {
          echo '<div class="alert alert-success">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Well done!</strong> New Customer created with success.';
          echo '</div>';       
        }else{
          echo '<div class="alert alert-error">';
            echo '<a class="close" data-dismiss="alert">×</a>';
            echo '<strong>Oh snap!</strong> change a few things up and try submitting again.';
          echo '</div>';          
        }
      }
      ?>
      
      <?php
      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '');

      //form validation
      echo validation_errors();
      
      echo form_open('admin/supplier/add', $attributes);
      ?>
       <div class="container">
           <div class="row">
               <div class="col-md-5">
        <fieldset>
          <!-- <div class="control-group">
            <label for="inputError" class="control-label">Customer ID</label>
            <div class="controls">
              <input type="text" id="" name="Customer_Id" value="<?php echo set_value('Customer_Id'); ?>" >
              
            </div>
          </div> -->
          <div class="control-group">
            <label for="inputError" class="control-label">Supplier Code</label>
            <div class="controls">
              <input type="text" id="" name="SupplierCode" value="<?php echo set_value('SupplierCode'); ?>" >
              
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Supplier Name</label>
            <div class="controls">
              <input type="text" id="" name="SupplierName" value="<?php echo set_value('SupplierName'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div><form>
          <div class="control-group">
            <label for="inputError" class="control-label">Billing Address</label>
            <div class="controls">
              <input type="text" id="" name="BillingAddress" value="<?php echo set_value('BillingAddress'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <center><input type="checkbox" name="billingtoo" onclick="FillBilling(this.form)">
Same Address</center>
          <div class="control-group">
            <label for="inputError" class="control-label">Shipping Address</label>
            <div class="controls">
              <input type="text" id="" name="ShippingAddress" value="<?php echo set_value('ShippingAddress'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div></form>
        </fieldset>
        

          
       
      <legend> Contact Details</legend>
           <div class="control-group">
            <label for="inputError" class="control-label">Phone Number</label>
            <div class="controls">
              <input type="text" id="" name="PhoneNumber" value="<?php echo set_value('PhoneNumber'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>   </form>
           <div class="control-group">
            <label for="inputError" class="control-label">Contact Person</label>
            <div class="controls">
              <input type="text" id="" name="ContactPerson" value="<?php echo set_value('ContactPerson'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label">Mobile Number1</label>
            <div class="controls">
              <input type="text" id="" name="MobileNumber1" value="<?php echo set_value('MobileNumber1'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label">Mobile Number2</label>
            <div class="controls">
              <input type="text" id="" name="MobileNumber2" value="<?php echo set_value('MobileNumber2'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label">Email</label>
            <div class="controls">
              <input type="text" id="" name=" Email" value="<?php echo set_value('  Email'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
         <div class="control-group">
            <label for="inputError" class="control-label">Fax</label>
            <div class="controls">
              <input type="text" id="" name="Fax" value="<?php echo set_value('Fax'); ?>" >
              
            </div>
          </div>
         
            <br><br>
           <fieldset>
            <legend>Tax Details</legend>
           
           <div class="control-group">
            <label for="inputError" class="control-label">Tin Number</label>
            <div class="controls">
              <input type="text" id="" name="TIN" value="<?php echo set_value('TIN'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label">CST Number</label>
            <div class="controls">
              <input type="text" id="" name="CST" value="<?php echo set_value('CST'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">ECC</label>
            <div class="controls">
              <input type="text" id="" name="ECC" value="<?php echo set_value('ECC'); ?>" >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
        </fieldset>
       </div>
      </div>
      
         <div class="col-md-10">
             <div class="form-actions">
           <center><button class="btn btn-primary" type="submit">Save changes</button>
            <button class="btn" type="reset" onclick='alert("Customer cancel successfully")'>Cancel</button></center>
          </div>
        </div>
     
      <?php echo form_close(); ?>

    </div>
            </div>
           
     