 <script src="<?php echo base_url(); ?>js/jquery-1.11.1.js"></script>
 <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
 <script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>

  <link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery-ui.css"> 
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/style.css"> 
  <script src="<?php echo base_url(); ?>js/jquery-1.11.0.min.js"></script>
  <script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
    <div class="container top">
         <div class="row">
        <div class="span10 columns">
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo site_url("admin"); ?>">
            <?php echo ucfirst($this->uri->segment(1));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li>
          <a href="<?php echo site_url("admin").'/'.$this->uri->segment(2); ?>">
            <?php echo ucfirst($this->uri->segment(2));?>
          </a> 
          <span class="divider">/</span>
        </li>
        <li class="active">
          <a href="#">View</a>
        </li>
      </ul>
    </div></div></div>
      
      <div class="page-header">
        <h2>
          View <?php echo ucfirst($this->uri->segment(2));?>
        </h2>
      
    </div>

      
      <?php
      //form data
      $attributes = array('class' => 'form-horizontal', 'id' => '');
          //form validation
      echo validation_errors();
      
      
      echo form_open('admin/inward/view', $attributes);
      ?>
       <div class="container">
           <div class="row">
               <div class="col-md-5">
                <div class="control-group">
            <!-- <label for="inputError" class="control-label">Order Id</label> -->
            <div class="controls">
              <input type="hidden" id="box1" name="inward_Id" value="<?php echo $manufacture[0]['inward_Id']; ?>" readonly>
              
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Inward Number</label>
            <div class="controls">
              <input type="text" id="box2" name="inward_number" value="<?php echo $manufacture[0]['inward_number']; ?>" readonly>
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Inward Date</label>
            <div class="controls">
              <!-- <p>Min Date: <input type="text" id="datepickermin"></p> -->
 
             <input type="text"  name="inwardDate" value="<?php echo $manufacture[0]['inwardDate']; ?>" readonly>
              <!-- <input type="date" id="result" name="OrderDate" value="<?php echo set_value('OrderDate'); ?>" onchange="calculate()" > -->
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Bill Number</label>
            <div class="controls">
              <!-- <p>Min Date: <input type="text" id="datepickermin"></p> -->
 
             <input type="text" name="BillNumber" value="<?php echo $manufacture[0]['BillNumber'];  ?>" readonly>
              <!-- <input type="date" id="result" name="OrderDate" value="<?php echo set_value('OrderDate'); ?>" onchange="calculate()" > -->
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Bill Date</label>
            <div class="controls">
              <!-- <p>Min Date: <input type="text" id="datepickermin"></p> -->
 
             <input type="text" name="BillDate" value="<?php echo $manufacture[0]['BillDate'];  ?>" readonly>
              <!-- <input type="date" id="result" name="OrderDate" value="<?php echo set_value('OrderDate'); ?>" onchange="calculate()" > -->
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
        </div>
          
          <div class="col-md-5">
             <div class="control-group">
            <label for="inputError" class="control-label" >Order Number</label>
            <div class="controls">
              <input type="text" id="Order_number" name="Order_number" value="<?php echo $manufacture[0]['Order_number']; ?>" readonly>             
              <!--<span class="help-inline">Woohoo!</span>-->
            </div></div>
         
            <div class="control-group">
            <label for="inputError" class="control-label" >Order Date</label>
            <div class="controls">             
              <input type="text" id="OrderDate" name="OrderDate" value="<?php echo $manufacture[0]['OrderDate'];  ?>" readonly>
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label" >Place Of Delivery</label>
            <div class="controls">
              <input type="text" id="PlaceOfDelivery" name="PlaceOfDelivery" value="<?php echo $manufacture[0]['PlaceOfDelivery'];  ?>" readonly>
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          
           <div class="control-group">

            <?php
              foreach ($po as $row) {
              ?>
            
                  
            <label for="inputError" class="control-label">Supplier Name</label>

            <?php 
            echo "<input type=text id=supplier name=Supplier_Id  readonly value=".$row->SupplierName." >";
            ?>
                      
                      <br><br>
                      <div class="control-group">
            <label for="inputError" class="control-label"> Address</label>
            <div class="controls">
              <textarea rows="5" cols="20" id="bil" name="Bill" readonly><?php 
                  echo $row->BillingAddress;                  
                  ?>
              </textarea>
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>

          <div class="control-group">
            <label for="inputError" class="control-label" >CST</label>
            <div class="controls">
            <?php 
            echo "<input type=text name=cst readonly value=".$row->CST.">";
            ?>
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div></div>
          
           <div class="control-group">
            <label for="inputError" class="control-label" >TIN</label>
            <div class="controls">
               <?php 
            echo "<input type=text name=tin readonly value=".$row->TIN.">";
            ?>
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
             <?php 
            }
            ?>
          </div>

          </div>
        </div></div>
        <hr>
       
     <!--  <div class="page-header">
        <h2>
          Purchase Order
        </h2>
      
    </div> -->
      
      

       <div class="container">
           
         <br><br>
         <div class="row">
         <div class="col-md-10">
          <table id="myTable" class="table table-striped table-bordered table-condensed">
           
         <tr>
            <td style=display:none;>id</td>
            <td>Product Name</td>
            <td>Product Unit</td>
            <td>Product Qunatity</td>
            <td>Product Price</td>
            <td>Total</td>
            <td>select</td>
        </tr>
        <tbody>
               <?php
              $i =1;
              foreach($pop as $row)
              {
                
                echo '<tr>';
                echo '<td style=display:none;>'.$i.'</td>';
                echo '<td>'.$row->ProductName.'</td>';
                echo '<td>'.$row->ProductUnit.'</td>';
                echo '<td>'.$row->Quantity.'</td>';
                echo '<td>'.$row->ProductPrice.'</td>';
                echo '<td>'.$row->Total.'</td>';
                echo '<td><input type=checkbox></input></td>';
                echo '</tr>';
                $i++;   
              }
              ?>     
           </tbody>
          </table>                         
  
     

    </div>
            </div><hr>
</div><hr><div class="row">
      <div class="col-md-5">
              <div class="control-group">
            <label for="inputError" class="control-label" >Total</label>
            <div class="controls">
              <input type="text" id="total" name="Total" value="<?php echo $manufacture[0]['Total']; ?>" onkeyup="gtotal();" readonly>
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>

             <div class="control-group">
            <label for="inputError" class="control-label">Tax Type</label>
            <div class="controls">
              <input type="text" id="" name="Tax_type" value="<?php echo $manufacture[0]['Tax_type']; ?>" readonly>              
            </div>
          </div>

          <div class="control-group">
            <label for="inputError" class="control-label">Tax</label>
            <div class="controls">
              <input type="text" id="tax" name="Tax" value="<?php echo $manufacture[0]['Tax']; ?>" onkeyup="gtotal();" readonly>
             
            </div>
          </div>

           <div class="control-group">
            <label for="inputError" class="control-label">Tax Amount</label>
            <div class="controls">
              <input type="text" id="atax" name="TaxAmount" onkeyup="gtotal();" value="<?php echo $manufacture[0]['TaxAmount']; ?>" readonly >
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>

      </div> 
      <div class="col-md-5">
            <div class="control-group">
            <label for="inputError" class="control-label">Packing And Forward</label>
            <div class="controls">
              <input type="text" id="paf" name="packingAndForward" value="<?php echo $manufacture[0]['packingAndForward']; ?>" onkeyup="gtotal();" readonly>
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label">Service Tax</label>
            <div class="controls">
              <input type="text" id="stax" name="servicetax" value="<?php echo $manufacture[0]['servicetax']; ?>" onkeyup="gtotal();" readonly>
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
          <div class="control-group">
            <label for="inputError" class="control-label">Other Charges</label>
            <div class="controls">
              <input type="text" id="otc" name="Otherchargs" value="<?php echo $manufacture[0]['Otherchargs']; ?>" onkeyup="gtotal();" readonly>
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
           <div class="control-group">
            <label for="inputError" class="control-label">Advance Payment</label>
            <div class="controls">
              <input type="text" id="ap" name="AdvancePayment" value="<?php echo $manufacture[0]['AdvancePayment']; ?>" readonly>
              
            </div>
          </div> 
          <div class="control-group">
            <label for="inputError" class="control-label" >Gross Total</label>
            <div class="controls">
              <input type="text" id="gtot" name="GrossTotal" value="<?php echo $manufacture[0]['GrossTotal']; ?>" readonly>
              <!--<span class="help-inline">Woohoo!</span>-->
            </div>
          </div>
               
      </div> 
    </div> 
      <div class="row">
         <div class="col-md-10">
             <div class="form-actions">
           <center><a href="admin/purchase"><button class="btn btn-primary" type="submit" onClick="clearform();">Save</button></a>
            <button class="btn" type="reset">Cancel</button></center>
          </div>
        </div>
      </div>