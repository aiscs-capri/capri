<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Untitled</title>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1">
<meta name="generator" content="Web Page Maker (unregistered version)">
<style type="text/css">
/*----------Text Styles----------*/
.ws6 {font-size: 8px;}
.ws7 {font-size: 9.3px;}
.ws8 {font-size: 11px;}
.ws9 {font-size: 12px;}
.ws10 {font-size: 13px;}
.ws11 {font-size: 15px;}
.ws12 {font-size: 16px;}
.ws14 {font-size: 19px;}
.ws16 {font-size: 21px;}
.ws18 {font-size: 24px;}
.ws20 {font-size: 27px;}
.ws22 {font-size: 29px;}
.ws24 {font-size: 32px;}
.ws26 {font-size: 35px;}
.ws28 {font-size: 37px;}
.ws36 {font-size: 48px;}
.ws48 {font-size: 64px;}
.ws72 {font-size: 96px;}
.wpmd {font-size: 13px;font-family: Arial,Helvetica,Sans-Serif;font-style: normal;font-weight: normal;}
/*----------Para Styles----------*/
DIV,UL,OL /* Left */
{
 margin-top: 0px;
 margin-bottom: 0px;
}
</style>

</head>
<body>
<div id="table1" style="position:absolute; overflow:hidden; left:12px; top:0px; width:756px; height:406px; z-index:0">
<div class="wpmd">
<div><TABLE bgcolor="white" border=1 cellpadding=0 bordercolorlight="white" bordercolordark="white" cellspacing=0>
<TR valign=top>
<TD colspan=2 height=211><div class="wpmd">
<div><BR></div>
<div align=center><IMG border=0 width=736 height=157 src="images/capri.PNG"></div>
<div align=center><BR></div>
</div>
</TD>
</TR>
<TR valign=top>
<TD width=376 height=174><div class="wpmd">
<div><B>&nbsp; To</B>,<BR><?php  foreach ($po as $row) {
                echo $row->SupplierName;
              }
 ?></div>
<div><BR></div>
</div>
</TD>
<TD width=369 height=174><div class="wpmd">
<div>&nbsp; <B>Order No&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </B>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $manufacture[0]['Order_number']; ?></div>
<div><B><BR></B></div>
<div><B>&nbsp; Order Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </B>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php  $date = $manufacture[0]['OrderDate'];  echo date('Y-M-d',strtotime($date));  ?></div>
<div><B><BR></B></div>
<div><B>&nbsp; Our Ref&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </B>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $manufacture[0]['OurRef']; ?></div>
<div><B><BR></B></div>
<div><B>&nbsp; Party's TIN No&nbsp; </B>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

<?php
              foreach ($po as $row) {
                echo $row->TIN;
              }
              ?>
  </div>
<div><B><BR></B></div>
<div><B>&nbsp; Party's CST No&nbsp; </B>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

<?php
              foreach ($po as $row) {
                echo $row->CST;
              }
              ?></div>
<div><BR></div>
</div>
</TD>
</TR>
</TABLE>
</div>
</div></div>

<div id="table2" style="position:absolute; overflow:hidden; left:14px; top:388px; width:747px; height:422px; z-index:1">
<div class="wpmd">
<div><TABLE bgcolor="#FFFFFF" border=1 cellpadding=0 bordercolorlight="#C0C0C0" bordercolordark="#808080" cellspacing=0>
<TR valign=top>
<TD width=45 height=21><div class="wpmd">
<div align=center><B>S No</B></div>
</div>
</TD>
<TD width=362 height=21><div class="wpmd">
<div align=center><B>Description</B></div>
</div>
</TD>
<TD width=65 height=21><div class="wpmd">
<div align=center><B>Quantity</B></div>
</div>
</TD>
<TD width=55 height=21><div class="wpmd">
<div align=center><B>Unit</B></div>
</div>
</TD>
<TD width=97 height=21><div class="wpmd">
<div align=center><B>Rate</B></div>
</div>
</TD>
<TD width=120 height=21><div class="wpmd">
<div align=center><B>Amount</B></div>
</div>
</TD>
</TR>
<?php
              $i =1;
              foreach($pop as $row)
              {
                
                echo '<tr>';
                echo '<td>'.$i.'</td>';
                echo '<td>'.$row->ProductName.'</td>';
                echo '<td>'.$row->ProductUnit.'</td>';
                echo '<td>'.$row->Quantity.'</td>';
                echo '<td>'.$row->ProductPrice.'</td>';
                echo '<td>'.$row->Total.'</td>';
                
                echo '</tr>';
                $i++;   
              }
              ?>     
 <TR valign=top>
<TD width=45 height=395><BR>
</TD>
<TD width=362 height=395><BR>
</TD>
<TD width=65 height=395><BR>
</TD>
<TD width=55 height=395><BR>
</TD>
<TD width=97 height=395><BR>
</TD>
<TD width=120 height=395><BR>
</TD>
</TR> 
</TABLE>
</div>
</div></div>

<div id="table3" style="position:absolute; overflow:hidden; left:8px; top:1021px; width:762px; height:109px; z-index:2">
<div class="wpmd">
<div><TABLE bgcolor="#FFFFFF" border=1 cellpadding=0 bordercolorlight="#C0C0C0" bordercolordark="#808080" cellspacing=0>
<TR valign=top>
<TD width=753 height=106><BR>
</TD>
</TR>
</TABLE>
</div>
</div></div>

<div id="text19" style="position:absolute; overflow:hidden; left:554px; top:1098px; width:198px; height:22px; z-index:3">
<div class="wpmd">
<div align=center><font class="ws11"><B>Authorised Signatory</B></font></div>
</div></div>

<div id="text20" style="position:absolute; overflow:hidden; left:555px; top:1031px; width:198px; height:22px; z-index:4">
<div class="wpmd">
<div align=center><font class="ws11">For</font><font class="ws11"><B> Capri Akvotech System</B></font></div>
</div></div>

<div id="text21" style="position:absolute; overflow:hidden; left:588px; top:214px; width:16px; height:22px; z-index:5">
<div class="wpmd">
<div align=center><font class="ws11"><B>:</B></font></div>
</div></div>

<div id="text22" style="position:absolute; overflow:hidden; left:589px; top:246px; width:16px; height:22px; z-index:6">
<div class="wpmd">
<div align=center><font class="ws11"><B>:</B></font></div>
</div></div>

<div id="text23" style="position:absolute; overflow:hidden; left:589px; top:276px; width:16px; height:22px; z-index:7">
<div class="wpmd">
<div align=center><font class="ws11"><B>:</B></font></div>
</div></div>

<div id="text24" style="position:absolute; overflow:hidden; left:589px; top:308px; width:16px; height:22px; z-index:8">
<div class="wpmd">
<div align=center><font class="ws11"><B>:</B></font></div>
</div></div>

<div id="text25" style="position:absolute; overflow:hidden; left:590px; top:338px; width:16px; height:22px; z-index:9">
<div class="wpmd">
<div align=center><font class="ws11"><B>:</B></font></div>
</div></div>

<div id="text26" style="position:absolute; overflow:hidden; left:193px; top:22px; width:426px; height:39px; z-index:10">
<div class="wpmd">
<div align=center><font class="ws26"><B><U>PURCHASE ORDER</U></B></font></div>
</div></div>

<div id="text3" style="position:absolute; overflow:hidden; left:649px; top:816px; width:16px; height:22px; z-index:11">
<div class="wpmd">
<div align=center><font class="ws11"><B>:</B></font></div>
</div></div>

<div id="table5" style="position:absolute; overflow:hidden; left:12px; top:806px; width:754px; height:216px; z-index:12">
<div class="wpmd">
<div><TABLE bgcolor="#FFFFFF" border=1 cellpadding=0 bordercolorlight="#C0C0C0" bordercolordark="#808080" cellspacing=0>
<TR valign=top>
<TD width=385><div class="wpmd">
<div><font class="ws12"><B>Terms And Condition:</B></font></div>
<div><BR></div>
<div>Date Of Delivery&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <B> :</B><?php  $date = $manufacture[0]['DateOfDelivery'];  echo date('Y-M-d',strtotime($date));  ?></div>
<div><B><BR></B></div>
<div>Place Of Delivery&nbsp;&nbsp;&nbsp;&nbsp; <B> :</B><?php echo $manufacture[0]['PlaceOfDelivery']; ?></div>
<div><B><BR></B></div>
<div>Quality&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <B> :</B><?php echo $manufacture[0]['Quality']; ?></div>
<div><BR></div>
<div><BR></div>
<div>Note&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <B> :</B><?php echo $manufacture[0]['Note']; ?></div>
<div><BR></div>
<div>PaymentTerm&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <B> :</B><?php echo $manufacture[0]['PaymentTerm']; ?></div>
<div><B><BR></B></div>
</div>
</TD>
<TD width=363><div class="wpmd">
<div align=center>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Total&nbsp;&nbsp;&nbsp;&nbsp; <B> :</B><?php echo $manufacture[0]['Total']; ?></div>
<div align=center><B><BR></B></div>
<div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Tax&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; %&nbsp;&nbsp; <B> :&nbsp;&nbsp;&nbsp; </B> <?php echo $manufacture[0]['TaxAmount']; ?></div>
<div><BR></div>
<div align=center>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Net Total&nbsp;&nbsp; &nbsp;&nbsp;<B>:</B><?php echo $manufacture[0]['GrossTotal']; ?></div>
<div align=center><BR></div>
<div align=center><BR></div>
</div>
</TD>
</TR>
</TABLE>
</div>
</div></div>
</body>
</html>